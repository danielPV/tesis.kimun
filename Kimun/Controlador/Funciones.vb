﻿Imports System.Configuration
Imports System.Data.SqlClient
Imports System.Xml

Public Class Funciones

    Public Function existeTabla_eTata(ByVal nombreTabla As String) As Boolean
        Dim consulta As New SqlCommand("SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME = @nombreTabla", conexion_eTata)

        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            consulta.Parameters.AddWithValue("@nombreTabla", nombreTabla)

            Dim n As Integer = CInt(consulta.ExecuteScalar())


            Return n > 0

            conexion_eTata.Close()
        Catch
            Return False
        End Try
    End Function

    Public Function existeTabla_controlProduccion(ByVal nombreTabla As String) As Boolean
        Dim consulta As New SqlCommand("SELECT COUNT(*) FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE = 'BASE TABLE' AND TABLE_NAME = @nombreTabla", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@nombreTabla", nombreTabla)

            Dim n As Integer = CInt(consulta.ExecuteScalar())


            Return n > 0

            conexion_controlProduccion.Close()
        Catch
            Return False
        End Try
    End Function

    Public Function existeColumna_controlProduccion(ByVal nombreTabla As String, ByVal nombreColumna As String) As Boolean
        Dim consulta As New SqlCommand("SELECT COUNT(*)
                                        FROM INFORMATION_SCHEMA.COLUMNS
                                        WHERE COLUMN_NAME = @nombreColumna AND TABLE_NAME = @nombreTabla", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If



            consulta.Parameters.AddWithValue("@nombreColumna", nombreColumna)
            consulta.Parameters.AddWithValue("@nombreTabla", nombreTabla)

            Dim n As Integer = CInt(consulta.ExecuteScalar())

            conexion_controlProduccion.Close()

            Return n > 0
        Catch
            Return False
        End Try
    End Function

    Public Function existeColumna_eTata(ByVal nombreTabla As String, ByVal nombreColumna As String) As Boolean
        Dim consulta As New SqlCommand("SELECT COUNT(*)
                                        FROM INFORMATION_SCHEMA.COLUMNS
                                        WHERE COLUMN_NAME = @nombreColumna AND TABLE_NAME = @nombreTabla", conexion_eTata)

        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            consulta.Parameters.AddWithValue("@nombreColumna", nombreColumna)
            consulta.Parameters.AddWithValue("@nombreTabla", nombreTabla)

            Dim n As Integer = CInt(consulta.ExecuteScalar())

            conexion_eTata.Close()
            Return n > 0
        Catch
            Return False
        End Try
    End Function

    Public Function tablaVacia_Torno() As Boolean

        Dim consulta As New SqlCommand("SELECT count(*) FROM TablaPronostico_Torno", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            Dim n As Integer = CInt(consulta.ExecuteScalar())

            Return n > 0

            conexion_controlProduccion.Close()
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function tablaVacia_Descortezador() As Boolean

        Dim consulta As New SqlCommand("SELECT count(*) FROM TablaPronostico_Descortezador", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            Dim n As Integer = CInt(consulta.ExecuteScalar())

            Return n > 0
            conexion_controlProduccion.Close()

        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function tablaVacia_Lijadora() As Boolean

        Dim consulta As New SqlCommand("SELECT count(*) FROM TablaPronostico_Lijadora", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            Dim n As Integer = CInt(consulta.ExecuteScalar())

            Return n > 0
            conexion_controlProduccion.Close()

        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function obtener_Servidor() As String
        Dim MisDocumentos As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

        Dim ruta As String = MisDocumentos & "\Kimun\config.xml"

        Dim documento As XmlReader = New XmlTextReader(ruta)

        While (documento.Read())
            Dim type = documento.NodeType

            If (type = XmlNodeType.Element) Then
                If (documento.Name = "Servidor") Then
                    Return documento.ReadInnerXml.ToString()
                End If
            Else
                Return 0
            End If

        End While
        Return 0
    End Function

    Public Function comprobar_conexion(ByVal servidor As String) As Boolean

        Dim etata = New SqlConnection("Data Source=" & servidor & ";Initial Catalog=eTata;Integrated Security=True")
        Dim produccion = New SqlConnection("Data Source=" & servidor & ";Initial Catalog=ControlProduccion;Integrated Security=True")

        Dim consulta_produccion As New SqlCommand("select * from sysdatabases where name = 'controlProduccion' ", produccion)
        Dim consulta_etata As New SqlCommand("select * from sysdatabases where name = 'eTata' ", etata)

        Try
            etata.Open()
            produccion.Open()

            If (consulta_etata.ExecuteScalar() = "eTata" Or consulta_produccion.ExecuteScalar() = "controlProduccion") Then
                Return True
            Else
                Return False
            End If

        Catch
            Return False
        End Try

    End Function

End Class
