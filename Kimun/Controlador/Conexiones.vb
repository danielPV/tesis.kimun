﻿Imports System.Data.Sql
Imports System.Data.SqlClient
Imports System.IO
Imports System.Xml
Imports System.Configuration
Imports System.Security.Permissions
Imports System.Data.Common

Module Conexiones

    'Variables
    Public conexion_controlProduccion As SqlConnection
    Public conexion_eTata As SqlConnection
    Public enunciado As SqlCommand
    Public adaptador As SqlDataAdapter

    Public conectar_controlProduccion As SqlConnection
    Public conectar_eTata As SqlConnection

    Dim cad As String

    '***********************************************************************************************************************************************'
    'FUNCION QUE LEE EL DOCUMENTO XML DONDE SE ENCUENTRA LA CONEXION AL SERVIDOR'
    '***********************************************************************************************************************************************'

    Dim Servidor As String
    Dim Usuario As String
    Dim Password As String

    Sub leer_XML()

        Dim MisDocumentos As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

        Dim ruta As String = MisDocumentos & "\Kimun\config.xml"

        Try

            If (My.Computer.FileSystem.FileExists(ruta) = True) Then
                'Dim filePermissions = New FileIOPermission(FileIOPermissionAccess.Read, ruta)
                'filePermissions.Demand()

                Dim documento As XmlReader = New XmlTextReader(ruta)

                While (documento.Read())
                    Dim type = documento.NodeType

                    If (type = XmlNodeType.Element) Then
                        If (documento.Name = "Servidor") Then
                            Servidor = documento.ReadInnerXml
                        End If

                        If (documento.Name = "Usuario") Then
                            Usuario = documento.ReadInnerXml
                        End If

                        If (documento.Name = "Password") Then
                            Password = documento.ReadInnerXml
                        End If
                    End If

                End While

                documento.Dispose()
                documento.Close()
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al leer el archivo XML")
        End Try
    End Sub

    Public Function test_Conection(ByVal bd As String, ByVal cadena As String) As Boolean

        Try
            If (bd = "eTata") Then
                abrir_eTata(cadena)
            End If

            If (bd = "controlProduccion") Then
                abrir_controlProduccion(cadena)
            End If

            Dim conn = New SqlConnection()
            conn.ConnectionString = cadena

            conn.Open()

            If (bd = "eTata") Then
                cerrar_eTata(cadena)
            End If

            If (bd = "controlProduccion") Then
                cerrar_controlProduccion(cadena)
            End If

            Return True
        Catch

            If (bd = "eTata") Then
                cerrar_eTata(cadena)
            End If

            If (bd = "controlProduccion") Then
                cerrar_controlProduccion(cadena)
            End If

            Return False
        End Try
    End Function

    Public Sub crear_conexiones()

        leer_XML()

        Try

            Dim config As Configuration = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath)
            Dim connectionStringsSection As ConnectionStringsSection = DirectCast(config.GetSection("connectionStrings"), ConnectionStringsSection)
            Dim conexion As New Conectar_Servidor
            Dim panelPrincipal As New Panel_Principal

            'Creamos una nueva cadena de conexion
            Dim cadena_conexion_controlProduccion As New ConnectionStringSettings()
            Dim cadena_conexion_eTata As New ConnectionStringSettings()

            'Asignamos el nombre de la cadena de conexion
            cadena_conexion_controlProduccion.Name = "conectar_controlProduccion"
            cadena_conexion_eTata.Name = "conectar_eTata"

            If (conexion.Check.Checked = True) Then
                'Damos la cadena de conexion
                cadena_conexion_controlProduccion.ConnectionString = "Data Source=" & Servidor & ";Initial Catalog=ControlProduccion;User ID= " & Usuario & ";Password= " & Password
                cadena_conexion_eTata.ConnectionString = "Data Source=" & Servidor & ";Initial Catalog=eTata;User ID= " & Usuario & ";Password= " & Password
                BuildConnectionString_controlProduccion(Servidor, "controlProduccion", Usuario, Password)
                BuildConnectionString_eTata(Servidor, "eTata", Usuario, Password)
            Else
                'Damos la cadena de conexion
                cadena_conexion_controlProduccion.ConnectionString = "Data Source=" & Servidor & ";Initial Catalog=ControlProduccion;Integrated Security=True"
                cadena_conexion_eTata.ConnectionString = "Data Source=" & Servidor & ";Initial Catalog=eTata;Integrated Security=True"
                BuildConnectionString_controlProduccion(Servidor, "controlProduccion", "", "")
                BuildConnectionString_eTata(Servidor, "eTata", "", "")
            End If

            'Nombre del Proveedor de datos incariable
            cadena_conexion_controlProduccion.ProviderName = "System.Data.SqlClient"
            cadena_conexion_eTata.ProviderName = "System.Data.SqlClient"


            'Añadimes la nueva cadena de conexion al objeto connectionString section
            connectionStringsSection.ConnectionStrings.Add(cadena_conexion_controlProduccion)
            connectionStringsSection.ConnectionStrings.Add(cadena_conexion_eTata)

            'Guardamos los cambios en el archivo de configuracion 
            config.Save(ConfigurationSaveMode.Full, False)
            'MsgBox("llega 2")
            If (test_Conection("controlProduccion", GetConexion_controlProduccion()) = True) Then
                'MsgBox("llega 3")
                panelPrincipal.estado_controlProduccion.ForeColor = Color.Green
                panelPrincipal.estado_controlProduccion.Text = "Conectado"
                abrir_controlProduccion()
                abrir_controlProduccion(GetConexion_controlProduccion())
            Else
                'MsgBox("llega 4")
                panelPrincipal.estado_controlProduccion.ForeColor = Color.Red
                panelPrincipal.estado_controlProduccion.Text = "No Conectado"
                Conectar_Servidor.Show()
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Error", "No se ha podido establecer conexión con la base de datos control Producción", ToolTipIcon.Warning)

            End If

            If (test_Conection("eTata", GetConexion_eTata()) = True) Then
                'MsgBox("llega 5")
                panelPrincipal.estado_eTata.ForeColor = Color.Green
                panelPrincipal.estado_eTata.Text = "Conectado"
                abrir_eTata()
                abrir_eTata(GetConexion_eTata())
            Else
                'MsgBox("llega 6")
                panelPrincipal.estado_eTata.ForeColor = Color.Red
                panelPrincipal.estado_eTata.Text = "No Conectado"
                Conectar_Servidor.Show()
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Error", "No se ha podido establecer conexión con la base de datos eTata", ToolTipIcon.Warning)

            End If

        Catch ex As Exception
            Dim panelPrincipal As New Panel_Principal
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al crear conexiones")
            PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Error", "No se ha podido establecer conexión con las bases de datos", ToolTipIcon.Warning)
            panelPrincipal.estado_controlProduccion.ForeColor = Color.Red
            panelPrincipal.estado_controlProduccion.Text = "No Conectado"

            panelPrincipal.estado_eTata.ForeColor = Color.Red
            panelPrincipal.estado_eTata.Text = "No Conectado"

            Conectar_Servidor.Show()
        End Try
    End Sub

    '***********************************************************************************************************************************************'
    'FUNCION DE CONEXION A LA BASE DE DATOS'
    '***********************************************************************************************************************************************'

    'Conexion a la base de datos controlProduccion
    Sub abrir_controlProduccion()

        Dim conexion As New Conectar_Servidor
        Dim panelPrincipal As New Panel_Principal
        leer_XML()

        Try
            If (conexion.Check.Checked = True) Then
                conexion_controlProduccion = New SqlConnection()
                conectar_controlProduccion.ConnectionString = "Data Source=" & Servidor & ";Initial Catalog=ControlProduccion;User ID= " & Usuario & ";Password= " & Password
                conexion_controlProduccion.Open()
            Else
                conexion_controlProduccion = New SqlConnection()
                conexion_controlProduccion.ConnectionString = "Data Source=" & Servidor & ";Initial Catalog=ControlProduccion;Integrated Security=True"
                conexion_controlProduccion.Open()
            End If

            panelPrincipal.estado_controlProduccion.ForeColor = Color.Green
            panelPrincipal.estado_controlProduccion.Text = "Conectado"
        Catch
            panelPrincipal.estado_controlProduccion.ForeColor = Color.Red
            panelPrincipal.estado_controlProduccion.Text = "No Conectado"
            Conectar_Servidor.Show()
        End Try
    End Sub

    Public Sub abrir_controlProduccion(ByVal cadena As String)
        Dim panelPrincipal As New Panel_Principal

        Try
            Dim conexion = New SqlConnection()

            conexion.ConnectionString = cadena
            conexion.Open()
            'PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Abrir Conexión Control Producción", "Conexión establecida", ToolTipIcon.Info)
            panelPrincipal.estado_controlProduccion.ForeColor = Color.Green
            panelPrincipal.estado_controlProduccion.Text = "Conectado"
        Catch
            panelPrincipal.estado_controlProduccion.ForeColor = Color.Red
            panelPrincipal.estado_controlProduccion.Text = "No Conectado"
            Conectar_Servidor.Show()
        End Try
    End Sub

    'Conexion a la base de datos eTata
    Sub abrir_eTata()

        Dim conexion As New Conectar_Servidor
        Dim panelPrincipal As New Panel_Principal
        leer_XML()

        Try
            If (conexion.Check.Checked = True) Then
                conexion_eTata = New SqlConnection()
                conexion_eTata.ConnectionString = "Data Source=" & Servidor & ";Initial Catalog=eTata;User ID= " & Usuario & ";Password= " & Password
                conexion_eTata.Open()
            Else
                conexion_eTata = New SqlConnection()
                conexion_eTata.ConnectionString = "Data Source=" & Servidor & ";Initial Catalog=eTata;Integrated Security=True"
                conexion_eTata.Open()
            End If

            panelPrincipal.estado_eTata.ForeColor = Color.Green
            panelPrincipal.estado_eTata.Text = "Conectado"
        Catch
            panelPrincipal.estado_eTata.ForeColor = Color.Red
            panelPrincipal.estado_eTata.Text = "No Conectado"
            Conectar_Servidor.Show()
        End Try
    End Sub

    Public Sub abrir_eTata(ByVal cadena As String)
        Dim panelPrincipal As New Panel_Principal

        Try
            Dim conexion = New SqlConnection()
            conexion.ConnectionString = cadena
            conexion.Open()
            'PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Abrir Conexión eTata", "Conexión establecida", ToolTipIcon.Info)
            panelPrincipal.estado_eTata.ForeColor = Color.Green
            panelPrincipal.estado_eTata.Text = "Conectado"
        Catch
            panelPrincipal.estado_eTata.ForeColor = Color.Red
            panelPrincipal.estado_eTata.Text = "No Conectado"
            Conectar_Servidor.Show()
        End Try

    End Sub

    Public Sub BuildConnectionString_eTata(ByVal DataSource As String, ByVal InitialCatalog As String, ByVal UserId As String, ByVal Password As String)
        Dim conexion As New Conectar_Servidor
        ' Obtenemos el archivo de configuración de la aplicación.
        '
        Dim config As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)

        ' Obtenemos la sección connectionStrings.
        '
        Dim section As ConnectionStringsSection = config.ConnectionStrings

        ' Obtenemos el objeto ConnectionStringSettings
        ' correspondiente al nombre de la cadena de
        ' conexión especificada.

        Dim settings As ConnectionStringSettings = section.ConnectionStrings.Item("conectar_eTata")

        If (settings Is Nothing) Then Return

        ' Creamos el objeto
        Dim builder As New SqlConnectionStringBuilder()

        ' Le asignamos el valor de la cadena de conexión
        builder.ConnectionString = settings.ConnectionString


        If (UserId = "") Then
            builder.DataSource = DataSource
            builder.InitialCatalog = InitialCatalog
            builder.IntegratedSecurity = True
        Else

            builder.DataSource = DataSource
            builder.InitialCatalog = InitialCatalog
            builder.UserID = UserId
            builder.Password = Password
        End If

        ' Le asignamos la cadena de conexión existente  
        ' en el objeto DbConnectionStringBuilder.  
        '
        settings.ConnectionString = builder.ConnectionString


        ' Modificamos la cadena de conexión en el archivo app.config.
        '
        AddAndSaveOneConnectionStringSettings(config, settings)
    End Sub

    Public Sub BuildConnectionString_controlProduccion(ByVal DataSource As String, ByVal InitialCatalog As String, ByVal UserId As String, ByVal Password As String)

        ' Obtenemos el archivo de configuración de la aplicación.
        '
        Dim config As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)

        ' Obtenemos la sección connectionStrings.
        '
        Dim section As ConnectionStringsSection = config.ConnectionStrings

        ' Obtenemos el objeto ConnectionStringSettings
        ' correspondiente al nombre de la cadena de
        ' conexión especificada.

        Dim settings As ConnectionStringSettings = section.ConnectionStrings.Item("conectar_controlProduccion")

        If (settings Is Nothing) Then Return

        ' Creamos el objeto
        Dim builder As New SqlConnectionStringBuilder()

        ' Le asignamos el valor de la cadena de conexión
        builder.ConnectionString = settings.ConnectionString

        If (UserId = "") Then
            builder.DataSource = DataSource
            builder.InitialCatalog = InitialCatalog
            builder.IntegratedSecurity = True
        Else
            builder.DataSource = DataSource
            builder.InitialCatalog = InitialCatalog
            builder.UserID = UserId
            builder.Password = Password
        End If

        ' Le asignamos la cadena de conexión existente  
        ' en el objeto DbConnectionStringBuilder.  
        '
        settings.ConnectionString = builder.ConnectionString

        ' Modificamos la cadena de conexión en el archivo app.config.
        '
        AddAndSaveOneConnectionStringSettings(config, settings)



    End Sub

    Public Sub AddAndSaveOneConnectionStringSettings(ByVal configuration As Configuration, ByVal connectionStringSettings As ConnectionStringSettings)

        ' You cannot add to ConfigurationManager.ConnectionStrings using
        ' ConfigurationManager.ConnectionStrings.Add
        ' (connectionStringSettings) -- This fails.

        ' But you can add to the configuration section and refresh the ConfigurationManager.

        ' Get the connection strings section; Even if it is in another file.
        Dim connectionStringsSection As ConnectionStringsSection = configuration.ConnectionStrings

        'MsgBox(connectionStringSettings.ConnectionString)

        ' Add the new element to the section.
        connectionStringsSection.ConnectionStrings.Add(connectionStringSettings)

        ' Save the configuration file.
        configuration.Save(ConfigurationSaveMode.Full, False)

        ' This is this needed. Otherwise the connection string does not show up in
        ' ConfigurationManager
        ConfigurationManager.RefreshSection("connectionStrings")

    End Sub

    Public Function GetConexion_eTata() As String
        Try
            Dim config As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            Dim cs As ConnectionStringSettings = config.ConnectionStrings.ConnectionStrings("conectar_eTata")

            If (cs Is Nothing) Then Throw New ArgumentNullException("conectar_eTata", "El nombre de la cadena de conexión no existe" _
                & " en el archivo de configuración de la aplicación.")

            Return cs.ConnectionString
        Catch
            Return ""
        End Try
    End Function

    Public Function GetConexion_controlProduccion() As String
        Try
            Dim config As Configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None)
            Dim cs As ConnectionStringSettings = config.ConnectionStrings.ConnectionStrings("conectar_controlProduccion")

            If (cs Is Nothing) Then Throw New ArgumentNullException("conectar_controlProduccion", "El nombre de la cadena de conexión no existe" _
                & " en el archivo de configuración de la aplicación.")

            Return cs.ConnectionString
        Catch
            Return ""
        End Try
    End Function

    Public Sub cerrar_eTata(ByVal cadena As String)

        Try
            Dim conexion = New SqlConnection(cadena)
            conexion.Close()
        Catch ex As Exception
            PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Cerrar Conexión eTata", "No se ha podido cerrar la conexión", ToolTipIcon.Warning)
        End Try

    End Sub

    Public Sub cerrar_controlProduccion(ByVal cadena As String)
        Try
            Dim conexion = New SqlConnection(cadena)
            conexion.Close()
        Catch ex As Exception
            PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Cerrar Control Producción", "No se ha podido cerrar la conexión", ToolTipIcon.Warning)
        End Try
    End Sub
End Module
