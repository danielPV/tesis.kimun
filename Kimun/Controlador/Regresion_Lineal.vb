﻿Imports System.Data.SqlClient

Module Regresion_Lineal

    Dim func As New Funciones

    Public Sub insertarDatos_TablaPronostico_Lijadora(ByVal linea As String, ByVal fecha1 As Date, ByVal fecha2 As Date)

        Dim crear_tablaPronostico As New SqlCommand("CREATE TABLE TablaPronostico_Lijadora (
			                                        Key_Pronostico int IDENTITY(1,1),
			                                        fecha date,
			                                        linea nvarchar(25),
			                                        produccion numeric(38,6),
			                                        pronostico numeric(38,6))", conexion_controlProduccion)

        Dim insertar_tablaPronostico As New SqlCommand("INSERT INTO TablaPronostico_Lijadora (fecha, linea, produccion)
	                                                    (SELECT
		                                                    Produccion.FechaProduccion,
		                                                    Linea.Linea,
		                                                    ROUND(AVG((Produccion.Produccion / ((450 - CAST(Produccion.TotalTM AS decimal))/ 60))), 6)
	                                                     FROM Linea 
	                                                     INNER JOIN Produccion ON Linea.IdLinea = Produccion.IdLinea 
	                                                     INNER JOIN TM ON Produccion.idProduccion = TM.idProduccion
	                                                     WHERE (Linea.Linea = @Linea) AND FechaProduccion BETWEEN @fecha_inicio AND @fecha_fin
	                                                     GROUP BY FechaProduccion, Linea)", conexion_controlProduccion)

        Dim eliminar_datos As New SqlCommand("DELETE 
                                              FROM TablaPronostico_Lijadora ", conexion_controlProduccion)

        Dim reiniciar_ID As New SqlCommand("DBCC CHECKIDENT (TablaPronostico_Lijadora, RESEED, 0)", conexion_controlProduccion)


        If (func.existeTabla_controlProduccion("TablaPronostico_Lijadora") = False) Then
            Try
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                crear_tablaPronostico.ExecuteNonQuery()
                conexion_controlProduccion.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al crear la tabla pronósticos")
            End Try
        Else
            Try
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                eliminar_datos.ExecuteNonQuery()
                reiniciar_ID.ExecuteNonQuery()
                conexion_controlProduccion.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al eliminar los datos de la tabla pronóstico lijadora")
            End Try
        End If

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            insertar_tablaPronostico.Parameters.AddWithValue("@linea", linea)
            insertar_tablaPronostico.Parameters.AddWithValue("@fecha_inicio", fecha1)
            insertar_tablaPronostico.Parameters.AddWithValue("@fecha_fin", fecha2)

            insertar_tablaPronostico.ExecuteNonQuery()
            insertarDatos_TablaFormula(linea, fecha1, fecha2)
            conexion_controlProduccion.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al insertar los datos del pronóstico de la lijadora")
        End Try
    End Sub

    Public Sub insertarDatos_TablaPronostico_Torno(ByVal linea As String, ByVal fecha1 As Date, ByVal fecha2 As Date)

        Dim crear_tablaPronostico As New SqlCommand("CREATE TABLE TablaPronostico_Torno (
			                                        Key_Pronostico int IDENTITY(1,1),
			                                        fecha date,
			                                        linea nvarchar(25),
			                                        produccion numeric(38,6),
			                                        pronostico numeric(38,6))", conexion_controlProduccion)

        Dim insertar_tablaPronostico As New SqlCommand("INSERT INTO TablaPronostico_Torno (fecha, linea, produccion)
	                                                    (SELECT
		                                                    Produccion.FechaProduccion,
		                                                    Linea.Linea,
		                                                    ROUND(AVG((Produccion.Produccion / ((450 - CAST(Produccion.TotalTM AS decimal))/ 60))), 6)
	                                                     FROM Linea 
	                                                     INNER JOIN Produccion ON Linea.IdLinea = Produccion.IdLinea 
	                                                     INNER JOIN TM ON Produccion.idProduccion = TM.idProduccion
	                                                     WHERE (Linea.Linea = @Linea) AND FechaProduccion BETWEEN @fecha_inicio AND @fecha_fin
	                                                     GROUP BY FechaProduccion, Linea)", conexion_controlProduccion)

        Dim eliminar_datos As New SqlCommand("DELETE 
                                              FROM TablaPronostico_Torno ", conexion_controlProduccion)

        Dim reiniciar_ID As New SqlCommand("DBCC CHECKIDENT (TablaPronostico_Torno, RESEED, 0)", conexion_controlProduccion)


        If (func.existeTabla_controlProduccion("TablaPronostico_Torno") = False) Then
            Try
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                crear_tablaPronostico.ExecuteNonQuery()
                conexion_controlProduccion.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al crear la tabla pronóstico del torno")
            End Try
        Else
            Try
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                eliminar_datos.ExecuteNonQuery()
                reiniciar_ID.ExecuteNonQuery()
                conexion_controlProduccion.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al eliminar la tabla pronóstico del torno")
            End Try
        End If

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            insertar_tablaPronostico.Parameters.AddWithValue("@linea", linea)
            insertar_tablaPronostico.Parameters.AddWithValue("@fecha_inicio", fecha1)
            insertar_tablaPronostico.Parameters.AddWithValue("@fecha_fin", fecha2)

            insertar_tablaPronostico.ExecuteScalar()
            insertarDatos_TablaFormula(linea, fecha1, fecha2)
            conexion_controlProduccion.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al insertar los datos del pronóstico del torno")
        End Try
    End Sub

    Public Sub insertarDatos_TablaPronostico_Descortezador(ByVal linea As String, ByVal fecha1 As Date, ByVal fecha2 As Date)

        Dim crear_tablaPronostico As New SqlCommand("CREATE TABLE TablaPronostico_Descortezador (
			                                        Key_Pronostico int IDENTITY(1,1),
			                                        fecha date,
			                                        linea nvarchar(25),
			                                        produccion numeric(38,6),
			                                        pronostico numeric(38,6))", conexion_controlProduccion)

        Dim insertar_tablaPronostico As New SqlCommand("INSERT INTO TablaPronostico_Descortezador (fecha, linea, produccion)
	                                                    (SELECT
		                                                    Produccion.FechaProduccion,
		                                                    Linea.Linea,
		                                                    ROUND(AVG((Produccion.Produccion / ((450 - CAST(Produccion.TotalTM AS decimal))/ 60))), 6)
	                                                     FROM Linea 
	                                                     INNER JOIN Produccion ON Linea.IdLinea = Produccion.IdLinea 
	                                                     INNER JOIN TM ON Produccion.idProduccion = TM.idProduccion
	                                                     WHERE (Linea.Linea = @Linea) AND FechaProduccion BETWEEN @fecha_inicio AND @fecha_fin
	                                                     GROUP BY FechaProduccion, Linea)", conexion_controlProduccion)

        Dim eliminar_datos As New SqlCommand("DELETE 
                                              FROM TablaPronostico_Descortezador ", conexion_controlProduccion)

        Dim reiniciar_ID As New SqlCommand("DBCC CHECKIDENT (TablaPronostico_Descortezador, RESEED, 0)", conexion_controlProduccion)


        If (func.existeTabla_controlProduccion("TablaPronostico_Descortezador") = False) Then
            Try
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                crear_tablaPronostico.ExecuteNonQuery()
                conexion_controlProduccion.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al crear la tabla pronóstico del descortezador")
            End Try
        Else
            Try
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                eliminar_datos.ExecuteNonQuery()
                reiniciar_ID.ExecuteNonQuery()
                conexion_controlProduccion.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al eliminar los datos de la tabla pronóstico descortezador")
            End Try
        End If

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            insertar_tablaPronostico.Parameters.AddWithValue("@linea", linea)
            insertar_tablaPronostico.Parameters.AddWithValue("@fecha_inicio", fecha1)
            insertar_tablaPronostico.Parameters.AddWithValue("@fecha_fin", fecha2)

            insertar_tablaPronostico.ExecuteScalar()
            insertarDatos_TablaFormula(linea, fecha1, fecha2)
            conexion_controlProduccion.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al insertar los datos del pronóstico del descortezador")
        End Try
    End Sub

    Public Sub insertarDatos_TablaFormula(ByVal linea As String, ByVal primeraFecha As Date, ByVal ultimaFecha As Date)

        Dim num_pronostico As Integer = 0
        Dim n As Integer = 0
        Dim fecha As Date

        'Se establece la fecha en la que empieza el pronostico
        If (Year(primeraFecha) < Year(ultimaFecha)) Then
            If (Month(primeraFecha) = 12) Then
                fecha = DateSerial(Year(ultimaFecha), 1, 1)
            Else
                fecha = DateSerial(Year(primeraFecha), Month(primeraFecha) + 1, 1)
            End If
        Else
            fecha = DateSerial(Year(ultimaFecha), Month(ultimaFecha) + 1, 1)
        End If

        'numero de meses de diferencia
        num_pronostico = DateDiff(DateInterval.Month, primeraFecha, ultimaFecha)

        Dim crear_tablaFormula As New SqlCommand("CREATE TABLE Formula (
				                                    linea nvarchar(25),
				                                    contador int,
				                                    SumX numeric(38,6),
				                                    SumY numeric(38,6),
				                                    SumXY numeric(38,6),
				                                    SumXsqrd numeric(38,6),
				                                    b numeric(38,6),
				                                    a numeric(38,6))", conexion_controlProduccion)

        Dim insertar_LijadoratablaFormula As New SqlCommand("INSERT INTO Formula(linea, contador, SumX, SumY, SumXY, SumXsqrd)
		                                                    (SELECT
			                                                    @linea,
			                                                    COUNT(*),
			                                                    sum(Key_Pronostico),
			                                                    sum(produccion),
			                                                    sum(Key_Pronostico * produccion),
			                                                    sum(power(Key_Pronostico,2))
		                                                     FROM TablaPronostico_Lijadora
		                                                     WHERE produccion IS NOT NULL
		                                                     GROUP BY linea	)", conexion_controlProduccion)

        Dim insertar_TornotablaFormula As New SqlCommand("INSERT INTO Formula(linea, contador, SumX, SumY, SumXY, SumXsqrd)
		                                                    (SELECT
			                                                    @linea,
			                                                    COUNT(*),
			                                                    sum(Key_Pronostico),
			                                                    sum(produccion),
			                                                    sum(Key_Pronostico * produccion),
			                                                    sum(power(Key_Pronostico,2))
		                                                     FROM TablaPronostico_Torno
		                                                     WHERE produccion IS NOT NULL
		                                                     GROUP BY linea	)", conexion_controlProduccion)

        Dim insertar_DescortezadortablaFormula As New SqlCommand("INSERT INTO Formula(linea, contador, SumX, SumY, SumXY, SumXsqrd)
		                                                    (SELECT
			                                                    @linea,
			                                                    COUNT(*),
			                                                    sum(Key_Pronostico),
			                                                    sum(produccion),
			                                                    sum(Key_Pronostico * produccion),
			                                                    sum(power(Key_Pronostico,2))
		                                                     FROM TablaPronostico_Descortezador
		                                                     WHERE produccion IS NOT NULL
		                                                     GROUP BY linea	)", conexion_controlProduccion)


        Dim eliminar_Formula As New SqlCommand("DELETE 
                                              FROM Formula
                                              WHERE linea = @linea", conexion_controlProduccion)

        Dim actualizar_B As New SqlCommand("UPDATE Formula
	                                        SET 
		                                        b = ROUND(((tb.contador * tb.SumXY) - (tb.SumX * tb.SumY)) / ((tb.contador * tb.SumXsqrd) - power(tb.SumX,2)), 6)
	                                        FROM (SELECT linea as XLinea, contador, SumX, SumY, SumXY, SumXsqrd FROM Formula) tb
	                                        WHERE linea = tb.XLinea", conexion_controlProduccion)

        Dim actualizar_A As New SqlCommand("UPDATE Formula
	                                        SET
		                                        a = ROUND(((tb2.SumY  / tb2.contador) - (tb2.b * (tb2.SumX / tb2.contador))), 6)
	                                        FROM (SELECT linea as XLinea, contador, SumX, SumY, SumXY, SumXsqrd, b FROM Formula) tb2
	                                        WHERE linea = tb2.XLinea", conexion_controlProduccion)

        Dim insertar_Pronostico_Lijadora As New SqlCommand("WHILE @Loop < @Pronostico
	                                                        BEGIN
                                                                INSERT INTO TablaPronostico_Lijadora(fecha, linea, pronostico)
		                                                        SELECT 
			                                                        DATEADD(mm, @Loop, @Fecha), 
			                                                        a.Linea,
			                                                        ROUND((Max(a) + (Max(b) * (MAX(Key_Pronostico) + 1))), 6)
		                                                        FROM TablaPronostico_Lijadora a 
		                                                        INNER JOIN 
			                                                        Formula b
		                                                        ON a.linea = b.linea
		                                                        GROUP BY a.linea
                                                            SET @Loop = @Loop +1
	                                                        END", conexion_controlProduccion)

        Dim insertar_Pronostico_Torno As New SqlCommand("WHILE @Loop < @Pronostico
	                                                    BEGIN
                                                            INSERT INTO TablaPronostico_Torno(fecha, linea, pronostico)
		                                                    SELECT 
			                                                    DATEADD(mm, @Loop, @Fecha), 
			                                                    a.Linea,
			                                                    ROUND((Max(a) + (Max(b) * (MAX(Key_Pronostico) + 1))), 6)
		                                                    FROM TablaPronostico_Torno a 
		                                                    INNER JOIN 
			                                                    Formula b
		                                                    ON a.linea = b.linea
		                                                    GROUP BY a.linea
                                                        SET @Loop = @Loop +1
	                                                    END", conexion_controlProduccion)

        Dim insertar_Pronostico_Descortezador As New SqlCommand("WHILE @Loop < @Pronostico
	                                                            BEGIN
                                                                    INSERT INTO TablaPronostico_Descortezador(fecha, linea, pronostico)
		                                                            SELECT 
			                                                            DATEADD(mm, @Loop, @Fecha),  
			                                                            a.Linea,
			                                                            ROUND((Max(a) + (Max(b) * (MAX(Key_Pronostico) + 1))), 6)
		                                                            FROM TablaPronostico_Descortezador a 
		                                                            INNER JOIN 
			                                                            Formula b
		                                                            ON a.linea = b.linea
		                                                            GROUP BY a.linea
                                                                SET @Loop = @Loop +1
	                                                            END", conexion_controlProduccion)

        If (func.existeTabla_controlProduccion("Formula") = False) Then
            Try
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                crear_tablaFormula.ExecuteNonQuery()

                conexion_controlProduccion.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al crear la tabla formula " & linea)
            End Try
        Else

            Try
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                eliminar_Formula.Parameters.AddWithValue("@linea", linea)
                eliminar_Formula.ExecuteNonQuery()

            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al eliminar la tabla formula " & linea)
            End Try

        End If

        Try

            If (linea = "Lijadora") Then
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                insertar_LijadoratablaFormula.Parameters.AddWithValue("@linea", linea)
                insertar_LijadoratablaFormula.ExecuteNonQuery()

                actualizar_B.ExecuteNonQuery()
                actualizar_A.ExecuteNonQuery()

                insertar_Pronostico_Lijadora.Parameters.AddWithValue("@Fecha", fecha)
                insertar_Pronostico_Lijadora.Parameters.AddWithValue("@Loop", n)
                insertar_Pronostico_Lijadora.Parameters.AddWithValue("@Pronostico", num_pronostico)

                insertar_Pronostico_Lijadora.ExecuteNonQuery()
            End If

            If (linea = "Torno") Then
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                insertar_TornotablaFormula.Parameters.AddWithValue("@linea", linea)
                insertar_TornotablaFormula.ExecuteNonQuery()

                actualizar_B.ExecuteNonQuery()
                actualizar_A.ExecuteNonQuery()

                insertar_Pronostico_Torno.Parameters.AddWithValue("@Fecha", fecha)
                insertar_Pronostico_Torno.Parameters.AddWithValue("@Loop", n)
                insertar_Pronostico_Torno.Parameters.AddWithValue("@Pronostico", num_pronostico)

                insertar_Pronostico_Torno.ExecuteNonQuery()
            End If

            If (linea = "Descortezador") Then
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                insertar_DescortezadortablaFormula.Parameters.AddWithValue("@linea", linea)
                insertar_DescortezadortablaFormula.ExecuteNonQuery()

                actualizar_B.ExecuteNonQuery()
                actualizar_A.ExecuteNonQuery()

                insertar_Pronostico_Descortezador.Parameters.AddWithValue("@Fecha", fecha)
                insertar_Pronostico_Descortezador.Parameters.AddWithValue("@Loop", n)
                insertar_Pronostico_Descortezador.Parameters.AddWithValue("@Pronostico", num_pronostico)

                insertar_Pronostico_Descortezador.ExecuteNonQuery()
            End If

            conexion_controlProduccion.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al insertar datos del pronóstico " & linea)
        End Try

    End Sub

End Module
