﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Reflection
Imports System.Security.Permissions
Imports System.Text
Imports System.Xml

Public Class Notificaciones

    Public enviado As Boolean
    Public Sub EnviarEmail(ByVal turno As String, ByVal inicio As Date, ByVal fin As Date)

        'Variables
        Dim fecha As String = Format(Now, "yyyy/MM/dd")
        Dim lectorHTML As String
        Dim html As String

        Dim MisDocumentos As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Dim correos() As String = {""}
        Dim correo_1 As String = ""
        Dim correo_2 As String = ""
        Dim correo_3 As String = ""
        Dim correo_4 As String = ""
        Dim correo_5 As String = ""

        lectorHTML = My.Resources.plantilla_CalidadAguas

        'Toma la plantilla en la que iran los datos
        'lectorHTML = My.Computer.FileSystem.ReadAllText("C:\plantilla\plantilla.txt", Encoding.UTF8)
        Dim builder As New StringBuilder(lectorHTML)

        'Obtiene el horario del turno
        Dim inicio_turno As Date = String.Format("{0} {1}", fecha, Format(inicio, "HH:mm:ss"))
        Dim final_turno As Date = String.Format("{0} {1}", fecha, Format(fin, "HH:mm:ss"))

        'Id cloruro = 1
        'Id ph = 6
        'Id Dureza = 7
        'Id Sulfito = 8
        'Id dureza A = 10
        'Id dureza M = 11
        'Id purgas = 12

        Dim valor_cloruro As Double = consultar(1, turno, CDate(inicio_turno), CDate(final_turno))
        Dim valor_ph As Double = consultar(6, turno, CDate(inicio_turno), CDate(final_turno))
        Dim valor_dureza As Double = consultar(7, turno, CDate(inicio_turno), CDate(final_turno))
        Dim valor_sulfito As Double = consultar(8, turno, CDate(inicio_turno), CDate(final_turno))
        Dim valor_durezaA As Double = consultar(10, turno, CDate(inicio_turno), CDate(final_turno))
        Dim valor_durezaM As Double = consultar(11, turno, CDate(inicio_turno), CDate(final_turno))
        Dim valor_purgas As Double = consultar(12, turno, CDate(inicio_turno), CDate(final_turno))


        If (existenDatos(turno, CDate(inicio_turno), CDate(final_turno)) = False) Then
            'No Existen Datos
            PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Notificación", "No existen registros para generar el informe.", ToolTipIcon.Warning)
            enviado = False
        Else

            Try
                Dim filePermissions = New FileIOPermission(FileIOPermissionAccess.AllAccess, "C:\")
                filePermissions.Demand()

                Dim ruta As String = MisDocumentos & "\Kimun\config.xml"

                'Pregunta si el directorio existe
                If My.Computer.FileSystem.FileExists(ruta) Then
                    Dim documento As XmlReader = New XmlTextReader(ruta)

                    While (documento.Read())
                        Dim type = documento.NodeType

                        If (type = XmlNodeType.Element) Then
                            If (documento.Name = "Correo") Then

                                correos = documento.ReadInnerXml.Split(", ")
                            End If
                        End If
                    End While

                    documento.Dispose()
                    documento.Close()

                    Dim num_correos As Integer = correos.Length()

                    If (correos(0) = "") Then
                        MsgBox("Se debe ingresar por lo menos un correo", MsgBoxStyle.Information)
                        Dim config As New PanelConfiguracion
                        Dim tab As TabPage = config.TabControl1.TabPages(3)
                        config.TabControl1.SelectedTab = tab
                        config.Show()
                        Exit Sub
                    Else
                        For index As Integer = 0 To num_correos - 1
                            If (correos(index) <> "") Then
                                If (index = 0) Then
                                    correo_1 = correos(index)
                                End If

                                If (index = 1) Then
                                    correo_2 = correos(index)
                                End If

                                If (index = 2) Then
                                    correo_3 = correos(index)
                                End If

                                If (index = 3) Then
                                    correo_4 = correos(index)
                                End If

                                If (index = 4) Then
                                    correo_5 = correos(index)
                                End If
                            End If
                        Next
                    End If

                End If
            Catch ex As Exception
                MessageBox.Show(ex.ToString, "Error leer el documento XML", MessageBoxButtons.OK)
            End Try

            Try

                'entrega los valores a los campos de la plantilla
                builder.Replace("###Turno###", turno)
                builder.Replace("###Fecha###", Format(Now, "dd/MM/yyyy"))
                builder.Replace("###Cloruros###", valor_cloruro.ToString)
                builder.Replace("###PH###", valor_ph.ToString)
                builder.Replace("###Dureza###", valor_dureza.ToString)
                builder.Replace("###Sulfitos###", valor_sulfito.ToString)
                builder.Replace("###DurezaA###", valor_durezaA.ToString)
                builder.Replace("###DurezaM###", valor_durezaM.ToString)
                builder.Replace("###Purgas###", valor_purgas.ToString)

                html = builder.ToString

                'conexiones para el envio del correo
                Dim mensaje As New MailMessage
                Dim SMTP As New SmtpClient
                SMTP.Credentials = New NetworkCredential("synapsys@rioitata.cl", "itata22")
                SMTP.Host = "mail.rioitata.cl"
                SMTP.Port = 25
                SMTP.EnableSsl = False

                'Estructura envio del correo

                'Pregunta si el archivo XML tiene correos
                If (correo_1 <> "") Then
                    mensaje.[To].Add(correo_1)
                End If

                If (correo_2 <> "") Then
                    mensaje.[To].Add(correo_2)
                End If

                If (correo_3 <> "") Then
                    mensaje.[To].Add(correo_3)
                End If

                If (correo_4 <> "") Then
                    mensaje.[To].Add(correo_4)
                End If

                If (correo_5 <> "") Then
                    mensaje.[To].Add(correo_5)
                End If

                mensaje.From = New MailAddress("synapsys@rioitata.cl", "Informe Kimün", Encoding.UTF8)
                mensaje.Subject = "[" & turno & "] Informe Calidad de Aguas "
                mensaje.SubjectEncoding = Encoding.UTF8
                mensaje.Body = html
                mensaje.BodyEncoding = Encoding.UTF8
                mensaje.Priority = MailPriority.Normal
                mensaje.IsBodyHtml = True

                SMTP.Send(mensaje)
                'Notificacion
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Notificación", "El informe de calidad de aguas fue enviado.", ToolTipIcon.Info)
                enviado = True

                Dim tool As New mostrar_Turno
                tool.ToolTip1.SetToolTip(tool.Button1, "Ya se envio el informe este turno")
                tool.cambiar_estado(False)

            Catch ex As SmtpException
                MessageBox.Show(ex.ToString, "Error al enviar el correo", MessageBoxButtons.OK)
            End Try

        End If


    End Sub

    Private Function consultar(ByVal idMedicion As Integer, ByVal turno As String, ByVal fecha1 As Date, ByVal fecha2 As Date) As Double

        Dim consulta As New SqlCommand("SELECT Avg(RegistroCaldera.Valor)
                                        FROM ParamMedicionCaldera INNER JOIN
                                                RegistroCaldera ON ParamMedicionCaldera.idMedicionCaldera = RegistroCaldera.idMedicionCaldera INNER JOIN
                                                CalidadCaldera ON RegistroCaldera.idControlCaldera = CalidadCaldera.idControlCaldera INNER JOIN
                                                Turno ON CalidadCaldera.TurnoControlCaldera = Turno.CodTurno
                                        WHERE Turno.Turno = @turno and CalidadCaldera.FechaControlCaldera between @fecha1 and @fecha2 and ParamMedicionCaldera.idMedicionCaldera = @id", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@id", idMedicion)
            consulta.Parameters.AddWithValue("@turno", turno)
            consulta.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            consulta.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            Return Format(consulta.ExecuteScalar(), "000.00")

            conexion_controlProduccion.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar datos email")
            Return 0
        End Try

    End Function

    Private Function existenDatos(ByVal turno As String, ByVal fecha1 As Date, ByVal fecha2 As Date) As Boolean

        Dim consulta As New SqlCommand("SELECT COUNT(*)
                                        FROM ParamMedicionCaldera INNER JOIN
                                                RegistroCaldera ON ParamMedicionCaldera.idMedicionCaldera = RegistroCaldera.idMedicionCaldera INNER JOIN
                                                CalidadCaldera ON RegistroCaldera.idControlCaldera = CalidadCaldera.idControlCaldera INNER JOIN
                                                Turno ON CalidadCaldera.TurnoControlCaldera = Turno.CodTurno
                                        WHERE Turno.Turno = @turno and CalidadCaldera.FechaControlCaldera between @fecha1 and @fecha2 ", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@turno", turno)
            consulta.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            consulta.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            Dim n As Integer = CInt(consulta.ExecuteScalar())

            Return n > 0

            conexion_controlProduccion.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "existe datos")
            Return False
        End Try


    End Function

End Class
