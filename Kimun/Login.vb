﻿Imports System.Data.SqlClient
Imports DevExpress.XtraSplashScreen

Public Class Login

    Public usu As String

    Private Sub Login_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Crear usuario admin

        'Comprobamos si hay conexion con la base de datos
        If (test_Conection("controlProduccion", GetConexion_controlProduccion()) = False And test_Conection("eTata", GetConexion_eTata()) = False) Then
            Conectar_Servidor.Show()
        Else
            abrir_controlProduccion()
            abrir_controlProduccion(GetConexion_controlProduccion())
            abrir_eTata()
            abrir_eTata(GetConexion_eTata())

            If (existe_admin() = False) Then
                crear_admin()
            End If

        End If
    End Sub

    Private Sub crear_admin()

        Dim crear As New SqlCommand("INSERT INTO Usuarios (RutUsuario, NombreUsuario, CargoUsuario, RolUsuario, Password, Unidad)  
                    VALUES( '0-0', 'Admin', 'Administrador', 'Administrador', 'admin', 'Administracion')", conexion_controlProduccion)

        Dim ejecutar As Integer

        Try
            ejecutar = crear.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear el admin")
        End Try

    End Sub

    Private Function existe_admin() As Boolean

        Dim consulta As New SqlCommand("SELECT COUNT(*)
                                        FROM Usuarios
                                        WHERE NombreUsuario = 'admin' ", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            Dim n As Integer = CInt(consulta.ExecuteScalar())

            Return n > 0

        Catch
            Return False
        End Try

    End Function

    Private Function consultar_usuario(ByVal usuario As String, ByVal password As String) As Boolean

        Dim consulta As New SqlCommand("SELECT COUNT(*)
                                        FROM Usuarios
                                        WHERE NombreUsuario = @nombre AND Password = @pass ", conexion_controlProduccion)


        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@nombre", usuario)
            consulta.Parameters.AddWithValue("@pass", password)

            Dim n As Integer = CInt(consulta.ExecuteScalar())

            Return n > 0

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar usuario")
            Return False
        End Try

    End Function

    Private Function consultar_rol(ByVal usuario As String, ByVal password As String) As String

        Dim consulta As New SqlCommand("SELECT RolUsuario
                                        FROM Usuarios
                                        WHERE NombreUsuario = @nombre AND Password = @pass ", conexion_controlProduccion)
        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@nombre", usuario)
            consulta.Parameters.AddWithValue("@pass", password)

            Return consulta.ExecuteScalar()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar rol del usuario")
            Return ""
        End Try

    End Function

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If (box_usuario.Text = "" Or box_password.Text = "") Then
            MsgBox("Por favor, ingrese su usuario y contraseña.", MsgBoxStyle.Information)
        Else
            If (consultar_usuario(box_usuario.Text, box_password.Text) = True) Then
                If (consultar_rol(box_usuario.Text, box_password.Text) = "admin                         " Or (consultar_rol(box_usuario.Text, box_password.Text) = "Administrador                 ")) Then
                    usu = box_usuario.Text
                    Dim c As Cursor = Me.Cursor
                    Dim panel As New Panel_Principal(usu)
                    Me.Cursor = Cursors.WaitCursor
                    panel.Show()
                    Me.Cursor = c
                    Me.Close()
                Else
                    usu = box_usuario.Text
                    Dim c As Cursor = Me.Cursor
                    Dim panel As New PanelDeControl(usu)

                    Me.Cursor = Cursors.WaitCursor
                    SplashScreenManager.ShowForm(GetType(SplashScreen1))
                    panel.Show()
                    SplashScreenManager.CloseForm()
                    Me.Cursor = c
                    Me.Close()
                End If
            Else
                MsgBox("El usuario no existe o su contraseña no es correcta." & vbCr & "Para mayor información póngase en contacto con el administrador.", MsgBoxStyle.Critical)
            End If
        End If
    End Sub

    Private Sub Login_KeyDown(sender As Object, e As KeyEventArgs) Handles MyBase.KeyDown
        Select Case e.KeyData
            Case Keys.Enter
                If (box_usuario.Text = "" Or box_password.Text = "") Then
                    MsgBox("Por favor, ingrese su usuario y contraseña.", MsgBoxStyle.Information)
                Else
                    If (consultar_usuario(box_usuario.Text, box_password.Text) = True) Then
                        If (consultar_rol(box_usuario.Text, box_password.Text) = "admin                         " Or (consultar_rol(box_usuario.Text, box_password.Text) = "Administrador                 ")) Then
                            usu = box_usuario.Text
                            Dim c As Cursor = Me.Cursor
                            Dim panel As New Panel_Principal(usu)
                            Me.Cursor = Cursors.WaitCursor
                            panel.Show()
                            Me.Cursor = c
                            Me.Close()
                        Else
                            usu = box_usuario.Text
                            Dim c As Cursor = Me.Cursor
                            Dim panel As New PanelDeControl(usu)

                            Me.Cursor = Cursors.WaitCursor
                            SplashScreenManager.ShowForm(GetType(SplashScreen1))
                            panel.Show()
                            SplashScreenManager.CloseForm()
                            Me.Cursor = c
                            Me.Close()
                        End If
                    Else
                        MsgBox("El usuario no existe o su contraseña no es correcta." & vbCr & "Para mayor información póngase en contacto con el administrador.", MsgBoxStyle.Critical)
                    End If
                End If
        End Select

    End Sub

    Private Sub box_password_KeyDown(sender As Object, e As KeyEventArgs) Handles box_password.KeyDown
        Select Case e.KeyData
            Case Keys.Enter
                If (box_usuario.Text = "" Or box_password.Text = "") Then
                    MsgBox("Por favor, ingrese su usuario y contraseña.", MsgBoxStyle.Information)
                Else
                    If (consultar_usuario(box_usuario.Text, box_password.Text) = True) Then
                        If (consultar_rol(box_usuario.Text, box_password.Text) = "admin                         " Or (consultar_rol(box_usuario.Text, box_password.Text) = "Administrador                 ")) Then
                            usu = box_usuario.Text
                            Dim c As Cursor = Me.Cursor
                            Dim panel As New Panel_Principal(usu)
                            Me.Cursor = Cursors.WaitCursor
                            panel.Show()
                            Me.Cursor = c
                            Me.Close()
                        Else
                            usu = box_usuario.Text
                            Dim c As Cursor = Me.Cursor
                            Dim panel As New PanelDeControl(usu)

                            Me.Cursor = Cursors.WaitCursor
                            SplashScreenManager.ShowForm(GetType(SplashScreen1))
                            panel.Show()
                            SplashScreenManager.CloseForm()
                            Me.Cursor = c
                            Me.Close()
                        End If
                    Else
                        MsgBox("El usuario no existe o su contraseña no es correcta." & vbCr & "Para mayor información póngase en contacto con el administrador.", MsgBoxStyle.Critical)
                    End If
                End If
        End Select
    End Sub

    Private Sub box_usuario_KeyDown(sender As Object, e As KeyEventArgs) Handles box_usuario.KeyDown
        Select Case e.KeyData
            Case Keys.Enter
                If (box_usuario.Text = "" Or box_password.Text = "") Then
                    MsgBox("Por favor, ingrese su usuario y contraseña.", MsgBoxStyle.Information)
                Else
                    If (consultar_usuario(box_usuario.Text, box_password.Text) = True) Then
                        If (consultar_rol(box_usuario.Text, box_password.Text) = "admin                         " Or (consultar_rol(box_usuario.Text, box_password.Text) = "Administrador                 ")) Then
                            usu = box_usuario.Text
                            Dim c As Cursor = Me.Cursor
                            Dim panel As New Panel_Principal(usu)
                            Me.Cursor = Cursors.WaitCursor
                            panel.Show()
                            Me.Cursor = c
                            Me.Close()
                        Else
                            usu = box_usuario.Text
                            Dim c As Cursor = Me.Cursor
                            Dim panel As New PanelDeControl(usu)

                            Me.Cursor = Cursors.WaitCursor
                            SplashScreenManager.ShowForm(GetType(SplashScreen1))
                            panel.Show()
                            SplashScreenManager.CloseForm()
                            Me.Cursor = c
                            Me.Close()
                        End If
                    Else
                        MsgBox("El usuario no existe o su contraseña no es correcta." & vbCr & "Para mayor información póngase en contacto con el administrador.", MsgBoxStyle.Critical)
                    End If
                End If
        End Select
    End Sub
End Class