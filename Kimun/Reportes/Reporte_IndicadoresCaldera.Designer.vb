﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Reporte_IndicadoresCaldera
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SelectQuery1 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column1 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression1 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table1 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column2 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression2 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column3 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression3 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table2 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column4 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression4 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column5 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression5 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table3 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column6 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression6 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table4 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Join1 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo1 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join2 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo2 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join3 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo3 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Reporte_IndicadoresCaldera))
        Dim SelectQuery2 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column7 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression7 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table5 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column8 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression8 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column9 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression9 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table6 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column10 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression10 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column11 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression11 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table7 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column12 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression12 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table8 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim QueryParameter1 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim QueryParameter2 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim Join4 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo4 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join5 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo5 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join6 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo6 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.SqlDataSource1 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.PageFooterBand1 = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.ReportHeaderBand1 = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.fecha2 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.fecha1 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrPivotGrid2 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.SqlDataSource2 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        Me.fieldEquivalencia1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldUM1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldDateAndTime1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldVal1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldLimiteMin1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldLimite1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel13 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.Title = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.FieldCaption = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.PageInfo = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DataField = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.TopMarginBand1 = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMarginBand1 = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPivotGrid1 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.XrPivotGridField1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField2 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField3 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField4 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField5 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField6 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 3.041649!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'SqlDataSource1
        '
        Me.SqlDataSource1.ConnectionName = "Kimun.My.MySettings.String_conexion_eTata"
        Me.SqlDataSource1.Name = "SqlDataSource1"
        ColumnExpression1.ColumnName = "Equivalencia"
        Table1.Name = "EquivalenciaCaldera"
        ColumnExpression1.Table = Table1
        Column1.Expression = ColumnExpression1
        ColumnExpression2.ColumnName = "UM"
        ColumnExpression2.Table = Table1
        Column2.Expression = ColumnExpression2
        ColumnExpression3.ColumnName = "DateAndTime"
        Table2.Name = "FloatTableCaldera"
        ColumnExpression3.Table = Table2
        Column3.Expression = ColumnExpression3
        ColumnExpression4.ColumnName = "Val"
        ColumnExpression4.Table = Table2
        Column4.Expression = ColumnExpression4
        ColumnExpression5.ColumnName = "LimiteMin"
        Table3.Name = "LimiteMinCaldera"
        ColumnExpression5.Table = Table3
        Column5.Expression = ColumnExpression5
        ColumnExpression6.ColumnName = "Limite"
        Table4.Name = "LimitesCaldera"
        ColumnExpression6.Table = Table4
        Column6.Expression = ColumnExpression6
        SelectQuery1.Columns.Add(Column1)
        SelectQuery1.Columns.Add(Column2)
        SelectQuery1.Columns.Add(Column3)
        SelectQuery1.Columns.Add(Column4)
        SelectQuery1.Columns.Add(Column5)
        SelectQuery1.Columns.Add(Column6)
        SelectQuery1.FilterString = "[EquivalenciaCaldera.Equivalencia] <> 'RETROLAVADO AGUA BLANDA'"
        SelectQuery1.Name = "EquivalenciaCaldera"
        RelationColumnInfo1.NestedKeyColumn = "TagIndex"
        RelationColumnInfo1.ParentKeyColumn = "TagIndex"
        Join1.KeyColumns.Add(RelationColumnInfo1)
        Join1.Nested = Table2
        Join1.Parent = Table1
        RelationColumnInfo2.NestedKeyColumn = "TagIndex"
        RelationColumnInfo2.ParentKeyColumn = "TagIndex"
        Join2.KeyColumns.Add(RelationColumnInfo2)
        Join2.Nested = Table3
        Join2.Parent = Table1
        RelationColumnInfo3.NestedKeyColumn = "TagIndex"
        RelationColumnInfo3.ParentKeyColumn = "TagIndex"
        Join3.KeyColumns.Add(RelationColumnInfo3)
        Join3.Nested = Table4
        Join3.Parent = Table1
        SelectQuery1.Relations.Add(Join1)
        SelectQuery1.Relations.Add(Join2)
        SelectQuery1.Relations.Add(Join3)
        SelectQuery1.Tables.Add(Table1)
        SelectQuery1.Tables.Add(Table2)
        SelectQuery1.Tables.Add(Table3)
        SelectQuery1.Tables.Add(Table4)
        Me.SqlDataSource1.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {SelectQuery1})
        Me.SqlDataSource1.ResultSchemaSerializable = resources.GetString("SqlDataSource1.ResultSchemaSerializable")
        '
        'XrLine1
        '
        Me.XrLine1.Dpi = 100.0!
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 60.45834!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(729.8333!, 2.0!)
        '
        'PageFooterBand1
        '
        Me.PageFooterBand1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrPageInfo2})
        Me.PageFooterBand1.Dpi = 100.0!
        Me.PageFooterBand1.HeightF = 29.0!
        Me.PageFooterBand1.Name = "PageFooterBand1"
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 100.0!
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(6.0!, 6.0!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(358.0!, 23.0!)
        Me.XrPageInfo1.StyleName = "PageInfo"
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.Dpi = 100.0!
        Me.XrPageInfo2.Format = "Página {0} de {1}"
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(376.0!, 6.0!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(358.0!, 23.0!)
        Me.XrPageInfo2.StyleName = "PageInfo"
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'ReportHeaderBand1
        '
        Me.ReportHeaderBand1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrLabel6, Me.XrPivotGrid2, Me.XrLabel1, Me.XrLabel3, Me.XrLabel4, Me.XrLabel2, Me.XrLabel13, Me.XrLine1, Me.XrLine2})
        Me.ReportHeaderBand1.Dpi = 100.0!
        Me.ReportHeaderBand1.HeightF = 230.9583!
        Me.ReportHeaderBand1.Name = "ReportHeaderBand1"
        '
        'XrLabel7
        '
        Me.XrLabel7.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.fecha2, "Text", "")})
        Me.XrLabel7.Dpi = 100.0!
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(567.0209!, 109.4583!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(172.9791!, 22.99999!)
        Me.XrLabel7.Text = "XrLabel7"
        '
        'fecha2
        '
        Me.fecha2.Description = "fecha fin"
        Me.fecha2.Name = "fecha2"
        Me.fecha2.Type = GetType(Date)
        '
        'XrLabel6
        '
        Me.XrLabel6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.fecha1, "Text", "")})
        Me.XrLabel6.Dpi = 100.0!
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(567.0209!, 75.0!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(172.9791!, 23.0!)
        Me.XrLabel6.Text = "XrLabel6"
        '
        'fecha1
        '
        Me.fecha1.Description = "fecha inicio"
        Me.fecha1.Name = "fecha1"
        Me.fecha1.Type = GetType(Date)
        '
        'XrPivotGrid2
        '
        Me.XrPivotGrid2.Appearance.Cell.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.XrPivotGrid2.DataMember = "EquivalenciaCaldera"
        Me.XrPivotGrid2.DataSource = Me.SqlDataSource2
        Me.XrPivotGrid2.Dpi = 100.0!
        Me.XrPivotGrid2.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.fieldEquivalencia1, Me.fieldUM1, Me.fieldDateAndTime1, Me.fieldVal1, Me.fieldLimiteMin1, Me.fieldLimite1})
        Me.XrPivotGrid2.LocationFloat = New DevExpress.Utils.PointFloat(181.0833!, 170.9583!)
        Me.XrPivotGrid2.Name = "XrPivotGrid2"
        Me.XrPivotGrid2.OptionsChartDataSource.ProvideDataByColumns = False
        Me.XrPivotGrid2.OptionsPrint.FilterSeparatorBarPadding = 3
        Me.XrPivotGrid2.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid2.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid2.OptionsPrint.PrintHorzLines = DevExpress.Utils.DefaultBoolean.[True]
        Me.XrPivotGrid2.OptionsPrint.PrintRowHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid2.OptionsPrint.PrintUnusedFilterFields = False
        Me.XrPivotGrid2.OptionsView.ShowColumnGrandTotalHeader = False
        Me.XrPivotGrid2.OptionsView.ShowColumnGrandTotals = False
        Me.XrPivotGrid2.OptionsView.ShowColumnHeaders = False
        Me.XrPivotGrid2.OptionsView.ShowColumnTotals = False
        Me.XrPivotGrid2.OptionsView.ShowRowGrandTotalHeader = False
        Me.XrPivotGrid2.OptionsView.ShowRowGrandTotals = False
        Me.XrPivotGrid2.OptionsView.ShowRowHeaders = False
        Me.XrPivotGrid2.OptionsView.ShowRowTotals = False
        Me.XrPivotGrid2.SizeF = New System.Drawing.SizeF(429.7084!, 50.0!)
        '
        'SqlDataSource2
        '
        Me.SqlDataSource2.ConnectionName = "Kimun.My.MySettings.String_conexion_eTata"
        Me.SqlDataSource2.Name = "SqlDataSource2"
        ColumnExpression7.ColumnName = "Equivalencia"
        Table5.Name = "EquivalenciaCaldera"
        ColumnExpression7.Table = Table5
        Column7.Expression = ColumnExpression7
        ColumnExpression8.ColumnName = "UM"
        ColumnExpression8.Table = Table5
        Column8.Expression = ColumnExpression8
        ColumnExpression9.ColumnName = "DateAndTime"
        Table6.Name = "FloatTableCaldera"
        ColumnExpression9.Table = Table6
        Column9.Expression = ColumnExpression9
        ColumnExpression10.ColumnName = "Val"
        ColumnExpression10.Table = Table6
        Column10.Expression = ColumnExpression10
        ColumnExpression11.ColumnName = "LimiteMin"
        Table7.Name = "LimiteMinCaldera"
        ColumnExpression11.Table = Table7
        Column11.Expression = ColumnExpression11
        ColumnExpression12.ColumnName = "Limite"
        Table8.Name = "LimitesCaldera"
        ColumnExpression12.Table = Table8
        Column12.Expression = ColumnExpression12
        SelectQuery2.Columns.Add(Column7)
        SelectQuery2.Columns.Add(Column8)
        SelectQuery2.Columns.Add(Column9)
        SelectQuery2.Columns.Add(Column10)
        SelectQuery2.Columns.Add(Column11)
        SelectQuery2.Columns.Add(Column12)
        SelectQuery2.FilterString = "[EquivalenciaCaldera.Equivalencia] <> 'RETROLAVADO AGUA BLANDA' And [FloatTableCa" &
    "ldera.DateAndTime] Between(?fecha1, ?fecha2)"
        SelectQuery2.Name = "EquivalenciaCaldera"
        QueryParameter1.Name = "fecha1"
        QueryParameter1.Type = GetType(DevExpress.DataAccess.Expression)
        QueryParameter1.Value = New DevExpress.DataAccess.Expression("[Parameters.fecha1]", GetType(Date))
        QueryParameter2.Name = "fecha2"
        QueryParameter2.Type = GetType(DevExpress.DataAccess.Expression)
        QueryParameter2.Value = New DevExpress.DataAccess.Expression("[Parameters.fecha2]", GetType(Date))
        SelectQuery2.Parameters.Add(QueryParameter1)
        SelectQuery2.Parameters.Add(QueryParameter2)
        RelationColumnInfo4.NestedKeyColumn = "TagIndex"
        RelationColumnInfo4.ParentKeyColumn = "TagIndex"
        Join4.KeyColumns.Add(RelationColumnInfo4)
        Join4.Nested = Table6
        Join4.Parent = Table5
        RelationColumnInfo5.NestedKeyColumn = "TagIndex"
        RelationColumnInfo5.ParentKeyColumn = "TagIndex"
        Join5.KeyColumns.Add(RelationColumnInfo5)
        Join5.Nested = Table7
        Join5.Parent = Table5
        RelationColumnInfo6.NestedKeyColumn = "TagIndex"
        RelationColumnInfo6.ParentKeyColumn = "TagIndex"
        Join6.KeyColumns.Add(RelationColumnInfo6)
        Join6.Nested = Table8
        Join6.Parent = Table5
        SelectQuery2.Relations.Add(Join4)
        SelectQuery2.Relations.Add(Join5)
        SelectQuery2.Relations.Add(Join6)
        SelectQuery2.Tables.Add(Table5)
        SelectQuery2.Tables.Add(Table6)
        SelectQuery2.Tables.Add(Table7)
        SelectQuery2.Tables.Add(Table8)
        Me.SqlDataSource2.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {SelectQuery2})
        Me.SqlDataSource2.ResultSchemaSerializable = resources.GetString("SqlDataSource2.ResultSchemaSerializable")
        '
        'fieldEquivalencia1
        '
        Me.fieldEquivalencia1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldEquivalencia1.AreaIndex = 0
        Me.fieldEquivalencia1.FieldName = "Equivalencia"
        Me.fieldEquivalencia1.Name = "fieldEquivalencia1"
        Me.fieldEquivalencia1.Width = 198
        '
        'fieldUM1
        '
        Me.fieldUM1.AreaIndex = 0
        Me.fieldUM1.FieldName = "UM"
        Me.fieldUM1.Name = "fieldUM1"
        '
        'fieldDateAndTime1
        '
        Me.fieldDateAndTime1.AreaIndex = 1
        Me.fieldDateAndTime1.FieldName = "DateAndTime"
        Me.fieldDateAndTime1.Name = "fieldDateAndTime1"
        '
        'fieldVal1
        '
        Me.fieldVal1.AreaIndex = 2
        Me.fieldVal1.FieldName = "Val"
        Me.fieldVal1.Name = "fieldVal1"
        '
        'fieldLimiteMin1
        '
        Me.fieldLimiteMin1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldLimiteMin1.AreaIndex = 0
        Me.fieldLimiteMin1.Caption = "Límite Mín"
        Me.fieldLimiteMin1.CellFormat.FormatString = "###.##"
        Me.fieldLimiteMin1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldLimiteMin1.FieldName = "LimiteMin"
        Me.fieldLimiteMin1.Name = "fieldLimiteMin1"
        Me.fieldLimiteMin1.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        Me.fieldLimiteMin1.UnboundFieldName = "Limite Mín"
        '
        'fieldLimite1
        '
        Me.fieldLimite1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldLimite1.AreaIndex = 1
        Me.fieldLimite1.Caption = "Límite Máx"
        Me.fieldLimite1.CellFormat.FormatString = "###.##"
        Me.fieldLimite1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldLimite1.FieldName = "Limite"
        Me.fieldLimite1.Name = "fieldLimite1"
        Me.fieldLimite1.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        Me.fieldLimite1.UnboundFieldName = "Limite Máx"
        '
        'XrLabel1
        '
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(27.87495!, 157.2917!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(132.3542!, 23.0!)
        Me.XrLabel1.Text = "Rango Indicadores:"
        '
        'XrLabel3
        '
        Me.XrLabel3.Dpi = 100.0!
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(401.3124!, 75.0!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(165.6251!, 23.0!)
        Me.XrLabel3.Text = "Rango Inicial del Informe: "
        '
        'XrLabel4
        '
        Me.XrLabel4.Dpi = 100.0!
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(401.3124!, 109.4583!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(165.6251!, 23.0!)
        Me.XrLabel4.Text = "Rango Final del Informe: "
        '
        'XrLabel2
        '
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(38.35414!, 75.0!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(121.875!, 23.0!)
        Me.XrLabel2.Text = "Operador Caldera: "
        '
        'XrLabel13
        '
        Me.XrLabel13.Dpi = 100.0!
        Me.XrLabel13.LocationFloat = New DevExpress.Utils.PointFloat(77.20824!, 10.00001!)
        Me.XrLabel13.Name = "XrLabel13"
        Me.XrLabel13.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel13.SizeF = New System.Drawing.SizeF(612.3749!, 39.0!)
        Me.XrLabel13.StyleName = "Title"
        Me.XrLabel13.StylePriority.UseTextAlignment = False
        Me.XrLabel13.Text = "Reporte Indicadores Caldera"
        Me.XrLabel13.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 100.0!
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(4.083284!, 142.9167!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(729.9167!, 2.0!)
        '
        'Title
        '
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.BorderColor = System.Drawing.Color.Black
        Me.Title.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.Title.BorderWidth = 1.0!
        Me.Title.Font = New System.Drawing.Font("Times New Roman", 24.0!)
        Me.Title.ForeColor = System.Drawing.Color.Black
        Me.Title.Name = "Title"
        '
        'FieldCaption
        '
        Me.FieldCaption.BackColor = System.Drawing.Color.Transparent
        Me.FieldCaption.BorderColor = System.Drawing.Color.Black
        Me.FieldCaption.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.FieldCaption.BorderWidth = 1.0!
        Me.FieldCaption.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.FieldCaption.ForeColor = System.Drawing.Color.Black
        Me.FieldCaption.Name = "FieldCaption"
        '
        'PageInfo
        '
        Me.PageInfo.BackColor = System.Drawing.Color.Transparent
        Me.PageInfo.BorderColor = System.Drawing.Color.Black
        Me.PageInfo.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.PageInfo.BorderWidth = 1.0!
        Me.PageInfo.Font = New System.Drawing.Font("Times New Roman", 8.0!)
        Me.PageInfo.ForeColor = System.Drawing.Color.Black
        Me.PageInfo.Name = "PageInfo"
        '
        'DataField
        '
        Me.DataField.BackColor = System.Drawing.Color.Transparent
        Me.DataField.BorderColor = System.Drawing.Color.Black
        Me.DataField.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.DataField.BorderWidth = 1.0!
        Me.DataField.Font = New System.Drawing.Font("Times New Roman", 8.0!)
        Me.DataField.ForeColor = System.Drawing.Color.Black
        Me.DataField.Name = "DataField"
        Me.DataField.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        '
        'TopMarginBand1
        '
        Me.TopMarginBand1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox1})
        Me.TopMarginBand1.Dpi = 100.0!
        Me.TopMarginBand1.HeightF = 69.70833!
        Me.TopMarginBand1.Name = "TopMarginBand1"
        '
        'BottomMarginBand1
        '
        Me.BottomMarginBand1.Dpi = 100.0!
        Me.BottomMarginBand1.HeightF = 100.0!
        Me.BottomMarginBand1.Name = "BottomMarginBand1"
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel5, Me.XrPivotGrid1})
        Me.ReportFooter.Dpi = 100.0!
        Me.ReportFooter.HeightF = 100.0!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'XrLabel5
        '
        Me.XrLabel5.Dpi = 100.0!
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(27.87495!, 0!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(132.3542!, 23.0!)
        Me.XrLabel5.Text = "Tabla Indicadores:"
        '
        'XrPivotGrid1
        '
        Me.XrPivotGrid1.Appearance.Cell.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.XrPivotGrid1.DataMember = "EquivalenciaCaldera"
        Me.XrPivotGrid1.DataSource = Me.SqlDataSource2
        Me.XrPivotGrid1.Dpi = 100.0!
        Me.XrPivotGrid1.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.XrPivotGridField1, Me.XrPivotGridField2, Me.XrPivotGridField3, Me.XrPivotGridField4, Me.XrPivotGridField5, Me.XrPivotGridField6})
        Me.XrPivotGrid1.LocationFloat = New DevExpress.Utils.PointFloat(27.87495!, 39.99999!)
        Me.XrPivotGrid1.Name = "XrPivotGrid1"
        Me.XrPivotGrid1.OptionsChartDataSource.ProvideDataByColumns = False
        Me.XrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3
        Me.XrPivotGrid1.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid1.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid1.OptionsPrint.PrintHorzLines = DevExpress.Utils.DefaultBoolean.[True]
        Me.XrPivotGrid1.OptionsPrint.PrintRowHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid1.OptionsPrint.PrintUnusedFilterFields = False
        Me.XrPivotGrid1.OptionsView.ShowColumnGrandTotalHeader = False
        Me.XrPivotGrid1.OptionsView.ShowColumnGrandTotals = False
        Me.XrPivotGrid1.OptionsView.ShowColumnHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowColumnTotals = False
        Me.XrPivotGrid1.OptionsView.ShowRowGrandTotalHeader = False
        Me.XrPivotGrid1.OptionsView.ShowRowGrandTotals = False
        Me.XrPivotGrid1.OptionsView.ShowRowHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowRowTotals = False
        Me.XrPivotGrid1.SizeF = New System.Drawing.SizeF(429.7084!, 50.0!)
        '
        'XrPivotGridField1
        '
        Me.XrPivotGridField1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.XrPivotGridField1.AreaIndex = 0
        Me.XrPivotGridField1.FieldName = "Equivalencia"
        Me.XrPivotGridField1.Name = "XrPivotGridField1"
        Me.XrPivotGridField1.Width = 96
        '
        'XrPivotGridField2
        '
        Me.XrPivotGridField2.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.XrPivotGridField2.AreaIndex = 1
        Me.XrPivotGridField2.FieldName = "UM"
        Me.XrPivotGridField2.Name = "XrPivotGridField2"
        '
        'XrPivotGridField3
        '
        Me.XrPivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.XrPivotGridField3.AreaIndex = 0
        Me.XrPivotGridField3.CellFormat.FormatString = "dd/MM/yyyy"
        Me.XrPivotGridField3.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.XrPivotGridField3.FieldName = "DateAndTime"
        Me.XrPivotGridField3.GroupInterval = DevExpress.XtraPivotGrid.PivotGroupInterval.[Date]
        Me.XrPivotGridField3.Name = "XrPivotGridField3"
        Me.XrPivotGridField3.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        Me.XrPivotGridField3.UnboundFieldName = "XrPivotGridField3"
        Me.XrPivotGridField3.ValueFormat.FormatString = "dd/MM/yyyy"
        Me.XrPivotGridField3.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.XrPivotGridField3.Width = 85
        '
        'XrPivotGridField4
        '
        Me.XrPivotGridField4.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.XrPivotGridField4.AreaIndex = 0
        Me.XrPivotGridField4.CellFormat.FormatString = "####.##"
        Me.XrPivotGridField4.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.XrPivotGridField4.FieldName = "Val"
        Me.XrPivotGridField4.Name = "XrPivotGridField4"
        Me.XrPivotGridField4.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        '
        'XrPivotGridField5
        '
        Me.XrPivotGridField5.AreaIndex = 0
        Me.XrPivotGridField5.Caption = "Límite Mín"
        Me.XrPivotGridField5.CellFormat.FormatString = "###.##"
        Me.XrPivotGridField5.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.XrPivotGridField5.FieldName = "LimiteMin"
        Me.XrPivotGridField5.Name = "XrPivotGridField5"
        Me.XrPivotGridField5.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        Me.XrPivotGridField5.UnboundFieldName = "Limite Mín"
        '
        'XrPivotGridField6
        '
        Me.XrPivotGridField6.AreaIndex = 1
        Me.XrPivotGridField6.Caption = "Límite Máx"
        Me.XrPivotGridField6.CellFormat.FormatString = "###.##"
        Me.XrPivotGridField6.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.XrPivotGridField6.FieldName = "Limite"
        Me.XrPivotGridField6.Name = "XrPivotGridField6"
        Me.XrPivotGridField6.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        Me.XrPivotGridField6.UnboundFieldName = "Limite Máx"
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Dpi = 100.0!
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 10.00001!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(219.7917!, 59.70832!)
        Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'Reporte_IndicadoresCaldera
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.PageFooterBand1, Me.ReportHeaderBand1, Me.TopMarginBand1, Me.BottomMarginBand1, Me.ReportFooter})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.SqlDataSource1, Me.SqlDataSource2})
        Me.DataMember = "EquivalenciaCaldera"
        Me.DataSource = Me.SqlDataSource2
        Me.Margins = New System.Drawing.Printing.Margins(48, 52, 70, 100)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.fecha1, Me.fecha2})
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.Title, Me.FieldCaption, Me.PageInfo, Me.DataField})
        Me.Version = "16.2"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents SqlDataSource1 As DevExpress.DataAccess.Sql.SqlDataSource
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents PageFooterBand1 As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents ReportHeaderBand1 As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrLabel13 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Title As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents FieldCaption As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents PageInfo As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents DataField As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents TopMarginBand1 As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMarginBand1 As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPivotGrid2 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents fieldEquivalencia1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldUM1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldDateAndTime1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldVal1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldLimiteMin1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldLimite1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPivotGrid1 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents XrPivotGridField1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField2 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField3 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField4 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField5 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField6 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents fecha2 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents fecha1 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents SqlDataSource2 As DevExpress.DataAccess.Sql.SqlDataSource
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
End Class
