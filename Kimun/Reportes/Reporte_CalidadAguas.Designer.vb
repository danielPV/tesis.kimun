﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Public Class Reporte_CalidadAguas
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Reporte_CalidadAguas))
        Dim SelectQuery1 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column1 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression1 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table1 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column2 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression2 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table2 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column3 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression3 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table3 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column4 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression4 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table4 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column5 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression5 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column6 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression6 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column7 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression7 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Join1 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo1 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join2 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo2 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join3 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo3 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim SelectQuery2 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column8 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression8 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table5 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column9 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression9 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table6 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column10 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression10 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table7 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column11 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression11 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column12 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression12 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column13 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression13 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column14 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression14 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table8 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim QueryParameter1 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim QueryParameter2 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim Join4 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo4 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join5 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo5 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join6 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo6 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim SelectQuery3 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column15 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression15 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table9 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column16 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression16 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table10 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column17 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression17 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table11 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column18 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression18 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column19 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression19 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column20 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression20 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column21 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression21 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table12 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim QueryParameter3 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim QueryParameter4 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim Join7 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo7 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join8 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo8 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join9 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo9 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.SqlDataSource1 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        Me.PageFooterBand1 = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.ReportHeaderBand1 = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrPivotGrid2 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.SqlDataSource2 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        Me.XrPivotGridField1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField2 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField3 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField4 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField5 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField6 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField7 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Fecha2 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Fecha1 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.Title = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.FieldCaption = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.PageInfo = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.DataField = New DevExpress.XtraReports.UI.XRControlStyle()
        Me.ReportFooter = New DevExpress.XtraReports.UI.ReportFooterBand()
        Me.XrLabel7 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPivotGrid1 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.fieldValor1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldFechaControlCaldera1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldParametroMedicionCaldera1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldMaxMedicionCaldera1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldMinMedicionCaldera1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldUMMedicionCaldera1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldTurno1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.ETataDataSet_Caldera1 = New Kimun.eTataDataSet_Caldera()
        Me.EquivalenciaCalderaTableAdapter = New Kimun.eTataDataSet_CalderaTableAdapters.EquivalenciaCalderaTableAdapter()
        Me.SqlDataSource3 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        CType(Me.ETataDataSet_Caldera1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 12.66664!
        Me.Detail.KeepTogether = True
        Me.Detail.KeepTogetherWithDetailReports = True
        Me.Detail.MultiColumn.Layout = DevExpress.XtraPrinting.ColumnLayout.AcrossThenDown
        Me.Detail.MultiColumn.Mode = DevExpress.XtraReports.UI.MultiColumnMode.UseColumnWidth
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox1})
        Me.TopMargin.Dpi = 100.0!
        Me.TopMargin.HeightF = 70.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Dpi = 100.0!
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(30.08331!, 10.00001!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(219.7917!, 59.70832!)
        Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 100.0!
        Me.BottomMargin.HeightF = 100.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'SqlDataSource1
        '
        Me.SqlDataSource1.ConnectionName = "conexion_ControlProduccion.dbo"
        Me.SqlDataSource1.Name = "SqlDataSource1"
        ColumnExpression1.ColumnName = "FechaControlCaldera"
        Table1.Name = "CalidadCaldera"
        ColumnExpression1.Table = Table1
        Column1.Expression = ColumnExpression1
        ColumnExpression2.ColumnName = "Turno"
        Table2.Name = "Turno"
        ColumnExpression2.Table = Table2
        Column2.Expression = ColumnExpression2
        ColumnExpression3.ColumnName = "Valor"
        Table3.Name = "RegistroCaldera"
        ColumnExpression3.Table = Table3
        Column3.Expression = ColumnExpression3
        ColumnExpression4.ColumnName = "ParametroMedicionCaldera"
        Table4.Name = "ParamMedicionCaldera"
        ColumnExpression4.Table = Table4
        Column4.Expression = ColumnExpression4
        ColumnExpression5.ColumnName = "MaxMedicionCaldera"
        ColumnExpression5.Table = Table4
        Column5.Expression = ColumnExpression5
        ColumnExpression6.ColumnName = "MinMedicionCaldera"
        ColumnExpression6.Table = Table4
        Column6.Expression = ColumnExpression6
        ColumnExpression7.ColumnName = "UMMedicionCaldera"
        ColumnExpression7.Table = Table4
        Column7.Expression = ColumnExpression7
        SelectQuery1.Columns.Add(Column1)
        SelectQuery1.Columns.Add(Column2)
        SelectQuery1.Columns.Add(Column3)
        SelectQuery1.Columns.Add(Column4)
        SelectQuery1.Columns.Add(Column5)
        SelectQuery1.Columns.Add(Column6)
        SelectQuery1.Columns.Add(Column7)
        SelectQuery1.FilterString = "[ParamMedicionCaldera.ParametroMedicionCaldera] <> 'Limpieza Tubos               " &
    "           '"
        SelectQuery1.Name = "RegistroCaldera"
        RelationColumnInfo1.NestedKeyColumn = "idControlCaldera"
        RelationColumnInfo1.ParentKeyColumn = "idControlCaldera"
        Join1.KeyColumns.Add(RelationColumnInfo1)
        Join1.Nested = Table1
        Join1.Parent = Table3
        RelationColumnInfo2.NestedKeyColumn = "idMedicionCaldera"
        RelationColumnInfo2.ParentKeyColumn = "idMedicionCaldera"
        Join2.KeyColumns.Add(RelationColumnInfo2)
        Join2.Nested = Table4
        Join2.Parent = Table3
        RelationColumnInfo3.NestedKeyColumn = "CodTurno"
        RelationColumnInfo3.ParentKeyColumn = "TurnoControlCaldera"
        Join3.KeyColumns.Add(RelationColumnInfo3)
        Join3.Nested = Table2
        Join3.Parent = Table1
        SelectQuery1.Relations.Add(Join1)
        SelectQuery1.Relations.Add(Join2)
        SelectQuery1.Relations.Add(Join3)
        SelectQuery1.Tables.Add(Table3)
        SelectQuery1.Tables.Add(Table1)
        SelectQuery1.Tables.Add(Table4)
        SelectQuery1.Tables.Add(Table2)
        Me.SqlDataSource1.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {SelectQuery1})
        Me.SqlDataSource1.ResultSchemaSerializable = resources.GetString("SqlDataSource1.ResultSchemaSerializable")
        '
        'PageFooterBand1
        '
        Me.PageFooterBand1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo1, Me.XrPageInfo2})
        Me.PageFooterBand1.Dpi = 100.0!
        Me.PageFooterBand1.HeightF = 33.00001!
        Me.PageFooterBand1.Name = "PageFooterBand1"
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 100.0!
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(9.999998!, 9.999974!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(313.0!, 23.0!)
        Me.XrPageInfo1.StyleName = "PageInfo"
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.Dpi = 100.0!
        Me.XrPageInfo2.Format = "Página {0} de {1}"
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(472.5!, 10.00001!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(313.0!, 23.0!)
        Me.XrPageInfo2.StyleName = "PageInfo"
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'ReportHeaderBand1
        '
        Me.ReportHeaderBand1.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPivotGrid2, Me.XrLabel1, Me.XrLabel6, Me.XrLabel5, Me.XrLabel4, Me.XrLabel3, Me.XrLine2, Me.XrLine1, Me.XrLabel2, Me.XrLabel15})
        Me.ReportHeaderBand1.Dpi = 100.0!
        Me.ReportHeaderBand1.HeightF = 232.5834!
        Me.ReportHeaderBand1.Name = "ReportHeaderBand1"
        '
        'XrPivotGrid2
        '
        Me.XrPivotGrid2.Appearance.Cell.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.XrPivotGrid2.Appearance.FieldHeader.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.XrPivotGrid2.DataMember = "RegistroCaldera"
        Me.XrPivotGrid2.DataSource = Me.SqlDataSource2
        Me.XrPivotGrid2.Dpi = 100.0!
        Me.XrPivotGrid2.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.XrPivotGridField1, Me.XrPivotGridField2, Me.XrPivotGridField3, Me.XrPivotGridField4, Me.XrPivotGridField5, Me.XrPivotGridField6, Me.XrPivotGridField7})
        Me.XrPivotGrid2.LocationFloat = New DevExpress.Utils.PointFloat(213.0!, 176.3334!)
        Me.XrPivotGrid2.Name = "XrPivotGrid2"
        Me.XrPivotGrid2.OptionsChartDataSource.ProvideDataByColumns = False
        Me.XrPivotGrid2.OptionsPrint.FilterSeparatorBarPadding = 3
        Me.XrPivotGrid2.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid2.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid2.OptionsPrint.PrintHorzLines = DevExpress.Utils.DefaultBoolean.[True]
        Me.XrPivotGrid2.OptionsPrint.PrintRowHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid2.OptionsPrint.PrintUnusedFilterFields = False
        Me.XrPivotGrid2.OptionsView.ShowColumnGrandTotalHeader = False
        Me.XrPivotGrid2.OptionsView.ShowColumnGrandTotals = False
        Me.XrPivotGrid2.OptionsView.ShowColumnHeaders = False
        Me.XrPivotGrid2.OptionsView.ShowColumnTotals = False
        Me.XrPivotGrid2.OptionsView.ShowRowGrandTotalHeader = False
        Me.XrPivotGrid2.OptionsView.ShowRowGrandTotals = False
        Me.XrPivotGrid2.OptionsView.ShowRowHeaders = False
        Me.XrPivotGrid2.OptionsView.ShowRowTotals = False
        Me.XrPivotGrid2.SizeF = New System.Drawing.SizeF(429.7084!, 50.0!)
        '
        'SqlDataSource2
        '
        Me.SqlDataSource2.ConnectionName = "conexion_ControlProduccion.dbo"
        Me.SqlDataSource2.Name = "SqlDataSource2"
        ColumnExpression8.ColumnName = "FechaControlCaldera"
        Table5.Name = "CalidadCaldera"
        ColumnExpression8.Table = Table5
        Column8.Expression = ColumnExpression8
        ColumnExpression9.ColumnName = "ParametroMedicionCaldera"
        Table6.Name = "ParamMedicionCaldera"
        ColumnExpression9.Table = Table6
        Column9.Expression = ColumnExpression9
        ColumnExpression10.ColumnName = "Valor"
        Table7.Name = "RegistroCaldera"
        ColumnExpression10.Table = Table7
        Column10.Expression = ColumnExpression10
        ColumnExpression11.ColumnName = "UMMedicionCaldera"
        ColumnExpression11.Table = Table6
        Column11.Expression = ColumnExpression11
        ColumnExpression12.ColumnName = "MinMedicionCaldera"
        ColumnExpression12.Table = Table6
        Column12.Expression = ColumnExpression12
        ColumnExpression13.ColumnName = "MaxMedicionCaldera"
        ColumnExpression13.Table = Table6
        Column13.Expression = ColumnExpression13
        ColumnExpression14.ColumnName = "Turno"
        Table8.Name = "Turno"
        ColumnExpression14.Table = Table8
        Column14.Expression = ColumnExpression14
        SelectQuery2.Columns.Add(Column8)
        SelectQuery2.Columns.Add(Column9)
        SelectQuery2.Columns.Add(Column10)
        SelectQuery2.Columns.Add(Column11)
        SelectQuery2.Columns.Add(Column12)
        SelectQuery2.Columns.Add(Column13)
        SelectQuery2.Columns.Add(Column14)
        SelectQuery2.FilterString = "[ParamMedicionCaldera.ParametroMedicionCaldera] <> 'Limpieza Tubos               " &
    "           ' And [CalidadCaldera.FechaControlCaldera] Between(?Fecha1, ?Fecha2)"
        SelectQuery2.MetaSerializable = "20|20|100|156"
        SelectQuery2.Name = "RegistroCaldera"
        QueryParameter1.Name = "Fecha1"
        QueryParameter1.Type = GetType(DevExpress.DataAccess.Expression)
        QueryParameter1.Value = New DevExpress.DataAccess.Expression("[Parameters.Fecha1]", GetType(Date))
        QueryParameter2.Name = "Fecha2"
        QueryParameter2.Type = GetType(DevExpress.DataAccess.Expression)
        QueryParameter2.Value = New DevExpress.DataAccess.Expression("[Parameters.Fecha2]", GetType(Date))
        SelectQuery2.Parameters.Add(QueryParameter1)
        SelectQuery2.Parameters.Add(QueryParameter2)
        RelationColumnInfo4.NestedKeyColumn = "idMedicionCaldera"
        RelationColumnInfo4.ParentKeyColumn = "idMedicionCaldera"
        Join4.KeyColumns.Add(RelationColumnInfo4)
        Join4.Nested = Table6
        Join4.Parent = Table7
        RelationColumnInfo5.NestedKeyColumn = "idControlCaldera"
        RelationColumnInfo5.ParentKeyColumn = "idControlCaldera"
        Join5.KeyColumns.Add(RelationColumnInfo5)
        Join5.Nested = Table5
        Join5.Parent = Table7
        RelationColumnInfo6.NestedKeyColumn = "CodTurno"
        RelationColumnInfo6.ParentKeyColumn = "TurnoControlCaldera"
        Join6.KeyColumns.Add(RelationColumnInfo6)
        Join6.Nested = Table8
        Join6.Parent = Table5
        SelectQuery2.Relations.Add(Join4)
        SelectQuery2.Relations.Add(Join5)
        SelectQuery2.Relations.Add(Join6)
        SelectQuery2.Tables.Add(Table7)
        SelectQuery2.Tables.Add(Table6)
        SelectQuery2.Tables.Add(Table5)
        SelectQuery2.Tables.Add(Table8)
        Me.SqlDataSource2.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {SelectQuery2})
        Me.SqlDataSource2.ResultSchemaSerializable = resources.GetString("SqlDataSource2.ResultSchemaSerializable")
        '
        'XrPivotGridField1
        '
        Me.XrPivotGridField1.AreaIndex = 1
        Me.XrPivotGridField1.FieldName = "Valor"
        Me.XrPivotGridField1.Name = "XrPivotGridField1"
        '
        'XrPivotGridField2
        '
        Me.XrPivotGridField2.AreaIndex = 0
        Me.XrPivotGridField2.CellFormat.FormatString = "dd/MM/yyyy"
        Me.XrPivotGridField2.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.XrPivotGridField2.FieldName = "FechaControlCaldera"
        Me.XrPivotGridField2.Name = "XrPivotGridField2"
        Me.XrPivotGridField2.ValueFormat.FormatString = "d"
        Me.XrPivotGridField2.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.XrPivotGridField2.Width = 103
        '
        'XrPivotGridField3
        '
        Me.XrPivotGridField3.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.XrPivotGridField3.AreaIndex = 0
        Me.XrPivotGridField3.FieldName = "ParametroMedicionCaldera"
        Me.XrPivotGridField3.Name = "XrPivotGridField3"
        Me.XrPivotGridField3.Width = 124
        '
        'XrPivotGridField4
        '
        Me.XrPivotGridField4.Appearance.Cell.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.XrPivotGridField4.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.XrPivotGridField4.AreaIndex = 1
        Me.XrPivotGridField4.Caption = "Máx"
        Me.XrPivotGridField4.FieldName = "MaxMedicionCaldera"
        Me.XrPivotGridField4.Name = "XrPivotGridField4"
        Me.XrPivotGridField4.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        Me.XrPivotGridField4.Width = 128
        '
        'XrPivotGridField5
        '
        Me.XrPivotGridField5.Appearance.Cell.TextHorizontalAlignment = DevExpress.Utils.HorzAlignment.Center
        Me.XrPivotGridField5.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.XrPivotGridField5.AreaIndex = 0
        Me.XrPivotGridField5.Caption = "Mín"
        Me.XrPivotGridField5.FieldName = "MinMedicionCaldera"
        Me.XrPivotGridField5.Name = "XrPivotGridField5"
        Me.XrPivotGridField5.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        Me.XrPivotGridField5.Width = 135
        '
        'XrPivotGridField6
        '
        Me.XrPivotGridField6.AreaIndex = 3
        Me.XrPivotGridField6.FieldName = "UMMedicionCaldera"
        Me.XrPivotGridField6.Name = "XrPivotGridField6"
        '
        'XrPivotGridField7
        '
        Me.XrPivotGridField7.AreaIndex = 2
        Me.XrPivotGridField7.FieldName = "Turno"
        Me.XrPivotGridField7.Name = "XrPivotGridField7"
        '
        'XrLabel1
        '
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(60.16661!, 153.3334!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(132.3542!, 23.0!)
        Me.XrLabel1.Text = "Rango Mediciones:"
        '
        'XrLabel6
        '
        Me.XrLabel6.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.Fecha2, "Text", "")})
        Me.XrLabel6.Dpi = 100.0!
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(580.2917!, 107.3334!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(173.2499!, 23.00002!)
        Me.XrLabel6.Text = "XrLabel6"
        '
        'Fecha2
        '
        Me.Fecha2.Name = "Fecha2"
        Me.Fecha2.Type = GetType(Date)
        Me.Fecha2.Visible = False
        '
        'XrLabel5
        '
        Me.XrLabel5.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.Fecha1, "Text", "")})
        Me.XrLabel5.Dpi = 100.0!
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(580.2917!, 73.91676!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(173.2499!, 23.0!)
        Me.XrLabel5.Text = "XrLabel5"
        '
        'Fecha1
        '
        Me.Fecha1.Name = "Fecha1"
        Me.Fecha1.Type = GetType(Date)
        Me.Fecha1.Visible = False
        '
        'XrLabel4
        '
        Me.XrLabel4.Dpi = 100.0!
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(414.6666!, 107.3334!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(165.6251!, 23.0!)
        Me.XrLabel4.Text = "Rango Final del Informe: "
        '
        'XrLabel3
        '
        Me.XrLabel3.Dpi = 100.0!
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(414.6666!, 71.99999!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(165.6251!, 23.0!)
        Me.XrLabel3.Text = "Rango Inicial del Informe: "
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 100.0!
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(40.04164!, 130.3334!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(727.0417!, 23.0!)
        '
        'XrLine1
        '
        Me.XrLine1.Dpi = 100.0!
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(39.95831!, 49.00001!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(727.125!, 23.0!)
        '
        'XrLabel2
        '
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(70.64578!, 73.91676!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(121.875!, 23.0!)
        Me.XrLabel2.Text = "Operador Caldera: "
        '
        'XrLabel15
        '
        Me.XrLabel15.Dpi = 100.0!
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(95.41667!, 10.00001!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(638.0!, 39.0!)
        Me.XrLabel15.StyleName = "Title"
        Me.XrLabel15.StylePriority.UseTextAlignment = False
        Me.XrLabel15.Text = "Reporte Calidad de Aguas "
        Me.XrLabel15.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopCenter
        '
        'Title
        '
        Me.Title.BackColor = System.Drawing.Color.Transparent
        Me.Title.BorderColor = System.Drawing.Color.Black
        Me.Title.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.Title.BorderWidth = 1.0!
        Me.Title.Font = New System.Drawing.Font("Times New Roman", 24.0!)
        Me.Title.ForeColor = System.Drawing.Color.Black
        Me.Title.Name = "Title"
        '
        'FieldCaption
        '
        Me.FieldCaption.BackColor = System.Drawing.Color.Transparent
        Me.FieldCaption.BorderColor = System.Drawing.Color.Black
        Me.FieldCaption.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.FieldCaption.BorderWidth = 1.0!
        Me.FieldCaption.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold)
        Me.FieldCaption.ForeColor = System.Drawing.Color.Black
        Me.FieldCaption.Name = "FieldCaption"
        '
        'PageInfo
        '
        Me.PageInfo.BackColor = System.Drawing.Color.Transparent
        Me.PageInfo.BorderColor = System.Drawing.Color.Black
        Me.PageInfo.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.PageInfo.BorderWidth = 1.0!
        Me.PageInfo.Font = New System.Drawing.Font("Times New Roman", 8.0!)
        Me.PageInfo.ForeColor = System.Drawing.Color.Black
        Me.PageInfo.Name = "PageInfo"
        '
        'DataField
        '
        Me.DataField.BackColor = System.Drawing.Color.Transparent
        Me.DataField.BorderColor = System.Drawing.Color.Black
        Me.DataField.Borders = DevExpress.XtraPrinting.BorderSide.None
        Me.DataField.BorderWidth = 1.0!
        Me.DataField.Font = New System.Drawing.Font("Times New Roman", 8.0!)
        Me.DataField.ForeColor = System.Drawing.Color.Black
        Me.DataField.Name = "DataField"
        Me.DataField.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        '
        'ReportFooter
        '
        Me.ReportFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrLabel7, Me.XrPivotGrid1})
        Me.ReportFooter.Dpi = 100.0!
        Me.ReportFooter.HeightF = 109.375!
        Me.ReportFooter.Name = "ReportFooter"
        '
        'XrLabel7
        '
        Me.XrLabel7.Dpi = 100.0!
        Me.XrLabel7.LocationFloat = New DevExpress.Utils.PointFloat(30.08331!, 0!)
        Me.XrLabel7.Name = "XrLabel7"
        Me.XrLabel7.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel7.SizeF = New System.Drawing.SizeF(132.3542!, 23.0!)
        Me.XrLabel7.Text = "Tabla Mediciones:"
        '
        'XrPivotGrid1
        '
        Me.XrPivotGrid1.AnchorHorizontal = CType((DevExpress.XtraReports.UI.HorizontalAnchorStyles.Left Or DevExpress.XtraReports.UI.HorizontalAnchorStyles.Right), DevExpress.XtraReports.UI.HorizontalAnchorStyles)
        Me.XrPivotGrid1.Appearance.Cell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.CustomTotalCell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.FieldHeader.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.FieldValue.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.FieldValueTotal.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.GrandTotalCell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.Lines.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.TotalCell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.DataMember = "RegistroCaldera"
        Me.XrPivotGrid1.DataSource = Me.SqlDataSource2
        Me.XrPivotGrid1.Dpi = 100.0!
        Me.XrPivotGrid1.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.fieldValor1, Me.fieldFechaControlCaldera1, Me.fieldParametroMedicionCaldera1, Me.fieldMaxMedicionCaldera1, Me.fieldMinMedicionCaldera1, Me.fieldUMMedicionCaldera1, Me.fieldTurno1})
        Me.XrPivotGrid1.LocationFloat = New DevExpress.Utils.PointFloat(9.87498!, 38.62495!)
        Me.XrPivotGrid1.Name = "XrPivotGrid1"
        Me.XrPivotGrid1.OptionsChartDataSource.ProvideDataByColumns = False
        Me.XrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3
        Me.XrPivotGrid1.OptionsPrint.PrintColumnHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid1.OptionsPrint.PrintDataHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid1.OptionsPrint.PrintHorzLines = DevExpress.Utils.DefaultBoolean.[True]
        Me.XrPivotGrid1.OptionsPrint.PrintRowHeaders = DevExpress.Utils.DefaultBoolean.[False]
        Me.XrPivotGrid1.OptionsPrint.PrintUnusedFilterFields = False
        Me.XrPivotGrid1.OptionsView.ShowColumnGrandTotalHeader = False
        Me.XrPivotGrid1.OptionsView.ShowColumnGrandTotals = False
        Me.XrPivotGrid1.OptionsView.ShowColumnHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowColumnTotals = False
        Me.XrPivotGrid1.OptionsView.ShowRowGrandTotalHeader = False
        Me.XrPivotGrid1.OptionsView.ShowRowGrandTotals = False
        Me.XrPivotGrid1.OptionsView.ShowRowHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowRowTotals = False
        Me.XrPivotGrid1.SizeF = New System.Drawing.SizeF(805.1249!, 50.0!)
        '
        'fieldValor1
        '
        Me.fieldValor1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldValor1.AreaIndex = 0
        Me.fieldValor1.CellFormat.FormatString = "###.##"
        Me.fieldValor1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldValor1.FieldName = "Valor"
        Me.fieldValor1.Name = "fieldValor1"
        Me.fieldValor1.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        Me.fieldValor1.ValueFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        '
        'fieldFechaControlCaldera1
        '
        Me.fieldFechaControlCaldera1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldFechaControlCaldera1.AreaIndex = 0
        Me.fieldFechaControlCaldera1.CellFormat.FormatString = "dd/MM/yyyy"
        Me.fieldFechaControlCaldera1.CellFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.fieldFechaControlCaldera1.FieldName = "FechaControlCaldera"
        Me.fieldFechaControlCaldera1.Name = "fieldFechaControlCaldera1"
        Me.fieldFechaControlCaldera1.ValueFormat.FormatString = "d"
        Me.fieldFechaControlCaldera1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.fieldFechaControlCaldera1.Width = 81
        '
        'fieldParametroMedicionCaldera1
        '
        Me.fieldParametroMedicionCaldera1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldParametroMedicionCaldera1.AreaIndex = 0
        Me.fieldParametroMedicionCaldera1.FieldName = "ParametroMedicionCaldera"
        Me.fieldParametroMedicionCaldera1.Name = "fieldParametroMedicionCaldera1"
        Me.fieldParametroMedicionCaldera1.Width = 72
        '
        'fieldMaxMedicionCaldera1
        '
        Me.fieldMaxMedicionCaldera1.AreaIndex = 2
        Me.fieldMaxMedicionCaldera1.FieldName = "MaxMedicionCaldera"
        Me.fieldMaxMedicionCaldera1.Name = "fieldMaxMedicionCaldera1"
        Me.fieldMaxMedicionCaldera1.Width = 69
        '
        'fieldMinMedicionCaldera1
        '
        Me.fieldMinMedicionCaldera1.AreaIndex = 1
        Me.fieldMinMedicionCaldera1.FieldName = "MinMedicionCaldera"
        Me.fieldMinMedicionCaldera1.Name = "fieldMinMedicionCaldera1"
        Me.fieldMinMedicionCaldera1.Width = 65
        '
        'fieldUMMedicionCaldera1
        '
        Me.fieldUMMedicionCaldera1.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.fieldUMMedicionCaldera1.AreaIndex = 1
        Me.fieldUMMedicionCaldera1.FieldName = "UMMedicionCaldera"
        Me.fieldUMMedicionCaldera1.Name = "fieldUMMedicionCaldera1"
        '
        'fieldTurno1
        '
        Me.fieldTurno1.AreaIndex = 0
        Me.fieldTurno1.FieldName = "Turno"
        Me.fieldTurno1.Name = "fieldTurno1"
        Me.fieldTurno1.Width = 63
        '
        'ETataDataSet_Caldera1
        '
        Me.ETataDataSet_Caldera1.DataSetName = "eTataDataSet_Caldera"
        Me.ETataDataSet_Caldera1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'EquivalenciaCalderaTableAdapter
        '
        Me.EquivalenciaCalderaTableAdapter.ClearBeforeFill = True
        '
        'SqlDataSource3
        '
        Me.SqlDataSource3.ConnectionName = "conexion_ControlProduccion.dbo"
        Me.SqlDataSource3.Name = "SqlDataSource3"
        ColumnExpression15.ColumnName = "Valor"
        Table9.Name = "RegistroCaldera"
        ColumnExpression15.Table = Table9
        Column15.Expression = ColumnExpression15
        ColumnExpression16.ColumnName = "FechaControlCaldera"
        Table10.Name = "CalidadCaldera"
        ColumnExpression16.Table = Table10
        Column16.Expression = ColumnExpression16
        ColumnExpression17.ColumnName = "ParametroMedicionCaldera"
        Table11.Name = "ParamMedicionCaldera"
        ColumnExpression17.Table = Table11
        Column17.Expression = ColumnExpression17
        ColumnExpression18.ColumnName = "MaxMedicionCaldera"
        ColumnExpression18.Table = Table11
        Column18.Expression = ColumnExpression18
        ColumnExpression19.ColumnName = "MinMedicionCaldera"
        ColumnExpression19.Table = Table11
        Column19.Expression = ColumnExpression19
        ColumnExpression20.ColumnName = "UMMedicionCaldera"
        ColumnExpression20.Table = Table11
        Column20.Expression = ColumnExpression20
        ColumnExpression21.ColumnName = "Turno"
        Table12.Name = "Turno"
        ColumnExpression21.Table = Table12
        Column21.Expression = ColumnExpression21
        SelectQuery3.Columns.Add(Column15)
        SelectQuery3.Columns.Add(Column16)
        SelectQuery3.Columns.Add(Column17)
        SelectQuery3.Columns.Add(Column18)
        SelectQuery3.Columns.Add(Column19)
        SelectQuery3.Columns.Add(Column20)
        SelectQuery3.Columns.Add(Column21)
        SelectQuery3.FilterString = "[ParamMedicionCaldera.ParametroMedicionCaldera] = 'Limpieza Tubos                " &
    "          ' And [CalidadCaldera.FechaControlCaldera] Between(?Fecha1, ?Fecha2)"
        SelectQuery3.Name = "RegistroCaldera"
        QueryParameter3.Name = "Fecha1"
        QueryParameter3.Type = GetType(DevExpress.DataAccess.Expression)
        QueryParameter3.Value = New DevExpress.DataAccess.Expression("[Parameters.Fecha1]", GetType(Date))
        QueryParameter4.Name = "Fecha2"
        QueryParameter4.Type = GetType(DevExpress.DataAccess.Expression)
        QueryParameter4.Value = New DevExpress.DataAccess.Expression("[Parameters.Fecha2]", GetType(Date))
        SelectQuery3.Parameters.Add(QueryParameter3)
        SelectQuery3.Parameters.Add(QueryParameter4)
        RelationColumnInfo7.NestedKeyColumn = "idControlCaldera"
        RelationColumnInfo7.ParentKeyColumn = "idControlCaldera"
        Join7.KeyColumns.Add(RelationColumnInfo7)
        Join7.Nested = Table10
        Join7.Parent = Table9
        RelationColumnInfo8.NestedKeyColumn = "idMedicionCaldera"
        RelationColumnInfo8.ParentKeyColumn = "idMedicionCaldera"
        Join8.KeyColumns.Add(RelationColumnInfo8)
        Join8.Nested = Table11
        Join8.Parent = Table9
        RelationColumnInfo9.NestedKeyColumn = "CodTurno"
        RelationColumnInfo9.ParentKeyColumn = "TurnoControlCaldera"
        Join9.KeyColumns.Add(RelationColumnInfo9)
        Join9.Nested = Table12
        Join9.Parent = Table10
        SelectQuery3.Relations.Add(Join7)
        SelectQuery3.Relations.Add(Join8)
        SelectQuery3.Relations.Add(Join9)
        SelectQuery3.Tables.Add(Table9)
        SelectQuery3.Tables.Add(Table10)
        SelectQuery3.Tables.Add(Table11)
        SelectQuery3.Tables.Add(Table12)
        Me.SqlDataSource3.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {SelectQuery3})
        Me.SqlDataSource3.ResultSchemaSerializable = resources.GetString("SqlDataSource3.ResultSchemaSerializable")
        '
        'Reporte_CalidadAguas
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.PageFooterBand1, Me.ReportHeaderBand1, Me.ReportFooter})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.SqlDataSource1, Me.SqlDataSource2, Me.SqlDataSource3})
        Me.DataMember = "RegistroCaldera"
        Me.DataSource = Me.SqlDataSource2
        Me.FilterString = "[FechaControlCaldera] Between(?Fecha1, ?Fecha2)"
        Me.Margins = New System.Drawing.Printing.Margins(25, 0, 70, 100)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.Fecha1, Me.Fecha2})
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.StyleSheet.AddRange(New DevExpress.XtraReports.UI.XRControlStyle() {Me.Title, Me.FieldCaption, Me.PageInfo, Me.DataField})
        Me.Version = "16.2"
        CType(Me.ETataDataSet_Caldera1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents SqlDataSource1 As DevExpress.DataAccess.Sql.SqlDataSource
    Friend WithEvents PageFooterBand1 As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents ReportHeaderBand1 As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents Title As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents FieldCaption As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents PageInfo As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents DataField As DevExpress.XtraReports.UI.XRControlStyle
    Friend WithEvents ReportFooter As DevExpress.XtraReports.UI.ReportFooterBand
    Friend WithEvents XrPivotGrid1 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents fieldFechaControlCaldera1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldTurno1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldValor1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldParametroMedicionCaldera1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldMaxMedicionCaldera1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldMinMedicionCaldera1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldUMMedicionCaldera1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents SqlDataSource2 As DevExpress.DataAccess.Sql.SqlDataSource
    Friend WithEvents ETataDataSet_Caldera1 As eTataDataSet_Caldera
    Friend WithEvents EquivalenciaCalderaTableAdapter As eTataDataSet_CalderaTableAdapters.EquivalenciaCalderaTableAdapter
    Friend WithEvents Fecha2 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents Fecha1 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents SqlDataSource3 As DevExpress.DataAccess.Sql.SqlDataSource
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel7 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPivotGrid2 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents XrPivotGridField1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField2 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField3 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField4 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField5 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField6 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField7 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
End Class
