﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Public Class Reporte_Lijadora
    Inherits DevExpress.XtraReports.UI.XtraReport

    'XtraReport overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Required by the Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Designer
    'It can be modified using the Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim SelectQuery1 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
        Dim Column1 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression1 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table1 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column2 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression2 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column3 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression3 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table2 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column4 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression4 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Column5 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression5 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table3 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim Column6 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
        Dim ColumnExpression6 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
        Dim Table4 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
        Dim QueryParameter1 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim QueryParameter2 As DevExpress.DataAccess.Sql.QueryParameter = New DevExpress.DataAccess.Sql.QueryParameter()
        Dim Join1 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo1 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join2 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo2 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim Join3 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
        Dim RelationColumnInfo3 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim MasterDetailInfo1 As DevExpress.DataAccess.Sql.MasterDetailInfo = New DevExpress.DataAccess.Sql.MasterDetailInfo()
        Dim RelationColumnInfo4 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim MasterDetailInfo2 As DevExpress.DataAccess.Sql.MasterDetailInfo = New DevExpress.DataAccess.Sql.MasterDetailInfo()
        Dim RelationColumnInfo5 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim MasterDetailInfo3 As DevExpress.DataAccess.Sql.MasterDetailInfo = New DevExpress.DataAccess.Sql.MasterDetailInfo()
        Dim RelationColumnInfo6 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Reporte_Lijadora))
        Me.Detail = New DevExpress.XtraReports.UI.DetailBand()
        Me.TopMargin = New DevExpress.XtraReports.UI.TopMarginBand()
        Me.BottomMargin = New DevExpress.XtraReports.UI.BottomMarginBand()
        Me.SqlDataSource1 = New DevExpress.DataAccess.Sql.SqlDataSource()
        Me.fecha1 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.fecha2 = New DevExpress.XtraReports.Parameters.Parameter()
        Me.XrPictureBox1 = New DevExpress.XtraReports.UI.XRPictureBox()
        Me.ReportHeader = New DevExpress.XtraReports.UI.ReportHeaderBand()
        Me.XrLabel15 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLine1 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLine2 = New DevExpress.XtraReports.UI.XRLine()
        Me.XrLabel4 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel3 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel1 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel2 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrLabel5 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPivotGrid1 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.fieldIdLinea1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldLinea1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldMeta1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrLabel6 = New DevExpress.XtraReports.UI.XRLabel()
        Me.XrPivotGrid2 = New DevExpress.XtraReports.UI.XRPivotGrid()
        Me.XrPivotGridField9 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.XrPivotGridField10 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldFechaProduccion1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldProduccion1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldTurno1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.fieldTM1 = New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField()
        Me.PageFooter = New DevExpress.XtraReports.UI.PageFooterBand()
        Me.XrPageInfo2 = New DevExpress.XtraReports.UI.XRPageInfo()
        Me.XrPageInfo1 = New DevExpress.XtraReports.UI.XRPageInfo()
        CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
        '
        'Detail
        '
        Me.Detail.Dpi = 100.0!
        Me.Detail.HeightF = 141.6667!
        Me.Detail.Name = "Detail"
        Me.Detail.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.Detail.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'TopMargin
        '
        Me.TopMargin.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPictureBox1})
        Me.TopMargin.Dpi = 100.0!
        Me.TopMargin.HeightF = 100.0!
        Me.TopMargin.Name = "TopMargin"
        Me.TopMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.TopMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'BottomMargin
        '
        Me.BottomMargin.Dpi = 100.0!
        Me.BottomMargin.HeightF = 100.0!
        Me.BottomMargin.Name = "BottomMargin"
        Me.BottomMargin.Padding = New DevExpress.XtraPrinting.PaddingInfo(0, 0, 0, 0, 100.0!)
        Me.BottomMargin.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopLeft
        '
        'SqlDataSource1
        '
        Me.SqlDataSource1.ConnectionName = "conexion_ControlProduccion.dbo"
        Me.SqlDataSource1.Name = "SqlDataSource1"
        ColumnExpression1.ColumnName = "Linea"
        Table1.MetaSerializable = "30|30|125|115"
        Table1.Name = "Linea"
        ColumnExpression1.Table = Table1
        Column1.Expression = ColumnExpression1
        ColumnExpression2.ColumnName = "Meta"
        ColumnExpression2.Table = Table1
        Column2.Expression = ColumnExpression2
        ColumnExpression3.ColumnName = "FechaProduccion"
        Table2.MetaSerializable = "185|30|125|191"
        Table2.Name = "Produccion"
        ColumnExpression3.Table = Table2
        Column3.Expression = ColumnExpression3
        ColumnExpression4.ColumnName = "Produccion"
        ColumnExpression4.Table = Table2
        Column4.Expression = ColumnExpression4
        ColumnExpression5.ColumnName = "TM"
        Table3.MetaSerializable = "30|200|125|153"
        Table3.Name = "TM"
        ColumnExpression5.Table = Table3
        Column5.Expression = ColumnExpression5
        ColumnExpression6.ColumnName = "Turno"
        Table4.MetaSerializable = "340|30|125|134"
        Table4.Name = "Turno"
        ColumnExpression6.Table = Table4
        Column6.Expression = ColumnExpression6
        SelectQuery1.Columns.Add(Column1)
        SelectQuery1.Columns.Add(Column2)
        SelectQuery1.Columns.Add(Column3)
        SelectQuery1.Columns.Add(Column4)
        SelectQuery1.Columns.Add(Column5)
        SelectQuery1.Columns.Add(Column6)
        SelectQuery1.FilterString = "[Linea.Linea] = 'Lijadora' And [Produccion.FechaProduccion] Between(?fecha1, ?fec" &
    "ha2)"
        SelectQuery1.GroupFilterString = ""
        SelectQuery1.MetaSerializable = "20|20|100|88"
        SelectQuery1.Name = "Linea"
        QueryParameter1.Name = "fecha1"
        QueryParameter1.Type = GetType(DevExpress.DataAccess.Expression)
        QueryParameter1.Value = New DevExpress.DataAccess.Expression("[Parameters.fecha1]", GetType(Date))
        QueryParameter2.Name = "fecha2"
        QueryParameter2.Type = GetType(DevExpress.DataAccess.Expression)
        QueryParameter2.Value = New DevExpress.DataAccess.Expression("[Parameters.fecha2]", GetType(Date))
        SelectQuery1.Parameters.Add(QueryParameter1)
        SelectQuery1.Parameters.Add(QueryParameter2)
        RelationColumnInfo1.NestedKeyColumn = "IdLinea"
        RelationColumnInfo1.ParentKeyColumn = "IdLinea"
        Join1.KeyColumns.Add(RelationColumnInfo1)
        Join1.Nested = Table2
        Join1.Parent = Table1
        RelationColumnInfo2.NestedKeyColumn = "IdLinea"
        RelationColumnInfo2.ParentKeyColumn = "IdLinea"
        Join2.KeyColumns.Add(RelationColumnInfo2)
        Join2.Nested = Table3
        Join2.Parent = Table1
        RelationColumnInfo3.NestedKeyColumn = "CodTurno"
        RelationColumnInfo3.ParentKeyColumn = "TurnoProduccion"
        Join3.KeyColumns.Add(RelationColumnInfo3)
        Join3.Nested = Table4
        Join3.Parent = Table2
        SelectQuery1.Relations.Add(Join1)
        SelectQuery1.Relations.Add(Join2)
        SelectQuery1.Relations.Add(Join3)
        SelectQuery1.Tables.Add(Table1)
        SelectQuery1.Tables.Add(Table2)
        SelectQuery1.Tables.Add(Table3)
        SelectQuery1.Tables.Add(Table4)
        Me.SqlDataSource1.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {SelectQuery1})
        MasterDetailInfo1.DetailQueryName = "Linea"
        RelationColumnInfo4.NestedKeyColumn = "IdLinea"
        RelationColumnInfo4.ParentKeyColumn = "IdLinea"
        MasterDetailInfo1.KeyColumns.Add(RelationColumnInfo4)
        MasterDetailInfo1.MasterQueryName = "Produccion"
        MasterDetailInfo2.DetailQueryName = "Linea"
        RelationColumnInfo5.NestedKeyColumn = "IdLinea"
        RelationColumnInfo5.ParentKeyColumn = "IdLinea"
        MasterDetailInfo2.KeyColumns.Add(RelationColumnInfo5)
        MasterDetailInfo2.MasterQueryName = "TM"
        MasterDetailInfo3.DetailQueryName = "Produccion"
        RelationColumnInfo6.NestedKeyColumn = "TurnoProduccion"
        RelationColumnInfo6.ParentKeyColumn = "CodTurno"
        MasterDetailInfo3.KeyColumns.Add(RelationColumnInfo6)
        MasterDetailInfo3.MasterQueryName = "Turno"
        Me.SqlDataSource1.Relations.AddRange(New DevExpress.DataAccess.Sql.MasterDetailInfo() {MasterDetailInfo1, MasterDetailInfo2, MasterDetailInfo3})
        Me.SqlDataSource1.ResultSchemaSerializable = resources.GetString("SqlDataSource1.ResultSchemaSerializable")
        '
        'fecha1
        '
        Me.fecha1.Name = "fecha1"
        Me.fecha1.Type = GetType(Date)
        '
        'fecha2
        '
        Me.fecha2.Name = "fecha2"
        Me.fecha2.Type = GetType(Date)
        '
        'XrPictureBox1
        '
        Me.XrPictureBox1.Dpi = 100.0!
        Me.XrPictureBox1.Image = CType(resources.GetObject("XrPictureBox1.Image"), System.Drawing.Image)
        Me.XrPictureBox1.LocationFloat = New DevExpress.Utils.PointFloat(0!, 30.29169!)
        Me.XrPictureBox1.Name = "XrPictureBox1"
        Me.XrPictureBox1.SizeF = New System.Drawing.SizeF(219.7917!, 59.70832!)
        Me.XrPictureBox1.Sizing = DevExpress.XtraPrinting.ImageSizeMode.StretchImage
        '
        'ReportHeader
        '
        Me.ReportHeader.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPivotGrid2, Me.XrLabel6, Me.XrPivotGrid1, Me.XrLabel2, Me.XrLabel1, Me.XrLabel4, Me.XrLabel3, Me.XrLine2, Me.XrLine1, Me.XrLabel15, Me.XrLabel5})
        Me.ReportHeader.Dpi = 100.0!
        Me.ReportHeader.HeightF = 307.7917!
        Me.ReportHeader.Name = "ReportHeader"
        '
        'XrLabel15
        '
        Me.XrLabel15.Dpi = 100.0!
        Me.XrLabel15.Font = New System.Drawing.Font("Times New Roman", 24.0!)
        Me.XrLabel15.LocationFloat = New DevExpress.Utils.PointFloat(120.8333!, 0!)
        Me.XrLabel15.Name = "XrLabel15"
        Me.XrLabel15.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel15.SizeF = New System.Drawing.SizeF(466.125!, 39.0!)
        Me.XrLabel15.StylePriority.UseFont = False
        Me.XrLabel15.Text = "Reporte Producción Lijadora"
        '
        'XrLine1
        '
        Me.XrLine1.Dpi = 100.0!
        Me.XrLine1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 39.0!)
        Me.XrLine1.Name = "XrLine1"
        Me.XrLine1.SizeF = New System.Drawing.SizeF(638.0!, 2.0!)
        '
        'XrLine2
        '
        Me.XrLine2.Dpi = 100.0!
        Me.XrLine2.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 120.2917!)
        Me.XrLine2.Name = "XrLine2"
        Me.XrLine2.SizeF = New System.Drawing.SizeF(638.0!, 2.0!)
        '
        'XrLabel4
        '
        Me.XrLabel4.Dpi = 100.0!
        Me.XrLabel4.LocationFloat = New DevExpress.Utils.PointFloat(54.1666!, 82.8125!)
        Me.XrLabel4.Name = "XrLabel4"
        Me.XrLabel4.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel4.SizeF = New System.Drawing.SizeF(165.6251!, 23.0!)
        Me.XrLabel4.Text = "Rango Final del Informe: "
        '
        'XrLabel3
        '
        Me.XrLabel3.Dpi = 100.0!
        Me.XrLabel3.LocationFloat = New DevExpress.Utils.PointFloat(54.1666!, 49.39582!)
        Me.XrLabel3.Name = "XrLabel3"
        Me.XrLabel3.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel3.SizeF = New System.Drawing.SizeF(165.6251!, 23.0!)
        Me.XrLabel3.Text = "Rango Inicial del Informe: "
        '
        'XrLabel1
        '
        Me.XrLabel1.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.fecha1, "Text", "")})
        Me.XrLabel1.Dpi = 100.0!
        Me.XrLabel1.LocationFloat = New DevExpress.Utils.PointFloat(230.2083!, 49.39582!)
        Me.XrLabel1.Name = "XrLabel1"
        Me.XrLabel1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrLabel1.SizeF = New System.Drawing.SizeF(156.25!, 23.0!)
        Me.XrLabel1.Text = "XrLabel1"
        '
        'XrLabel2
        '
        Me.XrLabel2.DataBindings.AddRange(New DevExpress.XtraReports.UI.XRBinding() {New DevExpress.XtraReports.UI.XRBinding(Me.fecha2, "Text", "")})
        Me.XrLabel2.Dpi = 100.0!
        Me.XrLabel2.LocationFloat = New DevExpress.Utils.PointFloat(230.2083!, 82.8125!)
        Me.XrLabel2.Name = "XrLabel2"
        Me.XrLabel2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 96.0!)
        Me.XrLabel2.SizeF = New System.Drawing.SizeF(156.25!, 23.0!)
        Me.XrLabel2.Text = "XrLabel2"
        '
        'XrLabel5
        '
        Me.XrLabel5.Dpi = 100.0!
        Me.XrLabel5.LocationFloat = New DevExpress.Utils.PointFloat(54.1666!, 135.8334!)
        Me.XrLabel5.Name = "XrLabel5"
        Me.XrLabel5.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel5.SizeF = New System.Drawing.SizeF(132.3542!, 23.0!)
        Me.XrLabel5.Text = "Meta Linea:"
        '
        'XrPivotGrid1
        '
        Me.XrPivotGrid1.Appearance.Cell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.CustomTotalCell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.FieldHeader.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.FieldValue.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.FieldValueGrandTotal.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.FieldValueTotal.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.GrandTotalCell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.Lines.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.Appearance.TotalCell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid1.DataMember = "Linea"
        Me.XrPivotGrid1.DataSource = Me.SqlDataSource1
        Me.XrPivotGrid1.Dpi = 100.0!
        Me.XrPivotGrid1.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.fieldIdLinea1, Me.fieldLinea1, Me.fieldMeta1})
        Me.XrPivotGrid1.LocationFloat = New DevExpress.Utils.PointFloat(207.0833!, 135.8334!)
        Me.XrPivotGrid1.Name = "XrPivotGrid1"
        Me.XrPivotGrid1.OptionsPrint.FilterSeparatorBarPadding = 3
        Me.XrPivotGrid1.OptionsView.ShowColumnGrandTotalHeader = False
        Me.XrPivotGrid1.OptionsView.ShowColumnGrandTotals = False
        Me.XrPivotGrid1.OptionsView.ShowColumnHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowColumnTotals = False
        Me.XrPivotGrid1.OptionsView.ShowDataHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowFilterHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowFilterSeparatorBar = False
        Me.XrPivotGrid1.OptionsView.ShowHorzLines = False
        Me.XrPivotGrid1.OptionsView.ShowRowGrandTotalHeader = False
        Me.XrPivotGrid1.OptionsView.ShowRowGrandTotals = False
        Me.XrPivotGrid1.OptionsView.ShowRowHeaders = False
        Me.XrPivotGrid1.OptionsView.ShowRowTotals = False
        Me.XrPivotGrid1.OptionsView.ShowVertLines = False
        Me.XrPivotGrid1.SizeF = New System.Drawing.SizeF(215.0!, 50.0!)
        '
        'fieldIdLinea1
        '
        Me.fieldIdLinea1.AreaIndex = 0
        Me.fieldIdLinea1.FieldName = "IdLinea"
        Me.fieldIdLinea1.Name = "fieldIdLinea1"
        '
        'fieldLinea1
        '
        Me.fieldLinea1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldLinea1.AreaIndex = 0
        Me.fieldLinea1.FieldName = "Linea"
        Me.fieldLinea1.Name = "fieldLinea1"
        '
        'fieldMeta1
        '
        Me.fieldMeta1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldMeta1.AreaIndex = 0
        Me.fieldMeta1.CellFormat.FormatString = "###.##"
        Me.fieldMeta1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldMeta1.FieldName = "Meta"
        Me.fieldMeta1.Name = "fieldMeta1"
        Me.fieldMeta1.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        '
        'XrLabel6
        '
        Me.XrLabel6.Dpi = 100.0!
        Me.XrLabel6.LocationFloat = New DevExpress.Utils.PointFloat(54.1666!, 214.7917!)
        Me.XrLabel6.Name = "XrLabel6"
        Me.XrLabel6.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrLabel6.SizeF = New System.Drawing.SizeF(132.3542!, 23.0!)
        Me.XrLabel6.Text = "Producción Linea:"
        '
        'XrPivotGrid2
        '
        Me.XrPivotGrid2.Appearance.Cell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid2.Appearance.CustomTotalCell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid2.Appearance.FieldHeader.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid2.Appearance.FieldValue.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid2.Appearance.FieldValueGrandTotal.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid2.Appearance.FieldValueTotal.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid2.Appearance.GrandTotalCell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid2.Appearance.Lines.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid2.Appearance.TotalCell.Font = New System.Drawing.Font("Tahoma", 8.25!)
        Me.XrPivotGrid2.DataMember = "Linea"
        Me.XrPivotGrid2.DataSource = Me.SqlDataSource1
        Me.XrPivotGrid2.Dpi = 100.0!
        Me.XrPivotGrid2.Fields.AddRange(New DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField() {Me.XrPivotGridField9, Me.XrPivotGridField10, Me.fieldFechaProduccion1, Me.fieldProduccion1, Me.fieldTurno1, Me.fieldTM1})
        Me.XrPivotGrid2.LocationFloat = New DevExpress.Utils.PointFloat(120.8333!, 237.7917!)
        Me.XrPivotGrid2.Name = "XrPivotGrid2"
        Me.XrPivotGrid2.OLAPConnectionString = ""
        Me.XrPivotGrid2.OptionsPrint.FilterSeparatorBarPadding = 3
        Me.XrPivotGrid2.OptionsView.ShowColumnGrandTotalHeader = False
        Me.XrPivotGrid2.OptionsView.ShowColumnGrandTotals = False
        Me.XrPivotGrid2.OptionsView.ShowColumnHeaders = False
        Me.XrPivotGrid2.OptionsView.ShowColumnTotals = False
        Me.XrPivotGrid2.OptionsView.ShowDataHeaders = False
        Me.XrPivotGrid2.OptionsView.ShowFilterHeaders = False
        Me.XrPivotGrid2.OptionsView.ShowFilterSeparatorBar = False
        Me.XrPivotGrid2.OptionsView.ShowRowGrandTotalHeader = False
        Me.XrPivotGrid2.OptionsView.ShowRowGrandTotals = False
        Me.XrPivotGrid2.OptionsView.ShowRowHeaders = False
        Me.XrPivotGrid2.OptionsView.ShowRowTotals = False
        Me.XrPivotGrid2.SizeF = New System.Drawing.SizeF(323.3333!, 50.0!)
        '
        'XrPivotGridField9
        '
        Me.XrPivotGridField9.Area = DevExpress.XtraPivotGrid.PivotArea.ColumnArea
        Me.XrPivotGridField9.AreaIndex = 0
        Me.XrPivotGridField9.FieldName = "Linea"
        Me.XrPivotGridField9.Name = "XrPivotGridField9"
        '
        'XrPivotGridField10
        '
        Me.XrPivotGridField10.AreaIndex = 0
        Me.XrPivotGridField10.FieldName = "Meta"
        Me.XrPivotGridField10.Name = "XrPivotGridField10"
        '
        'fieldFechaProduccion1
        '
        Me.fieldFechaProduccion1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldFechaProduccion1.AreaIndex = 0
        Me.fieldFechaProduccion1.FieldName = "FechaProduccion"
        Me.fieldFechaProduccion1.Name = "fieldFechaProduccion1"
        Me.fieldFechaProduccion1.ValueFormat.FormatString = "d"
        Me.fieldFechaProduccion1.ValueFormat.FormatType = DevExpress.Utils.FormatType.DateTime
        Me.fieldFechaProduccion1.Width = 92
        '
        'fieldProduccion1
        '
        Me.fieldProduccion1.Area = DevExpress.XtraPivotGrid.PivotArea.DataArea
        Me.fieldProduccion1.AreaIndex = 0
        Me.fieldProduccion1.CellFormat.FormatString = "###.##"
        Me.fieldProduccion1.CellFormat.FormatType = DevExpress.Utils.FormatType.Numeric
        Me.fieldProduccion1.FieldName = "Produccion"
        Me.fieldProduccion1.Name = "fieldProduccion1"
        Me.fieldProduccion1.SummaryType = DevExpress.Data.PivotGrid.PivotSummaryType.Average
        '
        'fieldTurno1
        '
        Me.fieldTurno1.Area = DevExpress.XtraPivotGrid.PivotArea.RowArea
        Me.fieldTurno1.AreaIndex = 1
        Me.fieldTurno1.FieldName = "Turno"
        Me.fieldTurno1.Name = "fieldTurno1"
        '
        'fieldTM1
        '
        Me.fieldTM1.AreaIndex = 1
        Me.fieldTM1.FieldName = "TM"
        Me.fieldTM1.Name = "fieldTM1"
        '
        'PageFooter
        '
        Me.PageFooter.Controls.AddRange(New DevExpress.XtraReports.UI.XRControl() {Me.XrPageInfo2, Me.XrPageInfo1})
        Me.PageFooter.Dpi = 100.0!
        Me.PageFooter.HeightF = 100.0!
        Me.PageFooter.Name = "PageFooter"
        '
        'XrPageInfo2
        '
        Me.XrPageInfo2.Dpi = 100.0!
        Me.XrPageInfo2.Format = "Página {0} de {1}"
        Me.XrPageInfo2.LocationFloat = New DevExpress.Utils.PointFloat(335.0!, 9.999974!)
        Me.XrPageInfo2.Name = "XrPageInfo2"
        Me.XrPageInfo2.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo2.SizeF = New System.Drawing.SizeF(313.0!, 23.0!)
        Me.XrPageInfo2.TextAlignment = DevExpress.XtraPrinting.TextAlignment.TopRight
        '
        'XrPageInfo1
        '
        Me.XrPageInfo1.Dpi = 100.0!
        Me.XrPageInfo1.LocationFloat = New DevExpress.Utils.PointFloat(10.00001!, 9.999974!)
        Me.XrPageInfo1.Name = "XrPageInfo1"
        Me.XrPageInfo1.Padding = New DevExpress.XtraPrinting.PaddingInfo(2, 2, 0, 0, 100.0!)
        Me.XrPageInfo1.PageInfo = DevExpress.XtraPrinting.PageInfo.DateTime
        Me.XrPageInfo1.SizeF = New System.Drawing.SizeF(313.0!, 23.0!)
        '
        'Reporte_Lijadora
        '
        Me.Bands.AddRange(New DevExpress.XtraReports.UI.Band() {Me.Detail, Me.TopMargin, Me.BottomMargin, Me.ReportHeader, Me.PageFooter})
        Me.ComponentStorage.AddRange(New System.ComponentModel.IComponent() {Me.SqlDataSource1})
        Me.DataMember = "Linea"
        Me.DataSource = Me.SqlDataSource1
        Me.Margins = New System.Drawing.Printing.Margins(101, 100, 100, 100)
        Me.Parameters.AddRange(New DevExpress.XtraReports.Parameters.Parameter() {Me.fecha1, Me.fecha2})
        Me.ScriptLanguage = DevExpress.XtraReports.ScriptLanguage.VisualBasic
        Me.Version = "16.2"
        CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

    End Sub
    Friend WithEvents Detail As DevExpress.XtraReports.UI.DetailBand
    Friend WithEvents TopMargin As DevExpress.XtraReports.UI.TopMarginBand
    Friend WithEvents BottomMargin As DevExpress.XtraReports.UI.BottomMarginBand
    Friend WithEvents SqlDataSource1 As DevExpress.DataAccess.Sql.SqlDataSource
    Friend WithEvents fecha1 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents fecha2 As DevExpress.XtraReports.Parameters.Parameter
    Friend WithEvents XrPictureBox1 As DevExpress.XtraReports.UI.XRPictureBox
    Friend WithEvents ReportHeader As DevExpress.XtraReports.UI.ReportHeaderBand
    Friend WithEvents XrLabel15 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLine2 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLine1 As DevExpress.XtraReports.UI.XRLine
    Friend WithEvents XrLabel4 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel3 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel2 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel1 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrLabel5 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPivotGrid1 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents fieldIdLinea1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldLinea1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldMeta1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrLabel6 As DevExpress.XtraReports.UI.XRLabel
    Friend WithEvents XrPivotGrid2 As DevExpress.XtraReports.UI.XRPivotGrid
    Friend WithEvents XrPivotGridField9 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents XrPivotGridField10 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldFechaProduccion1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldProduccion1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldTurno1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents fieldTM1 As DevExpress.XtraReports.UI.PivotGrid.XRPivotGridField
    Friend WithEvents PageFooter As DevExpress.XtraReports.UI.PageFooterBand
    Friend WithEvents XrPageInfo2 As DevExpress.XtraReports.UI.XRPageInfo
    Friend WithEvents XrPageInfo1 As DevExpress.XtraReports.UI.XRPageInfo
End Class
