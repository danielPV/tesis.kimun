﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Conectar_Servidor
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Conectar_Servidor))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Nom_Servidor = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Check = New System.Windows.Forms.CheckBox()
        Me.text_Password = New System.Windows.Forms.TextBox()
        Me.text_Usuario = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Guardar_Servidor = New System.Windows.Forms.Button()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(36, 39)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(49, 13)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Servidor:"
        '
        'Nom_Servidor
        '
        Me.Nom_Servidor.Location = New System.Drawing.Point(91, 36)
        Me.Nom_Servidor.Name = "Nom_Servidor"
        Me.Nom_Servidor.Size = New System.Drawing.Size(204, 20)
        Me.Nom_Servidor.TabIndex = 1
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Check)
        Me.GroupBox1.Controls.Add(Me.text_Password)
        Me.GroupBox1.Controls.Add(Me.text_Usuario)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(21, 75)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(271, 140)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Autentificación"
        '
        'Check
        '
        Me.Check.AutoSize = True
        Me.Check.Location = New System.Drawing.Point(53, 20)
        Me.Check.Name = "Check"
        Me.Check.Size = New System.Drawing.Size(157, 17)
        Me.Check.TabIndex = 5
        Me.Check.Text = "¿Seguridad SQL SERVER?"
        Me.Check.UseVisualStyleBackColor = True
        '
        'text_Password
        '
        Me.text_Password.Enabled = False
        Me.text_Password.Location = New System.Drawing.Point(126, 97)
        Me.text_Password.Name = "text_Password"
        Me.text_Password.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.text_Password.Size = New System.Drawing.Size(100, 20)
        Me.text_Password.TabIndex = 4
        '
        'text_Usuario
        '
        Me.text_Usuario.Enabled = False
        Me.text_Usuario.Location = New System.Drawing.Point(126, 52)
        Me.text_Usuario.Name = "text_Usuario"
        Me.text_Usuario.Size = New System.Drawing.Size(100, 20)
        Me.text_Usuario.TabIndex = 3
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(36, 100)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(64, 13)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Contraseña:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(36, 55)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(46, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Usuario:"
        '
        'Guardar_Servidor
        '
        Me.Guardar_Servidor.Location = New System.Drawing.Point(114, 229)
        Me.Guardar_Servidor.Name = "Guardar_Servidor"
        Me.Guardar_Servidor.Size = New System.Drawing.Size(75, 23)
        Me.Guardar_Servidor.TabIndex = 3
        Me.Guardar_Servidor.Text = "Guardar"
        Me.Guardar_Servidor.UseVisualStyleBackColor = True
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label4.Location = New System.Drawing.Point(95, 20)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(197, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "nombre de servidor\nombre de instancia"
        '
        'Conectar_Servidor
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(307, 266)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Guardar_Servidor)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Nom_Servidor)
        Me.Controls.Add(Me.Label1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "Conectar_Servidor"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Conectar"
        Me.TopMost = True
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Nom_Servidor As TextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents text_Password As TextBox
    Friend WithEvents text_Usuario As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Guardar_Servidor As Button
    Friend WithEvents Label4 As Label
    Public WithEvents Check As CheckBox
End Class
