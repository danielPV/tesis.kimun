﻿Imports System.Data.SqlClient

Public Class Lista_Usuarios
    Private Sub Lista_Usuarios_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        box_usuario.Enabled = False
        box_cargo.Enabled = False
        box_rol.Enabled = False
        box_password.Enabled = False
        box_unidad.Enabled = False
        boton_editar.Enabled = False
        boton_eliminar.Enabled = False

        Actualizar_Registros()

    End Sub

    Private Sub Actualizar_Registros()

        box_usuario.Text = ""
        box_cargo.Text = ""
        box_rol.Text = ""
        box_password.Text = ""
        box_unidad.Text = ""
        boton_editar.Enabled = False
        boton_eliminar.Enabled = False

        Dim consulta = New SqlCommand("SELECT RutUsuario As Rut, NombreUsuario As Usuario, CargoUsuario As Cargo, RolUsuario As Rol, Password As Contraseña, Unidad 
                                      FROM Usuarios", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            Dim dataAdapter As New SqlDataAdapter(consulta)
            Dim tabla As New DataTable
            dataAdapter.Fill(tabla)

            tabla_usuarios.DataSource = tabla

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al llenar la tabla de usuarios")
        End Try
    End Sub

    Private Sub GridView4_RowClick(sender As Object, e As DevExpress.XtraGrid.Views.Grid.RowClickEventArgs) Handles GridView4.RowClick

        box_usuario.Enabled = True
        box_cargo.Enabled = True
        box_rol.Enabled = True
        box_password.Enabled = True
        box_unidad.Enabled = True
        boton_editar.Enabled = True
        boton_eliminar.Enabled = True

        box_usuario.Text = GridView4.GetFocusedRowCellValue("Usuario")
        box_cargo.Text = GridView4.GetFocusedRowCellValue("Cargo")

        If (GridView4.GetFocusedRowCellValue("Rol") = "admin                         " Or GridView4.GetFocusedRowCellValue("Rol") = "Administrador                 ") Then
            box_rol.SelectedIndex = 0
        Else
            box_rol.SelectedIndex = 1
        End If

        box_password.Text = GridView4.GetFocusedRowCellValue("Contraseña")
        box_unidad.Text = GridView4.GetFocusedRowCellValue("Unidad")

    End Sub

    Private Sub boton_editar_Click(sender As Object, e As EventArgs) Handles boton_editar.Click

        Dim rut As String = GridView4.GetFocusedRowCellValue("Rut")
        Dim c As Cursor = Me.Cursor

        Dim actualizar_usuario As New SqlCommand("UPDATE Usuarios SET NombreUsuario = '" & box_usuario.Text & "' WHERE RutUsuario = '" & rut & "'  ", conexion_controlProduccion)
        Dim actualizar_cargo As New SqlCommand("UPDATE Usuarios SET CargoUsuario = '" & box_cargo.Text & "' WHERE RutUsuario = '" & rut & "' ", conexion_controlProduccion)
        Dim actualizar_rol As New SqlCommand("UPDATE Usuarios SET RolUsuario = '" & box_rol.Text & "' WHERE RutUsuario = '" & rut & "' ", conexion_controlProduccion)
        Dim actualizar_password As New SqlCommand("UPDATE Usuarios SET Password = '" & box_password.Text & "' WHERE RutUsuario = '" & rut & "' ", conexion_controlProduccion)
        Dim actualizar_unidad As New SqlCommand("UPDATE Usuarios SET Unidad = '" & box_unidad.Text & "' WHERE RutUsuario = '" & rut & "' ", conexion_controlProduccion)

        Dim ejecutar As Integer

        If (box_usuario.Text = "" Or box_cargo.Text = "" Or box_rol.Text = "" Or box_password.Text = "" Or box_unidad.Text = "") Then
            MsgBox("Por favor, debe ingresar todos los datos.", MsgBoxStyle.Exclamation)
        Else
            Try
                Me.Cursor = Cursors.WaitCursor
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                ejecutar = actualizar_usuario.ExecuteNonQuery()
                ejecutar = actualizar_cargo.ExecuteNonQuery()
                ejecutar = actualizar_rol.ExecuteNonQuery()
                ejecutar = actualizar_password.ExecuteNonQuery()
                ejecutar = actualizar_unidad.ExecuteNonQuery()

                MsgBox("Se ha editado el usuario satisfactoriamente", MsgBoxStyle.Information)
                Actualizar_Registros()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al editar usuario")
            Finally
                Me.Cursor = c
            End Try
        End If
    End Sub

    Private Sub boton_eliminar_Click(sender As Object, e As EventArgs) Handles boton_eliminar.Click

        Dim rut As String = GridView4.GetFocusedRowCellValue("Rut")
        Dim c As Cursor = Me.Cursor

        Dim eliminar As New SqlCommand("DELETE FROM Usuarios WHERE RutUsuario = '" & rut & "'  ", conexion_controlProduccion)

        Dim ejecutar As Integer

        Try
            Me.Cursor = Cursors.WaitCursor

            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            ejecutar = eliminar.ExecuteNonQuery()

            MsgBox("Se ha eliminado al usuario satisfactoriamente", MsgBoxStyle.Information)
            Actualizar_Registros()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al eliminar al usuario")
        Finally
            Me.Cursor = c
        End Try
    End Sub
End Class