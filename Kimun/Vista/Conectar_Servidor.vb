﻿Imports System.Configuration
Imports System.Data.Sql
Imports System.IO.Directory
Imports System.Security.Permissions
Imports System.Threading
Imports System.Xml

Public Class Conectar_Servidor

    Private Sub Guardar_Servidor_Click(sender As Object, e As EventArgs) Handles Guardar_Servidor.Click

        Dim settings As New XmlWriterSettings()
        settings.Indent = True
        Dim funciones As New Funciones

        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor

        If (funciones.comprobar_conexion(Nom_Servidor.Text) = True) Then
            If (Check.Checked = True) Then
                'Seguridad SQL SERVER

                If (text_Usuario.Text = "" Or text_Password.Text = "") Then
                    MsgBox("Debe llenar los campos de usuario y contraseña")

                Else

                    Try
                        Dim dir As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

                        'Se crea el directorio donde se guardaran las opciones
                        CreateDirectory(dir & "\Kimun")

                        'Dim filePermissions = New FileIOPermission(FileIOPermissionAccess.AllAccess, dir & "\Kimun\config.xml")
                        'filePermissions.Demand()

                        'Directiorio donde se guardara el archivo
                        Dim XmlWrt As XmlWriter = XmlWriter.Create(dir & "\Kimun\config.xml", settings)

                        With XmlWrt
                            'Declaracion por defecto XML
                            .WriteStartDocument()
                            'Comentario del XML
                            .WriteComment("XML Configuracion Kimun")
                            'Encabezado raiz XML
                            .WriteStartElement("Configuracion")
                            'Se inicia la configuracion
                            .WriteStartElement("Parametros")
                            'Parametros del nodo
                            .WriteStartElement("Servidor")
                            .WriteString(Nom_Servidor.Text)
                            .WriteEndElement()

                            .WriteStartElement("Usuario")
                            .WriteString(text_Usuario.Text)
                            .WriteEndElement()

                            .WriteStartElement("Password")
                            .WriteString(text_Password.Text)
                            .WriteEndElement()

                            .WriteStartElement("Correo")
                            .WriteString("")
                            .WriteEndElement()

                            'Termina la configuracion
                            .WriteEndElement()

                            'Se cierra XMLTextWriter
                            .WriteEndDocument()
                            .Dispose()
                            .Close()

                        End With
                    Catch ex As Exception
                        MsgBox(ex.Message, MsgBoxStyle.Information, "Error al guardar el archivo XML")
                    End Try

                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Notificación", "Se ha creado el archivo XML.", ToolTipIcon.Info)
                    crear_conexiones()

                    Me.Cursor = c
                    Me.Close()
                End If

            Else

                text_Usuario.Text = ""
                text_Password.Text = ""

                Try

                    Dim dir As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

                    'Se crea el directorio donde se guardaran las opciones
                    CreateDirectory(dir & "\Kimun")

                    'Dim filePermissions = New FileIOPermission(FileIOPermissionAccess.AllAccess, dir & "\Kimun\config.xml")
                    'filePermissions.Demand()

                    'Directiorio donde se guardara el archivo
                    Dim XmlWrt As XmlWriter = XmlWriter.Create(dir & "\Kimun\config.xml", settings)

                    With XmlWrt
                        'Declaracion por defecto XML
                        .WriteStartDocument()
                        'Comentario del XML
                        .WriteComment("XML Configuracion Kimun")
                        'Encabezado raiz XML
                        .WriteStartElement("Configuracion")
                        'Se inicia la configuracion
                        .WriteStartElement("Parametros")
                        'Parametros del nodo
                        .WriteStartElement("Servidor")
                        .WriteString(Nom_Servidor.Text)
                        .WriteEndElement()

                        .WriteStartElement("Usuario")
                        .WriteString(text_Usuario.Text)
                        .WriteEndElement()

                        .WriteStartElement("Password")
                        .WriteString(text_Password.Text)
                        .WriteEndElement()

                        .WriteStartElement("Correo")
                        .WriteString("")
                        .WriteEndElement()

                        'Termina la configuracion
                        .WriteEndElement()

                        'Se cierra XMLTextWriter
                        .WriteEndDocument()
                        .Dispose()
                        .Close()


                    End With

                Catch ex As Exception
                    MsgBox(ex.Message, MsgBoxStyle.Information, "Error al guardar el archivo XML")
                End Try

                'Seguridad Windows
                'PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Notificación", "Se ha creado el archivo XML.", ToolTipIcon.Info)
                crear_conexiones()

                Me.Cursor = c
                Me.Close()
            End If
        Else
            Me.Cursor = c
            MsgBox("El servidor e instancia ingresado no es válido, vuelva a ingresar un servidor valido.")
            Nom_Servidor.SelectAll()
            Nom_Servidor.Focus()
        End If

    End Sub

    Private Sub Conectar_Servidor_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim MisDocumentos As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)

        Try

            Dim ruta As String = MisDocumentos & "\Kimun\config.xml"

            'Pregunta si el directorio existe
            If (My.Computer.FileSystem.FileExists(ruta) = True) Then

                'Dim filePermissions = New FileIOPermission(FileIOPermissionAccess.Read, ruta)
                'filePermissions.Demand()

                Dim documento As XmlReader = New XmlTextReader(ruta)

                While (documento.Read())
                    Dim type = documento.NodeType

                    If (type = XmlNodeType.Element) Then
                        If (documento.Name = "Servidor") Then
                            Nom_Servidor.Text = documento.ReadInnerXml
                        End If

                        If (documento.Name = "Usuario") Then
                            text_Usuario.Text = documento.ReadInnerXml
                        End If
                    End If
                End While


                documento.Dispose()
                documento.Close()
            End If

        Catch ex As Exception
            PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Notificación", "Debe crear el archivo XML", ToolTipIcon.Info)
        End Try
    End Sub

    Private Sub Check_CheckedChanged(sender As Object, e As EventArgs) Handles Check.CheckedChanged

        If (text_Usuario.Enabled = False And text_Password.Enabled = False) Then
            text_Usuario.Enabled = True
            text_Password.Enabled = True
        Else
            text_Usuario.Enabled = False
            text_Password.Enabled = False
        End If
    End Sub

End Class