﻿Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraSplashScreen

Public Class generar_rangos

    Dim boton As String

    Public Sub New()
        InitializeComponent()
    End Sub

    Public Sub New(ByVal strParametro As String)
        Me.New()
        boton = strParametro
    End Sub

    Private Sub Generar_Click(sender As Object, e As EventArgs) Handles Generar.Click

        Dim inicio As Date = DateSerial(Year(Rango_Inicial.Value), Month(Rango_Inicial.Value), DateAndTime.Day(Rango_Inicial.Value))
        Dim fin As Date = DateSerial(Year(Rango_Final.Value), Month(Rango_Final.Value), DateAndTime.Day(Rango_Final.Value))

        If (boton = "calidad") Then
            Try
                Dim c As Cursor = Me.Cursor

                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim reporte_rangos As New Reporte_CalidadAguas

                Dim tool As New ReportPrintTool(reporte_rangos)

                'reporte_rangos.SqlDataSource2.ConnectionName = "conectar_controlProduccion"
                reporte_rangos.SqlDataSource2.Connection.ConnectionString = GetConexion_controlProduccion()

                reporte_rangos.Parameters("Fecha1").Value = inicio
                reporte_rangos.Parameters("Fecha2").Value = fin

                reporte_rangos.Parameters("Fecha1").Visible = False
                reporte_rangos.Parameters("Fecha2").Visible = False

                'tool.PreviewForm.ControlBox = False
                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
                Me.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de calidad de aguas")
            End Try

        End If

        If (boton = "indicadores") Then
            Try
                Dim c As Cursor = Me.Cursor

                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_eTata(GetConexion_eTata())

                Dim reporte_rangos As New Reporte_IndicadoresCaldera

                Dim tool As New ReportPrintTool(reporte_rangos)

                'reporte_rangos.SqlDataSource2.ConnectionName = "conectar_eTata"
                reporte_rangos.SqlDataSource2.Connection.ConnectionString = GetConexion_eTata()

                reporte_rangos.Parameters("fecha1").Value = inicio
                reporte_rangos.Parameters("fecha2").Value = fin

                reporte_rangos.Parameters("fecha1").Visible = False
                reporte_rangos.Parameters("fecha2").Visible = False

                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
                Me.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de indicadores de la caldera")
            End Try
        End If

        If (boton = "verde") Then
            Try
                Dim c As Cursor = Me.Cursor

                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim reporte_rangos As New Reporte_LineaVerde

                Dim tool As New ReportPrintTool(reporte_rangos)

                'reporte_rangos.SqlDataSource1.ConnectionName = "conectar_controlProduccion"
                reporte_rangos.SqlDataSource1.Connection.ConnectionString = GetConexion_controlProduccion()

                reporte_rangos.Parameters("fecha1").Value = inicio
                reporte_rangos.Parameters("fecha2").Value = fin

                reporte_rangos.Parameters("fecha1").Visible = False
                reporte_rangos.Parameters("fecha2").Visible = False

                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
                Me.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de la linea verde")
            End Try

        End If

        If (boton = "lijadora") Then
            Try
                Dim c As Cursor = Me.Cursor
                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim reporte_rangos As New Reporte_Lijadora

                Dim tool As New ReportPrintTool(reporte_rangos)

                'reporte_rangos.SqlDataSource1.ConnectionName = "conectar_controlProduccion"
                reporte_rangos.SqlDataSource1.Connection.ConnectionString = GetConexion_controlProduccion()

                reporte_rangos.Parameters("fecha1").Value = inicio
                reporte_rangos.Parameters("fecha2").Value = fin

                reporte_rangos.Parameters("fecha1").Visible = False
                reporte_rangos.Parameters("fecha2").Visible = False

                'tool.PreviewForm.ControlBox = False
                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
                Me.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de la lijadora")
            End Try

        End If
    End Sub

End Class