﻿Imports System.Data.SqlClient

Public Class Crear_Usuario
    Private Sub boton_guardar_Click(sender As Object, e As EventArgs) Handles boton_guardar.Click
        Dim c As Cursor = Me.Cursor

        If (box_rut.Text = "" Or box_digito.Text = "" Or box_usuario.Text = "" Or box_cargo.Text = "" Or box_rol.Text = "" Or box_password.Text = "" Or box_unidad.Text = "") Then
            MsgBox("Por favor, debe ingresar todos los datos.", MsgBoxStyle.Exclamation)
        Else
            Me.Cursor = Cursors.WaitCursor
            Dim rut As String = String.Format("{0}-{1}", box_rut.Text, box_digito.Text.ToUpper)

            Dim crear As New SqlCommand("INSERT INTO Usuarios (RutUsuario, NombreUsuario, CargoUsuario, RolUsuario, Password, Unidad)  
                    VALUES('" & rut & "', '" & box_usuario.Text & "', '" & box_cargo.Text & "', '" & box_rol.Text & "', '" & box_password.Text & "', '" & box_unidad.Text & "')", conexion_controlProduccion)

            Dim ejecutar As Integer

            Try
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                ejecutar = crear.ExecuteNonQuery()

                MsgBox("Se ha creado el usuario satisfactoriamente", MsgBoxStyle.Information)
                Me.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear el usuario")
            Finally
                Me.Cursor = c
            End Try
        End If
    End Sub
End Class

