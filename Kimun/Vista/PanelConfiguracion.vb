﻿Imports System.Data.SqlClient
Imports System.IO
Imports System.Security.Permissions
Imports System.Text.RegularExpressions
Imports System.Xml
Imports MSXML2

Public Class PanelConfiguracion

    Dim funciones As New Funciones
    Private Function existeRegistroCaldera(ByVal nombreTabla As String) As Boolean

        If (nombreTabla = "LimiteMinCaldera") Then
            Dim consulta As New SqlCommand("SELECT COUNT(*) FROM LimiteMinCaldera WHERE IdLimiteMin = 1 or IdLimiteMin = 2 or IdLimiteMin = 3 or IdLimiteMin = 4 or IdLimiteMin = 5 or IdLimiteMin = 6 or IdLimiteMin = 7 ", conexion_eTata)

            Try
                If (conexion_eTata.State = ConnectionState.Closed) Then
                    conexion_eTata.Open()
                End If

                Dim n As Integer = CInt(consulta.ExecuteScalar())

                conexion_eTata.Close()

                Return n > 0
            Catch
                Return False
            End Try
        End If

        If (nombreTabla = "LimitesCaldera") Then
            Dim consulta As New SqlCommand("SELECT COUNT(*) FROM LimitesCaldera WHERE IdLimite = 1 or IdLimite = 2 or IdLimite = 3 or IdLimite = 4 or IdLimite = 5 or IdLimite = 6 or IdLimite = 7 ", conexion_eTata)

            Try
                If (conexion_eTata.State = ConnectionState.Closed) Then
                    conexion_eTata.Open()
                End If

                Dim n As Integer = CInt(consulta.ExecuteScalar())

                conexion_eTata.Close()

                Return n > 0
            Catch
                Return False
            End Try
        End If
        Return 0
    End Function

    Private Function existeRegistroTurno(ByVal columna As String, ByVal cod As String) As Boolean
        Dim consulta As New SqlCommand("SELECT COUNT(*)
                                        FROM Turno, INFORMATION_SCHEMA.COLUMNS
                                        WHERE COLUMN_NAME = @Columna  and CodTurno = @codigo ", conexion_controlProduccion)


        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@Columna", columna)
            consulta.Parameters.AddWithValue("@codigo", cod)

            Dim n As Integer = CInt(consulta.ExecuteScalar())

            Return n > 0

            conexion_controlProduccion.Close()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al seleccionar los registros del turno")
            Return False
        End Try
    End Function

    Private Sub PanelConfiguracion_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        '****************************************************************************************************************
        '***********                                  LIMITES CALDERAS                                         **********
        '****************************************************************************************************************

        'Consultas
        '****************************************************************************************************************

        'Consulta Create Limite Max
        Dim crear_LimitesCaldera As New SqlCommand("CREATE TABLE [dbo].[LimitesCaldera](
	                                                    [IdLimite] [int] NOT NULL,
	                                                    [Limite] [int] NOT NULL,
	                                                    [TagIndex] [smallint] NOT NULL,
                                                     CONSTRAINT [PK_LimitesCaldera] PRIMARY KEY CLUSTERED 
                                                        (
	                                                        [IdLimite] ASC
                                                        )
                                                    )", conexion_eTata)

        'Consulta Create Limite Min
        Dim crear_LimiteMinCaldera As New SqlCommand("CREATE TABLE [dbo].[LimiteMinCaldera](
	                                                    [IdLimiteMin] [int] NOT NULL,
	                                                    [LimiteMin] [int] NOT NULL,
	                                                    [TagIndex] [smallint] NOT NULL,
                                                     CONSTRAINT [PK_LimiteMinCaldera] PRIMARY KEY CLUSTERED 
                                                        (
	                                                        [IdLimiteMin] ASC
                                                        )
                                                    )", conexion_eTata)


        'Consultas select tabla limite
        Dim mostrarMax_NivelDomo As New SqlCommand("SELECT Limite FROM LimitesCaldera WHERE TagIndex = 51", conexion_eTata)
        Dim mostrarMax_PresionDomo As New SqlCommand("SELECT Limite FROM LimitesCaldera WHERE TagIndex = 53", conexion_eTata)
        Dim mostrarMax_ConsumoVapor As New SqlCommand("SELECT Limite FROM LimitesCaldera WHERE TagIndex = 38", conexion_eTata)
        Dim mostrarMax_TempAgua As New SqlCommand("SELECT Limite FROM LimitesCaldera WHERE TagIndex = 28", conexion_eTata)
        Dim mostrarMax_PresionAgua As New SqlCommand("SELECT Limite FROM LimitesCaldera WHERE TagIndex = 39", conexion_eTata)
        Dim mostrarMax_TempVapor As New SqlCommand("SELECT Limite FROM LimitesCaldera WHERE TagIndex = 2", conexion_eTata)

        'Consultas select tabla limite min
        Dim mostrarMix_NivelDomo As New SqlCommand("SELECT LimiteMin FROM LimiteMinCaldera WHERE TagIndex = 51", conexion_eTata)
        Dim mostrarMix_PresionDomo As New SqlCommand("SELECT LimiteMin FROM LimiteMinCaldera WHERE TagIndex = 53", conexion_eTata)
        Dim mostrarMix_ConsumoVapor As New SqlCommand("SELECT LimiteMin FROM LimiteMinCaldera WHERE TagIndex = 38", conexion_eTata)
        Dim mostrarMix_TempAgua As New SqlCommand("SELECT LimiteMin FROM LimiteMinCaldera WHERE TagIndex = 28", conexion_eTata)
        Dim mostrarMix_PresionAgua As New SqlCommand("SELECT LimiteMin FROM LimiteMinCaldera WHERE TagIndex = 39", conexion_eTata)
        Dim mostrarMix_TempVapor As New SqlCommand("SELECT LimiteMin FROM LimiteMinCaldera WHERE TagIndex = 2", conexion_eTata)


        Try
            'Verifica si existe la tabla LimitesCaldera, si no la crea
            If (funciones.existeTabla_eTata("LimitesCaldera") = False) Then

                If (conexion_eTata.State = ConnectionState.Closed) Then
                    conexion_eTata.Open()
                End If

                crear_LimitesCaldera.ExecuteNonQuery()
                conexion_eTata.Close()
            End If

            'Verifica si existe la tabla LimiteMinCaldera, si no la crea
            If (funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then

                If (conexion_eTata.State = ConnectionState.Closed) Then
                    conexion_eTata.Open()
                End If

                crear_LimiteMinCaldera.ExecuteNonQuery()
                conexion_eTata.Close()
            End If

            'Llena los textbox
            If (existeRegistroCaldera("LimitesCaldera") = True) Then

                If (conexion_eTata.State = ConnectionState.Closed) Then
                    conexion_eTata.Open()
                End If

                Max_NivelDomo.Text = mostrarMax_NivelDomo.ExecuteScalar.ToString
                Max_PresionDomo.Text = mostrarMax_PresionDomo.ExecuteScalar.ToString
                Max_ConsumoVapor.Text = mostrarMax_ConsumoVapor.ExecuteScalar.ToString
                Max_TempAgua.Text = mostrarMax_TempAgua.ExecuteScalar.ToString
                Max_PresionAgua.Text = mostrarMax_PresionAgua.ExecuteScalar.ToString
                Max_TempVapor.Text = mostrarMax_TempVapor.ExecuteScalar.ToString
            Else
                Max_NivelDomo.Text = "-"
                Max_PresionDomo.Text = "-"
                Max_ConsumoVapor.Text = "-"
                Max_TempAgua.Text = "-"
                Max_PresionAgua.Text = "-"
                Max_TempVapor.Text = "-"
            End If

            If (existeRegistroCaldera("LimiteMinCaldera") = True) Then

                If (conexion_eTata.State = ConnectionState.Closed) Then
                    conexion_eTata.Open()
                End If

                Min_NivelDomo.Text = mostrarMix_NivelDomo.ExecuteScalar.ToString
                Min_PresionDomo.Text = mostrarMix_PresionDomo.ExecuteScalar.ToString
                Min_ConsumoVapor.Text = mostrarMix_ConsumoVapor.ExecuteScalar.ToString
                Min_TempAgua.Text = mostrarMix_TempAgua.ExecuteScalar.ToString
                Min_PresionAgua.Text = mostrarMix_PresionAgua.ExecuteScalar.ToString
                Min_TempVapor.Text = mostrarMix_TempVapor.ExecuteScalar.ToString
            Else
                Min_NivelDomo.Text = "-"
                Min_PresionDomo.Text = "-"
                Min_ConsumoVapor.Text = "-"
                Min_TempAgua.Text = "-"
                Min_PresionAgua.Text = "-"
                Min_TempVapor.Text = "-"
            End If
            conexion_eTata.Close()
        Catch
            'MsgBox(ex.Message, MsgBoxStyle.Information, "Error al leer los limites")
            Max_NivelDomo.Text = "-"
            Max_PresionDomo.Text = "-"
            Max_ConsumoVapor.Text = "-"
            Max_TempAgua.Text = "-"
            Max_PresionAgua.Text = "-"
            Max_TempVapor.Text = "-"

            Min_NivelDomo.Text = "-"
            Min_PresionDomo.Text = "-"
            Min_ConsumoVapor.Text = "-"
            Min_TempAgua.Text = "-"
            Min_PresionAgua.Text = "-"
            Min_TempVapor.Text = "-"
        End Try



        '****************************************************************************************************************
        '***********                                         METAS                                             **********
        '****************************************************************************************************************

        'Consulta Select 

        Dim mostrar_ValorMetaTorno As New SqlCommand("SELECT Meta FROM Linea WHERE Linea = 'Torno' ", conexion_controlProduccion)
        Dim mostrar_ValorMetaDescortezador As New SqlCommand("SELECT Meta FROM Linea WHERE Linea = 'Descortezador' ", conexion_controlProduccion)
        Dim mostrar_ValorMetaLijadora As New SqlCommand("SELECT Meta FROM Linea WHERE Linea = 'Lijadora' ", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            'Mostrar los valores en el textbox
            Valor_MetaTorno.Text = mostrar_ValorMetaTorno.ExecuteScalar.ToString
            Valor_MetaDescortezador.Text = mostrar_ValorMetaDescortezador.ExecuteScalar.ToString
            Valor_MetaLijadora.Text = mostrar_ValorMetaLijadora.ExecuteScalar.ToString

            conexion_controlProduccion.Close()
        Catch
            Valor_MetaTorno.Text = "-"
            Valor_MetaDescortezador.Text = "-"
            Valor_MetaLijadora.Text = "-"
        End Try

        '****************************************************************************************************************
        '***********                                       TURNOS                                             ***********
        '****************************************************************************************************************

        Dim agregarInicio_columnasTurnos As New SqlCommand("ALTER TABLE Turno ADD hora_inicio TIME(1)", conexion_controlProduccion)
        Dim agregarFin_columnasTurnos As New SqlCommand("ALTER TABLE Turno ADD hora_finalizacion TIME(1)", conexion_controlProduccion)

        Dim mostrarValor_inicioTA As New SqlCommand("SELECT hora_inicio FROM Turno WHERE CodTurno = 'TA' ", conexion_controlProduccion)
        Dim mostrarValor_finTA As New SqlCommand("SELECT hora_finalizacion FROM Turno WHERE CodTurno = 'TA' ", conexion_controlProduccion)
        Dim mostrarValor_inicioTB As New SqlCommand("SELECT hora_inicio FROM Turno WHERE CodTurno = 'TB' ", conexion_controlProduccion)
        Dim mostrarValor_finTB As New SqlCommand("SELECT hora_finalizacion FROM Turno WHERE CodTurno = 'TB'", conexion_controlProduccion)
        Dim mostrarValor_inicioTC As New SqlCommand("SELECT hora_inicio FROM Turno WHERE CodTurno = 'TC' ", conexion_controlProduccion)
        Dim mostrarValor_finTC As New SqlCommand("SELECT hora_finalizacion FROM Turno WHERE CodTurno = 'TC'", conexion_controlProduccion)

        Try
            'Comprueba si existen las columnas
            If (funciones.existeColumna_controlProduccion("Turno", "hora_inicio") = False) Then
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                agregarInicio_columnasTurnos.ExecuteNonQuery()

                TA_HoraInicio.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
                TB_HoraInicio.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
                TC_HoraInicio.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")

                conexion_controlProduccion.Close()
            Else
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                If (existeRegistroTurno("hora_inicio", "TA") = True) Then
                    TA_HoraInicio.EditValue = mostrarValor_inicioTA.ExecuteScalar.ToString()
                Else
                    TA_HoraInicio.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
                End If

                If (existeRegistroTurno("hora_inicio", "TB") = True) Then
                    TB_HoraInicio.EditValue = mostrarValor_inicioTB.ExecuteScalar.ToString()
                Else
                    TB_HoraInicio.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
                End If

                If (existeRegistroTurno("hora_inicio", "TC") = True) Then
                    TC_HoraInicio.EditValue = mostrarValor_inicioTC.ExecuteScalar.ToString()
                Else
                    TC_HoraInicio.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
                End If

                conexion_controlProduccion.Close()
            End If

            If (funciones.existeColumna_controlProduccion("Turno", "hora_finalizacion") = False) Then

                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                agregarFin_columnasTurnos.ExecuteNonQuery()

                TA_HoraFin.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
                TB_HoraFin.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
                TC_HoraFin.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")

                conexion_controlProduccion.Close()

            Else
                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                If (existeRegistroTurno("hora_finalizacion", "TA") = True) Then
                    TA_HoraFin.EditValue = mostrarValor_finTA.ExecuteScalar.ToString()
                Else
                    TA_HoraFin.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
                End If

                If (existeRegistroTurno("hora_finalizacion", "TB") = True) Then
                    TB_HoraFin.EditValue = mostrarValor_finTB.ExecuteScalar.ToString()
                Else
                    TB_HoraFin.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
                End If

                If (existeRegistroTurno("hora_finalizacion", "TC") = True) Then
                    TC_HoraFin.EditValue = mostrarValor_finTC.ExecuteScalar.ToString()
                Else
                    TC_HoraFin.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
                End If

                conexion_controlProduccion.Close()
            End If
        Catch ex As Exception
            TA_HoraInicio.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
            TB_HoraInicio.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
            TC_HoraInicio.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")

            TA_HoraFin.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
            TB_HoraFin.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
            TC_HoraFin.EditValue = Format("{HH: mm : ss zzz}", "00:00:000")
        End Try



        '****************************************************************************************************************
        '***********                                      CORREOS                                             ***********
        '****************************************************************************************************************

        Dim MisDocumentos As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Dim correos() As String = {""}

        Try

            Dim ruta As String = MisDocumentos & "\Kimun\config.xml"

            'Dim filePermissions = New FileIOPermission(FileIOPermissionAccess.AllAccess, ruta)
            'filePermissions.Demand()

            'Pregunta si el directorio existe
            If (My.Computer.FileSystem.FileExists(ruta) = True) Then
                Dim documento As XmlReader = New XmlTextReader(ruta)

                While (documento.Read())
                    Dim type = documento.NodeType

                    If (type = XmlNodeType.Element) Then
                        If (documento.Name = "Correo") Then

                            correos = documento.ReadInnerXml.Split(", ")
                        End If
                    End If
                End While

                documento.Close()

                Dim num_correos As Integer = correos.Length()

                For index As Integer = 0 To num_correos - 1

                    If (correos(index) <> "") Then
                        If (index = 0) Then
                            valor_correo1.Text = correos(index)
                            Check_correo1.Checked = True
                        End If

                        If (index = 1) Then
                            valor_correo2.Text = correos(index)
                            Check_correo2.Checked = True
                        End If

                        If (index = 2) Then
                            valor_correo3.Text = correos(index)
                            Check_correo3.Checked = True
                        End If

                        If (index = 3) Then
                            valor_correo4.Text = correos(index)
                            Check_correo4.Checked = True
                        End If

                        If (index = 4) Then
                            valor_correo5.Text = correos(index)
                            Check_correo5.Checked = True
                        End If

                    End If
                Next
            End If
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al mostrar los correos")
        End Try
    End Sub

    Private Sub Button_Guardar_Click(sender As Object, e As EventArgs) Handles Button_Guardar.Click

        'Consulta a Base de datos eTata

        Dim insertarLimMaxCaldera As New SqlCommand("INSERT INTO LimitesCaldera (IdLimite, Limite, TagIndex)  
                    VALUES('" & 1 & "', '" & Max_NivelDomo.Text & "', '" & 51 & "'),
                                           ('" & 2 & "', '" & Max_PresionDomo.Text & "', '" & 53 & "'),
                                          ('" & 3 & "', '" & Max_ConsumoVapor.Text & "', '" & 38 & "'),
                                         ('" & 4 & "', '" & Max_TempAgua.Text & "', '" & 28 & "'),
                                        ('" & 5 & "', '" & Max_PresionAgua.Text & "', '" & 39 & "'),
                                       ('" & 6 & "', '" & Max_TempVapor.Text & "', '" & 2 & "'),
                                      ('" & 7 & "', '" & 1 & "', '" & 52 & "')", conexion_eTata)

        Dim insertarLimMinCaldera As New SqlCommand("INSERT INTO LimiteMinCaldera (IdLimiteMin, LimiteMin, TagIndex)  
                    VALUES('" & 1 & "', '" & Min_NivelDomo.Text & "', '" & 51 & "'),
                                           ('" & 2 & "', '" & Min_PresionDomo.Text & "', '" & 53 & "'),
                                          ('" & 3 & "', '" & Min_ConsumoVapor.Text & "', '" & 38 & "'),
                                         ('" & 4 & "', '" & Min_TempAgua.Text & "', '" & 28 & "'),
                                        ('" & 5 & "', '" & Min_PresionAgua.Text & "', '" & 39 & "'),
                                       ('" & 6 & "', '" & Min_TempVapor.Text & "', '" & 2 & "'),
                                      ('" & 7 & "', '" & 0 & "', '" & 52 & "')", conexion_eTata)


        Dim ActualizarMaxNivelDomo As New SqlCommand("UPDATE LimitesCaldera SET Limite = '" & Max_NivelDomo.Text & "' WHERE IdLimite = 1 ", conexion_eTata)
        Dim ActualizarMaxPresionDomo As New SqlCommand("UPDATE LimitesCaldera SET Limite = '" & Max_PresionDomo.Text & "' WHERE IdLimite = 2 ", conexion_eTata)
        Dim ActualizarMaxConsumoVapor As New SqlCommand("UPDATE LimitesCaldera SET Limite = '" & Max_ConsumoVapor.Text & "' WHERE IdLimite = 3 ", conexion_eTata)
        Dim ActualizarMaxTempAgua As New SqlCommand("UPDATE LimitesCaldera SET Limite = '" & Max_TempAgua.Text & "' WHERE IdLimite = 4 ", conexion_eTata)
        Dim ActualizarMaxPresionAgua As New SqlCommand("UPDATE LimitesCaldera SET Limite = '" & Max_PresionAgua.Text & "' WHERE IdLimite = 5 ", conexion_eTata)
        Dim ActualizarMaxTempVapor As New SqlCommand("UPDATE LimitesCaldera SET Limite = '" & Max_TempVapor.Text & "' WHERE IdLimite = 6 ", conexion_eTata)

        Dim ActualizarMinNivelDomo As New SqlCommand("UPDATE LimiteMinCaldera SET LimiteMin = '" & Min_NivelDomo.Text & "' WHERE IdLimiteMin = 1 ", conexion_eTata)
        Dim ActualizarMinPresionDomo As New SqlCommand("UPDATE LimiteMinCaldera SET LimiteMin = '" & Min_PresionDomo.Text & "' WHERE IdLimiteMin = 2 ", conexion_eTata)
        Dim ActualizarMinConsumoVapor As New SqlCommand("UPDATE LimiteMinCaldera SET LimiteMin = '" & Min_ConsumoVapor.Text & "' WHERE IdLimiteMin = 3 ", conexion_eTata)
        Dim ActualizarMinTempAgua As New SqlCommand("UPDATE LimiteMinCaldera SET LimiteMin = '" & Min_TempAgua.Text & "' WHERE IdLimiteMin = 4 ", conexion_eTata)
        Dim ActualizarMinPresionAgua As New SqlCommand("UPDATE LimiteMinCaldera SET LimiteMin = '" & Min_PresionAgua.Text & "' WHERE IdLimiteMin = 5 ", conexion_eTata)
        Dim ActualizarMinTempVapor As New SqlCommand("UPDATE LimiteMinCaldera SET LimiteMin = '" & Min_TempVapor.Text & "' WHERE IdLimiteMin = 6 ", conexion_eTata)

        'Actualizacion Tabla Metas

        Dim ActualizarMetaTorno As New SqlCommand("UPDATE Linea SET Meta = '" & Valor_MetaTorno.Text & "' WHERE Linea = 'Torno' ", conexion_controlProduccion)
        Dim ActualizarMetaDescortezador As New SqlCommand("UPDATE Linea SET Meta = '" & Valor_MetaDescortezador.Text & "' WHERE Linea = 'Descortezador' ", conexion_controlProduccion)
        Dim ActualizarMetaLijadora As New SqlCommand("UPDATE Linea SET Meta = '" & Valor_MetaLijadora.Text & "' WHERE Linea = 'Lijadora' ", conexion_controlProduccion)

        Dim ejecutarInsert As Integer
        Dim ejecutarActualizacion As Integer
        Dim ejecutarActualizacionMetas As Integer
        Dim ejecutarActualizacionTurnos As Integer
        Dim bandera1 As Boolean
        Dim bandera2 As Boolean

        'Actualizar tabla Turnos
        Dim actualizar_TurnoA As New SqlCommand("UPDATE Turno SET hora_inicio = @TA_inicio, hora_finalizacion= @TA_fin WHERE CodTurno = 'TA' ", conexion_controlProduccion)
        Dim actualizar_TurnoB As New SqlCommand("UPDATE Turno SET hora_inicio = @TB_inicio, hora_finalizacion= @TB_fin WHERE CodTurno = 'TB' ", conexion_controlProduccion)
        Dim actualizar_TurnoC As New SqlCommand("UPDATE Turno SET hora_inicio = @TC_inicio, hora_finalizacion= @TC_fin WHERE CodTurno = 'TC' ", conexion_controlProduccion)

        If (existeRegistroCaldera("LimitesCaldera") = False) Or (existeRegistroCaldera("LimiteMinCaldera") = False) Then
            Try

                If (existeRegistroCaldera("LimitesCaldera") = False) And (existeRegistroCaldera("LimiteMinCaldera") = False) Then

                    If (conexion_eTata.State = ConnectionState.Closed) Then
                        conexion_eTata.Open()
                    End If

                    If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                        conexion_controlProduccion.Open()
                    End If

                    ejecutarInsert = insertarLimMaxCaldera.ExecuteNonQuery()
                    ejecutarInsert = insertarLimMinCaldera.ExecuteNonQuery()

                    ejecutarActualizacionMetas = ActualizarMetaTorno.ExecuteNonQuery()
                    ejecutarActualizacionMetas = ActualizarMetaDescortezador.ExecuteNonQuery()
                    ejecutarActualizacionMetas = ActualizarMetaLijadora.ExecuteNonQuery()

                    'Actualizacion Tabla Turnos

                    'Agregamos los parametros a la consulta
                    actualizar_TurnoA.Parameters.AddWithValue("@TA_inicio", TA_HoraInicio.EditValue)
                    actualizar_TurnoA.Parameters.AddWithValue("@TA_fin", TA_HoraFin.EditValue)
                    actualizar_TurnoB.Parameters.AddWithValue("@TB_inicio", TB_HoraInicio.EditValue)
                    actualizar_TurnoB.Parameters.AddWithValue("@TB_fin", TB_HoraFin.EditValue)
                    actualizar_TurnoC.Parameters.AddWithValue("@TC_inicio", TC_HoraInicio.EditValue)
                    actualizar_TurnoC.Parameters.AddWithValue("@TC_fin", TC_HoraFin.EditValue)

                    ejecutarActualizacionTurnos = actualizar_TurnoA.ExecuteNonQuery()
                    ejecutarActualizacionTurnos = actualizar_TurnoB.ExecuteNonQuery()
                    ejecutarActualizacionTurnos = actualizar_TurnoC.ExecuteNonQuery()

                    'Guarda correos
                    If (Check_correo1.Checked = False And Check_correo2.Checked = False And Check_correo3.Checked = False And Check_correo4.Checked = False And Check_correo5.Checked = False) Then
                        MsgBox("Debe seleccionar un correo electrónico para enviar informe")
                    Else
                        guardar_correos()
                    End If

                    MsgBox("Datos guardados con éxito")
                    conexion_eTata.Close()
                Else
                    If (existeRegistroCaldera("LimitesCaldera") = False) Then

                        If (conexion_eTata.State = ConnectionState.Closed) Then
                            conexion_eTata.Open()
                        End If

                        ejecutarInsert = insertarLimMaxCaldera.ExecuteNonQuery()

                        bandera2 = True
                    Else

                        If (conexion_eTata.State = ConnectionState.Closed) Then
                            conexion_eTata.Open()
                        End If

                        ejecutarActualizacion = ActualizarMaxNivelDomo.ExecuteNonQuery()
                        ejecutarActualizacion = ActualizarMaxPresionDomo.ExecuteNonQuery()
                        ejecutarActualizacion = ActualizarMaxConsumoVapor.ExecuteNonQuery()
                        ejecutarActualizacion = ActualizarMaxTempAgua.ExecuteNonQuery()
                        ejecutarActualizacion = ActualizarMaxPresionAgua.ExecuteNonQuery()
                        ejecutarActualizacion = ActualizarMaxTempVapor.ExecuteNonQuery()

                        bandera1 = True
                    End If

                    If (existeRegistroCaldera("LimiteMinCaldera") = False) Then

                        If (conexion_eTata.State = ConnectionState.Closed) Then
                            conexion_eTata.Open()
                        End If

                        ejecutarInsert = insertarLimMinCaldera.ExecuteNonQuery()

                        If (bandera1 = True) Then
                            MsgBox("Datos insertados y actualizados con éxito")
                        Else
                            MsgBox("Error al actualizar los datos de la tabla 'LimitesCaldera'")
                        End If
                    Else

                        If (conexion_eTata.State = ConnectionState.Closed) Then
                            conexion_eTata.Open()
                        End If

                        ejecutarActualizacion = ActualizarMinNivelDomo.ExecuteNonQuery()
                        ejecutarActualizacion = ActualizarMinPresionDomo.ExecuteNonQuery()
                        ejecutarActualizacion = ActualizarMinConsumoVapor.ExecuteNonQuery()
                        ejecutarActualizacion = ActualizarMinTempAgua.ExecuteNonQuery()
                        ejecutarActualizacion = ActualizarMinPresionAgua.ExecuteNonQuery()
                        ejecutarActualizacion = ActualizarMinTempVapor.ExecuteNonQuery()

                        'Actualizacion Tabla Metas
                        If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                            conexion_controlProduccion.Open()
                        End If

                        ejecutarActualizacionMetas = ActualizarMetaTorno.ExecuteNonQuery()
                        ejecutarActualizacionMetas = ActualizarMetaDescortezador.ExecuteNonQuery()
                        ejecutarActualizacionMetas = ActualizarMetaLijadora.ExecuteNonQuery()

                        'Actualizacion Tabla Turnos

                        'Agregamos los parametros a la consulta
                        actualizar_TurnoA.Parameters.AddWithValue("@TA_inicio", TA_HoraInicio.EditValue)
                        actualizar_TurnoA.Parameters.AddWithValue("@TA_fin", TA_HoraFin.EditValue)
                        actualizar_TurnoB.Parameters.AddWithValue("@TB_inicio", TB_HoraInicio.EditValue)
                        actualizar_TurnoB.Parameters.AddWithValue("@TB_fin", TB_HoraFin.EditValue)
                        actualizar_TurnoC.Parameters.AddWithValue("@TC_inicio", TC_HoraInicio.EditValue)
                        actualizar_TurnoC.Parameters.AddWithValue("@TC_fin", TC_HoraFin.EditValue)

                        ejecutarActualizacionTurnos = actualizar_TurnoA.ExecuteNonQuery()
                        ejecutarActualizacionTurnos = actualizar_TurnoB.ExecuteNonQuery()
                        ejecutarActualizacionTurnos = actualizar_TurnoC.ExecuteNonQuery()

                        'Guarda correos
                        If (Check_correo1.Checked = False And Check_correo2.Checked = False And Check_correo3.Checked = False And Check_correo4.Checked = False And Check_correo5.Checked = False) Then
                            MsgBox("Debe seleccionar un correo electrónico para enviar informe")
                        Else
                            guardar_correos()
                        End If

                        If (bandera2 = True) Then
                            MsgBox("Datos insertados y actualizados con éxito")
                        Else
                            MsgBox("Error al insertar los datos de la tabla 'LimitesCaldera'")
                        End If
                    End If
                    conexion_controlProduccion.Close()
                    conexion_eTata.Close()
                End If
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al insertar los datos")
            End Try
        Else
            Try
                If (conexion_eTata.State = ConnectionState.Closed) Then
                    conexion_eTata.Open()
                End If

                If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                    conexion_controlProduccion.Open()
                End If

                ejecutarActualizacion = ActualizarMaxNivelDomo.ExecuteNonQuery()
                ejecutarActualizacion = ActualizarMaxPresionDomo.ExecuteNonQuery()
                ejecutarActualizacion = ActualizarMaxConsumoVapor.ExecuteNonQuery()
                ejecutarActualizacion = ActualizarMaxTempAgua.ExecuteNonQuery()
                ejecutarActualizacion = ActualizarMaxPresionAgua.ExecuteNonQuery()
                ejecutarActualizacion = ActualizarMaxTempVapor.ExecuteNonQuery()

                ejecutarActualizacion = ActualizarMinNivelDomo.ExecuteNonQuery()
                ejecutarActualizacion = ActualizarMinPresionDomo.ExecuteNonQuery()
                ejecutarActualizacion = ActualizarMinConsumoVapor.ExecuteNonQuery()
                ejecutarActualizacion = ActualizarMinTempAgua.ExecuteNonQuery()
                ejecutarActualizacion = ActualizarMinPresionAgua.ExecuteNonQuery()
                ejecutarActualizacion = ActualizarMinTempVapor.ExecuteNonQuery()

                'Actualizacion Tabla Metas
                ejecutarActualizacionMetas = ActualizarMetaTorno.ExecuteNonQuery()
                ejecutarActualizacionMetas = ActualizarMetaDescortezador.ExecuteNonQuery()
                ejecutarActualizacionMetas = ActualizarMetaLijadora.ExecuteNonQuery()

                'Actualizacion Tabla Turnos

                'Agregamos los parametros a la consulta
                actualizar_TurnoA.Parameters.AddWithValue("@TA_inicio", TA_HoraInicio.EditValue)
                actualizar_TurnoA.Parameters.AddWithValue("@TA_fin", TA_HoraFin.EditValue)
                actualizar_TurnoB.Parameters.AddWithValue("@TB_inicio", TB_HoraInicio.EditValue)
                actualizar_TurnoB.Parameters.AddWithValue("@TB_fin", TB_HoraFin.EditValue)
                actualizar_TurnoC.Parameters.AddWithValue("@TC_inicio", TC_HoraInicio.EditValue)
                actualizar_TurnoC.Parameters.AddWithValue("@TC_fin", TC_HoraFin.EditValue)

                ejecutarActualizacionTurnos = actualizar_TurnoA.ExecuteNonQuery()
                ejecutarActualizacionTurnos = actualizar_TurnoB.ExecuteNonQuery()
                ejecutarActualizacionTurnos = actualizar_TurnoC.ExecuteNonQuery()

                'Guarda correos
                If (Check_correo1.Checked = False And Check_correo2.Checked = False And Check_correo3.Checked = False And Check_correo4.Checked = False And Check_correo5.Checked = False) Then
                    MsgBox("Debe seleccionar un correo electrónico para enviar informe")
                Else
                    guardar_correos()
                End If

                MsgBox("Datos actualizados con éxito")
                conexion_controlProduccion.Close()
                conexion_eTata.Close()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error de Actualización de datos")
            End Try
        End If

        Me.Close()
    End Sub

    Private Sub guardar_correos()

        Dim MisDocumentos As String = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)
        Dim lista As String = ""

        Try
            'Ruta documento donde se guardaran los correos
            Dim ruta As String = MisDocumentos & "\Kimun\config.xml"

            Dim doc As XmlDocument = New XmlDocument
            doc.Load(ruta)
            doc.AsParallel()


            'Dim filePermissions = New FileIOPermission(FileIOPermissionAccess.AllAccess, ruta)
            'filePermissions.Assert()

            'Lee el xml creado anteriormente
            'Dim MiXml As New DOMDocument60
            'MiXml.load(ruta)

            'guarda en una cadena los correos seleccionados
            If (Check_correo1.Checked = True) Then
                lista = String.Format("{0}", valor_correo1.Text)
            End If

            If (Check_correo2.Checked = True) Then
                If (Check_correo1.Checked = True) Then
                    lista = String.Format("{0}, {1}", lista, valor_correo2.Text)
                Else
                    lista = String.Format("{0}", valor_correo2.Text)
                End If
            End If

            If (Check_correo3.Checked = True) Then
                If (Check_correo1.Checked = True Or Check_correo2.Checked = True) Then
                    lista = String.Format("{0}, {1}", lista, valor_correo3.Text)
                Else
                    lista = String.Format("{0}", valor_correo3.Text)
                End If
            End If

            If (Check_correo4.Checked = True) Then
                If (Check_correo1.Checked = True Or Check_correo2.Checked = True Or Check_correo3.Checked = True) Then
                    lista = String.Format("{0}, {1}", lista, valor_correo4.Text)
                Else
                    lista = String.Format("{0}", valor_correo4.Text)
                End If
            End If

            If (Check_correo5.Checked = True) Then
                If (Check_correo1.Checked = True Or Check_correo2.Checked = True Or Check_correo3.Checked = True Or Check_correo4.Checked = True) Then
                    lista = String.Format("{0}, {1}", lista, valor_correo5.Text)
                Else
                    lista = String.Format("{0}", valor_correo5.Text)
                End If
            End If


            'Modifica el XML
            'MiXml.selectSingleNode("Configuracion/Parametros/Correo").text = lista

            'Dim nodes As XmlNodeList = doc.SelectNodes("Configuracion/Parametros/Correo")

            doc.SelectSingleNode("Configuracion/Parametros/Correo").InnerText = lista

            doc.Save(ruta)



            'Guarda los datos en el archivo xml
            'MiXml.save(ruta)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al guardar correos")
        End Try
    End Sub


    'VALIDACION CORREO ELECTRONICO PESTAÑA CORREO
    Private Function validar_email(ByVal email As String) As Boolean
        Return Regex.IsMatch(email, "^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$")
    End Function

    Private Sub valor_correo1_Leave(sender As Object, e As EventArgs) Handles valor_correo1.Leave
        If (validar_email(valor_correo1.Text) = False And valor_correo1.Text <> "") Then
            MsgBox("Dirección de correo electrónico no válido, el correo debe tener el siguiente formato: nombre@dominio.com")
            valor_correo1.SelectAll()
            valor_correo1.Focus()
        End If
    End Sub

    Private Sub valor_correo2_Leave(sender As Object, e As EventArgs) Handles valor_correo2.Leave
        If (validar_email(valor_correo2.Text) = False And valor_correo2.Text <> "") Then
            MsgBox("Dirección de correo electrónico no válido, el correo debe tener el siguiente formato: nombre@dominio.com")
            valor_correo2.SelectAll()
            valor_correo2.Focus()
        End If
    End Sub

    Private Sub valor_correo3_Leave(sender As Object, e As EventArgs) Handles valor_correo3.Leave
        If (validar_email(valor_correo3.Text) = False And valor_correo3.Text <> "") Then
            MsgBox("Dirección de correo electrónico no válido, el correo debe tener el siguiente formato: nombre@dominio.com")
            valor_correo3.SelectAll()
            valor_correo3.Focus()
        End If
    End Sub

    Private Sub valor_correo4_Leave(sender As Object, e As EventArgs) Handles valor_correo4.Leave
        If (validar_email(valor_correo4.Text) = False And valor_correo4.Text <> "") Then
            MsgBox("Dirección de correo electrónico no válido, el correo debe tener el siguiente formato: nombre@dominio.com")
            valor_correo4.SelectAll()
            valor_correo4.Focus()
        End If
    End Sub

    Private Sub valor_correo5_Leave(sender As Object, e As EventArgs) Handles valor_correo5.Leave
        If (validar_email(valor_correo5.Text) = False And valor_correo5.Text <> "") Then
            MsgBox("Dirección de correo electrónico no válido, el correo debe tener el siguiente formato: nombre@dominio.com")
            valor_correo5.SelectAll()
            valor_correo5.Focus()
        End If
    End Sub

End Class