﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class PanelDeControl
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim SplashScreenManager1 As DevExpress.XtraSplashScreen.SplashScreenManager = New DevExpress.XtraSplashScreen.SplashScreenManager(Me, Nothing, True, True, True)
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PanelDeControl))
        Me.DefaultLookAndFeel1 = New DevExpress.LookAndFeel.DefaultLookAndFeel(Me.components)
        Me.Panel_Lineas = New System.Windows.Forms.Panel()
        Me.Panel_Verde = New System.Windows.Forms.Panel()
        Me.Historicos_Verde = New System.Windows.Forms.Button()
        Me.Panel_IndicadoresVerde = New System.Windows.Forms.Panel()
        Me.Boton_DetallesVerde = New System.Windows.Forms.Button()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.Label_Verde = New System.Windows.Forms.Label()
        Me.Panel_Lijadora = New System.Windows.Forms.Panel()
        Me.SeparatorControl4 = New DevExpress.XtraEditors.SeparatorControl()
        Me.Historicos_Lijadora = New System.Windows.Forms.Button()
        Me.Panel_IndicadoresLijadora = New System.Windows.Forms.Panel()
        Me.Boton_DetallesLijadora = New System.Windows.Forms.Button()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.Label_Lijadora = New System.Windows.Forms.Label()
        Me.Panel_Caldera = New System.Windows.Forms.Panel()
        Me.SeparatorControl3 = New DevExpress.XtraEditors.SeparatorControl()
        Me.Historicos_Caldera = New System.Windows.Forms.Button()
        Me.Panel_IndicadoresCaldera = New System.Windows.Forms.Panel()
        Me.Boton_DetallesCaldera = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.Label_Caldera = New System.Windows.Forms.Label()
        Me.Boton_Configuracion = New System.Windows.Forms.Button()
        Me.Boton_ConfiguracionServidor = New System.Windows.Forms.Button()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.cerrar_sesion = New System.Windows.Forms.Button()
        Me.Panel_turno = New System.Windows.Forms.Panel()
        Me.icono_notificacion = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.Label2 = New System.Windows.Forms.Label()
        Me.usuario = New System.Windows.Forms.Label()
        Me.Panel_Lineas.SuspendLayout()
        Me.Panel_Verde.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel_Lijadora.SuspendLayout()
        CType(Me.SeparatorControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.Panel_Caldera.SuspendLayout()
        CType(Me.SeparatorControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'SplashScreenManager1
        '
        SplashScreenManager1.ClosingDelay = 2000
        '
        'Panel_Lineas
        '
        Me.Panel_Lineas.BackColor = System.Drawing.Color.Transparent
        Me.Panel_Lineas.Controls.Add(Me.Panel_Verde)
        Me.Panel_Lineas.Controls.Add(Me.Panel_Lijadora)
        Me.Panel_Lineas.Controls.Add(Me.Panel_Caldera)
        Me.Panel_Lineas.Location = New System.Drawing.Point(12, 73)
        Me.Panel_Lineas.Name = "Panel_Lineas"
        Me.Panel_Lineas.Size = New System.Drawing.Size(1146, 460)
        Me.Panel_Lineas.TabIndex = 0
        '
        'Panel_Verde
        '
        Me.Panel_Verde.BackColor = System.Drawing.Color.Snow
        Me.Panel_Verde.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel_Verde.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_Verde.Controls.Add(Me.Historicos_Verde)
        Me.Panel_Verde.Controls.Add(Me.Panel_IndicadoresVerde)
        Me.Panel_Verde.Controls.Add(Me.Boton_DetallesVerde)
        Me.Panel_Verde.Controls.Add(Me.PictureBox3)
        Me.Panel_Verde.Controls.Add(Me.Label_Verde)
        Me.Panel_Verde.Location = New System.Drawing.Point(787, 3)
        Me.Panel_Verde.Name = "Panel_Verde"
        Me.Panel_Verde.Size = New System.Drawing.Size(331, 445)
        Me.Panel_Verde.TabIndex = 2
        '
        'Historicos_Verde
        '
        Me.Historicos_Verde.BackColor = System.Drawing.Color.SeaGreen
        Me.Historicos_Verde.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Historicos_Verde.FlatAppearance.BorderColor = System.Drawing.Color.SeaGreen
        Me.Historicos_Verde.Image = Global.Kimun.My.Resources.Resources.archivo
        Me.Historicos_Verde.Location = New System.Drawing.Point(284, 3)
        Me.Historicos_Verde.Name = "Historicos_Verde"
        Me.Historicos_Verde.Size = New System.Drawing.Size(38, 43)
        Me.Historicos_Verde.TabIndex = 14
        Me.ToolTip1.SetToolTip(Me.Historicos_Verde, "Datos historicos de la linea verde")
        Me.Historicos_Verde.UseVisualStyleBackColor = False
        '
        'Panel_IndicadoresVerde
        '
        Me.Panel_IndicadoresVerde.Location = New System.Drawing.Point(7, 69)
        Me.Panel_IndicadoresVerde.Name = "Panel_IndicadoresVerde"
        Me.Panel_IndicadoresVerde.Size = New System.Drawing.Size(315, 335)
        Me.Panel_IndicadoresVerde.TabIndex = 13
        '
        'Boton_DetallesVerde
        '
        Me.Boton_DetallesVerde.FlatAppearance.BorderColor = System.Drawing.Color.SeaGreen
        Me.Boton_DetallesVerde.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Boton_DetallesVerde.Location = New System.Drawing.Point(126, 406)
        Me.Boton_DetallesVerde.Name = "Boton_DetallesVerde"
        Me.Boton_DetallesVerde.Size = New System.Drawing.Size(75, 23)
        Me.Boton_DetallesVerde.TabIndex = 4
        Me.Boton_DetallesVerde.Text = "Detalles"
        Me.Boton_DetallesVerde.UseVisualStyleBackColor = True
        '
        'PictureBox3
        '
        Me.PictureBox3.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox3.Image = Global.Kimun.My.Resources.Resources._006_madera
        Me.PictureBox3.Location = New System.Drawing.Point(24, 13)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(52, 50)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 3
        Me.PictureBox3.TabStop = False
        '
        'Label_Verde
        '
        Me.Label_Verde.AutoSize = True
        Me.Label_Verde.BackColor = System.Drawing.Color.Transparent
        Me.Label_Verde.Font = New System.Drawing.Font("Arial", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Verde.ForeColor = System.Drawing.Color.SeaGreen
        Me.Label_Verde.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label_Verde.ImageKey = "(ninguno)"
        Me.Label_Verde.Location = New System.Drawing.Point(122, 25)
        Me.Label_Verde.Name = "Label_Verde"
        Me.Label_Verde.Size = New System.Drawing.Size(132, 26)
        Me.Label_Verde.TabIndex = 2
        Me.Label_Verde.Text = "Linea Verde"
        '
        'Panel_Lijadora
        '
        Me.Panel_Lijadora.BackColor = System.Drawing.Color.Snow
        Me.Panel_Lijadora.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel_Lijadora.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_Lijadora.Controls.Add(Me.SeparatorControl4)
        Me.Panel_Lijadora.Controls.Add(Me.Historicos_Lijadora)
        Me.Panel_Lijadora.Controls.Add(Me.Panel_IndicadoresLijadora)
        Me.Panel_Lijadora.Controls.Add(Me.Boton_DetallesLijadora)
        Me.Panel_Lijadora.Controls.Add(Me.PictureBox2)
        Me.Panel_Lijadora.Controls.Add(Me.Label_Lijadora)
        Me.Panel_Lijadora.Location = New System.Drawing.Point(411, 3)
        Me.Panel_Lijadora.Name = "Panel_Lijadora"
        Me.Panel_Lijadora.Size = New System.Drawing.Size(331, 445)
        Me.Panel_Lijadora.TabIndex = 1
        '
        'SeparatorControl4
        '
        Me.SeparatorControl4.AutoSizeMode = True
        Me.SeparatorControl4.LineThickness = 4
        Me.SeparatorControl4.Location = New System.Drawing.Point(7, 384)
        Me.SeparatorControl4.Name = "SeparatorControl4"
        Me.SeparatorControl4.Size = New System.Drawing.Size(315, 22)
        Me.SeparatorControl4.TabIndex = 13
        '
        'Historicos_Lijadora
        '
        Me.Historicos_Lijadora.BackColor = System.Drawing.Color.DarkCyan
        Me.Historicos_Lijadora.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Historicos_Lijadora.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan
        Me.Historicos_Lijadora.Image = Global.Kimun.My.Resources.Resources.archivo
        Me.Historicos_Lijadora.Location = New System.Drawing.Point(288, 3)
        Me.Historicos_Lijadora.Name = "Historicos_Lijadora"
        Me.Historicos_Lijadora.Size = New System.Drawing.Size(38, 43)
        Me.Historicos_Lijadora.TabIndex = 11
        Me.ToolTip1.SetToolTip(Me.Historicos_Lijadora, "Datos historicos de la lijadora")
        Me.Historicos_Lijadora.UseVisualStyleBackColor = False
        '
        'Panel_IndicadoresLijadora
        '
        Me.Panel_IndicadoresLijadora.Location = New System.Drawing.Point(7, 68)
        Me.Panel_IndicadoresLijadora.Name = "Panel_IndicadoresLijadora"
        Me.Panel_IndicadoresLijadora.Size = New System.Drawing.Size(315, 315)
        Me.Panel_IndicadoresLijadora.TabIndex = 10
        '
        'Boton_DetallesLijadora
        '
        Me.Boton_DetallesLijadora.FlatAppearance.BorderColor = System.Drawing.Color.DarkCyan
        Me.Boton_DetallesLijadora.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Boton_DetallesLijadora.Location = New System.Drawing.Point(126, 406)
        Me.Boton_DetallesLijadora.Name = "Boton_DetallesLijadora"
        Me.Boton_DetallesLijadora.Size = New System.Drawing.Size(75, 23)
        Me.Boton_DetallesLijadora.TabIndex = 3
        Me.Boton_DetallesLijadora.Text = "Detalles"
        Me.Boton_DetallesLijadora.UseVisualStyleBackColor = True
        '
        'PictureBox2
        '
        Me.PictureBox2.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox2.Image = Global.Kimun.My.Resources.Resources._007_sierra
        Me.PictureBox2.Location = New System.Drawing.Point(22, 13)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(52, 50)
        Me.PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox2.TabIndex = 2
        Me.PictureBox2.TabStop = False
        '
        'Label_Lijadora
        '
        Me.Label_Lijadora.AutoSize = True
        Me.Label_Lijadora.BackColor = System.Drawing.Color.Transparent
        Me.Label_Lijadora.Font = New System.Drawing.Font("Arial", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Lijadora.ForeColor = System.Drawing.Color.DarkCyan
        Me.Label_Lijadora.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label_Lijadora.ImageKey = "(ninguno)"
        Me.Label_Lijadora.Location = New System.Drawing.Point(118, 25)
        Me.Label_Lijadora.Name = "Label_Lijadora"
        Me.Label_Lijadora.Size = New System.Drawing.Size(94, 26)
        Me.Label_Lijadora.TabIndex = 1
        Me.Label_Lijadora.Text = "Lijadora"
        '
        'Panel_Caldera
        '
        Me.Panel_Caldera.AutoScroll = True
        Me.Panel_Caldera.AutoSize = True
        Me.Panel_Caldera.BackColor = System.Drawing.Color.Snow
        Me.Panel_Caldera.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Panel_Caldera.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Panel_Caldera.Controls.Add(Me.SeparatorControl3)
        Me.Panel_Caldera.Controls.Add(Me.Historicos_Caldera)
        Me.Panel_Caldera.Controls.Add(Me.Panel_IndicadoresCaldera)
        Me.Panel_Caldera.Controls.Add(Me.Boton_DetallesCaldera)
        Me.Panel_Caldera.Controls.Add(Me.PictureBox1)
        Me.Panel_Caldera.Controls.Add(Me.Label_Caldera)
        Me.Panel_Caldera.ForeColor = System.Drawing.SystemColors.ControlText
        Me.Panel_Caldera.Location = New System.Drawing.Point(30, 3)
        Me.Panel_Caldera.Name = "Panel_Caldera"
        Me.Panel_Caldera.Size = New System.Drawing.Size(331, 445)
        Me.Panel_Caldera.TabIndex = 0
        '
        'SeparatorControl3
        '
        Me.SeparatorControl3.AutoSizeMode = True
        Me.SeparatorControl3.Location = New System.Drawing.Point(7, 384)
        Me.SeparatorControl3.Name = "SeparatorControl3"
        Me.SeparatorControl3.Size = New System.Drawing.Size(315, 20)
        Me.SeparatorControl3.TabIndex = 9
        '
        'Historicos_Caldera
        '
        Me.Historicos_Caldera.BackColor = System.Drawing.Color.Tomato
        Me.Historicos_Caldera.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Historicos_Caldera.FlatAppearance.BorderColor = System.Drawing.Color.Tomato
        Me.Historicos_Caldera.Image = Global.Kimun.My.Resources.Resources.archivo
        Me.Historicos_Caldera.Location = New System.Drawing.Point(288, 3)
        Me.Historicos_Caldera.Name = "Historicos_Caldera"
        Me.Historicos_Caldera.Size = New System.Drawing.Size(38, 43)
        Me.Historicos_Caldera.TabIndex = 7
        Me.ToolTip1.SetToolTip(Me.Historicos_Caldera, "Datos historicos de la caldera")
        Me.Historicos_Caldera.UseVisualStyleBackColor = False
        '
        'Panel_IndicadoresCaldera
        '
        Me.Panel_IndicadoresCaldera.Location = New System.Drawing.Point(7, 69)
        Me.Panel_IndicadoresCaldera.Name = "Panel_IndicadoresCaldera"
        Me.Panel_IndicadoresCaldera.Size = New System.Drawing.Size(315, 315)
        Me.Panel_IndicadoresCaldera.TabIndex = 5
        '
        'Boton_DetallesCaldera
        '
        Me.Boton_DetallesCaldera.FlatAppearance.BorderColor = System.Drawing.Color.Tomato
        Me.Boton_DetallesCaldera.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Boton_DetallesCaldera.Location = New System.Drawing.Point(126, 406)
        Me.Boton_DetallesCaldera.Name = "Boton_DetallesCaldera"
        Me.Boton_DetallesCaldera.Size = New System.Drawing.Size(75, 23)
        Me.Boton_DetallesCaldera.TabIndex = 2
        Me.Boton_DetallesCaldera.Text = "Detalles"
        Me.Boton_DetallesCaldera.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.BackColor = System.Drawing.Color.Transparent
        Me.PictureBox1.Image = Global.Kimun.My.Resources.Resources.seguro1
        Me.PictureBox1.Location = New System.Drawing.Point(21, 13)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(52, 50)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'Label_Caldera
        '
        Me.Label_Caldera.AutoSize = True
        Me.Label_Caldera.BackColor = System.Drawing.Color.Transparent
        Me.Label_Caldera.Font = New System.Drawing.Font("Arial", 16.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Caldera.ForeColor = System.Drawing.Color.Tomato
        Me.Label_Caldera.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Label_Caldera.ImageKey = "(ninguno)"
        Me.Label_Caldera.Location = New System.Drawing.Point(121, 25)
        Me.Label_Caldera.Name = "Label_Caldera"
        Me.Label_Caldera.Size = New System.Drawing.Size(90, 26)
        Me.Label_Caldera.TabIndex = 0
        Me.Label_Caldera.Text = "Caldera"
        '
        'Boton_Configuracion
        '
        Me.Boton_Configuracion.AccessibleDescription = ""
        Me.Boton_Configuracion.AccessibleName = ""
        Me.Boton_Configuracion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Boton_Configuracion.FlatAppearance.BorderColor = System.Drawing.Color.SlateGray
        Me.Boton_Configuracion.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Boton_Configuracion.Image = Global.Kimun.My.Resources.Resources.rueda_dentada
        Me.Boton_Configuracion.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.Boton_Configuracion.Location = New System.Drawing.Point(969, 539)
        Me.Boton_Configuracion.Name = "Boton_Configuracion"
        Me.Boton_Configuracion.RightToLeft = System.Windows.Forms.RightToLeft.Yes
        Me.Boton_Configuracion.Size = New System.Drawing.Size(189, 48)
        Me.Boton_Configuracion.TabIndex = 1
        Me.Boton_Configuracion.Text = "Configuración"
        Me.Boton_Configuracion.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.Boton_Configuracion.UseCompatibleTextRendering = True
        Me.Boton_Configuracion.UseVisualStyleBackColor = True
        '
        'Boton_ConfiguracionServidor
        '
        Me.Boton_ConfiguracionServidor.AutoEllipsis = True
        Me.Boton_ConfiguracionServidor.BackColor = System.Drawing.Color.Transparent
        Me.Boton_ConfiguracionServidor.BackgroundImage = Global.Kimun.My.Resources.Resources.base_de_datos
        Me.Boton_ConfiguracionServidor.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Boton_ConfiguracionServidor.Location = New System.Drawing.Point(12, 539)
        Me.Boton_ConfiguracionServidor.Name = "Boton_ConfiguracionServidor"
        Me.Boton_ConfiguracionServidor.Size = New System.Drawing.Size(52, 48)
        Me.Boton_ConfiguracionServidor.TabIndex = 2
        Me.ToolTip1.SetToolTip(Me.Boton_ConfiguracionServidor, "Configurar conexión ")
        Me.Boton_ConfiguracionServidor.UseVisualStyleBackColor = False
        '
        'cerrar_sesion
        '
        Me.cerrar_sesion.AutoEllipsis = True
        Me.cerrar_sesion.BackColor = System.Drawing.Color.Transparent
        Me.cerrar_sesion.BackgroundImage = Global.Kimun.My.Resources.Resources.cerrar_sesion
        Me.cerrar_sesion.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.cerrar_sesion.Location = New System.Drawing.Point(94, 539)
        Me.cerrar_sesion.Name = "cerrar_sesion"
        Me.cerrar_sesion.Size = New System.Drawing.Size(52, 48)
        Me.cerrar_sesion.TabIndex = 6
        Me.ToolTip1.SetToolTip(Me.cerrar_sesion, "Cerrar Sesión ")
        Me.cerrar_sesion.UseVisualStyleBackColor = False
        '
        'Panel_turno
        '
        Me.Panel_turno.BackColor = System.Drawing.Color.Transparent
        Me.Panel_turno.Location = New System.Drawing.Point(458, 12)
        Me.Panel_turno.Name = "Panel_turno"
        Me.Panel_turno.Size = New System.Drawing.Size(321, 45)
        Me.Panel_turno.TabIndex = 3
        '
        'icono_notificacion
        '
        Me.icono_notificacion.Icon = CType(resources.GetObject("icono_notificacion.Icon"), System.Drawing.Icon)
        Me.icono_notificacion.Text = "Kimün"
        Me.icono_notificacion.Visible = True
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.BackColor = System.Drawing.Color.Transparent
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!)
        Me.Label2.Location = New System.Drawing.Point(13, 12)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(90, 17)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "Bienvenido/a"
        '
        'usuario
        '
        Me.usuario.AutoSize = True
        Me.usuario.BackColor = System.Drawing.Color.Transparent
        Me.usuario.Font = New System.Drawing.Font("Microsoft Sans Serif", 18.0!)
        Me.usuario.ForeColor = System.Drawing.Color.FromArgb(CType(CType(236, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.usuario.Location = New System.Drawing.Point(39, 29)
        Me.usuario.Name = "usuario"
        Me.usuario.Size = New System.Drawing.Size(144, 29)
        Me.usuario.TabIndex = 5
        Me.usuario.Text = "Daniel Peña"
        '
        'PanelDeControl
        '
        Me.AllowDrop = True
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.BackgroundImage = Global.Kimun.My.Resources.Resources.fondoPantalla
        Me.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.ClientSize = New System.Drawing.Size(1170, 599)
        Me.Controls.Add(Me.cerrar_sesion)
        Me.Controls.Add(Me.usuario)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Panel_turno)
        Me.Controls.Add(Me.Boton_ConfiguracionServidor)
        Me.Controls.Add(Me.Boton_Configuracion)
        Me.Controls.Add(Me.Panel_Lineas)
        Me.DoubleBuffered = True
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "PanelDeControl"
        Me.RightToLeftLayout = True
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Kimün"
        Me.Panel_Lineas.ResumeLayout(False)
        Me.Panel_Lineas.PerformLayout()
        Me.Panel_Verde.ResumeLayout(False)
        Me.Panel_Verde.PerformLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel_Lijadora.ResumeLayout(False)
        Me.Panel_Lijadora.PerformLayout()
        CType(Me.SeparatorControl4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.Panel_Caldera.ResumeLayout(False)
        Me.Panel_Caldera.PerformLayout()
        CType(Me.SeparatorControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents DefaultLookAndFeel1 As DevExpress.LookAndFeel.DefaultLookAndFeel
    Friend WithEvents Panel_Lineas As Panel
    Friend WithEvents Panel_Verde As Panel
    Friend WithEvents Panel_Lijadora As Panel
    Friend WithEvents Panel_Caldera As Panel
    Friend WithEvents Boton_Configuracion As Button
    Friend WithEvents Label_Caldera As Label
    Friend WithEvents Label_Verde As Label
    Friend WithEvents Label_Lijadora As Label
    Friend WithEvents PictureBox1 As PictureBox
    Friend WithEvents PictureBox2 As PictureBox
    Friend WithEvents PictureBox3 As PictureBox
    Friend WithEvents Boton_DetallesVerde As Button
    Friend WithEvents Boton_DetallesLijadora As Button
    Friend WithEvents Boton_DetallesCaldera As Button
    Friend WithEvents Panel_IndicadoresCaldera As Panel
    Friend WithEvents Panel_IndicadoresLijadora As Panel
    Friend WithEvents Panel_IndicadoresVerde As Panel
    Friend WithEvents Boton_ConfiguracionServidor As Button
    Friend WithEvents Historicos_Caldera As Button
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Historicos_Verde As Button
    Friend WithEvents Historicos_Lijadora As Button
    Friend WithEvents Panel_turno As Panel
    Public WithEvents icono_notificacion As NotifyIcon
    Friend WithEvents SeparatorControl4 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents SeparatorControl3 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents Label2 As Label
    Friend WithEvents usuario As Label
    Friend WithEvents cerrar_sesion As Button
End Class
