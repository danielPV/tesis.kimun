﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Lista_Usuarios
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Lista_Usuarios))
        Me.box_unidad = New System.Windows.Forms.TextBox()
        Me.box_password = New System.Windows.Forms.TextBox()
        Me.box_rol = New System.Windows.Forms.ComboBox()
        Me.box_cargo = New System.Windows.Forms.TextBox()
        Me.box_usuario = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.boton_editar = New System.Windows.Forms.Button()
        Me.boton_eliminar = New System.Windows.Forms.Button()
        Me.SeparatorControl1 = New DevExpress.XtraEditors.SeparatorControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.tabla_usuarios = New DevExpress.XtraGrid.GridControl()
        Me.Label1 = New System.Windows.Forms.Label()
        CType(Me.SeparatorControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.tabla_usuarios, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'box_unidad
        '
        Me.box_unidad.Location = New System.Drawing.Point(107, 191)
        Me.box_unidad.Name = "box_unidad"
        Me.box_unidad.Size = New System.Drawing.Size(141, 20)
        Me.box_unidad.TabIndex = 27
        '
        'box_password
        '
        Me.box_password.Location = New System.Drawing.Point(107, 159)
        Me.box_password.Name = "box_password"
        Me.box_password.Size = New System.Drawing.Size(141, 20)
        Me.box_password.TabIndex = 25
        '
        'box_rol
        '
        Me.box_rol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.box_rol.Items.AddRange(New Object() {"Administrador", "Observador"})
        Me.box_rol.Location = New System.Drawing.Point(107, 124)
        Me.box_rol.Name = "box_rol"
        Me.box_rol.Size = New System.Drawing.Size(141, 21)
        Me.box_rol.TabIndex = 24
        '
        'box_cargo
        '
        Me.box_cargo.Location = New System.Drawing.Point(107, 90)
        Me.box_cargo.Name = "box_cargo"
        Me.box_cargo.Size = New System.Drawing.Size(141, 20)
        Me.box_cargo.TabIndex = 23
        '
        'box_usuario
        '
        Me.box_usuario.Location = New System.Drawing.Point(107, 50)
        Me.box_usuario.Name = "box_usuario"
        Me.box_usuario.Size = New System.Drawing.Size(141, 20)
        Me.box_usuario.TabIndex = 22
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(8, 198)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 13)
        Me.Label6.TabIndex = 19
        Me.Label6.Text = "Unidad:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(8, 162)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 18
        Me.Label5.Text = "Contraseña:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 127)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(26, 13)
        Me.Label4.TabIndex = 17
        Me.Label4.Text = "Rol:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 93)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 16
        Me.Label3.Text = "Cargo:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 53)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 15
        Me.Label2.Text = "Nombre Usuario:"
        '
        'boton_editar
        '
        Me.boton_editar.Location = New System.Drawing.Point(70, 250)
        Me.boton_editar.Name = "boton_editar"
        Me.boton_editar.Size = New System.Drawing.Size(106, 36)
        Me.boton_editar.TabIndex = 29
        Me.boton_editar.Text = "Editar"
        Me.boton_editar.UseVisualStyleBackColor = True
        '
        'boton_eliminar
        '
        Me.boton_eliminar.Location = New System.Drawing.Point(520, 278)
        Me.boton_eliminar.Name = "boton_eliminar"
        Me.boton_eliminar.Size = New System.Drawing.Size(168, 23)
        Me.boton_eliminar.TabIndex = 30
        Me.boton_eliminar.Text = "Eliminar"
        Me.boton_eliminar.UseVisualStyleBackColor = True
        '
        'SeparatorControl1
        '
        Me.SeparatorControl1.LineOrientation = System.Windows.Forms.Orientation.Vertical
        Me.SeparatorControl1.Location = New System.Drawing.Point(250, 12)
        Me.SeparatorControl1.Name = "SeparatorControl1"
        Me.SeparatorControl1.Size = New System.Drawing.Size(25, 289)
        Me.SeparatorControl1.TabIndex = 31
        '
        'GridView4
        '
        Me.GridView4.ActiveFilterEnabled = False
        Me.GridView4.FocusRectStyle = DevExpress.XtraGrid.Views.Grid.DrawFocusRectStyle.RowFocus
        Me.GridView4.GridControl = Me.tabla_usuarios
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsBehavior.Editable = False
        Me.GridView4.OptionsPrint.ShowPrintExportProgress = False
        Me.GridView4.OptionsPrint.UsePrintStyles = False
        Me.GridView4.OptionsView.ShowGroupPanel = False
        Me.GridView4.PaintStyleName = "Skin"
        '
        'tabla_usuarios
        '
        Me.tabla_usuarios.Location = New System.Drawing.Point(277, 12)
        Me.tabla_usuarios.MainView = Me.GridView4
        Me.tabla_usuarios.Name = "tabla_usuarios"
        Me.tabla_usuarios.ShowOnlyPredefinedDetails = True
        Me.tabla_usuarios.Size = New System.Drawing.Size(657, 248)
        Me.tabla_usuarios.TabIndex = 28
        Me.tabla_usuarios.UseDisabledStatePainter = False
        Me.tabla_usuarios.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(11, 12)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(215, 13)
        Me.Label1.TabIndex = 32
        Me.Label1.Text = "Seleccione un usuario para editar o eliminar:"
        '
        'Lista_Usuarios
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(946, 313)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.SeparatorControl1)
        Me.Controls.Add(Me.boton_eliminar)
        Me.Controls.Add(Me.boton_editar)
        Me.Controls.Add(Me.tabla_usuarios)
        Me.Controls.Add(Me.box_unidad)
        Me.Controls.Add(Me.box_password)
        Me.Controls.Add(Me.box_rol)
        Me.Controls.Add(Me.box_cargo)
        Me.Controls.Add(Me.box_usuario)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Lista_Usuarios"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Lista De Usuarios"
        Me.TopMost = True
        CType(Me.SeparatorControl1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.tabla_usuarios, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents box_unidad As TextBox
    Friend WithEvents box_password As TextBox
    Friend WithEvents box_rol As ComboBox
    Friend WithEvents box_cargo As TextBox
    Friend WithEvents box_usuario As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents boton_editar As Button
    Friend WithEvents boton_eliminar As Button
    Friend WithEvents SeparatorControl1 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents tabla_usuarios As DevExpress.XtraGrid.GridControl
    Friend WithEvents Label1 As Label
End Class
