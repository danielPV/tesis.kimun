﻿
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraSplashScreen

Public Class Panel_Principal

    Public abierto As Boolean = False
    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        'abierto = True
    End Sub

    Private nombre As String

    Public Sub New(ByVal nom As String)
        Me.New()
        nombre = nom
    End Sub
    Private Sub Panel_Principal_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim c As Cursor = Me.Cursor

        nombre_usuario.Text = nombre

        abierto = True

        Try
            If (test_Conection("controlProduccion", GetConexion_controlProduccion()) = False And test_Conection("eTata", GetConexion_eTata()) = False) Then
                'No existe la cadena de conexión conectar_controlProduccion
                estado_controlProduccion.ForeColor = Color.Red
                estado_controlProduccion.Text = "No Conectado"
                estado_eTata.ForeColor = Color.Red
                estado_eTata.Text = "No Conectado"
                Me.Cursor = Cursors.WaitCursor
                Conectar_Servidor.Show()
            Else
                If (test_Conection("controlProduccion", GetConexion_controlProduccion()) = False) Then
                    estado_controlProduccion.ForeColor = Color.Red
                    estado_controlProduccion.Text = "No Conectado"
                    Me.Cursor = Cursors.WaitCursor
                    Conectar_Servidor.Show()
                Else
                    estado_controlProduccion.ForeColor = Color.Green
                    estado_controlProduccion.Text = "Conectado"
                    abrir_controlProduccion()
                    abrir_controlProduccion(GetConexion_controlProduccion())
                End If

                If (test_Conection("eTata", GetConexion_eTata()) = False) Then
                    estado_eTata.ForeColor = Color.Red
                    estado_eTata.Text = "No Conectado"
                    Me.Cursor = Cursors.WaitCursor
                    Conectar_Servidor.Show()
                Else
                    estado_eTata.ForeColor = Color.Green
                    estado_eTata.Text = "Conectado"
                    abrir_eTata()
                    abrir_eTata(GetConexion_eTata())
                End If
            End If
        Catch
            estado_controlProduccion.ForeColor = Color.Red
            estado_controlProduccion.Text = "No Conectado"

            estado_eTata.ForeColor = Color.Red
            estado_eTata.Text = "No Conectado"
            Me.Cursor = Cursors.WaitCursor
            Conectar_Servidor.Show()

        Finally
            Me.Cursor = c
        End Try

    End Sub

    Private Sub StatusBarToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles StatusBarToolStripMenuItem.Click
        Me.StatusStrip.Visible = Me.StatusBarToolStripMenuItem.Checked
    End Sub

    Private Sub CascadeToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles CascadeToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.Cascade)
    End Sub

    Private Sub TileVerticalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileVerticalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileVertical)
    End Sub

    Private Sub TileHorizontalToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles TileHorizontalToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.TileHorizontal)
    End Sub

    Private Sub ArrangeIconsToolStripMenuItem_Click(ByVal sender As Object, ByVal e As EventArgs) Handles ArrangeIconsToolStripMenuItem.Click
        Me.LayoutMdi(MdiLayout.ArrangeIcons)
    End Sub



    Private Sub AbrirPanelDeControlToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirPanelDeControlToolStripMenuItem.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Dim c As Cursor = Me.Cursor
            Dim panel As New PanelDeControl(nombre)

            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(SplashScreen1))
            panel.Show(Me)
            SplashScreenManager.CloseForm()
            Me.Cursor = c
        End If

    End Sub

    Private Sub AbrirConfiguraciónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AbrirConfiguraciónToolStripMenuItem.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        PanelConfiguracion.Show()
        Me.Cursor = c
    End Sub

    Private Sub ConfigurarConexiónToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ConfigurarConexiónToolStripMenuItem.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        Conectar_Servidor.Show()
        Me.Cursor = c
    End Sub

    Private Sub LineaVerdeToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles LineaVerdeToolStripMenuItem1.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Dim c As Cursor = Me.Cursor

            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))
            Panel_HistoricosVerde.Show()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        End If


    End Sub

    Private Sub LijadoraToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles LijadoraToolStripMenuItem1.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Dim c As Cursor = Me.Cursor

            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))
            Panel_HistoricosLijadora.Show()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        End If
    End Sub

    Private Sub CalderaToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles CalderaToolStripMenuItem1.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Dim c As Cursor = Me.Cursor

            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))
            Panel_HistoricosCaldera.Show()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        End If

    End Sub

    'REPORTES CALIDAD DE AGUAS
    Private Sub DiarioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DiarioToolStripMenuItem.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Try
                Dim c As Cursor = Me.Cursor

                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim reporte_diario As New Reporte_CalidadAguas

                Dim tool As New ReportPrintTool(reporte_diario)

                Dim final_dia As Date = String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

                'reporte_diario.SqlDataSource2.ConnectionName = "conectar_controlProduccion"
                reporte_diario.SqlDataSource2.Connection.ConnectionString = GetConexion_controlProduccion()

                reporte_diario.Parameters("Fecha1").Value = Format(Now, "yyyy/MM/dd")
                reporte_diario.Parameters("Fecha2").Value = final_dia

                reporte_diario.Parameters("Fecha1").Visible = False
                reporte_diario.Parameters("Fecha2").Visible = False

                'tool.PreviewForm.ControlBox = False
                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de calidad de aguas")
            End Try
        End If
    End Sub

    Private Sub MensualToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles MensualToolStripMenuItem.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else

            Try
                Dim c As Cursor = Me.Cursor

                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim reporte_mensual As New Reporte_CalidadAguas

                Dim tool As New ReportPrintTool(reporte_mensual)

                Dim final_mes As Date = String.Format("{0} {1}", Format(DateSerial(Year(Now), Month(Now) + 1, 0), "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

                'reporte_mensual.SqlDataSource2.ConnectionName = "conectar_controlProduccion"
                reporte_mensual.SqlDataSource2.Connection.ConnectionString = GetConexion_controlProduccion()

                reporte_mensual.Parameters("Fecha1").Value = DateSerial(Year(Now), Month(Now), 1)
                reporte_mensual.Parameters("Fecha2").Value = final_mes

                reporte_mensual.Parameters("Fecha1").Visible = False
                reporte_mensual.Parameters("Fecha2").Visible = False

                'tool.PreviewForm.ControlBox = False
                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de calidad de aguas")
            End Try
        End If
    End Sub

    Private Sub RangosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles RangosToolStripMenuItem.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Dim cadena As String = "calidad"
            Dim enviar As New generar_rangos(cadena)

            enviar.Show()
        End If

    End Sub

    Private Sub RangosToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles RangosToolStripMenuItem1.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Try
                Dim c As Cursor = Me.Cursor

                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_eTata(GetConexion_eTata())

                Dim reporte_diario As New Reporte_IndicadoresCaldera

                Dim tool As New ReportPrintTool(reporte_diario)

                Dim final_dia As Date = String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

                'reporte_diario.SqlDataSource2.ConnectionName = "conectar_eTata"
                reporte_diario.SqlDataSource2.Connection.ConnectionString = GetConexion_eTata()

                reporte_diario.Parameters("fecha1").Value = Format(Now, "yyyy/MM/dd")
                reporte_diario.Parameters("fecha2").Value = final_dia

                reporte_diario.Parameters("fecha1").Visible = False
                reporte_diario.Parameters("fecha2").Visible = False

                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de indicadores de la caldera")
            End Try

        End If
    End Sub

    Private Sub DiariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles DiariosToolStripMenuItem.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Try
                Dim c As Cursor = Me.Cursor

                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_eTata(GetConexion_eTata())

                Dim reporte_mensual As New Reporte_IndicadoresCaldera

                Dim tool As New ReportPrintTool(reporte_mensual)

                Dim final_mes As Date = String.Format("{0} {1}", Format(DateSerial(Year(Now), Month(Now) + 1, 0), "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

                'reporte_mensual.SqlDataSource2.ConnectionName = "conectar_eTata"
                reporte_mensual.SqlDataSource2.Connection.ConnectionString = GetConexion_eTata()

                reporte_mensual.Parameters("fecha1").Value = DateSerial(Year(Now), Month(Now), 1)
                reporte_mensual.Parameters("fecha2").Value = final_mes

                reporte_mensual.Parameters("fecha1").Visible = False
                reporte_mensual.Parameters("fecha2").Visible = False

                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de indicadores de la caldera")
            End Try

        End If
    End Sub

    Private Sub MensualToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles MensualToolStripMenuItem1.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Dim cadena As String = "indicadores"
            Dim enviar As New generar_rangos(cadena)

            enviar.Show()
        End If

    End Sub

    'REPORTES LINEA VERDE
    Private Sub DiarioToolStripMenuItem1_Click(sender As Object, e As EventArgs) Handles DiarioToolStripMenuItem1.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Try
                Dim c As Cursor = Me.Cursor

                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim reporte_diario As New Reporte_LineaVerde

                Dim tool As New ReportPrintTool(reporte_diario)

                Dim final_dia As Date = String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

                'reporte_diario.SqlDataSource1.ConnectionName = "conectar_controlProduccion"
                reporte_diario.SqlDataSource1.Connection.ConnectionString = GetConexion_controlProduccion()

                reporte_diario.Parameters("fecha1").Value = Format(Now, "yyyy/MM/dd")
                reporte_diario.Parameters("fecha2").Value = final_dia

                reporte_diario.Parameters("fecha1").Visible = False
                reporte_diario.Parameters("fecha2").Visible = False

                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de la linea verde")
            End Try

        End If

    End Sub

    Private Sub MensualToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles MensualToolStripMenuItem2.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Try
                Dim c As Cursor = Me.Cursor

                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim reporte_mensual As New Reporte_LineaVerde

                Dim tool As New ReportPrintTool(reporte_mensual)

                Dim final_mes As Date = String.Format("{0} {1}", Format(DateSerial(Year(Now), Month(Now) + 1, 0), "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

                'reporte_mensual.SqlDataSource1.ConnectionName = "conectar_controlProduccion"
                reporte_mensual.SqlDataSource1.Connection.ConnectionString = GetConexion_controlProduccion()

                reporte_mensual.Parameters("fecha1").Value = DateSerial(Year(Now), Month(Now), 1)
                reporte_mensual.Parameters("fecha2").Value = final_mes

                reporte_mensual.Parameters("fecha1").Visible = False
                reporte_mensual.Parameters("fecha2").Visible = False

                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de la linea verde")
            End Try

        End If

    End Sub

    Private Sub RangosToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles RangosToolStripMenuItem2.Click

        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Dim cadena As String = "verde"
            Dim enviar As New generar_rangos(cadena)
            enviar.Show()
        End If
    End Sub


    Private Sub AgregarUsuarioToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles AgregarUsuarioToolStripMenuItem.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        Crear_Usuario.Show()
        Me.Cursor = c
    End Sub

    Private Sub ListaDeUsuariosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles ListaDeUsuariosToolStripMenuItem.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        Lista_Usuarios.Show()
        Me.Cursor = c
    End Sub

    Private Sub DiarioToolStripMenuItem2_Click(sender As Object, e As EventArgs) Handles DiarioToolStripMenuItem2.Click
        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Try
                Dim c As Cursor = Me.Cursor
                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim reporte_diario As New Reporte_Lijadora

                Dim tool As New ReportPrintTool(reporte_diario)

                Dim final_dia As Date = String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

                'reporte_diario.SqlDataSource1.ConnectionName = "conectar_controlProduccion"
                reporte_diario.SqlDataSource1.Connection.ConnectionString = GetConexion_controlProduccion()

                reporte_diario.Parameters("fecha1").Value = Format(Now, "yyyy/MM/dd")
                reporte_diario.Parameters("fecha2").Value = final_dia

                reporte_diario.Parameters("fecha1").Visible = False
                reporte_diario.Parameters("fecha2").Visible = False

                'tool.PreviewForm.ControlBox = False
                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de la lijadora")
            End Try

        End If
    End Sub

    Private Sub MensualToolStripMenuItem3_Click(sender As Object, e As EventArgs) Handles MensualToolStripMenuItem3.Click
        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Try
                Dim c As Cursor = Me.Cursor
                Me.Cursor = Cursors.WaitCursor
                SplashScreenManager.ShowForm(GetType(WaitForm1))

                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim reporte_mensual As New Reporte_Lijadora

                Dim tool As New ReportPrintTool(reporte_mensual)

                Dim final_mes As Date = String.Format("{0} {1}", Format(DateSerial(Year(Now), Month(Now) + 1, 0), "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

                'reporte_mensual.SqlDataSource1.ConnectionName = "conectar_controlProduccion"
                reporte_mensual.SqlDataSource1.Connection.ConnectionString = GetConexion_controlProduccion()

                reporte_mensual.Parameters("fecha1").Value = DateSerial(Year(Now), Month(Now), 1)
                reporte_mensual.Parameters("fecha2").Value = final_mes

                reporte_mensual.Parameters("fecha1").Visible = False
                reporte_mensual.Parameters("fecha2").Visible = False

                'tool.PreviewForm.ControlBox = False
                tool.PreviewForm.Hide()
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
                tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
                tool.ShowPreview()
                Me.Cursor = c
                SplashScreenManager.CloseForm()
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de la lijadora")
            End Try

        End If
    End Sub

    Private Sub PorRangosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PorRangosToolStripMenuItem.Click
        Dim funciones As New Funciones

        If (funciones.existeTabla_eTata("LimitesCaldera") = False Or funciones.existeTabla_eTata("LimiteMinCaldera") = False) Then
            MsgBox("Por favor configure los datos necesarios para el correcto funcionamiento del software")
            PanelConfiguracion.Show()
        Else
            Dim cadena As String = "lijadora"
            Dim enviar As New generar_rangos(cadena)
            enviar.Show()
        End If
    End Sub
End Class
