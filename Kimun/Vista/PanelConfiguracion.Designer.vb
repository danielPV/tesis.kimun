﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class PanelConfiguracion
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(PanelConfiguracion))
        Me.Button_Guardar = New System.Windows.Forms.Button()
        Me.Valor_MetaLijadora = New System.Windows.Forms.TextBox()
        Me.Valor_MetaTorno = New System.Windows.Forms.TextBox()
        Me.Valor_MetaDescortezador = New System.Windows.Forms.TextBox()
        Me.Label21 = New System.Windows.Forms.Label()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.Label19 = New System.Windows.Forms.Label()
        Me.TC_HoraFin = New DevExpress.XtraEditors.TimeSpanEdit()
        Me.TC_HoraInicio = New DevExpress.XtraEditors.TimeSpanEdit()
        Me.TB_HoraFin = New DevExpress.XtraEditors.TimeSpanEdit()
        Me.TB_HoraInicio = New DevExpress.XtraEditors.TimeSpanEdit()
        Me.TA_HoraFin = New DevExpress.XtraEditors.TimeSpanEdit()
        Me.Label26 = New System.Windows.Forms.Label()
        Me.Label25 = New System.Windows.Forms.Label()
        Me.Label22 = New System.Windows.Forms.Label()
        Me.Label23 = New System.Windows.Forms.Label()
        Me.Label24 = New System.Windows.Forms.Label()
        Me.TA_HoraInicio = New DevExpress.XtraEditors.TimeSpanEdit()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.LabelControl12 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl11 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl10 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl9 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl7 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl6 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl5 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl4 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl3 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl2 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl1 = New DevExpress.XtraEditors.LabelControl()
        Me.LabelControl8 = New DevExpress.XtraEditors.LabelControl()
        Me.Label16 = New System.Windows.Forms.Label()
        Me.Label17 = New System.Windows.Forms.Label()
        Me.Min_TempVapor = New System.Windows.Forms.TextBox()
        Me.Max_TempVapor = New System.Windows.Forms.TextBox()
        Me.Label18 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Min_PresionAgua = New System.Windows.Forms.TextBox()
        Me.Max_PresionAgua = New System.Windows.Forms.TextBox()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Min_TempAgua = New System.Windows.Forms.TextBox()
        Me.Max_TempAgua = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Min_ConsumoVapor = New System.Windows.Forms.TextBox()
        Me.Max_ConsumoVapor = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Min_PresionDomo = New System.Windows.Forms.TextBox()
        Me.Max_PresionDomo = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Min_NivelDomo = New System.Windows.Forms.TextBox()
        Me.Max_NivelDomo = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.Label35 = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.Label33 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.valor_correo5 = New DevExpress.XtraEditors.TextEdit()
        Me.valor_correo4 = New DevExpress.XtraEditors.TextEdit()
        Me.valor_correo3 = New DevExpress.XtraEditors.TextEdit()
        Me.valor_correo2 = New DevExpress.XtraEditors.TextEdit()
        Me.valor_correo1 = New DevExpress.XtraEditors.TextEdit()
        Me.Check_correo5 = New System.Windows.Forms.CheckBox()
        Me.Check_correo4 = New System.Windows.Forms.CheckBox()
        Me.Check_correo3 = New System.Windows.Forms.CheckBox()
        Me.Check_correo2 = New System.Windows.Forms.CheckBox()
        Me.Check_correo1 = New System.Windows.Forms.CheckBox()
        Me.Label32 = New System.Windows.Forms.Label()
        CType(Me.TC_HoraFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TC_HoraInicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TB_HoraFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TB_HoraInicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TA_HoraFin.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.TA_HoraInicio.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        Me.TabPage4.SuspendLayout()
        CType(Me.valor_correo5.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.valor_correo4.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.valor_correo3.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.valor_correo2.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.valor_correo1.Properties, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button_Guardar
        '
        Me.Button_Guardar.FlatAppearance.BorderColor = System.Drawing.SystemColors.MenuHighlight
        Me.Button_Guardar.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button_Guardar.Location = New System.Drawing.Point(179, 333)
        Me.Button_Guardar.Name = "Button_Guardar"
        Me.Button_Guardar.Size = New System.Drawing.Size(78, 28)
        Me.Button_Guardar.TabIndex = 1
        Me.Button_Guardar.Text = "Guardar "
        Me.Button_Guardar.UseVisualStyleBackColor = True
        '
        'Valor_MetaLijadora
        '
        Me.Valor_MetaLijadora.Location = New System.Drawing.Point(204, 180)
        Me.Valor_MetaLijadora.Name = "Valor_MetaLijadora"
        Me.Valor_MetaLijadora.Size = New System.Drawing.Size(65, 20)
        Me.Valor_MetaLijadora.TabIndex = 6
        '
        'Valor_MetaTorno
        '
        Me.Valor_MetaTorno.Location = New System.Drawing.Point(204, 97)
        Me.Valor_MetaTorno.Name = "Valor_MetaTorno"
        Me.Valor_MetaTorno.Size = New System.Drawing.Size(65, 20)
        Me.Valor_MetaTorno.TabIndex = 5
        '
        'Valor_MetaDescortezador
        '
        Me.Valor_MetaDescortezador.Location = New System.Drawing.Point(204, 139)
        Me.Valor_MetaDescortezador.Name = "Valor_MetaDescortezador"
        Me.Valor_MetaDescortezador.Size = New System.Drawing.Size(65, 20)
        Me.Valor_MetaDescortezador.TabIndex = 4
        '
        'Label21
        '
        Me.Label21.AutoSize = True
        Me.Label21.Location = New System.Drawing.Point(89, 183)
        Me.Label21.Name = "Label21"
        Me.Label21.Size = New System.Drawing.Size(77, 13)
        Me.Label21.TabIndex = 3
        Me.Label21.Text = "Meta Lijadora: "
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Location = New System.Drawing.Point(89, 142)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(109, 13)
        Me.Label20.TabIndex = 2
        Me.Label20.Text = "Meta Descortezador: "
        '
        'Label19
        '
        Me.Label19.AutoSize = True
        Me.Label19.Location = New System.Drawing.Point(89, 100)
        Me.Label19.Name = "Label19"
        Me.Label19.Size = New System.Drawing.Size(65, 13)
        Me.Label19.TabIndex = 1
        Me.Label19.Text = "Meta Torno:"
        '
        'TC_HoraFin
        '
        Me.TC_HoraFin.EditValue = Nothing
        Me.TC_HoraFin.Location = New System.Drawing.Point(246, 188)
        Me.TC_HoraFin.Name = "TC_HoraFin"
        Me.TC_HoraFin.Properties.AllowEditDays = False
        Me.TC_HoraFin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TC_HoraFin.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
        Me.TC_HoraFin.Properties.Mask.EditMask = "HH:mm:ss"
        Me.TC_HoraFin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
        Me.TC_HoraFin.Size = New System.Drawing.Size(100, 20)
        Me.TC_HoraFin.TabIndex = 14
        '
        'TC_HoraInicio
        '
        Me.TC_HoraInicio.EditValue = Nothing
        Me.TC_HoraInicio.Location = New System.Drawing.Point(114, 188)
        Me.TC_HoraInicio.Name = "TC_HoraInicio"
        Me.TC_HoraInicio.Properties.AllowEditDays = False
        Me.TC_HoraInicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TC_HoraInicio.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
        Me.TC_HoraInicio.Properties.Mask.EditMask = "HH:mm:ss"
        Me.TC_HoraInicio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
        Me.TC_HoraInicio.Size = New System.Drawing.Size(100, 20)
        Me.TC_HoraInicio.TabIndex = 13
        '
        'TB_HoraFin
        '
        Me.TB_HoraFin.EditValue = Nothing
        Me.TB_HoraFin.Location = New System.Drawing.Point(246, 151)
        Me.TB_HoraFin.Name = "TB_HoraFin"
        Me.TB_HoraFin.Properties.AllowEditDays = False
        Me.TB_HoraFin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TB_HoraFin.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
        Me.TB_HoraFin.Properties.Mask.EditMask = "HH:mm:ss"
        Me.TB_HoraFin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
        Me.TB_HoraFin.Size = New System.Drawing.Size(100, 20)
        Me.TB_HoraFin.TabIndex = 12
        '
        'TB_HoraInicio
        '
        Me.TB_HoraInicio.EditValue = Nothing
        Me.TB_HoraInicio.Location = New System.Drawing.Point(114, 151)
        Me.TB_HoraInicio.Name = "TB_HoraInicio"
        Me.TB_HoraInicio.Properties.AllowEditDays = False
        Me.TB_HoraInicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TB_HoraInicio.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
        Me.TB_HoraInicio.Properties.Mask.EditMask = "HH:mm:ss"
        Me.TB_HoraInicio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
        Me.TB_HoraInicio.Size = New System.Drawing.Size(100, 20)
        Me.TB_HoraInicio.TabIndex = 11
        '
        'TA_HoraFin
        '
        Me.TA_HoraFin.EditValue = Nothing
        Me.TA_HoraFin.Location = New System.Drawing.Point(246, 114)
        Me.TA_HoraFin.Name = "TA_HoraFin"
        Me.TA_HoraFin.Properties.AllowEditDays = False
        Me.TA_HoraFin.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TA_HoraFin.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
        Me.TA_HoraFin.Properties.Mask.EditMask = "HH:mm:ss"
        Me.TA_HoraFin.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
        Me.TA_HoraFin.Size = New System.Drawing.Size(100, 20)
        Me.TA_HoraFin.TabIndex = 10
        '
        'Label26
        '
        Me.Label26.AutoSize = True
        Me.Label26.Location = New System.Drawing.Point(251, 90)
        Me.Label26.Name = "Label26"
        Me.Label26.Size = New System.Drawing.Size(88, 13)
        Me.Label26.TabIndex = 8
        Me.Label26.Text = "Hora Finalizacion"
        '
        'Label25
        '
        Me.Label25.AutoSize = True
        Me.Label25.Location = New System.Drawing.Point(130, 90)
        Me.Label25.Name = "Label25"
        Me.Label25.Size = New System.Drawing.Size(58, 13)
        Me.Label25.TabIndex = 7
        Me.Label25.Text = "Hora Inicio"
        '
        'Label22
        '
        Me.Label22.AutoSize = True
        Me.Label22.Location = New System.Drawing.Point(50, 191)
        Me.Label22.Name = "Label22"
        Me.Label22.Size = New System.Drawing.Size(51, 13)
        Me.Label22.TabIndex = 3
        Me.Label22.Text = "Turno C: "
        '
        'Label23
        '
        Me.Label23.AutoSize = True
        Me.Label23.Location = New System.Drawing.Point(50, 154)
        Me.Label23.Name = "Label23"
        Me.Label23.Size = New System.Drawing.Size(51, 13)
        Me.Label23.TabIndex = 2
        Me.Label23.Text = "Turno B: "
        '
        'Label24
        '
        Me.Label24.AutoSize = True
        Me.Label24.Location = New System.Drawing.Point(50, 117)
        Me.Label24.Name = "Label24"
        Me.Label24.Size = New System.Drawing.Size(48, 13)
        Me.Label24.TabIndex = 1
        Me.Label24.Text = "Turno A:"
        '
        'TA_HoraInicio
        '
        Me.TA_HoraInicio.EditValue = Nothing
        Me.TA_HoraInicio.Location = New System.Drawing.Point(114, 114)
        Me.TA_HoraInicio.Name = "TA_HoraInicio"
        Me.TA_HoraInicio.Properties.AllowEditDays = False
        Me.TA_HoraInicio.Properties.Buttons.AddRange(New DevExpress.XtraEditors.Controls.EditorButton() {New DevExpress.XtraEditors.Controls.EditorButton(DevExpress.XtraEditors.Controls.ButtonPredefines.Combo)})
        Me.TA_HoraInicio.Properties.EditValueChangedFiringMode = DevExpress.XtraEditors.Controls.EditValueChangedFiringMode.[Default]
        Me.TA_HoraInicio.Properties.Mask.EditMask = "HH:mm:ss"
        Me.TA_HoraInicio.Properties.Mask.MaskType = DevExpress.XtraEditors.Mask.MaskType.None
        Me.TA_HoraInicio.Size = New System.Drawing.Size(100, 20)
        Me.TA_HoraInicio.TabIndex = 9
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(2, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(434, 323)
        Me.TabControl1.TabIndex = 4
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.LabelControl12)
        Me.TabPage1.Controls.Add(Me.LabelControl11)
        Me.TabPage1.Controls.Add(Me.LabelControl10)
        Me.TabPage1.Controls.Add(Me.LabelControl9)
        Me.TabPage1.Controls.Add(Me.LabelControl7)
        Me.TabPage1.Controls.Add(Me.LabelControl6)
        Me.TabPage1.Controls.Add(Me.LabelControl5)
        Me.TabPage1.Controls.Add(Me.LabelControl4)
        Me.TabPage1.Controls.Add(Me.LabelControl3)
        Me.TabPage1.Controls.Add(Me.LabelControl2)
        Me.TabPage1.Controls.Add(Me.LabelControl1)
        Me.TabPage1.Controls.Add(Me.LabelControl8)
        Me.TabPage1.Controls.Add(Me.Label16)
        Me.TabPage1.Controls.Add(Me.Label17)
        Me.TabPage1.Controls.Add(Me.Min_TempVapor)
        Me.TabPage1.Controls.Add(Me.Max_TempVapor)
        Me.TabPage1.Controls.Add(Me.Label18)
        Me.TabPage1.Controls.Add(Me.Label13)
        Me.TabPage1.Controls.Add(Me.Label14)
        Me.TabPage1.Controls.Add(Me.Min_PresionAgua)
        Me.TabPage1.Controls.Add(Me.Max_PresionAgua)
        Me.TabPage1.Controls.Add(Me.Label15)
        Me.TabPage1.Controls.Add(Me.Label10)
        Me.TabPage1.Controls.Add(Me.Label11)
        Me.TabPage1.Controls.Add(Me.Min_TempAgua)
        Me.TabPage1.Controls.Add(Me.Max_TempAgua)
        Me.TabPage1.Controls.Add(Me.Label12)
        Me.TabPage1.Controls.Add(Me.Label7)
        Me.TabPage1.Controls.Add(Me.Label8)
        Me.TabPage1.Controls.Add(Me.Min_ConsumoVapor)
        Me.TabPage1.Controls.Add(Me.Max_ConsumoVapor)
        Me.TabPage1.Controls.Add(Me.Label9)
        Me.TabPage1.Controls.Add(Me.Label4)
        Me.TabPage1.Controls.Add(Me.Label5)
        Me.TabPage1.Controls.Add(Me.Min_PresionDomo)
        Me.TabPage1.Controls.Add(Me.Max_PresionDomo)
        Me.TabPage1.Controls.Add(Me.Label6)
        Me.TabPage1.Controls.Add(Me.Label3)
        Me.TabPage1.Controls.Add(Me.Label2)
        Me.TabPage1.Controls.Add(Me.Min_NivelDomo)
        Me.TabPage1.Controls.Add(Me.Max_NivelDomo)
        Me.TabPage1.Controls.Add(Me.Label1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(426, 297)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "Límites Caldera "
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'LabelControl12
        '
        Me.LabelControl12.Location = New System.Drawing.Point(395, 257)
        Me.LabelControl12.Name = "LabelControl12"
        Me.LabelControl12.Size = New System.Drawing.Size(12, 13)
        Me.LabelControl12.TabIndex = 91
        Me.LabelControl12.Text = "°C"
        '
        'LabelControl11
        '
        Me.LabelControl11.Location = New System.Drawing.Point(247, 257)
        Me.LabelControl11.Name = "LabelControl11"
        Me.LabelControl11.Size = New System.Drawing.Size(12, 13)
        Me.LabelControl11.TabIndex = 102
        Me.LabelControl11.Text = "°C"
        '
        'LabelControl10
        '
        Me.LabelControl10.Location = New System.Drawing.Point(395, 213)
        Me.LabelControl10.Name = "LabelControl10"
        Me.LabelControl10.Size = New System.Drawing.Size(16, 13)
        Me.LabelControl10.TabIndex = 101
        Me.LabelControl10.Text = "Bar"
        '
        'LabelControl9
        '
        Me.LabelControl9.Location = New System.Drawing.Point(247, 213)
        Me.LabelControl9.Name = "LabelControl9"
        Me.LabelControl9.Size = New System.Drawing.Size(16, 13)
        Me.LabelControl9.TabIndex = 100
        Me.LabelControl9.Text = "Bar"
        '
        'LabelControl7
        '
        Me.LabelControl7.Location = New System.Drawing.Point(395, 171)
        Me.LabelControl7.Name = "LabelControl7"
        Me.LabelControl7.Size = New System.Drawing.Size(12, 13)
        Me.LabelControl7.TabIndex = 99
        Me.LabelControl7.Text = "°C"
        '
        'LabelControl6
        '
        Me.LabelControl6.Location = New System.Drawing.Point(247, 171)
        Me.LabelControl6.Name = "LabelControl6"
        Me.LabelControl6.Size = New System.Drawing.Size(12, 13)
        Me.LabelControl6.TabIndex = 98
        Me.LabelControl6.Text = "°C"
        '
        'LabelControl5
        '
        Me.LabelControl5.Location = New System.Drawing.Point(395, 130)
        Me.LabelControl5.Name = "LabelControl5"
        Me.LabelControl5.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl5.TabIndex = 97
        Me.LabelControl5.Text = "(T/H)"
        '
        'LabelControl4
        '
        Me.LabelControl4.Location = New System.Drawing.Point(247, 130)
        Me.LabelControl4.Name = "LabelControl4"
        Me.LabelControl4.Size = New System.Drawing.Size(25, 13)
        Me.LabelControl4.TabIndex = 96
        Me.LabelControl4.Text = "(T/H)"
        '
        'LabelControl3
        '
        Me.LabelControl3.Location = New System.Drawing.Point(395, 89)
        Me.LabelControl3.Name = "LabelControl3"
        Me.LabelControl3.Size = New System.Drawing.Size(16, 13)
        Me.LabelControl3.TabIndex = 95
        Me.LabelControl3.Text = "Bar"
        '
        'LabelControl2
        '
        Me.LabelControl2.Location = New System.Drawing.Point(247, 89)
        Me.LabelControl2.Name = "LabelControl2"
        Me.LabelControl2.Size = New System.Drawing.Size(16, 13)
        Me.LabelControl2.TabIndex = 94
        Me.LabelControl2.Text = "Bar"
        '
        'LabelControl1
        '
        Me.LabelControl1.Location = New System.Drawing.Point(395, 47)
        Me.LabelControl1.Name = "LabelControl1"
        Me.LabelControl1.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl1.TabIndex = 93
        Me.LabelControl1.Text = "%"
        '
        'LabelControl8
        '
        Me.LabelControl8.Location = New System.Drawing.Point(247, 47)
        Me.LabelControl8.Name = "LabelControl8"
        Me.LabelControl8.Size = New System.Drawing.Size(11, 13)
        Me.LabelControl8.TabIndex = 92
        Me.LabelControl8.Text = "%"
        '
        'Label16
        '
        Me.Label16.AutoSize = True
        Me.Label16.Location = New System.Drawing.Point(328, 238)
        Me.Label16.Name = "Label16"
        Me.Label16.Size = New System.Drawing.Size(26, 13)
        Me.Label16.TabIndex = 90
        Me.Label16.Text = "Mín"
        '
        'Label17
        '
        Me.Label17.AutoSize = True
        Me.Label17.Location = New System.Drawing.Point(178, 238)
        Me.Label17.Name = "Label17"
        Me.Label17.Size = New System.Drawing.Size(27, 13)
        Me.Label17.TabIndex = 89
        Me.Label17.Text = "Máx"
        '
        'Min_TempVapor
        '
        Me.Min_TempVapor.Location = New System.Drawing.Point(289, 254)
        Me.Min_TempVapor.Name = "Min_TempVapor"
        Me.Min_TempVapor.Size = New System.Drawing.Size(100, 20)
        Me.Min_TempVapor.TabIndex = 88
        '
        'Max_TempVapor
        '
        Me.Max_TempVapor.Location = New System.Drawing.Point(141, 254)
        Me.Max_TempVapor.Name = "Max_TempVapor"
        Me.Max_TempVapor.Size = New System.Drawing.Size(100, 20)
        Me.Max_TempVapor.TabIndex = 87
        '
        'Label18
        '
        Me.Label18.AutoSize = True
        Me.Label18.Location = New System.Drawing.Point(17, 257)
        Me.Label18.Name = "Label18"
        Me.Label18.Size = New System.Drawing.Size(104, 13)
        Me.Label18.TabIndex = 86
        Me.Label18.Text = "Temperatura Vapor: "
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(328, 194)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(26, 13)
        Me.Label13.TabIndex = 85
        Me.Label13.Text = "Mín"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(178, 194)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(27, 13)
        Me.Label14.TabIndex = 84
        Me.Label14.Text = "Máx"
        '
        'Min_PresionAgua
        '
        Me.Min_PresionAgua.Location = New System.Drawing.Point(289, 210)
        Me.Min_PresionAgua.Name = "Min_PresionAgua"
        Me.Min_PresionAgua.Size = New System.Drawing.Size(100, 20)
        Me.Min_PresionAgua.TabIndex = 83
        '
        'Max_PresionAgua
        '
        Me.Max_PresionAgua.Location = New System.Drawing.Point(141, 210)
        Me.Max_PresionAgua.Name = "Max_PresionAgua"
        Me.Max_PresionAgua.Size = New System.Drawing.Size(100, 20)
        Me.Max_PresionAgua.TabIndex = 82
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(15, 213)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(76, 13)
        Me.Label15.TabIndex = 81
        Me.Label15.Text = "Presión Agua: "
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(328, 152)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(26, 13)
        Me.Label10.TabIndex = 80
        Me.Label10.Text = "Mín"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(178, 152)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(27, 13)
        Me.Label11.TabIndex = 79
        Me.Label11.Text = "Máx"
        '
        'Min_TempAgua
        '
        Me.Min_TempAgua.Location = New System.Drawing.Point(289, 168)
        Me.Min_TempAgua.Name = "Min_TempAgua"
        Me.Min_TempAgua.Size = New System.Drawing.Size(100, 20)
        Me.Min_TempAgua.TabIndex = 78
        '
        'Max_TempAgua
        '
        Me.Max_TempAgua.Location = New System.Drawing.Point(141, 168)
        Me.Max_TempAgua.Name = "Max_TempAgua"
        Me.Max_TempAgua.Size = New System.Drawing.Size(100, 20)
        Me.Max_TempAgua.TabIndex = 77
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(15, 171)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(101, 13)
        Me.Label12.TabIndex = 76
        Me.Label12.Text = "Temperatura Agua: "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(326, 111)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(26, 13)
        Me.Label7.TabIndex = 75
        Me.Label7.Text = "Mín"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(176, 111)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(27, 13)
        Me.Label8.TabIndex = 74
        Me.Label8.Text = "Máx"
        '
        'Min_ConsumoVapor
        '
        Me.Min_ConsumoVapor.Location = New System.Drawing.Point(287, 127)
        Me.Min_ConsumoVapor.Name = "Min_ConsumoVapor"
        Me.Min_ConsumoVapor.Size = New System.Drawing.Size(100, 20)
        Me.Min_ConsumoVapor.TabIndex = 73
        '
        'Max_ConsumoVapor
        '
        Me.Max_ConsumoVapor.Location = New System.Drawing.Point(139, 127)
        Me.Max_ConsumoVapor.Name = "Max_ConsumoVapor"
        Me.Max_ConsumoVapor.Size = New System.Drawing.Size(100, 20)
        Me.Max_ConsumoVapor.TabIndex = 72
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(15, 130)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(119, 13)
        Me.Label9.TabIndex = 71
        Me.Label9.Text = "Consumo Vapor Domo: "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(328, 70)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(26, 13)
        Me.Label4.TabIndex = 70
        Me.Label4.Text = "Mín"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(178, 70)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(27, 13)
        Me.Label5.TabIndex = 69
        Me.Label5.Text = "Máx"
        '
        'Min_PresionDomo
        '
        Me.Min_PresionDomo.Location = New System.Drawing.Point(289, 86)
        Me.Min_PresionDomo.Name = "Min_PresionDomo"
        Me.Min_PresionDomo.Size = New System.Drawing.Size(100, 20)
        Me.Min_PresionDomo.TabIndex = 68
        '
        'Max_PresionDomo
        '
        Me.Max_PresionDomo.Location = New System.Drawing.Point(141, 86)
        Me.Max_PresionDomo.Name = "Max_PresionDomo"
        Me.Max_PresionDomo.Size = New System.Drawing.Size(100, 20)
        Me.Max_PresionDomo.TabIndex = 67
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(17, 89)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(79, 13)
        Me.Label6.TabIndex = 66
        Me.Label6.Text = "Presión Domo: "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(328, 28)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(26, 13)
        Me.Label3.TabIndex = 65
        Me.Label3.Text = "Mín"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(178, 28)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(27, 13)
        Me.Label2.TabIndex = 64
        Me.Label2.Text = "Máx"
        '
        'Min_NivelDomo
        '
        Me.Min_NivelDomo.Location = New System.Drawing.Point(289, 44)
        Me.Min_NivelDomo.Name = "Min_NivelDomo"
        Me.Min_NivelDomo.Size = New System.Drawing.Size(100, 20)
        Me.Min_NivelDomo.TabIndex = 63
        '
        'Max_NivelDomo
        '
        Me.Max_NivelDomo.Location = New System.Drawing.Point(141, 44)
        Me.Max_NivelDomo.Name = "Max_NivelDomo"
        Me.Max_NivelDomo.Size = New System.Drawing.Size(100, 20)
        Me.Max_NivelDomo.TabIndex = 62
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(17, 47)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(68, 13)
        Me.Label1.TabIndex = 61
        Me.Label1.Text = "Nivel Domo: "
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.Label35)
        Me.TabPage2.Controls.Add(Me.Label34)
        Me.TabPage2.Controls.Add(Me.Label33)
        Me.TabPage2.Controls.Add(Me.Label31)
        Me.TabPage2.Controls.Add(Me.TC_HoraFin)
        Me.TabPage2.Controls.Add(Me.TC_HoraInicio)
        Me.TabPage2.Controls.Add(Me.Label24)
        Me.TabPage2.Controls.Add(Me.TB_HoraFin)
        Me.TabPage2.Controls.Add(Me.TA_HoraInicio)
        Me.TabPage2.Controls.Add(Me.TB_HoraInicio)
        Me.TabPage2.Controls.Add(Me.Label23)
        Me.TabPage2.Controls.Add(Me.TA_HoraFin)
        Me.TabPage2.Controls.Add(Me.Label22)
        Me.TabPage2.Controls.Add(Me.Label26)
        Me.TabPage2.Controls.Add(Me.Label25)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(426, 297)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Turnos"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'Label35
        '
        Me.Label35.AutoSize = True
        Me.Label35.Location = New System.Drawing.Point(40, 269)
        Me.Label35.Name = "Label35"
        Me.Label35.Size = New System.Drawing.Size(156, 13)
        Me.Label35.TabIndex = 18
        Me.Label35.Text = "de finalización correspondiente."
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Location = New System.Drawing.Point(40, 256)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(349, 13)
        Me.Label34.TabIndex = 17
        Me.Label34.Text = "Para el correcto funcionamiento del software reste un segundo a la hora "
        '
        'Label33
        '
        Me.Label33.AutoSize = True
        Me.Label33.Location = New System.Drawing.Point(16, 234)
        Me.Label33.Name = "Label33"
        Me.Label33.Size = New System.Drawing.Size(36, 13)
        Me.Label33.TabIndex = 16
        Me.Label33.Text = "Nota: "
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Location = New System.Drawing.Point(41, 31)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(230, 13)
        Me.Label31.TabIndex = 15
        Me.Label31.Text = "Ingrese la hora de inicio y termino de los turnos:"
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.Label30)
        Me.TabPage3.Controls.Add(Me.Label29)
        Me.TabPage3.Controls.Add(Me.Label28)
        Me.TabPage3.Controls.Add(Me.Label27)
        Me.TabPage3.Controls.Add(Me.Valor_MetaLijadora)
        Me.TabPage3.Controls.Add(Me.Valor_MetaTorno)
        Me.TabPage3.Controls.Add(Me.Label19)
        Me.TabPage3.Controls.Add(Me.Valor_MetaDescortezador)
        Me.TabPage3.Controls.Add(Me.Label20)
        Me.TabPage3.Controls.Add(Me.Label21)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(426, 297)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Metas"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Location = New System.Drawing.Point(38, 30)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(243, 13)
        Me.Label30.TabIndex = 10
        Me.Label30.Text = "Ingrese la meta por turnos de las siguientes lineas:"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Location = New System.Drawing.Point(275, 183)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(48, 13)
        Me.Label29.TabIndex = 9
        Me.Label29.Text = "Tableros"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Location = New System.Drawing.Point(275, 142)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(39, 13)
        Me.Label28.TabIndex = 8
        Me.Label28.Text = "Trozos"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Location = New System.Drawing.Point(275, 100)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(18, 13)
        Me.Label27.TabIndex = 7
        Me.Label27.Text = "m³"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.valor_correo5)
        Me.TabPage4.Controls.Add(Me.valor_correo4)
        Me.TabPage4.Controls.Add(Me.valor_correo3)
        Me.TabPage4.Controls.Add(Me.valor_correo2)
        Me.TabPage4.Controls.Add(Me.valor_correo1)
        Me.TabPage4.Controls.Add(Me.Check_correo5)
        Me.TabPage4.Controls.Add(Me.Check_correo4)
        Me.TabPage4.Controls.Add(Me.Check_correo3)
        Me.TabPage4.Controls.Add(Me.Check_correo2)
        Me.TabPage4.Controls.Add(Me.Check_correo1)
        Me.TabPage4.Controls.Add(Me.Label32)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(426, 297)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Correos"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'valor_correo5
        '
        Me.valor_correo5.Location = New System.Drawing.Point(175, 232)
        Me.valor_correo5.Name = "valor_correo5"
        Me.valor_correo5.Size = New System.Drawing.Size(172, 20)
        Me.valor_correo5.TabIndex = 17
        '
        'valor_correo4
        '
        Me.valor_correo4.Location = New System.Drawing.Point(175, 192)
        Me.valor_correo4.Name = "valor_correo4"
        Me.valor_correo4.Size = New System.Drawing.Size(172, 20)
        Me.valor_correo4.TabIndex = 16
        '
        'valor_correo3
        '
        Me.valor_correo3.Location = New System.Drawing.Point(175, 152)
        Me.valor_correo3.Name = "valor_correo3"
        Me.valor_correo3.Size = New System.Drawing.Size(172, 20)
        Me.valor_correo3.TabIndex = 15
        '
        'valor_correo2
        '
        Me.valor_correo2.Location = New System.Drawing.Point(175, 112)
        Me.valor_correo2.Name = "valor_correo2"
        Me.valor_correo2.Size = New System.Drawing.Size(172, 20)
        Me.valor_correo2.TabIndex = 14
        '
        'valor_correo1
        '
        Me.valor_correo1.Location = New System.Drawing.Point(175, 74)
        Me.valor_correo1.Name = "valor_correo1"
        Me.valor_correo1.Size = New System.Drawing.Size(172, 20)
        Me.valor_correo1.TabIndex = 13
        '
        'Check_correo5
        '
        Me.Check_correo5.AutoSize = True
        Me.Check_correo5.Location = New System.Drawing.Point(84, 234)
        Me.Check_correo5.Name = "Check_correo5"
        Me.Check_correo5.Size = New System.Drawing.Size(69, 17)
        Me.Check_correo5.TabIndex = 12
        Me.Check_correo5.Text = "Correo 5:"
        Me.Check_correo5.UseVisualStyleBackColor = True
        '
        'Check_correo4
        '
        Me.Check_correo4.AutoSize = True
        Me.Check_correo4.Location = New System.Drawing.Point(84, 194)
        Me.Check_correo4.Name = "Check_correo4"
        Me.Check_correo4.Size = New System.Drawing.Size(69, 17)
        Me.Check_correo4.TabIndex = 11
        Me.Check_correo4.Text = "Correo 4:"
        Me.Check_correo4.UseVisualStyleBackColor = True
        '
        'Check_correo3
        '
        Me.Check_correo3.AutoSize = True
        Me.Check_correo3.Location = New System.Drawing.Point(84, 154)
        Me.Check_correo3.Name = "Check_correo3"
        Me.Check_correo3.Size = New System.Drawing.Size(69, 17)
        Me.Check_correo3.TabIndex = 10
        Me.Check_correo3.Text = "Correo 3:"
        Me.Check_correo3.UseVisualStyleBackColor = True
        '
        'Check_correo2
        '
        Me.Check_correo2.AutoSize = True
        Me.Check_correo2.Location = New System.Drawing.Point(84, 114)
        Me.Check_correo2.Name = "Check_correo2"
        Me.Check_correo2.Size = New System.Drawing.Size(69, 17)
        Me.Check_correo2.TabIndex = 9
        Me.Check_correo2.Text = "Correo 2:"
        Me.Check_correo2.UseVisualStyleBackColor = True
        '
        'Check_correo1
        '
        Me.Check_correo1.AutoSize = True
        Me.Check_correo1.Location = New System.Drawing.Point(84, 74)
        Me.Check_correo1.Name = "Check_correo1"
        Me.Check_correo1.Size = New System.Drawing.Size(69, 17)
        Me.Check_correo1.TabIndex = 8
        Me.Check_correo1.Text = "Correo 1:"
        Me.Check_correo1.UseVisualStyleBackColor = True
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Location = New System.Drawing.Point(21, 18)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(369, 13)
        Me.Label32.TabIndex = 7
        Me.Label32.Text = "Seleccione los correos a los que desea enviarle el reporte al finalizar el turno:" &
    ""
        '
        'PanelConfiguracion
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.ClientSize = New System.Drawing.Size(437, 376)
        Me.Controls.Add(Me.TabControl1)
        Me.Controls.Add(Me.Button_Guardar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.Name = "PanelConfiguracion"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Ventana de Configuración"
        CType(Me.TC_HoraFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TC_HoraInicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TB_HoraFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TB_HoraInicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TA_HoraFin.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.TA_HoraInicio.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage2.PerformLayout()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        CType(Me.valor_correo5.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.valor_correo4.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.valor_correo3.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.valor_correo2.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.valor_correo1.Properties, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents Button_Guardar As Button
    Friend WithEvents Valor_MetaLijadora As TextBox
    Friend WithEvents Valor_MetaTorno As TextBox
    Friend WithEvents Valor_MetaDescortezador As TextBox
    Friend WithEvents Label21 As Label
    Friend WithEvents Label20 As Label
    Friend WithEvents Label19 As Label
    Friend WithEvents Label26 As Label
    Friend WithEvents Label25 As Label
    Friend WithEvents Label22 As Label
    Friend WithEvents Label23 As Label
    Friend WithEvents Label24 As Label
    Friend WithEvents TA_HoraInicio As DevExpress.XtraEditors.TimeSpanEdit
    Friend WithEvents TC_HoraFin As DevExpress.XtraEditors.TimeSpanEdit
    Friend WithEvents TC_HoraInicio As DevExpress.XtraEditors.TimeSpanEdit
    Friend WithEvents TB_HoraFin As DevExpress.XtraEditors.TimeSpanEdit
    Friend WithEvents TB_HoraInicio As DevExpress.XtraEditors.TimeSpanEdit
    Friend WithEvents TA_HoraFin As DevExpress.XtraEditors.TimeSpanEdit
    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents LabelControl12 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl11 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl10 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl9 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl7 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl6 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl5 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl4 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl3 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl2 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl1 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents LabelControl8 As DevExpress.XtraEditors.LabelControl
    Friend WithEvents Label16 As Label
    Friend WithEvents Label17 As Label
    Friend WithEvents Min_TempVapor As TextBox
    Friend WithEvents Max_TempVapor As TextBox
    Friend WithEvents Label18 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Min_PresionAgua As TextBox
    Friend WithEvents Max_PresionAgua As TextBox
    Friend WithEvents Label15 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Min_TempAgua As TextBox
    Friend WithEvents Max_TempAgua As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Min_ConsumoVapor As TextBox
    Friend WithEvents Max_ConsumoVapor As TextBox
    Friend WithEvents Label9 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Min_PresionDomo As TextBox
    Friend WithEvents Max_PresionDomo As TextBox
    Friend WithEvents Label6 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Min_NivelDomo As TextBox
    Friend WithEvents Max_NivelDomo As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents valor_correo5 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents valor_correo4 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents valor_correo3 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents valor_correo2 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents valor_correo1 As DevExpress.XtraEditors.TextEdit
    Friend WithEvents Check_correo5 As CheckBox
    Friend WithEvents Check_correo4 As CheckBox
    Friend WithEvents Check_correo3 As CheckBox
    Friend WithEvents Check_correo2 As CheckBox
    Friend WithEvents Check_correo1 As CheckBox
    Friend WithEvents Label32 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label35 As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents Label33 As Label
End Class
