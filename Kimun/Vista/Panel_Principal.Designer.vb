﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Panel_Principal
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub


    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Panel_Principal))
        Me.MenuStrip = New System.Windows.Forms.MenuStrip()
        Me.CuentasDeUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AgregarUsuarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListaDeUsuariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ControlProducciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LineaVerdeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LijadoraToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DescortezadorToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfiguraciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbrirConfiguraciónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ConfigurarConexiónToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalderaToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ParametrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IngresarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ModificarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EliminarDatosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.EstadisticasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LineaVerdeToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LijadoraToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalderaToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.InformesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalderaToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.CalidadDeAguasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiarioToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MensualToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RangosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IndicadoresToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RangosToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiariosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MensualToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LineaVerdeToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiarioToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MensualToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.RangosToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.LijadoraToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.DiarioToolStripMenuItem2 = New System.Windows.Forms.ToolStripMenuItem()
        Me.MensualToolStripMenuItem3 = New System.Windows.Forms.ToolStripMenuItem()
        Me.PorRangosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PanelDeControlToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.AbrirPanelDeControlToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip = New System.Windows.Forms.StatusStrip()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.estado_eTata = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.estado_controlProduccion = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.nombre_usuario = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me.ContentsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.IndexToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SearchToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator8 = New System.Windows.Forms.ToolStripSeparator()
        Me.AboutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewWindowToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CascadeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileVerticalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TileHorizontalToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CloseAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ArrangeIconsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OptionsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusBarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.UndoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.RedoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator6 = New System.Windows.Forms.ToolStripSeparator()
        Me.CutToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CopyToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PasteToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator7 = New System.Windows.Forms.ToolStripSeparator()
        Me.SelectAllToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.NewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.OpenToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator3 = New System.Windows.Forms.ToolStripSeparator()
        Me.SaveToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SaveAsToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator4 = New System.Windows.Forms.ToolStripSeparator()
        Me.PrintToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintPreviewToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PrintSetupToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator5 = New System.Windows.Forms.ToolStripSeparator()
        Me.ExitToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.MenuStrip.SuspendLayout()
        Me.StatusStrip.SuspendLayout()
        Me.SuspendLayout()
        '
        'MenuStrip
        '
        Me.MenuStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CuentasDeUsuarioToolStripMenuItem, Me.ControlProducciónToolStripMenuItem, Me.ConfiguraciónToolStripMenuItem, Me.CalderaToolStripMenuItem, Me.EstadisticasToolStripMenuItem, Me.InformesToolStripMenuItem, Me.PanelDeControlToolStripMenuItem})
        Me.MenuStrip.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip.Name = "MenuStrip"
        Me.MenuStrip.Size = New System.Drawing.Size(779, 24)
        Me.MenuStrip.TabIndex = 5
        Me.MenuStrip.Text = "MenuStrip"
        '
        'CuentasDeUsuarioToolStripMenuItem
        '
        Me.CuentasDeUsuarioToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AgregarUsuarioToolStripMenuItem, Me.ListaDeUsuariosToolStripMenuItem})
        Me.CuentasDeUsuarioToolStripMenuItem.Name = "CuentasDeUsuarioToolStripMenuItem"
        Me.CuentasDeUsuarioToolStripMenuItem.Size = New System.Drawing.Size(121, 20)
        Me.CuentasDeUsuarioToolStripMenuItem.Text = "Cuentas de Usuario"
        '
        'AgregarUsuarioToolStripMenuItem
        '
        Me.AgregarUsuarioToolStripMenuItem.Name = "AgregarUsuarioToolStripMenuItem"
        Me.AgregarUsuarioToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.AgregarUsuarioToolStripMenuItem.Text = "Agregar Usuario"
        '
        'ListaDeUsuariosToolStripMenuItem
        '
        Me.ListaDeUsuariosToolStripMenuItem.Name = "ListaDeUsuariosToolStripMenuItem"
        Me.ListaDeUsuariosToolStripMenuItem.Size = New System.Drawing.Size(162, 22)
        Me.ListaDeUsuariosToolStripMenuItem.Text = "Lista de Usuarios"
        '
        'ControlProducciónToolStripMenuItem
        '
        Me.ControlProducciónToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LineaVerdeToolStripMenuItem, Me.LijadoraToolStripMenuItem, Me.DescortezadorToolStripMenuItem})
        Me.ControlProducciónToolStripMenuItem.Name = "ControlProducciónToolStripMenuItem"
        Me.ControlProducciónToolStripMenuItem.Size = New System.Drawing.Size(123, 20)
        Me.ControlProducciónToolStripMenuItem.Text = "Control Producción"
        Me.ControlProducciónToolStripMenuItem.Visible = False
        '
        'LineaVerdeToolStripMenuItem
        '
        Me.LineaVerdeToolStripMenuItem.Name = "LineaVerdeToolStripMenuItem"
        Me.LineaVerdeToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.LineaVerdeToolStripMenuItem.Text = "Torno"
        '
        'LijadoraToolStripMenuItem
        '
        Me.LijadoraToolStripMenuItem.Name = "LijadoraToolStripMenuItem"
        Me.LijadoraToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.LijadoraToolStripMenuItem.Text = "Lijadora"
        '
        'DescortezadorToolStripMenuItem
        '
        Me.DescortezadorToolStripMenuItem.Name = "DescortezadorToolStripMenuItem"
        Me.DescortezadorToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.DescortezadorToolStripMenuItem.Text = "Descortezador"
        '
        'ConfiguraciónToolStripMenuItem
        '
        Me.ConfiguraciónToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AbrirConfiguraciónToolStripMenuItem, Me.ConfigurarConexiónToolStripMenuItem})
        Me.ConfiguraciónToolStripMenuItem.Name = "ConfiguraciónToolStripMenuItem"
        Me.ConfiguraciónToolStripMenuItem.Size = New System.Drawing.Size(95, 20)
        Me.ConfiguraciónToolStripMenuItem.Text = "Configuración"
        '
        'AbrirConfiguraciónToolStripMenuItem
        '
        Me.AbrirConfiguraciónToolStripMenuItem.Name = "AbrirConfiguraciónToolStripMenuItem"
        Me.AbrirConfiguraciónToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.AbrirConfiguraciónToolStripMenuItem.Text = "Configurar Kimün"
        '
        'ConfigurarConexiónToolStripMenuItem
        '
        Me.ConfigurarConexiónToolStripMenuItem.Name = "ConfigurarConexiónToolStripMenuItem"
        Me.ConfigurarConexiónToolStripMenuItem.Size = New System.Drawing.Size(177, 22)
        Me.ConfigurarConexiónToolStripMenuItem.Text = "Configurar Servidor"
        '
        'CalderaToolStripMenuItem
        '
        Me.CalderaToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ParametrosToolStripMenuItem})
        Me.CalderaToolStripMenuItem.Name = "CalderaToolStripMenuItem"
        Me.CalderaToolStripMenuItem.Size = New System.Drawing.Size(59, 20)
        Me.CalderaToolStripMenuItem.Text = "Caldera"
        Me.CalderaToolStripMenuItem.Visible = False
        '
        'ParametrosToolStripMenuItem
        '
        Me.ParametrosToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.IngresarDatosToolStripMenuItem, Me.ModificarDatosToolStripMenuItem, Me.EliminarDatosToolStripMenuItem})
        Me.ParametrosToolStripMenuItem.Name = "ParametrosToolStripMenuItem"
        Me.ParametrosToolStripMenuItem.Size = New System.Drawing.Size(152, 22)
        Me.ParametrosToolStripMenuItem.Text = "Parametros"
        '
        'IngresarDatosToolStripMenuItem
        '
        Me.IngresarDatosToolStripMenuItem.Name = "IngresarDatosToolStripMenuItem"
        Me.IngresarDatosToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.IngresarDatosToolStripMenuItem.Text = "Ingresar Datos"
        '
        'ModificarDatosToolStripMenuItem
        '
        Me.ModificarDatosToolStripMenuItem.Name = "ModificarDatosToolStripMenuItem"
        Me.ModificarDatosToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.ModificarDatosToolStripMenuItem.Text = "Modificar Datos"
        '
        'EliminarDatosToolStripMenuItem
        '
        Me.EliminarDatosToolStripMenuItem.Name = "EliminarDatosToolStripMenuItem"
        Me.EliminarDatosToolStripMenuItem.Size = New System.Drawing.Size(158, 22)
        Me.EliminarDatosToolStripMenuItem.Text = "Eliminar Datos"
        '
        'EstadisticasToolStripMenuItem
        '
        Me.EstadisticasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.LineaVerdeToolStripMenuItem1, Me.LijadoraToolStripMenuItem1, Me.CalderaToolStripMenuItem1})
        Me.EstadisticasToolStripMenuItem.Name = "EstadisticasToolStripMenuItem"
        Me.EstadisticasToolStripMenuItem.Size = New System.Drawing.Size(79, 20)
        Me.EstadisticasToolStripMenuItem.Text = "Estadisticas"
        '
        'LineaVerdeToolStripMenuItem1
        '
        Me.LineaVerdeToolStripMenuItem1.Name = "LineaVerdeToolStripMenuItem1"
        Me.LineaVerdeToolStripMenuItem1.Size = New System.Drawing.Size(134, 22)
        Me.LineaVerdeToolStripMenuItem1.Text = "Linea Verde"
        '
        'LijadoraToolStripMenuItem1
        '
        Me.LijadoraToolStripMenuItem1.Name = "LijadoraToolStripMenuItem1"
        Me.LijadoraToolStripMenuItem1.Size = New System.Drawing.Size(134, 22)
        Me.LijadoraToolStripMenuItem1.Text = "Lijadora"
        '
        'CalderaToolStripMenuItem1
        '
        Me.CalderaToolStripMenuItem1.Name = "CalderaToolStripMenuItem1"
        Me.CalderaToolStripMenuItem1.Size = New System.Drawing.Size(134, 22)
        Me.CalderaToolStripMenuItem1.Text = "Caldera"
        '
        'InformesToolStripMenuItem
        '
        Me.InformesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ReportesToolStripMenuItem})
        Me.InformesToolStripMenuItem.Name = "InformesToolStripMenuItem"
        Me.InformesToolStripMenuItem.Size = New System.Drawing.Size(66, 20)
        Me.InformesToolStripMenuItem.Text = "Informes"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CalderaToolStripMenuItem2, Me.LineaVerdeToolStripMenuItem2, Me.LijadoraToolStripMenuItem2})
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(120, 22)
        Me.ReportesToolStripMenuItem.Text = "Reportes"
        '
        'CalderaToolStripMenuItem2
        '
        Me.CalderaToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CalidadDeAguasToolStripMenuItem, Me.IndicadoresToolStripMenuItem})
        Me.CalderaToolStripMenuItem2.Name = "CalderaToolStripMenuItem2"
        Me.CalderaToolStripMenuItem2.Size = New System.Drawing.Size(134, 22)
        Me.CalderaToolStripMenuItem2.Text = "Caldera"
        '
        'CalidadDeAguasToolStripMenuItem
        '
        Me.CalidadDeAguasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiarioToolStripMenuItem, Me.MensualToolStripMenuItem, Me.RangosToolStripMenuItem})
        Me.CalidadDeAguasToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.CalidadDeAguasToolStripMenuItem.Name = "CalidadDeAguasToolStripMenuItem"
        Me.CalidadDeAguasToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.CalidadDeAguasToolStripMenuItem.Text = "Calidad de Aguas"
        '
        'DiarioToolStripMenuItem
        '
        Me.DiarioToolStripMenuItem.Name = "DiarioToolStripMenuItem"
        Me.DiarioToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.DiarioToolStripMenuItem.Text = "Diario"
        '
        'MensualToolStripMenuItem
        '
        Me.MensualToolStripMenuItem.Name = "MensualToolStripMenuItem"
        Me.MensualToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.MensualToolStripMenuItem.Text = "Mensual"
        '
        'RangosToolStripMenuItem
        '
        Me.RangosToolStripMenuItem.Name = "RangosToolStripMenuItem"
        Me.RangosToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.RangosToolStripMenuItem.Text = "Rangos"
        '
        'IndicadoresToolStripMenuItem
        '
        Me.IndicadoresToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.RangosToolStripMenuItem1, Me.DiariosToolStripMenuItem, Me.MensualToolStripMenuItem1})
        Me.IndicadoresToolStripMenuItem.Name = "IndicadoresToolStripMenuItem"
        Me.IndicadoresToolStripMenuItem.Size = New System.Drawing.Size(166, 22)
        Me.IndicadoresToolStripMenuItem.Text = "Indicadores"
        '
        'RangosToolStripMenuItem1
        '
        Me.RangosToolStripMenuItem1.Name = "RangosToolStripMenuItem1"
        Me.RangosToolStripMenuItem1.Size = New System.Drawing.Size(119, 22)
        Me.RangosToolStripMenuItem1.Text = "Diario"
        '
        'DiariosToolStripMenuItem
        '
        Me.DiariosToolStripMenuItem.Name = "DiariosToolStripMenuItem"
        Me.DiariosToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.DiariosToolStripMenuItem.Text = "Mensual"
        '
        'MensualToolStripMenuItem1
        '
        Me.MensualToolStripMenuItem1.Name = "MensualToolStripMenuItem1"
        Me.MensualToolStripMenuItem1.Size = New System.Drawing.Size(119, 22)
        Me.MensualToolStripMenuItem1.Text = "Rangos"
        '
        'LineaVerdeToolStripMenuItem2
        '
        Me.LineaVerdeToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiarioToolStripMenuItem1, Me.MensualToolStripMenuItem2, Me.RangosToolStripMenuItem2})
        Me.LineaVerdeToolStripMenuItem2.Name = "LineaVerdeToolStripMenuItem2"
        Me.LineaVerdeToolStripMenuItem2.Size = New System.Drawing.Size(134, 22)
        Me.LineaVerdeToolStripMenuItem2.Text = "Linea Verde"
        '
        'DiarioToolStripMenuItem1
        '
        Me.DiarioToolStripMenuItem1.Name = "DiarioToolStripMenuItem1"
        Me.DiarioToolStripMenuItem1.Size = New System.Drawing.Size(119, 22)
        Me.DiarioToolStripMenuItem1.Text = "Diario"
        '
        'MensualToolStripMenuItem2
        '
        Me.MensualToolStripMenuItem2.Name = "MensualToolStripMenuItem2"
        Me.MensualToolStripMenuItem2.Size = New System.Drawing.Size(119, 22)
        Me.MensualToolStripMenuItem2.Text = "Mensual"
        '
        'RangosToolStripMenuItem2
        '
        Me.RangosToolStripMenuItem2.Name = "RangosToolStripMenuItem2"
        Me.RangosToolStripMenuItem2.Size = New System.Drawing.Size(119, 22)
        Me.RangosToolStripMenuItem2.Text = "Rangos"
        '
        'LijadoraToolStripMenuItem2
        '
        Me.LijadoraToolStripMenuItem2.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.DiarioToolStripMenuItem2, Me.MensualToolStripMenuItem3, Me.PorRangosToolStripMenuItem})
        Me.LijadoraToolStripMenuItem2.Name = "LijadoraToolStripMenuItem2"
        Me.LijadoraToolStripMenuItem2.Size = New System.Drawing.Size(134, 22)
        Me.LijadoraToolStripMenuItem2.Text = "Lijadora"
        '
        'DiarioToolStripMenuItem2
        '
        Me.DiarioToolStripMenuItem2.Name = "DiarioToolStripMenuItem2"
        Me.DiarioToolStripMenuItem2.Size = New System.Drawing.Size(119, 22)
        Me.DiarioToolStripMenuItem2.Text = "Diario"
        '
        'MensualToolStripMenuItem3
        '
        Me.MensualToolStripMenuItem3.Name = "MensualToolStripMenuItem3"
        Me.MensualToolStripMenuItem3.Size = New System.Drawing.Size(119, 22)
        Me.MensualToolStripMenuItem3.Text = "Mensual"
        '
        'PorRangosToolStripMenuItem
        '
        Me.PorRangosToolStripMenuItem.Name = "PorRangosToolStripMenuItem"
        Me.PorRangosToolStripMenuItem.Size = New System.Drawing.Size(119, 22)
        Me.PorRangosToolStripMenuItem.Text = "Rangos"
        '
        'PanelDeControlToolStripMenuItem
        '
        Me.PanelDeControlToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.AbrirPanelDeControlToolStripMenuItem})
        Me.PanelDeControlToolStripMenuItem.Name = "PanelDeControlToolStripMenuItem"
        Me.PanelDeControlToolStripMenuItem.Size = New System.Drawing.Size(107, 20)
        Me.PanelDeControlToolStripMenuItem.Text = "Panel de Control"
        '
        'AbrirPanelDeControlToolStripMenuItem
        '
        Me.AbrirPanelDeControlToolStripMenuItem.Name = "AbrirPanelDeControlToolStripMenuItem"
        Me.AbrirPanelDeControlToolStripMenuItem.Size = New System.Drawing.Size(191, 22)
        Me.AbrirPanelDeControlToolStripMenuItem.Text = "Abrir Panel de Control"
        '
        'StatusStrip
        '
        Me.StatusStrip.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripStatusLabel1, Me.estado_eTata, Me.ToolStripStatusLabel2, Me.estado_controlProduccion, Me.ToolStripStatusLabel3, Me.nombre_usuario})
        Me.StatusStrip.Location = New System.Drawing.Point(0, 431)
        Me.StatusStrip.Name = "StatusStrip"
        Me.StatusStrip.Size = New System.Drawing.Size(779, 22)
        Me.StatusStrip.TabIndex = 7
        Me.StatusStrip.Text = "StatusStrip"
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(79, 17)
        Me.ToolStripStatusLabel1.Text = "Estado eTata: "
        '
        'estado_eTata
        '
        Me.estado_eTata.ForeColor = System.Drawing.SystemColors.ControlText
        Me.estado_eTata.Name = "estado_eTata"
        Me.estado_eTata.Size = New System.Drawing.Size(0, 17)
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(150, 17)
        Me.ToolStripStatusLabel2.Text = "Estado  controlProducción:"
        '
        'estado_controlProduccion
        '
        Me.estado_controlProduccion.Name = "estado_controlProduccion"
        Me.estado_controlProduccion.Size = New System.Drawing.Size(0, 17)
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(53, 17)
        Me.ToolStripStatusLabel3.Text = "Usuario: "
        '
        'nombre_usuario
        '
        Me.nombre_usuario.Name = "nombre_usuario"
        Me.nombre_usuario.Size = New System.Drawing.Size(120, 17)
        Me.nombre_usuario.Text = "ToolStripStatusLabel4"
        '
        'ContentsToolStripMenuItem
        '
        Me.ContentsToolStripMenuItem.Name = "ContentsToolStripMenuItem"
        Me.ContentsToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.F1), System.Windows.Forms.Keys)
        Me.ContentsToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.ContentsToolStripMenuItem.Text = "&Contenido"
        '
        'IndexToolStripMenuItem
        '
        Me.IndexToolStripMenuItem.Image = CType(resources.GetObject("IndexToolStripMenuItem.Image"), System.Drawing.Image)
        Me.IndexToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.IndexToolStripMenuItem.Name = "IndexToolStripMenuItem"
        Me.IndexToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.IndexToolStripMenuItem.Text = "&Índice"
        '
        'SearchToolStripMenuItem
        '
        Me.SearchToolStripMenuItem.Image = CType(resources.GetObject("SearchToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SearchToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.SearchToolStripMenuItem.Name = "SearchToolStripMenuItem"
        Me.SearchToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.SearchToolStripMenuItem.Text = "&Buscar"
        '
        'ToolStripSeparator8
        '
        Me.ToolStripSeparator8.Name = "ToolStripSeparator8"
        Me.ToolStripSeparator8.Size = New System.Drawing.Size(173, 6)
        '
        'AboutToolStripMenuItem
        '
        Me.AboutToolStripMenuItem.Name = "AboutToolStripMenuItem"
        Me.AboutToolStripMenuItem.Size = New System.Drawing.Size(176, 22)
        Me.AboutToolStripMenuItem.Text = "&Acerca de..."
        '
        'NewWindowToolStripMenuItem
        '
        Me.NewWindowToolStripMenuItem.Name = "NewWindowToolStripMenuItem"
        Me.NewWindowToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.NewWindowToolStripMenuItem.Text = "&Nueva ventana"
        '
        'CascadeToolStripMenuItem
        '
        Me.CascadeToolStripMenuItem.Name = "CascadeToolStripMenuItem"
        Me.CascadeToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.CascadeToolStripMenuItem.Text = "&Cascada"
        '
        'TileVerticalToolStripMenuItem
        '
        Me.TileVerticalToolStripMenuItem.Name = "TileVerticalToolStripMenuItem"
        Me.TileVerticalToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.TileVerticalToolStripMenuItem.Text = "Mosaico &vertical"
        '
        'TileHorizontalToolStripMenuItem
        '
        Me.TileHorizontalToolStripMenuItem.Name = "TileHorizontalToolStripMenuItem"
        Me.TileHorizontalToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.TileHorizontalToolStripMenuItem.Text = "Mosaico &horizontal"
        '
        'CloseAllToolStripMenuItem
        '
        Me.CloseAllToolStripMenuItem.Name = "CloseAllToolStripMenuItem"
        Me.CloseAllToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.CloseAllToolStripMenuItem.Text = "C&errar todo"
        '
        'ArrangeIconsToolStripMenuItem
        '
        Me.ArrangeIconsToolStripMenuItem.Name = "ArrangeIconsToolStripMenuItem"
        Me.ArrangeIconsToolStripMenuItem.Size = New System.Drawing.Size(175, 22)
        Me.ArrangeIconsToolStripMenuItem.Text = "&Organizar iconos"
        '
        'OptionsToolStripMenuItem
        '
        Me.OptionsToolStripMenuItem.Name = "OptionsToolStripMenuItem"
        Me.OptionsToolStripMenuItem.Size = New System.Drawing.Size(124, 22)
        Me.OptionsToolStripMenuItem.Text = "&Opciones"
        '
        'ToolBarToolStripMenuItem
        '
        Me.ToolBarToolStripMenuItem.Checked = True
        Me.ToolBarToolStripMenuItem.CheckOnClick = True
        Me.ToolBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.ToolBarToolStripMenuItem.Name = "ToolBarToolStripMenuItem"
        Me.ToolBarToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.ToolBarToolStripMenuItem.Text = "&Barra de herramientas"
        '
        'StatusBarToolStripMenuItem
        '
        Me.StatusBarToolStripMenuItem.Checked = True
        Me.StatusBarToolStripMenuItem.CheckOnClick = True
        Me.StatusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked
        Me.StatusBarToolStripMenuItem.Name = "StatusBarToolStripMenuItem"
        Me.StatusBarToolStripMenuItem.Size = New System.Drawing.Size(189, 22)
        Me.StatusBarToolStripMenuItem.Text = "&Barra de estado"
        '
        'UndoToolStripMenuItem
        '
        Me.UndoToolStripMenuItem.Image = CType(resources.GetObject("UndoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.UndoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.UndoToolStripMenuItem.Name = "UndoToolStripMenuItem"
        Me.UndoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Z), System.Windows.Forms.Keys)
        Me.UndoToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.UndoToolStripMenuItem.Text = "&Deshacer"
        '
        'RedoToolStripMenuItem
        '
        Me.RedoToolStripMenuItem.Image = CType(resources.GetObject("RedoToolStripMenuItem.Image"), System.Drawing.Image)
        Me.RedoToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.RedoToolStripMenuItem.Name = "RedoToolStripMenuItem"
        Me.RedoToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.Y), System.Windows.Forms.Keys)
        Me.RedoToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.RedoToolStripMenuItem.Text = "&Rehacer"
        '
        'ToolStripSeparator6
        '
        Me.ToolStripSeparator6.Name = "ToolStripSeparator6"
        Me.ToolStripSeparator6.Size = New System.Drawing.Size(201, 6)
        '
        'CutToolStripMenuItem
        '
        Me.CutToolStripMenuItem.Image = CType(resources.GetObject("CutToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CutToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.CutToolStripMenuItem.Name = "CutToolStripMenuItem"
        Me.CutToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.X), System.Windows.Forms.Keys)
        Me.CutToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.CutToolStripMenuItem.Text = "Cor&tar"
        '
        'CopyToolStripMenuItem
        '
        Me.CopyToolStripMenuItem.Image = CType(resources.GetObject("CopyToolStripMenuItem.Image"), System.Drawing.Image)
        Me.CopyToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.CopyToolStripMenuItem.Name = "CopyToolStripMenuItem"
        Me.CopyToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.C), System.Windows.Forms.Keys)
        Me.CopyToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.CopyToolStripMenuItem.Text = "&Copiar"
        '
        'PasteToolStripMenuItem
        '
        Me.PasteToolStripMenuItem.Image = CType(resources.GetObject("PasteToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PasteToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.PasteToolStripMenuItem.Name = "PasteToolStripMenuItem"
        Me.PasteToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.V), System.Windows.Forms.Keys)
        Me.PasteToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.PasteToolStripMenuItem.Text = "&Pegar"
        '
        'ToolStripSeparator7
        '
        Me.ToolStripSeparator7.Name = "ToolStripSeparator7"
        Me.ToolStripSeparator7.Size = New System.Drawing.Size(201, 6)
        '
        'SelectAllToolStripMenuItem
        '
        Me.SelectAllToolStripMenuItem.Name = "SelectAllToolStripMenuItem"
        Me.SelectAllToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.A), System.Windows.Forms.Keys)
        Me.SelectAllToolStripMenuItem.Size = New System.Drawing.Size(204, 22)
        Me.SelectAllToolStripMenuItem.Text = "Seleccionar &todo"
        '
        'NewToolStripMenuItem
        '
        Me.NewToolStripMenuItem.Image = CType(resources.GetObject("NewToolStripMenuItem.Image"), System.Drawing.Image)
        Me.NewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.NewToolStripMenuItem.Name = "NewToolStripMenuItem"
        Me.NewToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.N), System.Windows.Forms.Keys)
        Me.NewToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.NewToolStripMenuItem.Text = "&Nuevo"
        '
        'OpenToolStripMenuItem
        '
        Me.OpenToolStripMenuItem.Image = CType(resources.GetObject("OpenToolStripMenuItem.Image"), System.Drawing.Image)
        Me.OpenToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.OpenToolStripMenuItem.Name = "OpenToolStripMenuItem"
        Me.OpenToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.O), System.Windows.Forms.Keys)
        Me.OpenToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.OpenToolStripMenuItem.Text = "&Abrir"
        '
        'ToolStripSeparator3
        '
        Me.ToolStripSeparator3.Name = "ToolStripSeparator3"
        Me.ToolStripSeparator3.Size = New System.Drawing.Size(203, 6)
        '
        'SaveToolStripMenuItem
        '
        Me.SaveToolStripMenuItem.Image = CType(resources.GetObject("SaveToolStripMenuItem.Image"), System.Drawing.Image)
        Me.SaveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.SaveToolStripMenuItem.Name = "SaveToolStripMenuItem"
        Me.SaveToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.S), System.Windows.Forms.Keys)
        Me.SaveToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.SaveToolStripMenuItem.Text = "&Guardar"
        '
        'SaveAsToolStripMenuItem
        '
        Me.SaveAsToolStripMenuItem.Name = "SaveAsToolStripMenuItem"
        Me.SaveAsToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.SaveAsToolStripMenuItem.Text = "Guardar &como"
        '
        'ToolStripSeparator4
        '
        Me.ToolStripSeparator4.Name = "ToolStripSeparator4"
        Me.ToolStripSeparator4.Size = New System.Drawing.Size(203, 6)
        '
        'PrintToolStripMenuItem
        '
        Me.PrintToolStripMenuItem.Image = CType(resources.GetObject("PrintToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PrintToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.PrintToolStripMenuItem.Name = "PrintToolStripMenuItem"
        Me.PrintToolStripMenuItem.ShortcutKeys = CType((System.Windows.Forms.Keys.Control Or System.Windows.Forms.Keys.P), System.Windows.Forms.Keys)
        Me.PrintToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.PrintToolStripMenuItem.Text = "&Imprimir"
        '
        'PrintPreviewToolStripMenuItem
        '
        Me.PrintPreviewToolStripMenuItem.Image = CType(resources.GetObject("PrintPreviewToolStripMenuItem.Image"), System.Drawing.Image)
        Me.PrintPreviewToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black
        Me.PrintPreviewToolStripMenuItem.Name = "PrintPreviewToolStripMenuItem"
        Me.PrintPreviewToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.PrintPreviewToolStripMenuItem.Text = "&Vista previa de impresión"
        '
        'PrintSetupToolStripMenuItem
        '
        Me.PrintSetupToolStripMenuItem.Name = "PrintSetupToolStripMenuItem"
        Me.PrintSetupToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.PrintSetupToolStripMenuItem.Text = "Configurar impresión"
        '
        'ToolStripSeparator5
        '
        Me.ToolStripSeparator5.Name = "ToolStripSeparator5"
        Me.ToolStripSeparator5.Size = New System.Drawing.Size(203, 6)
        '
        'ExitToolStripMenuItem
        '
        Me.ExitToolStripMenuItem.Name = "ExitToolStripMenuItem"
        Me.ExitToolStripMenuItem.Size = New System.Drawing.Size(206, 22)
        Me.ExitToolStripMenuItem.Text = "&Salir"
        '
        'Panel_Principal
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(779, 453)
        Me.Controls.Add(Me.MenuStrip)
        Me.Controls.Add(Me.StatusStrip)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.IsMdiContainer = True
        Me.MainMenuStrip = Me.MenuStrip
        Me.Name = "Panel_Principal"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Panel Principal"
        Me.MenuStrip.ResumeLayout(False)
        Me.MenuStrip.PerformLayout()
        Me.StatusStrip.ResumeLayout(False)
        Me.StatusStrip.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents ToolTip As System.Windows.Forms.ToolTip
    Friend WithEvents estado_eTata As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ControlProducciónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LineaVerdeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LijadoraToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CalderaToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ParametrosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents IngresarDatosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ModificarDatosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EliminarDatosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConfiguraciónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents EstadisticasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents InformesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ReportesToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PanelDeControlToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AbrirPanelDeControlToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContentsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents IndexToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SearchToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator8 As ToolStripSeparator
    Friend WithEvents AboutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NewWindowToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CascadeToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TileVerticalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents TileHorizontalToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CloseAllToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ArrangeIconsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OptionsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolBarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents StatusBarToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents UndoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RedoToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator6 As ToolStripSeparator
    Friend WithEvents CutToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents CopyToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PasteToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator7 As ToolStripSeparator
    Friend WithEvents SelectAllToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents NewToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents OpenToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator3 As ToolStripSeparator
    Friend WithEvents SaveToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents SaveAsToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator4 As ToolStripSeparator
    Friend WithEvents PrintToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintPreviewToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PrintSetupToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ToolStripSeparator5 As ToolStripSeparator
    Friend WithEvents ExitToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AbrirConfiguraciónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ConfigurarConexiónToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LineaVerdeToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents LijadoraToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents CalderaToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents DescortezadorToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents estado_controlProduccion As ToolStripStatusLabel
    Public WithEvents StatusStrip As StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As ToolStripStatusLabel
    Friend WithEvents CalderaToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents LineaVerdeToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents CalidadDeAguasToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents DiarioToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MensualToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RangosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents IndicadoresToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents RangosToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents DiariosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents MensualToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents DiarioToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents MensualToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents RangosToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents ToolStripStatusLabel3 As ToolStripStatusLabel
    Friend WithEvents nombre_usuario As ToolStripStatusLabel
    Public WithEvents MenuStrip As MenuStrip
    Friend WithEvents CuentasDeUsuarioToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents AgregarUsuarioToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ListaDeUsuariosToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents LijadoraToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents DiarioToolStripMenuItem2 As ToolStripMenuItem
    Friend WithEvents MensualToolStripMenuItem3 As ToolStripMenuItem
    Friend WithEvents PorRangosToolStripMenuItem As ToolStripMenuItem
End Class
