﻿
Imports DevExpress.XtraSplashScreen

Public Class PanelDeControl

    Private nombre As String

    Public Sub New()

        ' Esta llamada es exigida por el diseñador.
        InitializeComponent()

        ' Agregue cualquier inicialización después de la llamada a InitializeComponent().
        abrir_controlProduccion()
        abrir_eTata()
        'crear_conexiones()
    End Sub

    Public Sub New(ByVal nom As String)
        Me.New()
        nombre = nom
    End Sub

    Private Sub PanelDeControl_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim indi_Caldera As New Indicadores_Caldera
        Dim indi_Lijadora As New Indicadores_Lijadora
        Dim indi_Verde As New Indicadores_Verde
        Dim view_turno As New mostrar_Turno

        usuario.Text = nombre

        Panel_IndicadoresCaldera.Controls.Add(indi_Caldera)
        Panel_IndicadoresLijadora.Controls.Add(indi_Lijadora)
        Panel_IndicadoresVerde.Controls.Add(indi_Verde)
        Panel_turno.Controls.Add(view_turno)
    End Sub


    Private Sub Boton_Configuracion_Click(sender As Object, e As EventArgs) Handles Boton_Configuracion.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        PanelConfiguracion.Show()
        Me.Cursor = c
    End Sub

    Private Sub Boton_DetallesCaldera_Click(sender As Object, e As EventArgs) Handles Boton_DetallesCaldera.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        SplashScreenManager.ShowForm(GetType(WaitForm1))
        Panel_DetallesCaldera.Show()
        Me.Cursor = c
        SplashScreenManager.CloseForm()
    End Sub

    Private Sub Boton_DetallesVerde_Click(sender As Object, e As EventArgs) Handles Boton_DetallesVerde.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        SplashScreenManager.ShowForm(GetType(WaitForm1))
        Panel_DetallesVerde.Show()
        Me.Cursor = c
        SplashScreenManager.CloseForm()
    End Sub

    Private Sub Boton_DetallesLijadora_Click(sender As Object, e As EventArgs) Handles Boton_DetallesLijadora.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        SplashScreenManager.ShowForm(GetType(WaitForm1))
        Panel_DetallesLijadora.Show()
        Me.Cursor = c
        SplashScreenManager.CloseForm()
    End Sub

    Private Sub Boton_ConfiguracionServidor_Click(sender As Object, e As EventArgs) Handles Boton_ConfiguracionServidor.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        Conectar_Servidor.Show()
        Me.Cursor = c
    End Sub

    Private Sub Historicos_Caldera_Click(sender As Object, e As EventArgs) Handles Historicos_Caldera.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        SplashScreenManager.ShowForm(GetType(WaitForm1))
        Panel_HistoricosCaldera.Show()
        Me.Cursor = c
        SplashScreenManager.CloseForm()
    End Sub

    Private Sub Historicos_Lijadora_Click(sender As Object, e As EventArgs) Handles Historicos_Lijadora.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        SplashScreenManager.ShowForm(GetType(WaitForm1))
        Panel_HistoricosLijadora.Show()
        Me.Cursor = c
        SplashScreenManager.CloseForm()
    End Sub

    Private Sub Historicos_Verde_Click(sender As Object, e As EventArgs) Handles Historicos_Verde.Click
        Dim c As Cursor = Me.Cursor

        Me.Cursor = Cursors.WaitCursor
        SplashScreenManager.ShowForm(GetType(WaitForm1))
        Panel_HistoricosVerde.Show()
        Me.Cursor = c
        SplashScreenManager.CloseForm()
    End Sub

    Private Sub cerrar_sesion_Click(sender As Object, e As EventArgs) Handles cerrar_sesion.Click

        Dim confirmacion As String = MsgBox("¿Desea cerrar sesión?", vbOKCancel, "Cerrar Sesión")

        Try
            If (confirmacion = vbOK) Then
                Login.Show()
                CType(Me.Owner, Panel_Principal).Close()
                Me.Close()
            End If
        Catch
            Login.Show()
            Me.Close()
        End Try

    End Sub

    Private Sub PanelDeControl_FormClosing(sender As Object, e As FormClosingEventArgs) Handles MyBase.FormClosing
        icono_notificacion.Visible = False
        icono_notificacion.Dispose()
    End Sub
End Class