﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Crear_Usuario
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Crear_Usuario))
        Me.boton_guardar = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.box_rut = New System.Windows.Forms.TextBox()
        Me.box_usuario = New System.Windows.Forms.TextBox()
        Me.box_cargo = New System.Windows.Forms.TextBox()
        Me.box_rol = New System.Windows.Forms.ComboBox()
        Me.box_password = New System.Windows.Forms.TextBox()
        Me.box_unidad = New System.Windows.Forms.TextBox()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.box_digito = New System.Windows.Forms.TextBox()
        Me.SuspendLayout()
        '
        'boton_guardar
        '
        Me.boton_guardar.Location = New System.Drawing.Point(102, 236)
        Me.boton_guardar.Name = "boton_guardar"
        Me.boton_guardar.Size = New System.Drawing.Size(102, 23)
        Me.boton_guardar.TabIndex = 0
        Me.boton_guardar.Text = "Crear Usuario"
        Me.boton_guardar.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(25, 18)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(27, 13)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "Rut:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(25, 52)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(86, 13)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Nombre Usuario:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(25, 92)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(38, 13)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Cargo:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(25, 126)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(26, 13)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Rol:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(25, 161)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(64, 13)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Contraseña:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(25, 197)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(44, 13)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Unidad:"
        '
        'box_rut
        '
        Me.box_rut.Location = New System.Drawing.Point(141, 15)
        Me.box_rut.MaxLength = 8
        Me.box_rut.Name = "box_rut"
        Me.box_rut.Size = New System.Drawing.Size(98, 20)
        Me.box_rut.TabIndex = 7
        '
        'box_usuario
        '
        Me.box_usuario.Location = New System.Drawing.Point(141, 49)
        Me.box_usuario.Name = "box_usuario"
        Me.box_usuario.Size = New System.Drawing.Size(141, 20)
        Me.box_usuario.TabIndex = 9
        '
        'box_cargo
        '
        Me.box_cargo.Location = New System.Drawing.Point(141, 89)
        Me.box_cargo.Name = "box_cargo"
        Me.box_cargo.Size = New System.Drawing.Size(141, 20)
        Me.box_cargo.TabIndex = 10
        '
        'box_rol
        '
        Me.box_rol.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.box_rol.Items.AddRange(New Object() {"Administrador", "Observador"})
        Me.box_rol.Location = New System.Drawing.Point(141, 123)
        Me.box_rol.Name = "box_rol"
        Me.box_rol.Size = New System.Drawing.Size(141, 21)
        Me.box_rol.TabIndex = 11
        '
        'box_password
        '
        Me.box_password.Location = New System.Drawing.Point(141, 158)
        Me.box_password.Name = "box_password"
        Me.box_password.Size = New System.Drawing.Size(141, 20)
        Me.box_password.TabIndex = 12
        '
        'box_unidad
        '
        Me.box_unidad.Location = New System.Drawing.Point(141, 190)
        Me.box_unidad.Name = "box_unidad"
        Me.box_unidad.Size = New System.Drawing.Size(141, 20)
        Me.box_unidad.TabIndex = 13
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(244, 18)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(10, 13)
        Me.Label7.TabIndex = 13
        Me.Label7.Text = "-"
        '
        'box_digito
        '
        Me.box_digito.Location = New System.Drawing.Point(259, 15)
        Me.box_digito.MaxLength = 1
        Me.box_digito.Name = "box_digito"
        Me.box_digito.Size = New System.Drawing.Size(23, 20)
        Me.box_digito.TabIndex = 8
        '
        'Crear_Usuario
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(305, 271)
        Me.Controls.Add(Me.box_digito)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.box_unidad)
        Me.Controls.Add(Me.box_password)
        Me.Controls.Add(Me.box_rol)
        Me.Controls.Add(Me.box_cargo)
        Me.Controls.Add(Me.box_usuario)
        Me.Controls.Add(Me.box_rut)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.boton_guardar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.MaximizeBox = False
        Me.MinimizeBox = False
        Me.Name = "Crear_Usuario"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Crear Usuario"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents boton_guardar As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents box_rut As TextBox
    Friend WithEvents box_usuario As TextBox
    Friend WithEvents box_cargo As TextBox
    Friend WithEvents box_rol As ComboBox
    Friend WithEvents box_password As TextBox
    Friend WithEvents box_unidad As TextBox
    Friend WithEvents Label7 As Label
    Friend WithEvents box_digito As TextBox
End Class
