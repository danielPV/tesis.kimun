﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Indicadores_Verde
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label_ValorDescortezados = New System.Windows.Forms.Label()
        Me.Label_ValorFull = New System.Windows.Forms.Label()
        Me.Label_ValorMedia = New System.Windows.Forms.Label()
        Me.Label_ValorRandom = New System.Windows.Forms.Label()
        Me.Label_ValorFish = New System.Windows.Forms.Label()
        Me.Label_ValorTM = New System.Windows.Forms.Label()
        Me.Tiempo_Verde = New System.Windows.Forms.Timer(Me.components)
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Imagen_Descortezador = New System.Windows.Forms.PictureBox()
        Me.Imagen_Full = New System.Windows.Forms.PictureBox()
        Me.Imagen_Media = New System.Windows.Forms.PictureBox()
        Me.Imagen_Random = New System.Windows.Forms.PictureBox()
        Me.Imagen_Fish = New System.Windows.Forms.PictureBox()
        Me.Imagen_TM = New System.Windows.Forms.PictureBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.ValorOLD_trozosDescr = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.ValorOLD_fishTail = New System.Windows.Forms.Label()
        Me.ValorOLD_conteoRandom = New System.Windows.Forms.Label()
        Me.ValorOLD_mediaLamina = New System.Windows.Forms.Label()
        Me.ValorOLD_fullSheet = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.ValorOLD_tiempoMuerto = New System.Windows.Forms.Label()
        Me.Label_EstadoVerde = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        CType(Me.Imagen_Descortezador, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_Full, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_Media, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_Random, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_Fish, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_TM, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label2.Location = New System.Drawing.Point(30, 35)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(132, 19)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Descortezador: "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label3.Location = New System.Drawing.Point(30, 16)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(95, 19)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Full Sheet: "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label4.Location = New System.Drawing.Point(30, 53)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(124, 19)
        Me.Label4.TabIndex = 4
        Me.Label4.Text = "Media Lamina: "
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label5.Location = New System.Drawing.Point(30, 100)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(84, 19)
        Me.Label5.TabIndex = 5
        Me.Label5.Text = "Random: "
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label6.Location = New System.Drawing.Point(30, 132)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(82, 19)
        Me.Label6.TabIndex = 6
        Me.Label6.Text = "Fish Tail: "
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label7.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label7.Location = New System.Drawing.Point(31, 294)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(133, 19)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Tiempo Muerto: "
        '
        'Label_ValorDescortezados
        '
        Me.Label_ValorDescortezados.AutoSize = True
        Me.Label_ValorDescortezados.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorDescortezados.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorDescortezados.Location = New System.Drawing.Point(188, 34)
        Me.Label_ValorDescortezados.Name = "Label_ValorDescortezados"
        Me.Label_ValorDescortezados.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorDescortezados.TabIndex = 8
        '
        'Label_ValorFull
        '
        Me.Label_ValorFull.AutoSize = True
        Me.Label_ValorFull.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorFull.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorFull.Location = New System.Drawing.Point(188, 15)
        Me.Label_ValorFull.Name = "Label_ValorFull"
        Me.Label_ValorFull.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorFull.TabIndex = 10
        '
        'Label_ValorMedia
        '
        Me.Label_ValorMedia.AutoSize = True
        Me.Label_ValorMedia.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorMedia.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorMedia.Location = New System.Drawing.Point(188, 52)
        Me.Label_ValorMedia.Name = "Label_ValorMedia"
        Me.Label_ValorMedia.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorMedia.TabIndex = 11
        '
        'Label_ValorRandom
        '
        Me.Label_ValorRandom.AutoSize = True
        Me.Label_ValorRandom.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorRandom.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorRandom.Location = New System.Drawing.Point(188, 99)
        Me.Label_ValorRandom.Name = "Label_ValorRandom"
        Me.Label_ValorRandom.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorRandom.TabIndex = 12
        '
        'Label_ValorFish
        '
        Me.Label_ValorFish.AutoSize = True
        Me.Label_ValorFish.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorFish.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorFish.Location = New System.Drawing.Point(188, 133)
        Me.Label_ValorFish.Name = "Label_ValorFish"
        Me.Label_ValorFish.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorFish.TabIndex = 13
        '
        'Label_ValorTM
        '
        Me.Label_ValorTM.AutoSize = True
        Me.Label_ValorTM.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorTM.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorTM.Location = New System.Drawing.Point(190, 291)
        Me.Label_ValorTM.Name = "Label_ValorTM"
        Me.Label_ValorTM.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorTM.TabIndex = 14
        '
        'Tiempo_Verde
        '
        Me.Tiempo_Verde.Enabled = True
        Me.Tiempo_Verde.Interval = 60000
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label1.Location = New System.Drawing.Point(30, 16)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(62, 19)
        Me.Label1.TabIndex = 15
        Me.Label1.Text = "Trozos"
        '
        'Imagen_Descortezador
        '
        Me.Imagen_Descortezador.InitialImage = Nothing
        Me.Imagen_Descortezador.Location = New System.Drawing.Point(162, 35)
        Me.Imagen_Descortezador.Name = "Imagen_Descortezador"
        Me.Imagen_Descortezador.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_Descortezador.TabIndex = 17
        Me.Imagen_Descortezador.TabStop = False
        '
        'Imagen_Full
        '
        Me.Imagen_Full.InitialImage = Nothing
        Me.Imagen_Full.Location = New System.Drawing.Point(162, 16)
        Me.Imagen_Full.Name = "Imagen_Full"
        Me.Imagen_Full.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_Full.TabIndex = 18
        Me.Imagen_Full.TabStop = False
        '
        'Imagen_Media
        '
        Me.Imagen_Media.InitialImage = Nothing
        Me.Imagen_Media.Location = New System.Drawing.Point(162, 53)
        Me.Imagen_Media.Name = "Imagen_Media"
        Me.Imagen_Media.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_Media.TabIndex = 19
        Me.Imagen_Media.TabStop = False
        '
        'Imagen_Random
        '
        Me.Imagen_Random.InitialImage = Nothing
        Me.Imagen_Random.Location = New System.Drawing.Point(162, 100)
        Me.Imagen_Random.Name = "Imagen_Random"
        Me.Imagen_Random.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_Random.TabIndex = 20
        Me.Imagen_Random.TabStop = False
        '
        'Imagen_Fish
        '
        Me.Imagen_Fish.InitialImage = Nothing
        Me.Imagen_Fish.Location = New System.Drawing.Point(162, 134)
        Me.Imagen_Fish.Name = "Imagen_Fish"
        Me.Imagen_Fish.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_Fish.TabIndex = 21
        Me.Imagen_Fish.TabStop = False
        '
        'Imagen_TM
        '
        Me.Imagen_TM.InitialImage = Nothing
        Me.Imagen_TM.Location = New System.Drawing.Point(164, 294)
        Me.Imagen_TM.Name = "Imagen_TM"
        Me.Imagen_TM.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_TM.TabIndex = 22
        Me.Imagen_TM.TabStop = False
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.ValorOLD_trozosDescr)
        Me.GroupBox1.Controls.Add(Me.Label9)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label_ValorDescortezados)
        Me.GroupBox1.Controls.Add(Me.Imagen_Descortezador)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 27)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(309, 80)
        Me.GroupBox1.TabIndex = 23
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Descortezador"
        '
        'ValorOLD_trozosDescr
        '
        Me.ValorOLD_trozosDescr.AutoSize = True
        Me.ValorOLD_trozosDescr.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_trozosDescr.Location = New System.Drawing.Point(165, 60)
        Me.ValorOLD_trozosDescr.Name = "ValorOLD_trozosDescr"
        Me.ValorOLD_trozosDescr.Size = New System.Drawing.Size(115, 13)
        Me.ValorOLD_trozosDescr.TabIndex = 19
        Me.ValorOLD_trozosDescr.Text = "ValorOLD_trozosDescr"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label9.Location = New System.Drawing.Point(29, 60)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(135, 13)
        Me.Label9.TabIndex = 18
        Me.Label9.Text = "Trozos descortezador ayer:"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.ValorOLD_fishTail)
        Me.GroupBox2.Controls.Add(Me.ValorOLD_conteoRandom)
        Me.GroupBox2.Controls.Add(Me.ValorOLD_mediaLamina)
        Me.GroupBox2.Controls.Add(Me.ValorOLD_fullSheet)
        Me.GroupBox2.Controls.Add(Me.Label8)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.Label11)
        Me.GroupBox2.Controls.Add(Me.Label10)
        Me.GroupBox2.Controls.Add(Me.Label3)
        Me.GroupBox2.Controls.Add(Me.Label4)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Imagen_Fish)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Imagen_Random)
        Me.GroupBox2.Controls.Add(Me.Label_ValorFull)
        Me.GroupBox2.Controls.Add(Me.Imagen_Media)
        Me.GroupBox2.Controls.Add(Me.Label_ValorMedia)
        Me.GroupBox2.Controls.Add(Me.Imagen_Full)
        Me.GroupBox2.Controls.Add(Me.Label_ValorRandom)
        Me.GroupBox2.Controls.Add(Me.Label_ValorFish)
        Me.GroupBox2.Location = New System.Drawing.Point(3, 111)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(309, 180)
        Me.GroupBox2.TabIndex = 24
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Torno"
        '
        'ValorOLD_fishTail
        '
        Me.ValorOLD_fishTail.AutoSize = True
        Me.ValorOLD_fishTail.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_fishTail.Location = New System.Drawing.Point(165, 162)
        Me.ValorOLD_fishTail.Name = "ValorOLD_fishTail"
        Me.ValorOLD_fishTail.Size = New System.Drawing.Size(45, 13)
        Me.ValorOLD_fishTail.TabIndex = 29
        Me.ValorOLD_fishTail.Text = "Label17"
        '
        'ValorOLD_conteoRandom
        '
        Me.ValorOLD_conteoRandom.AutoSize = True
        Me.ValorOLD_conteoRandom.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_conteoRandom.Location = New System.Drawing.Point(165, 119)
        Me.ValorOLD_conteoRandom.Name = "ValorOLD_conteoRandom"
        Me.ValorOLD_conteoRandom.Size = New System.Drawing.Size(45, 13)
        Me.ValorOLD_conteoRandom.TabIndex = 28
        Me.ValorOLD_conteoRandom.Text = "Label16"
        '
        'ValorOLD_mediaLamina
        '
        Me.ValorOLD_mediaLamina.AutoSize = True
        Me.ValorOLD_mediaLamina.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_mediaLamina.Location = New System.Drawing.Point(165, 79)
        Me.ValorOLD_mediaLamina.Name = "ValorOLD_mediaLamina"
        Me.ValorOLD_mediaLamina.Size = New System.Drawing.Size(45, 13)
        Me.ValorOLD_mediaLamina.TabIndex = 27
        Me.ValorOLD_mediaLamina.Text = "Label15"
        '
        'ValorOLD_fullSheet
        '
        Me.ValorOLD_fullSheet.AutoSize = True
        Me.ValorOLD_fullSheet.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_fullSheet.Location = New System.Drawing.Point(165, 40)
        Me.ValorOLD_fullSheet.Name = "ValorOLD_fullSheet"
        Me.ValorOLD_fullSheet.Size = New System.Drawing.Size(45, 13)
        Me.ValorOLD_fullSheet.TabIndex = 26
        Me.ValorOLD_fullSheet.Text = "Label14"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label8.Location = New System.Drawing.Point(85, 162)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(72, 13)
        Me.Label8.TabIndex = 25
        Me.Label8.Text = "Fish Tail ayer:"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label12.Location = New System.Drawing.Point(85, 119)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(73, 13)
        Me.Label12.TabIndex = 24
        Me.Label12.Text = "Random ayer:"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label11.Location = New System.Drawing.Point(59, 79)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(99, 13)
        Me.Label11.TabIndex = 23
        Me.Label11.Text = "Media Lamina ayer:"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label10.Location = New System.Drawing.Point(78, 40)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(80, 13)
        Me.Label10.TabIndex = 22
        Me.Label10.Text = "Full Sheet ayer:"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label13.Location = New System.Drawing.Point(58, 316)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(103, 13)
        Me.Label13.TabIndex = 25
        Me.Label13.Text = "Tiempo muerto ayer:"
        '
        'ValorOLD_tiempoMuerto
        '
        Me.ValorOLD_tiempoMuerto.AutoSize = True
        Me.ValorOLD_tiempoMuerto.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_tiempoMuerto.Location = New System.Drawing.Point(168, 316)
        Me.ValorOLD_tiempoMuerto.Name = "ValorOLD_tiempoMuerto"
        Me.ValorOLD_tiempoMuerto.Size = New System.Drawing.Size(45, 13)
        Me.ValorOLD_tiempoMuerto.TabIndex = 30
        Me.ValorOLD_tiempoMuerto.Text = "Label18"
        '
        'Label_EstadoVerde
        '
        Me.Label_EstadoVerde.AutoSize = True
        Me.Label_EstadoVerde.BackColor = System.Drawing.Color.Transparent
        Me.Label_EstadoVerde.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_EstadoVerde.Location = New System.Drawing.Point(141, 4)
        Me.Label_EstadoVerde.Name = "Label_EstadoVerde"
        Me.Label_EstadoVerde.Size = New System.Drawing.Size(63, 20)
        Me.Label_EstadoVerde.TabIndex = 33
        Me.Label_EstadoVerde.Text = "Label2"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.BackColor = System.Drawing.Color.Transparent
        Me.Label14.Location = New System.Drawing.Point(151, 9)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(0, 13)
        Me.Label14.TabIndex = 32
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.BackColor = System.Drawing.Color.Transparent
        Me.Label15.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label15.Location = New System.Drawing.Point(77, 4)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(68, 20)
        Me.Label15.TabIndex = 31
        Me.Label15.Text = "Estado: "
        '
        'Indicadores_Verde
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.Label_EstadoVerde)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.Label15)
        Me.Controls.Add(Me.ValorOLD_tiempoMuerto)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Imagen_TM)
        Me.Controls.Add(Me.Label_ValorTM)
        Me.Controls.Add(Me.Label7)
        Me.Name = "Indicadores_Verde"
        Me.Size = New System.Drawing.Size(315, 335)
        CType(Me.Imagen_Descortezador, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_Full, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_Media, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_Random, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_Fish, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_TM, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label_ValorDescortezados As Label
    Friend WithEvents Label_ValorFull As Label
    Friend WithEvents Label_ValorMedia As Label
    Friend WithEvents Label_ValorRandom As Label
    Friend WithEvents Label_ValorFish As Label
    Friend WithEvents Label_ValorTM As Label
    Friend WithEvents Tiempo_Verde As Timer
    Friend WithEvents Label1 As Label
    Friend WithEvents Imagen_Descortezador As PictureBox
    Friend WithEvents Imagen_Full As PictureBox
    Friend WithEvents Imagen_Media As PictureBox
    Friend WithEvents Imagen_Random As PictureBox
    Friend WithEvents Imagen_Fish As PictureBox
    Friend WithEvents Imagen_TM As PictureBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents ValorOLD_trozosDescr As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents ValorOLD_fishTail As Label
    Friend WithEvents ValorOLD_conteoRandom As Label
    Friend WithEvents ValorOLD_mediaLamina As Label
    Friend WithEvents ValorOLD_fullSheet As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents ValorOLD_tiempoMuerto As Label
    Friend WithEvents Label_EstadoVerde As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label15 As Label
End Class
