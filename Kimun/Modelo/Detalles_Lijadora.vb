﻿Imports System.Data.SqlClient
Imports DevExpress.Diagram.Core
Imports DevExpress.XtraCharts
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraSplashScreen

Public Class Detalles_Lijadora

    Private Sub Detalles_Lijadora_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'REGRESION LINEAL
        Dim inicio_Calculo As Date = Format(DateAdd(DateInterval.Month, -6, Now), "yyyy-MM-dd")
        Dim fin_Calculo As Date = Format(Now, "yyyy-MM-dd")

        'Se llenan las bases de datos para mostrar los datos
        insertarDatos_TablaPronostico_Lijadora("Lijadora", inicio_Calculo, fin_Calculo)
        regresion_Lijadora()


        'INDICADORES DE PRODUCCION
        Dim inicioMes As Date = Format(DateSerial(Year(Now), Month(Now), 1), "yyyy-MM-dd")

        Dim inicio_anioAnterior As Date = Format(DateAdd(DateInterval.Year, -1, inicioMes), "yyyy-MM-dd")
        Dim fin_anioAnterior As Date = Format(DateAdd(DateInterval.Year, -1, Now), "yyyy-MM-dd")
        Dim inicio_hoy As Date = Format(inicioMes, "yyyy-MM-dd")
        Dim fin_hoy As Date = Format(Now, "yyyy-MM-dd")
        Dim inicio_mesAnterior As Date = Format(DateAdd(DateInterval.Month, -1, inicioMes), "yyyy-MM-dd")
        Dim fin_mesAnterior As Date = Format(DateAdd(DateInterval.Month, -1, Now), "yyyy-MM-dd")

        Dim valor_anioAnterior As Double = mostrar_Produccion("anioAnterior", inicio_anioAnterior, fin_anioAnterior)
        Dim valor_hoy As Double = mostrar_Produccion("hoy", inicio_hoy, fin_hoy)
        Dim valor_mesAnterior As Double = mostrar_Produccion("mesAnterior", inicio_mesAnterior, fin_mesAnterior)

        Prod_AnioAnterior.Text = String.Format("{0} {1}", valor_anioAnterior, "tableros")
        Prod_Hoy.Text = String.Format("{0} {1}", valor_hoy, "tableros")
        Prod_MesAnterior.Text = String.Format("{0} {1}", valor_mesAnterior, "tableros")

        'Mostrar resumen produccion
        comparacion_produccion.Text = comparaciones_produccion(valor_anioAnterior, valor_hoy)
        comparacion_produccion2.Text = comparaciones_produccion2(valor_mesAnterior, valor_hoy)

        mostrar_IndicadoresProduccion()
        comparaciones_chispa()
    End Sub


    Private Sub comparaciones_chispa()

        Dim contador_pasado As Integer = 0
        Dim contador_presente As Integer = 0
        Dim cadena As String = ""
        Dim inicio_mes As Date = Format(DateSerial(Year(Now), Month(Now), 1), "yyyy-MM-dd")


        Dim consultar_pasado = New SqlCommand("SELECT COUNT(FloatLijadora.Val)
                                        FROM EquivalenciaLijadora INNER JOIN
                                        FloatLijadora ON EquivalenciaLijadora.TagIndex = FloatLijadora.TagIndex
                                        WHERE EquivalenciaLijadora.Equivalencia = 'Chispa detectada' AND FloatLijadora.Val > 0 AND FloatLijadora.DateAndTime between @fecha1 AND @fecha2 ", conexion_eTata)

        Dim consultar_presente = New SqlCommand("SELECT COUNT(FloatLijadora.Val)
                                        FROM EquivalenciaLijadora INNER JOIN
                                        FloatLijadora ON EquivalenciaLijadora.TagIndex = FloatLijadora.TagIndex
                                        WHERE EquivalenciaLijadora.Equivalencia = 'Chispa detectada' AND FloatLijadora.Val > 0 AND FloatLijadora.DateAndTime between @fecha1 AND @fecha2 ", conexion_eTata)

        Try

            If (conexion_eTata.State = ConnectionState.Closed) Then
                conectar_eTata.Open()
            End If

            Dim fecha1 As Date = Format(DateSerial(Year(Now), Month(Now) - 1, 1), "yyyy-MM-dd")
            Dim fecha2 As Date = Format(DateSerial(Year(Now), Month(Now), 1), "yyyy-MM-dd")

            consultar_pasado.Parameters.AddWithValue("@fecha1", fecha1)
            consultar_pasado.Parameters.AddWithValue("@fecha2", fecha2)

            consultar_presente.Parameters.AddWithValue("@fecha1", inicio_mes)
            consultar_presente.Parameters.AddWithValue("@fecha2", Now)

            contador_pasado = consultar_pasado.ExecuteScalar()
            contador_presente = consultar_presente.ExecuteScalar()

            If (contador_presente > 0) Then
                cadena = "Este mes se han observado " & contador_presente & " chispas en la lijadora"
                semaforo_chispa.StateIndex = 1
            Else
                cadena = "Este mes, no se han detectado chispas en la lijadora"
                semaforo_chispa.StateIndex = 3
            End If

            If (contador_pasado > 0) Then
                Dim delta As Integer = Math.Abs(contador_presente - contador_pasado)

                If (contador_presente > contador_pasado) Then
                    If (cadena.Contains("se han observado") = True) Then
                        cadena = String.Format("{0}{1}", cadena, ", número que ha aumentado respecto al mes anterior, con una diferencia en contra de " & delta & " chispas.")
                    Else
                        cadena = String.Format("{0}{1}", cadena, ", mientras que el mes pasado, el número de chispas fue de " & contador_pasado & ".")
                    End If
                    semaforo_chispa.StateIndex = 1
                Else
                    If (cadena.Contains("se han observado") = True) Then
                        cadena = String.Format("{0}{1}", cadena, ", número que ha disminuido respecto al mes anterior, con una diferencia a favor de " & delta & " chispas.")
                    Else
                        cadena = String.Format("{0}{1}", cadena, ", mientras que el mes pasado, el número de chispas fue de " & contador_pasado & ".")
                    End If
                    semaforo_chispa.StateIndex = 3
                End If
            Else
                If (cadena.Contains("no se han detectado chispas") = True) Then
                    cadena = String.Format("{0} {1}", cadena, " al igual que en el mes anterior.")
                Else
                    cadena = String.Format("{0}{1}", cadena, ", mientras que el mes pasado, no se observaron chispas en la lijadora.")
                End If
                semaforo_chispa.StateIndex = 3
            End If

            comparacion_chispa.Text = cadena

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar el numero de chispas")
        End Try

    End Sub

    Private Sub regresion_Lijadora()
        '***************************************************************************************
        '***************************** TAB Pronostico Torno ************************************
        '***************************************************************************************

        'consulta que rescata los datos de la tabla 
        Dim consultar_tablaRegresionLineal = New SqlCommand("SELECT  fecha As Fecha, produccion As Producción
                                                             FROM TablaPronostico_Lijadora
                                                             WHERE fecha <= @fecha
                                                             GROUP BY fecha, produccion", conexion_controlProduccion)

        Dim consultar_pronostico_Lijadora = New SqlCommand("SELECT  fecha As Fecha, pronostico AS Pronóstico
                                                             FROM TablaPronostico_Lijadora
                                                             WHERE fecha > @fecha", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consultar_tablaRegresionLineal.Parameters.AddWithValue("@fecha", Now)
            consultar_pronostico_Lijadora.Parameters.AddWithValue("@fecha", Now)

            Dim dataAdapter As New SqlDataAdapter(consultar_tablaRegresionLineal)
            Dim dataAdapter_pronostico As New SqlDataAdapter(consultar_pronostico_Lijadora)
            Dim tabla As New DataTable
            Dim tabla_pronostico As New DataTable

            'Llena la tabla
            dataAdapter.Fill(tabla)
            dataAdapter_pronostico.Fill(tabla_pronostico)

            Tabla_RegresionLineal.DataSource = tabla
            tabla_pronostico1.DataSource = tabla_pronostico

            'Consulta para grafico
            Dim mensaje As String = vbCr & "No existen datos suficientes para calcular el pronóstico. Deben existir datos de producción de la lijadora por lo menos de un mes para poder realizar los calculos."

            Dim formato As New StringFormat
            formato.Alignment = StringAlignment.Center
            Dim consultar_Pronostico As New SqlCommand("SELECT Key_Pronostico, produccion FROM TablaPronostico_Lijadora", conexion_controlProduccion)
            Dim funcion As New Funciones

            If (funcion.tablaVacia_Lijadora() = False) Then
                grafico_lijadora.CreateGraphics.FillRectangle(Brushes.White, grafico_lijadora.Bounds)
                grafico_lijadora.CreateGraphics.DrawString(mensaje, grafico_lijadora.EmptyChartText.Font, Brushes.Black, grafico_lijadora.Bounds, formato)
            Else
                Dim dataAdapter2 = New SqlDataAdapter(consultar_Pronostico)
                Dim dataSet = New DataSet()
                Dim serie = New Series("Producción", ViewType.Line)

                'Se agrega la linea al grafico
                grafico_lijadora.Series.Add(serie)
                dataAdapter2.Fill(dataSet, "TablaPronostico_Lijadora")
                serie.DataSource = dataSet.Tables(0)
                grafico_lijadora.Series(0).ArgumentScaleType = ScaleType.Numerical

                serie.ArgumentDataMember = "Key_Pronostico"
                serie.ValueDataMembers(0) = "produccion"

                Dim myText As EmptyChartText = grafico_lijadora.EmptyChartText
                myText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.True
                myText.Text = "No existen datos suficientes para calcular el pronóstico.
                            Deben existir datos de producción de la lijadora por lo menos 
                            de un mes para poder realizar los calculos."
                myText.TextColor = Color.Black

                'Se crea la linea de la regresion lineal
                Dim myLine As New RegressionLine(ValueLevel.Open)

                'Se añade la linea al grafico
                CType(grafico_lijadora.Series(0).View, XYDiagramSeriesViewBase).Indicators.Add(myLine)

                ' Customize the regression line's appearance.
                myLine.LineStyle.DashStyle = DashStyle.DashDot
                myLine.LineStyle.Thickness = 2
                myLine.Color = Color.Crimson
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al calcular regresión lineal de la lijadora")
        End Try
    End Sub

    Private Sub mostrar_IndicadoresProduccion()

        Dim MesAnt_TM As Double = 0
        Dim MesAct_TM As Double = 0
        Dim MesAnt_Produccion As Double = 0
        Dim MesAct_produccion As Double = 0
        Dim MesAnt_Disponibilidad As String = ""
        Dim MesAct_Disponibilidad As String = ""
        Dim MesAnt_Productividad As String = ""
        Dim MesAct_Productividad As String = ""
        Dim MesAnt_FO As String = ""
        Dim MesAct_FO As String = ""

        Dim consultar_MesAnterior_TM = New SqlCommand("SELECT AVG(Produccion.TotalTM)
                                                    FROM Linea INNER JOIN
                                                        TM ON Linea.IdLinea = TM.IdLinea INNER JOIN
                                                        Produccion ON Linea.IdLinea = Produccion.IdLinea INNER JOIN
                                                        Turno ON Produccion.TurnoProduccion = Turno.CodTurno
                                                    WHERE (Linea.Linea = 'Lijadora') AND (Produccion.FechaProduccion BETWEEN @fecha1 AND @fecha2)", conexion_controlProduccion)

        Dim consultar_MesActual_TM = New SqlCommand("SELECT AVG(Produccion.TotalTM)
                                                    FROM Linea INNER JOIN
                                                        TM ON Linea.IdLinea = TM.IdLinea INNER JOIN
                                                        Produccion ON Linea.IdLinea = Produccion.IdLinea INNER JOIN
                                                        Turno ON Produccion.TurnoProduccion = Turno.CodTurno
                                                    WHERE (Linea.Linea = 'Lijadora') AND (Produccion.FechaProduccion BETWEEN @fecha1 AND @fecha2)", conexion_controlProduccion)

        Dim consultar_MesAnterior_produccion = New SqlCommand("SELECT AVG(Produccion.Produccion)
                                                    FROM Linea INNER JOIN
                                                     TM ON Linea.IdLinea = TM.IdLinea INNER JOIN
                                                     Produccion ON Linea.IdLinea = Produccion.IdLinea INNER JOIN
                                                     Turno ON Produccion.TurnoProduccion = Turno.CodTurno
                                                    WHERE (Linea.Linea = 'Lijadora') AND (Produccion.FechaProduccion BETWEEN @fecha1 AND @fecha2)", conexion_controlProduccion)

        Dim consultar_MesActual_produccion = New SqlCommand("SELECT AVG(Produccion.Produccion)
                                                    FROM Linea INNER JOIN
                                                     TM ON Linea.IdLinea = TM.IdLinea INNER JOIN
                                                     Produccion ON Linea.IdLinea = Produccion.IdLinea INNER JOIN
                                                     Turno ON Produccion.TurnoProduccion = Turno.CodTurno
                                                    WHERE (Linea.Linea = 'Lijadora') AND (Produccion.FechaProduccion BETWEEN @fecha1 AND @fecha2)", conexion_controlProduccion)


        Dim consultar_MesActual_meta = New SqlCommand("SELECT Avg(Linea.Meta)
                                            FROM Linea INNER JOIN
                                             TM ON Linea.IdLinea = TM.IdLinea INNER JOIN
                                             Produccion ON Linea.IdLinea = Produccion.IdLinea INNER JOIN
                                             Turno ON Produccion.TurnoProduccion = Turno.CodTurno
                                            WHERE (Linea.Linea = 'Lijadora') AND (Produccion.FechaProduccion BETWEEN @fecha1 AND @fecha2)", conexion_controlProduccion)

        Dim consultar_MesAnterior_meta = New SqlCommand("SELECT Avg(Linea.Meta)
                                            FROM Linea INNER JOIN
                                             TM ON Linea.IdLinea = TM.IdLinea INNER JOIN
                                             Produccion ON Linea.IdLinea = Produccion.IdLinea INNER JOIN
                                             Turno ON Produccion.TurnoProduccion = Turno.CodTurno
                                            WHERE (Linea.Linea = 'Lijadora') AND (Produccion.FechaProduccion BETWEEN @fecha1 AND @fecha2)", conexion_controlProduccion)

        Dim ant_tiempoMuerto As Double
        Dim ant_produccion As Double
        Dim ant_meta As Double

        Dim act_tiempoMuerto As Double
        Dim act_produccion As Double
        Dim act_meta As Double

        Dim inicio_mesAnterior As Date = Format(DateSerial(Year(Now), Month(Now) - 1, 1), "yyyy-MM-dd")
        Dim fin_mesAnterior As Date = Format(DateSerial(Year(Now), Month(Now), 1), "yyyy-MM-dd")

        Dim inicio_mesActual As Date = Format(DateSerial(Year(Now), Month(Now), 1), "yyyy-MM-dd")

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            'CONSULTAS MES ANTERIOR

            'PARAMETROS
            consultar_MesAnterior_TM.Parameters.AddWithValue("@fecha1", inicio_mesAnterior)
            consultar_MesAnterior_TM.Parameters.AddWithValue("@fecha2", fin_mesAnterior)

            consultar_MesAnterior_produccion.Parameters.AddWithValue("@fecha1", inicio_mesAnterior)
            consultar_MesAnterior_produccion.Parameters.AddWithValue("@fecha2", fin_mesAnterior)

            consultar_MesAnterior_meta.Parameters.AddWithValue("@fecha1", inicio_mesAnterior)
            consultar_MesAnterior_meta.Parameters.AddWithValue("@fecha2", fin_mesAnterior)

            'PRODUCCION MES ANTERIOR
            If (consultar_MesAnterior_TM.ExecuteScalar().ToString = "") Then
                ant_tiempoMuerto = 0
            Else
                ant_tiempoMuerto = Format(consultar_MesAnterior_TM.ExecuteScalar(), "000.00")
            End If

            If (consultar_MesAnterior_produccion.ExecuteScalar().ToString = "") Then
                ant_produccion = 0
            Else
                ant_produccion = Format(consultar_MesAnterior_produccion.ExecuteScalar(), "000.00")
            End If

            If (consultar_MesAnterior_meta.ExecuteScalar().ToString = "") Then
                ant_meta = 0
            Else
                ant_meta = consultar_MesAnterior_meta.ExecuteScalar()
            End If

            MesAnt_TM = (450 - ant_tiempoMuerto) / 60
            MesAnt_Produccion = Format(ant_produccion / MesAnt_TM, "000.00")

            MA_Produccion.Text = String.Format("{0} {1}", MesAnt_Produccion, "Tableros")

            'DISPONIBILIDAD MES ANTERIOR
            Dim ant_disponibilidad As Double = 0
            ant_disponibilidad = (450 - ant_tiempoMuerto) / 450

            MesAnt_Disponibilidad = FormatPercent(ant_disponibilidad)

            MA_Dispo.Text = MesAnt_Disponibilidad

            'PRODUCTIVIDAD MES ANTERIOR
            Dim ant_productividad As Double = 0
            ant_productividad = MesAnt_Produccion / ant_meta

            MesAnt_Productividad = FormatPercent(ant_productividad)

            MA_Producti.Text = MesAnt_Productividad

            'FACTOR OPERACIONAL
            Dim ant_fo As Double = 0
            ant_fo = ant_disponibilidad * ant_productividad

            MesAnt_FO = FormatPercent(ant_fo)

            MA_FO.Text = MesAnt_FO

            'CONSULTAS MES ACTUAL

            'PARAMETROS
            consultar_MesActual_TM.Parameters.AddWithValue("@fecha1", inicio_mesActual)
            consultar_MesActual_TM.Parameters.AddWithValue("@fecha2", Now)

            consultar_MesActual_produccion.Parameters.AddWithValue("@fecha1", inicio_mesActual)
            consultar_MesActual_produccion.Parameters.AddWithValue("@fecha2", Now)

            consultar_MesActual_meta.Parameters.AddWithValue("@fecha1", inicio_mesActual)
            consultar_MesActual_meta.Parameters.AddWithValue("@fecha2", Now)

            If (consultar_MesActual_TM.ExecuteScalar().ToString = "") Then
                act_tiempoMuerto = 0
            Else
                act_tiempoMuerto = Format(consultar_MesActual_TM.ExecuteScalar(), "000.00")
            End If

            If (consultar_MesActual_produccion.ExecuteScalar().ToString = "") Then
                act_produccion = 0
            Else
                act_produccion = Format(consultar_MesActual_produccion.ExecuteScalar(), "000.00")
            End If

            If (consultar_MesActual_meta.ExecuteScalar().ToString = "") Then
                act_meta = 0
            Else
                act_meta = consultar_MesActual_meta.ExecuteScalar()
            End If

            'PRODUCCION MES ACTUAL
            MesAct_TM = (450 - act_tiempoMuerto) / 60
            MesAct_produccion = Format(act_produccion / MesAct_TM, "000.00")

            MAC_Produccion.Text = String.Format("{0} {1}", MesAct_produccion, "tableros")

            'DISPONIBILIDAD MES ACTUAL
            Dim act_disponibilidad As Double = 0
            act_disponibilidad = (450 - act_tiempoMuerto) / 450

            MesAct_Disponibilidad = FormatPercent(act_disponibilidad)

            MAC_Dispo.Text = MesAct_Disponibilidad

            'PRODUCTIVIDAD MES ACTUAL
            Dim act_productividad As Double = 0
            act_productividad = MesAct_produccion / act_meta

            MesAct_Productividad = FormatPercent(act_productividad)

            MAC_Producti.Text = MesAct_Productividad

            'FACTOR OPERACIONAL MES ACTUAL
            Dim act_fo As Double = 0
            act_fo = act_disponibilidad * act_productividad

            MesAct_FO = FormatPercent(act_fo)

            MAC_FO.Text = MesAct_FO

            conexion_controlProduccion.Close()

            'comparaciones indicadores produccion
            comparacion_IndicadoresProduccion.Text = comparaciones_IndicadorProduccion(MesAnt_Produccion, MesAct_produccion)

            'comparacion disponibilidad
            comparacion_disponibilidad.Text = comparaciones_disponibilidad(ant_disponibilidad, act_disponibilidad)

            'comparacion productividad
            comparacion_productividad.Text = comparaciones_productividad(ant_productividad, act_productividad)

            'comparacion factor operacional
            comparacion_fo.Text = comparaciones_FactorOperacional(ant_fo, act_fo)

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al calcular los indicadores de producción de la lijadora")
        End Try

    End Sub

    Private Function mostrar_Produccion(ByVal epoca As String, ByVal fecha1 As Date, ByVal fecha2 As Date) As Double



        Dim produccion As Double = 0
        Dim TM As Double = 0


        Dim consultar_Produccion = New SqlCommand("SELECT AVG(Produccion.Produccion)
                                                    FROM Linea INNER JOIN
                                                     TM ON Linea.IdLinea = TM.IdLinea INNER JOIN
                                                     Produccion ON Linea.IdLinea = Produccion.IdLinea INNER JOIN
                                                     Turno ON Produccion.TurnoProduccion = Turno.CodTurno
                                                    WHERE (Linea.Linea = 'Lijadora') AND (Produccion.FechaProduccion BETWEEN @fecha1 AND @fecha2)", conexion_controlProduccion)

        Dim consultar_TM = New SqlCommand("SELECT AVG(Produccion.TotalTM)
                                                    FROM Linea INNER JOIN
                                                        TM ON Linea.IdLinea = TM.IdLinea INNER JOIN
                                                        Produccion ON Linea.IdLinea = Produccion.IdLinea INNER JOIN
                                                        Turno ON Produccion.TurnoProduccion = Turno.CodTurno
                                                    WHERE (Linea.Linea = 'Lijadora') AND (Produccion.FechaProduccion BETWEEN @fecha1 AND @fecha2)", conexion_controlProduccion)


        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consultar_Produccion.Parameters.AddWithValue("@fecha1", fecha1)
            consultar_Produccion.Parameters.AddWithValue("@fecha2", fecha2)
            consultar_TM.Parameters.AddWithValue("@fecha1", fecha1)
            consultar_TM.Parameters.AddWithValue("@fecha2", fecha2)

            If (consultar_Produccion.ExecuteScalar().ToString = "") Then
                produccion = 0
            Else
                produccion = Format(consultar_Produccion.ExecuteScalar(), "000.00")
            End If

            If (consultar_TM.ExecuteScalar().ToString = "") Then
                TM = 0
            Else
                TM = Format(consultar_TM.ExecuteScalar(), "000.00")
            End If

            Dim TM_total As Double = (450 - TM) / 60

            Dim prod_total As Double = Format(produccion / TM_total, "000.00")

            Return prod_total

        Catch ex As Exception
            'MsgBox(ex.Message, MsgBoxStyle.Information, "Error al calcular la producción de la lijadora")

            If (epoca = "anioAnterior") Then
                Prod_AnioAnterior.ForeColor = Color.Red
                ToolTip1.SetToolTip(Prod_AnioAnterior, "No se encontrarón datos del año anterior.")

            End If

            If (epoca = "hoy") Then
                Prod_Hoy.ForeColor = Color.Red
                ToolTip1.SetToolTip(Prod_Hoy, "No se encontrarón datos del dia de hoy.")
            End If

            If (epoca = "mesAnterior") Then
                Prod_MesAnterior.ForeColor = Color.Red
                ToolTip1.SetToolTip(Prod_MesAnterior, "No se encontrarón datos del mes anterior.")
            End If

            Return 0

        End Try

    End Function

    Private Function comparaciones_IndicadorProduccion(ByVal valor_MesAnterior As Double, ByVal valor_MesActual As Double) As String

        Dim cadena As String = ""
        Dim delta As Double

        If (valor_MesAnterior < valor_MesActual) Then
            delta = Math.Abs(valor_MesActual - valor_MesAnterior)
            cadena = "El promedio de producción de la lijadora este mes ha superado el promedio de producción de la misma linea en el mes anterior, con una diferencia de " & delta & "  tableros."
            semaforo_IndicadoresProduccion.StateIndex = 3
        End If

        If (valor_MesAnterior > valor_MesActual) Then
            delta = Math.Abs(valor_MesAnterior - valor_MesActual)
            cadena = "El promedio de producción de la lijadora este mes es de " & valor_MesActual & " tableros, teniendo un déficit de un " & delta & " tableros respecto al promedio de producción del mes anterior."

            If (valor_MesAnterior * 0.5 > valor_MesActual) Then
                semaforo_IndicadoresProduccion.StateIndex = 2
            Else
                semaforo_IndicadoresProduccion.StateIndex = 1
            End If

        End If

        If (valor_MesAnterior = valor_MesActual) Then
            cadena = "El promedio de producción de la lijadora este mes es igual al promedio de producción del mes anterior."
            semaforo_IndicadoresProduccion.StateIndex = 3
        End If

        Return cadena
    End Function

    Private Function comparaciones_produccion(ByVal valor_ayer As Double, ByVal valor_hoy As Double) As String
        Dim cadena As String = ""
        Dim delta As Double

        If (valor_ayer < valor_hoy) Then
            delta = Format(Math.Abs(valor_ayer - valor_hoy), "000.00")
            cadena = "La producción de la lijadora es superior a la producción del año pasado en este mismo instante, con un margen favorable de " & delta & " tableros."
            semaforo_produccion.StateIndex = 3
        End If

        If (valor_ayer > valor_hoy) Then
            delta = Format(Math.Abs(valor_hoy - valor_ayer), "000.00")
            cadena = "La producción de la lijadora es inferior a la producción del año pasado en este mismo instante, con un margen en contra de " & delta & " tableros."
            semaforo_produccion.StateIndex = 1
        End If

        If (valor_ayer = valor_hoy) Then
            cadena = "La producción de la lijadora es igual a la producción del año pasado en este mismo instante."
            semaforo_produccion.StateIndex = 3
        End If

        Return cadena
    End Function

    Private Function comparaciones_produccion2(ByVal valor_MA As Double, ByVal valor_hoy As Double) As String
        Dim cadena As String = ""
        Dim delta As Double

        If (valor_MA < valor_hoy) Then
            delta = Format(Math.Abs(valor_MA - valor_hoy), "000.00")
            cadena = "La producción de la lijadora es superior a la producción del mes anterior en este mismo instante, con un margen favorable de  " & delta & " tableros."
            semaforo_produccion2.StateIndex = 3
        End If

        If (valor_MA > valor_hoy) Then
            delta = Format(Math.Abs(valor_hoy - valor_MA), "000.00")
            cadena = "La producción de la lijadora es inferior a la producción del mes anterior en este mismo instante, con un margen en contra de " & delta & " tableros."
            semaforo_produccion2.StateIndex = 1
        End If

        If (valor_MA = valor_hoy) Then
            cadena = "La producción de la lijadora es igual a la producción del mes anterior en este mismo instante."
            semaforo_produccion2.StateIndex = 3
        End If

        Return cadena
    End Function

    Private Function comparaciones_disponibilidad(ByVal valor_MA As Double, ByVal valor_Hoy As Double) As String
        Dim cadena As String = ""
        Dim delta As Double

        If (valor_MA < valor_Hoy) Then
            delta = Math.Abs(valor_MA - valor_Hoy)
            cadena = "La disponibilidad de la lijadora es superior al porcentaje del mes anterior en este mismo instante, con un margen favorable de " & FormatPercent(delta) & "."
            semaforo_disponibilidad.StateIndex = 3
        End If

        If (valor_MA > valor_Hoy) Then
            delta = Math.Abs(valor_MA - valor_Hoy)
            cadena = "La disponibilidad de la lijadora es inferior al porcentaje del mes anterior en este mismo instante, con un margen en contra de " & FormatPercent(delta) & "."
            semaforo_disponibilidad.StateIndex = 1
        End If

        If (valor_MA = valor_Hoy) Then
            cadena = "La disponibilidad de la lijadora es igual al porcentaje del mes anterior en este mismo instante."
            semaforo_disponibilidad.StateIndex = 3
        End If

        Return cadena
    End Function

    Private Function comparaciones_productividad(ByVal valor_MA As Double, ByVal valor_Hoy As Double) As String
        Dim cadena As String = ""
        Dim delta As Double

        If (valor_MA < valor_Hoy) Then
            delta = Math.Abs(valor_MA - valor_Hoy)
            cadena = "La productividad de la lijadora es superior al porcentaje del mes anterior en este mismo instante, con un margen favorable de " & FormatPercent(delta) & "."
            semaforo_productividad.StateIndex = 3
        End If

        If (valor_MA > valor_Hoy) Then
            delta = Math.Abs(valor_MA - valor_Hoy)
            cadena = "La productividad de la lijadora es inferior al porcentaje del mes anterior en este mismo instante, con un margen en contra de " & FormatPercent(delta) & "."
            semaforo_productividad.StateIndex = 1
        End If

        If (valor_MA = valor_Hoy) Then
            cadena = "La productividad de la lijadora es igual al porcentaje del mes anterior en este mismo instante."
            semaforo_productividad.StateIndex = 3
        End If

        If (Double.NaN.Equals(valor_MA) = True Or Double.NaN.Equals(valor_Hoy) = True) Then
            cadena = "No hay registros para calcular la productividad de la lijadora."
            semaforo_productividad.StateIndex = 1
        End If

        Return cadena
    End Function

    Private Function comparaciones_FactorOperacional(ByVal valor_MA As Double, ByVal valor_Hoy As Double) As String
        Dim cadena As String = ""
        Dim delta As Double

        If (valor_MA < valor_Hoy) Then
            delta = Math.Abs(valor_MA - valor_Hoy)
            cadena = "El factor operacional de la lijadora es superior al porcentaje del mes anterior en este mismo instante, con un margen favorable de " & FormatPercent(delta) & "."
            semaforo_fo.StateIndex = 3
        End If

        If (valor_MA > valor_Hoy) Then
            delta = Math.Abs(valor_MA - valor_Hoy)
            cadena = "El factor operacional de la lijadora es inferior al porcentaje del mes anterior en este mismo instante, con un margen en contra de " & FormatPercent(delta) & "."
            semaforo_fo.StateIndex = 1
        End If

        If (valor_MA = valor_Hoy) Then
            cadena = "El factor operacional de la lijadora es igual al porcentaje del mes anterior en este mismo instante."
            semaforo_fo.StateIndex = 3
        End If

        If (Double.NaN.Equals(valor_MA) = True Or Double.NaN.Equals(valor_Hoy) = True) Then
            cadena = "No hay registros para calcular el factor operacional de la lijadora."
            semaforo_fo.StateIndex = 1
        End If

        Return cadena
    End Function

    Private Sub generar_Informe_Click(sender As Object, e As EventArgs) Handles generar_Informe.Click
        Try
            Dim c As Cursor = Me.Cursor
            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            abrir_controlProduccion(GetConexion_controlProduccion())

            Dim reporte_rangos As New Reporte_Lijadora

            Dim tool As New ReportPrintTool(reporte_rangos)

            'reporte_rangos.SqlDataSource1.ConnectionName = "conectar_controlProduccion"
            reporte_rangos.SqlDataSource1.Connection.ConnectionString = GetConexion_controlProduccion()

            reporte_rangos.Parameters("fecha1").Value = Lijadora_calendarioInicial.SelectionRange.Start
            reporte_rangos.Parameters("fecha2").Value = Lijadora_calendarioFinal.SelectionRange.Start

            reporte_rangos.Parameters("fecha1").Visible = False
            reporte_rangos.Parameters("fecha2").Visible = False

            'tool.PreviewForm.ControlBox = False
            tool.PreviewForm.Hide()
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
            tool.ShowPreview()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de la lijadora")
        End Try

    End Sub

    Private Sub lijadora_informeDiario_Click(sender As Object, e As EventArgs) Handles lijadora_informeDiario.Click
        Try
            Dim c As Cursor = Me.Cursor
            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            abrir_controlProduccion(GetConexion_controlProduccion())

            Dim reporte_diario As New Reporte_Lijadora

            Dim tool As New ReportPrintTool(reporte_diario)

            Dim final_dia As Date = String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

            'reporte_diario.SqlDataSource1.ConnectionName = "conectar_controlProduccion"
            reporte_diario.SqlDataSource1.Connection.ConnectionString = GetConexion_controlProduccion()

            reporte_diario.Parameters("fecha1").Value = Format(Now, "yyyy/MM/dd")
            reporte_diario.Parameters("fecha2").Value = final_dia

            reporte_diario.Parameters("fecha1").Visible = False
            reporte_diario.Parameters("fecha2").Visible = False

            'tool.PreviewForm.ControlBox = False
            tool.PreviewForm.Hide()
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
            tool.ShowPreview()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de la lijadora")
        End Try

    End Sub

    Private Sub lijadora_informeMensual_Click(sender As Object, e As EventArgs) Handles lijadora_informeMensual.Click
        Try
            Dim c As Cursor = Me.Cursor
            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            abrir_controlProduccion(GetConexion_controlProduccion())

            Dim reporte_mensual As New Reporte_Lijadora

            Dim tool As New ReportPrintTool(reporte_mensual)

            Dim final_mes As Date = String.Format("{0} {1}", Format(DateSerial(Year(Now), Month(Now) + 1, 0), "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

            'reporte_mensual.SqlDataSource1.ConnectionName = "conectar_controlProduccion"
            reporte_mensual.SqlDataSource1.Connection.ConnectionString = GetConexion_controlProduccion()

            reporte_mensual.Parameters("fecha1").Value = DateSerial(Year(Now), Month(Now), 1)
            reporte_mensual.Parameters("fecha2").Value = final_mes

            reporte_mensual.Parameters("fecha1").Visible = False
            reporte_mensual.Parameters("fecha2").Visible = False

            'tool.PreviewForm.ControlBox = False
            tool.PreviewForm.Hide()
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
            tool.ShowPreview()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de la lijadora")
        End Try

    End Sub


End Class
