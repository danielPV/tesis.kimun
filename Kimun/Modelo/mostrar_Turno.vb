﻿Imports System.Data.SqlClient

Public Class mostrar_Turno

    Public mostrarTurno As mostrar_Turno
    Private Sub mostrar_Turno_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Timer_Turno.Interval = 60000

        mostrar_Turno()

        Timer_Turno.Start()
    End Sub

    Private Sub mostrar_Turno()

        Dim hora_actual As Date = Now.ToString("HH:mm:ss")

        'Asignamos las consultas a variables
        Dim inicio_TA As String = inicio_turno("TA")
        Dim final_TA As String = termino_turno("TA")
        Dim inicio_TB As String = inicio_turno("TB")
        Dim final_TB As String = termino_turno("TB")
        Dim inicio_TC As String = inicio_turno("TC")
        Dim final_TC As String = termino_turno("TC")

        If (consultar_turno() = "Turno A") Then

            Label_Turno.Text = "Turno A"
            Label_Turno.ForeColor = Color.Crimson

            If ((Format(DateAdd(DateInterval.Minute, -5, CDate(final_TA)), "HH:mm:ss") <= hora_actual And CDate(inicio_TB) >= hora_actual) And Button1.Enabled = True) Then
                'MsgBox("Turno A")

                If (Button1.Enabled = True) Then
                    'Se genera el informe
                    Dim notificacion As New Notificaciones
                    notificacion.EnviarEmail("Turno A", inicio_TA, final_TA)
                    cambiar_estado(False)
                    ToolTip1.SetToolTip(Button1, "El informe se envió automáticamente")
                End If
            End If

        End If

        If (consultar_turno() = "Turno B") Then

            Label_Turno.Text = "Turno B"
            Label_Turno.ForeColor = Color.Crimson

            If ((Format(DateAdd(DateInterval.Minute, -5, CDate(final_TB)), "HH:mm:ss") <= hora_actual And CDate(inicio_TC) >= hora_actual) And Button1.Enabled = True) Then
                'MsgBox("Turno B")

                If (Button1.Enabled = True) Then
                    'Se genera el informe
                    Dim notificacion As New Notificaciones
                    notificacion.EnviarEmail("Turno B", inicio_TB, final_TB)
                    cambiar_estado(False)
                    ToolTip1.SetToolTip(Button1, "El informe se envió automáticamente")
                End If
            End If

        End If

        If (consultar_turno() = "Turno C") Then

            Label_Turno.Text = "Turno C"
            Label_Turno.ForeColor = Color.Crimson

            If ((Format(DateAdd(DateInterval.Minute, -5, CDate(final_TC)), "HH:mm:ss") <= hora_actual And CDate(inicio_TA) >= hora_actual) And Button1.Enabled = True) Then
                'MsgBox("Turno C")

                If (Button1.Enabled = True) Then
                    'Se genera el informe
                    Dim notificacion As New Notificaciones
                    notificacion.EnviarEmail("Turno C", inicio_TC, final_TC)
                    cambiar_estado(False)
                    ToolTip1.SetToolTip(Button1, "El informe se envió automáticamente")
                End If
            End If

        End If
    End Sub

    Public Function consultar_turno() As String

        'Consultamos la hora actual
        Dim hora_actual As Date = Now.ToString("HH:mm:ss")

        'Asignamos las consultas a variables
        Dim inicio_TA As String = inicio_turno("TA")
        Dim final_TA As String = termino_turno("TA")
        Dim inicio_TB As String = inicio_turno("TB")
        Dim final_TB As String = termino_turno("TB")
        Dim inicio_TC As String = inicio_turno("TC")
        Dim final_TC As String = termino_turno("TC")

        'Turno A
        If (CDate(inicio_TA) < hora_actual And CDate(final_TA) > hora_actual) Then
            Return "Turno A"
        End If

        'Turno B
        If (CDate(inicio_TB) < hora_actual And CDate(final_TB) > hora_actual) Then
            Return "Turno B"
        End If

        'Turno C
        If (CDate(inicio_TC) < hora_actual And CDate(final_TC) > hora_actual) Then
            Return "Turno C"
        End If

        Return ""

    End Function

    Public Function inicio_turno(ByVal codigo As String) As String

        Dim consulta As New SqlCommand("SELECT hora_inicio FROM Turno WHERE CodTurno = @cod ", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@cod", codigo)

            Return consulta.ExecuteScalar.ToString()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar inicio turno")
            Return ""
        End Try
    End Function

    Public Function termino_turno(ByVal codigo As String) As String

        Dim consulta As New SqlCommand("SELECT hora_finalizacion FROM Turno WHERE CodTurno = @cod ", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@cod", codigo)

            Return consulta.ExecuteScalar.ToString()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar termino turno")
            Return ""
        End Try
    End Function

    Public Sub cambiar_estado(ByVal estado As Boolean)
        Button1.Enabled = estado
        Button1.Refresh()
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Dim c As Cursor = Me.Cursor
        Me.Cursor = Cursors.WaitCursor
        Dim notificacion As New Notificaciones
        Dim hora_actual As Date = Now.ToString("HH:mm:ss")

        'Asignamos las consultas a variables
        Dim inicio_TA As String = inicio_turno("TA")
        Dim final_TA As String = termino_turno("TA")
        Dim inicio_TB As String = inicio_turno("TB")
        Dim final_TB As String = termino_turno("TB")
        Dim inicio_TC As String = inicio_turno("TC")
        Dim final_TC As String = termino_turno("TC")

        If (consultar_turno() = "Turno A") Then
            notificacion.EnviarEmail("Turno A", inicio_TA, final_TA)
            cambiar_estado(False)
            'ToolTip1.SetToolTip(Button1, "Ya se envio el informe este turno")
        End If

        If (consultar_turno() = "Turno B") Then
            notificacion.EnviarEmail("Turno B", inicio_TB, final_TB)
            cambiar_estado(False)
            'ToolTip1.SetToolTip(Button1, "Ya se envio el informe este turno")
        End If

        If (consultar_turno() = "Turno C") Then
            notificacion.EnviarEmail("Turno C", inicio_TC, final_TC)
            cambiar_estado(False)
            'ToolTip1.SetToolTip(Button1, "Ya se envio el informe este turno")
        End If

        If (notificacion.enviado = False) Then
            cambiar_estado(True)
        End If
        Me.Cursor = c
    End Sub

    Private Sub Timer_Turno_Tick(sender As Object, e As EventArgs) Handles Timer_Turno.Tick
        Dim notificacion As New Notificaciones
        Timer_Turno.Stop()

        mostrar_Turno()

        Timer_Turno.Start()
    End Sub
End Class
