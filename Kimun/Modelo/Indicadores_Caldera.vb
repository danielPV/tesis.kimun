﻿Imports System.Data.SqlClient

Public Class Indicadores_Caldera

    Dim valor_NivelDomo As Double
    Dim valorOld_NivelDomo As Double
    Dim valor_Consumo As Double
    Dim valorOld_Consumo As Double
    Dim valor_Temperatura As Double
    Dim valorOld_Temperatura As Double

    Dim tolerancia As Integer = 0

    'Se carga el componente
    Private Sub Indicadores_Caldera_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Tiempo.Interval = 60000

        'Codigo verdadero VALOR DIA ANTERIOR A LA MISMA HORA
        valorOld_NivelDomo = consultar("NIVEL DOMO", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOld_Consumo = consultar("CONSUMO VAPOR DOMO", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOld_Temperatura = consultar("T° VAPOR", DateAdd(DateInterval.Day, -1, Now), "old")

        'VALORES AHORA
        valor_NivelDomo = consultar("NIVEL DOMO", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_Consumo = consultar("CONSUMO VAPOR DOMO", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_Temperatura = consultar("T° VAPOR", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")

        'Mostrar los valores
        Label_ValorNivelDomo.Text = String.Format("{0} {1}", valor_NivelDomo, "%")
        mostrarLimites_NivelDomo(valor_NivelDomo, consultar_limiteMayor(51), consultar_limiteMenor(51)) 'funcion que genera el color del valor
        Label_ValorConsumo.Text = String.Format("{0} {1}", valor_Consumo, "T/H")
        mostrarLimites_Consumo(valor_Consumo, consultar_limiteMayor(38), consultar_limiteMenor(38))
        Label_ValorTemperatura.Text = String.Format("{0} {1}", valor_Temperatura, "°C")
        mostrarLimites_Temperatura(valor_Temperatura, consultar_limiteMayor(2), consultar_limiteMenor(2))

        ValorOLD_nivel.Text = String.Format("{0} {1}", valorOld_NivelDomo, "%")
        ValorOLD_consumoDomo.Text = String.Format("{0} {1}", valorOld_Consumo, "T/H")
        ValorOLD_temp.Text = String.Format("{0} {1}", valorOld_Temperatura, "°C")

        'Muestra las flechas
        mostrarFlecha_NivelDomo(valor_NivelDomo, valorOld_NivelDomo)
        mostrarFlecha_Consumo(valor_Consumo, valorOld_Consumo)
        mostrarFlecha_Temperatura(valor_Temperatura, valorOld_Temperatura)

        Tiempo.Start()
    End Sub

    'Funcion que muestra una fecha hacia arriba si el valor nuevo es superior al interior, abajo si el valor nuevo es inferior al anterior y igual si los valores son iguales
    'Recibe en sus argumentos el valor actual y el valor antiguo

    'Funcion mostrar flecha para nivel domo
    Private Sub mostrarFlecha_NivelDomo(ByVal valor As Double, ByVal valorAntiguo As Double)

        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_NivelDomo.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_NivelDomo, "El nivel del domo aumento " & delta & " % respecto al mismo instante el día de ayer.")

        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_NivelDomo.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_NivelDomo, "El nivel del domo disminuyó " & delta & "% respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_NivelDomo.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_NivelDomo, "El nivel del domo tiene igual porcentaje en este mismo instante el día de ayer.")
        End If

    End Sub

    'Funcion mostrar flecha para consumo de vapor
    Private Sub mostrarFlecha_Consumo(ByVal valor As Double, ByVal valorAntiguo As Double)
        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Consumo.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_Consumo, "El consumo de vapor del domo aumento " & delta & " (T/H) respecto al mismo instante el día de ayer.")
        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Consumo.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_Consumo, "El consumo de vapor del domo disminuyó " & delta & " (T/H) respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_Consumo.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_Consumo, "El consumo de vapor del domo tiene un valor (T/H) igual respecto al mismo instante el día de ayer.")
        End If
    End Sub

    'Funcion mostrar flecha para temperatura del vapor
    Private Sub mostrarFlecha_Temperatura(ByVal valor As Double, ByVal valorAntiguo As Double)
        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Temperatura.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_Temperatura, "La temperatura del vapor aumento " & delta & " °C respecto al mismo instante el día de ayer.")
        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Temperatura.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_Temperatura, "La temperatura del vapor disminuyó " & delta & " °C respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_Temperatura.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_Temperatura, "La temperatura del vapor tiene un valor de °C igual respecto al mismo instante el día de ayer.")
        End If
    End Sub

    'Funcion para mostrar el label del nivel del domo en rojo si excede el limite max o min
    Private Sub mostrarLimites_NivelDomo(ByVal valor As Double, ByVal max As Integer, ByVal min As Integer)
        If (valor >= max Or valor <= min) Then
            Label_ValorNivelDomo.ForeColor = Color.Red
            semaforo_nivelDomo.StateIndex = 1
            ToolTip1.SetToolTip(Label_ValorNivelDomo, "El nivel del domo está fuera de los límites establecidos.")

            If (valor <> 0) Then
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Caldera", "El nivel del domo está fuera de los límites establecidos.", ToolTipIcon.Warning)
            End If

        Else
            If (valor >= max - max / 10 Or valor <= min + max / 10) Then
                Label_ValorNivelDomo.ForeColor = Color.OrangeRed
                semaforo_nivelDomo.StateIndex = 2
                ToolTip1.SetToolTip(Label_ValorNivelDomo, "El nivel del domo está cerca de los límites establecidos.")
            Else
                Label_ValorNivelDomo.ForeColor = Color.Blue
                semaforo_nivelDomo.StateIndex = 3
                ToolTip1.SetToolTip(Label_ValorNivelDomo, "El nivel del domo está normal.")
            End If
        End If
    End Sub

    'Funcion para mostrar el label del consumo del vapor en rojo si excede el limite max o min
    Private Sub mostrarLimites_Consumo(ByVal valor As Double, ByVal max As Integer, ByVal min As Integer)
        If (valor >= max Or valor <= min) Then
            Label_ValorConsumo.ForeColor = Color.Red
            semaforo_consumoDomo.StateIndex = 1
            ToolTip1.SetToolTip(Label_ValorConsumo, "El consumo de vapor del domo está fuera de los límites establecidos.")

            If (valor <> 0) Then
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Caldera", "El consumo de vapor del domo está fuera de los límites establecidos.", ToolTipIcon.Warning)
            End If
        Else
            If (valor >= max - max / 10 Or valor <= min + (max / 10)) Then
                Label_ValorConsumo.ForeColor = Color.OrangeRed
                semaforo_consumoDomo.StateIndex = 2
                ToolTip1.SetToolTip(Label_ValorConsumo, "El consumo de vapor del domo está cerca de los límites establecidos.")
            Else
                Label_ValorConsumo.ForeColor = Color.Blue
                semaforo_consumoDomo.StateIndex = 3
                ToolTip1.SetToolTip(Label_ValorConsumo, "El consumo de vapor del domo está normal.")
            End If
        End If
    End Sub

    'Funcion para mostrar el label de la temperatura del vapor en rojo si excede el limite max o min
    Private Sub mostrarLimites_Temperatura(ByVal valor As Double, ByVal max As Integer, ByVal min As Integer)
        If (valor >= max Or valor <= min) Then
            Label_ValorTemperatura.ForeColor = Color.Red
            semaforo_tempVapor.StateIndex = 1
            ToolTip1.SetToolTip(Label_ValorTemperatura, "La temperatura del vapor está fuera de los límites establecidos.")

            If (valor <> 0) Then
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Caldera", "La temperatura del vapor está fuera de los límites establecidos.", ToolTipIcon.Warning)
            End If
        Else
                If (valor >= max - max / 50 Or valor <= min + max / 50) Then
                Label_ValorTemperatura.ForeColor = Color.OrangeRed
                semaforo_tempVapor.StateIndex = 2
                ToolTip1.SetToolTip(Label_ValorTemperatura, "La temperatura del vapor está cerca de los límites establecidos.")
            Else
                Label_ValorTemperatura.ForeColor = Color.Blue
                semaforo_tempVapor.StateIndex = 3
                ToolTip1.SetToolTip(Label_ValorTemperatura, "La temperatura del vapor está normal.")
            End If
        End If
    End Sub

    'Funcion que realiza consulta sql
    'Recibe como argumentos el nombre de la equivalencia y la fecha en la que se quiere realizar la consulta
    Private Function consultar(ByVal equivalencia As String, ByVal fecha As Date, ByVal tipo As String) As Double

        Dim consulta As New SqlCommand("SELECT FloatTableCaldera.Val
                                        FROM  EquivalenciaCaldera INNER JOIN
                                        FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex
                                        WHERE (EquivalenciaCaldera.Equivalencia = @equi) AND (FloatTableCaldera.DateAndTime between @fecha AND @fecha2)", conexion_eTata)



        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            consulta.Parameters.AddWithValue("@equi", equivalencia)
            consulta.Parameters.AddWithValue("@fecha", String.Format("{0} {1}", Format(fecha, "yyyy/MM/dd"), Format(TimeSerial(Hour(fecha), Minute(fecha), 0), "HH:mm:ss")))
            consulta.Parameters.AddWithValue("@fecha2", String.Format("{0} {1}", Format(fecha, "yyyy/MM/dd"), Format(TimeSerial(Hour(fecha), Minute(fecha) + 1, 0), "HH:mm:ss")))

            Label_Estado.Text = "Funcionando"
            Label_Estado.ForeColor = Color.Green

            If (consulta.ExecuteScalar().ToString = "") Then
                'MsgBox("new llega1")
                Label_Estado.Text = "Linea Parada"
                Label_Estado.ForeColor = Color.Red

                semaforo_nivelDomo.StateIndex = 1
                semaforo_consumoDomo.StateIndex = 1
                semaforo_tempVapor.StateIndex = 1

                Return 0
            Else
                'MsgBox("new llega2")
                'Return Format(consulta.ExecuteScalar(), "000.00")
                Return consulta.ExecuteScalar()
            End If

            conexion_eTata.Close()
        Catch ex As Exception
            'MsgBox("llega exception")
            'MsgBox("Se produjo un error al consultar los datos", MsgBoxStyle.Information, "Error al obtener Datos")
            'MsgBox(ex.Message, MsgBoxStyle.Information, "consulta caldera")

            If (tipo = "old") Then
                If (equivalencia = "NIVEL DOMO") Then
                    ValorOLD_nivel.ForeColor = Color.Red
                    Label5.ForeColor = Color.Red
                End If

                If (equivalencia = "CONSUMO VAPOR DOMO") Then
                    ValorOLD_consumoDomo.ForeColor = Color.Red
                    Label6.ForeColor = Color.Red
                End If

                If (equivalencia = "T° VAPOR") Then
                    ValorOLD_temp.ForeColor = Color.Red
                    Label7.ForeColor = Color.Red
                End If

                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "No se encontraron datos del día de ayer.", ToolTipIcon.Info)
                ToolTip1.SetToolTip(Label_Estado, "No se encontraron algunos datos del día de ayer.")

            Else

                If (equivalencia = "NIVEL DOMO") Then
                    ToolTip1.SetToolTip(Label_Estado, equivalencia + ": Error en la conexión.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (equivalencia = "CONSUMO VAPOR DOMO") Then
                    ToolTip1.SetToolTip(Label_Estado, equivalencia + ": Error en la conexión.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (equivalencia = "T° VAPOR") Then
                    ToolTip1.SetToolTip(Label_Estado, equivalencia + ": Error en la conexión.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (tolerancia = 3) Then
                    Label_Estado.Text = "ERROR"
                    Label_Estado.ForeColor = Color.Red
                End If

                If (tolerancia = 2) Then
                    Label_Estado.Text = "ERROR"
                    Label_Estado.ForeColor = Color.OrangeRed
                End If

                If (tolerancia = 1) Then
                    Label_Estado.Text = "ERROR"
                    Label_Estado.ForeColor = Color.Orange
                End If

            End If


            Return 0
        End Try

    End Function

    'Funcion que entrega el valor del limite mayor de un indicador
    Private Function consultar_limiteMayor(ByVal TagIndex As Integer) As Integer

        Dim consulta As New SqlCommand("SELECT LimitesCaldera.Limite
                                        FROM  LimitesCaldera
                                        WHERE (LimitesCaldera.TagIndex = @tag) ", conexion_eTata)

        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            consulta.Parameters.AddWithValue("@tag", TagIndex)

            Return consulta.ExecuteScalar()
            conexion_eTata.Close()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al obtener Datos")
            Return 0
        End Try
    End Function

    'Funcion que entrega el valor del limite menor de un indicador
    Private Function consultar_limiteMenor(ByVal TagIndex As Integer) As Integer

        Dim consulta As New SqlCommand("SELECT LimiteMinCaldera.LimiteMin
                                        FROM  LimiteMinCaldera
                                        WHERE (LimiteMinCaldera.TagIndex = @tag) ", conexion_eTata)

        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            consulta.Parameters.AddWithValue("@tag", TagIndex)

            Return consulta.ExecuteScalar()
            conexion_eTata.Close()
        Catch ex As Exception
            Return 0
        End Try
    End Function

    'Funcion del componente Timer para realizar las consultas cada cierto tiempo
    'Esta funcion se realiza cada 60 segundos
    Private Sub Tiempo_Tick(sender As Object, e As EventArgs) Handles Tiempo.Tick
        'Se detiene el Timer
        Tiempo.Stop()

        tolerancia = 0

        'Realiza las consultas y toma el valor del indicador de temperatura

        'Codigo verdadero
        valor_NivelDomo = consultar("NIVEL DOMO", CDate(Format(Now, "yyyy-MM-dd HH:mm:ss")), "new")
        valor_Consumo = consultar("CONSUMO VAPOR DOMO", CDate(Format(Now, "yyyy-MM-dd HH:mm:ss")), "new")
        valor_Temperatura = consultar("T° VAPOR", CDate(Format(Now, "yyyy-MM-dd HH:mm:ss")), "new")

        valorOld_NivelDomo = consultar("NIVEL DOMO", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOld_Consumo = consultar("CONSUMO VAPOR DOMO", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOld_Temperatura = consultar("T° VAPOR", DateAdd(DateInterval.Day, -1, Now), "old")

        'Dibuja el valor de la consulta en los Label
        Label_ValorNivelDomo.Text = String.Format("{0} {1}", valor_NivelDomo, "%")
        mostrarLimites_NivelDomo(valor_NivelDomo, consultar_limiteMayor(51), consultar_limiteMenor(51)) 'Colorea en rojo el label si el valor excede el limite del nivel del domo
        Label_ValorConsumo.Text = String.Format("{0} {1}", valor_Consumo, "T/H")
        mostrarLimites_Consumo(valor_Consumo, consultar_limiteMayor(38), consultar_limiteMenor(38))
        Label_ValorTemperatura.Text = String.Format("{0} {1}", valor_Temperatura, "°C")
        mostrarLimites_Temperatura(valor_Temperatura, consultar_limiteMayor(2), consultar_limiteMenor(2))

        ValorOLD_nivel.Text = String.Format("{0} {1}", valorOld_NivelDomo, "%")
        ValorOLD_consumoDomo.Text = String.Format("{0} {1}", valorOld_Consumo, "T/H")
        ValorOLD_temp.Text = String.Format("{0} {1}", valorOld_Temperatura, "°C")

        'Muestra las flechas
        mostrarFlecha_NivelDomo(valor_NivelDomo, valorOld_NivelDomo)
        mostrarFlecha_Consumo(valor_Consumo, valorOld_Consumo)
        mostrarFlecha_Temperatura(valor_Temperatura, valorOld_Temperatura)

        'Se vuelve a ejecutar el timer
        Tiempo.Start()
    End Sub
End Class
