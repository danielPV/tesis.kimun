﻿Imports System.Data.SqlClient

Public Class Indicadores_Lijadora

    Dim valor_mesa1 As Integer
    Dim valorOLD_mesaUno As Integer
    Dim valor_mesa2 As Integer
    Dim valorOLD_mesaDos As Integer
    Dim valor_mesa3 As Integer
    Dim valorOLD_mesaTres As Integer
    Dim valor_trabajo As Integer
    Dim valorOLD_trabajo As Integer

    Dim tolerancia As Integer = 0

    Private Sub Indicadores_Lijadora_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Tiempo_Lijadora.Interval = 60000


        'VALORES AYER
        valorOLD_mesaUno = consultar("Cantidad Tableros Mesa 1", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_mesaDos = consultar("Cantidad Tableros Mesa 2", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_mesaTres = consultar("Cantidad Tableros Mesa 3", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_trabajo = consultar("Velocidad de trabajo", DateAdd(DateInterval.Day, -1, Now), "old")

        'MUESTRA VALORES DE AYER
        ValorOLD_mesa1.Text = String.Format("{0} {1}", valorOLD_mesaUno, " Tableros")
        ValorOLD_mesa2.Text = String.Format("{0} {1}", valorOLD_mesaDos, " Tableros")
        ValorOLD_mesa3.Text = String.Format("{0} {1}", valorOLD_mesaTres, " Tableros")
        ValorOLD_velocidad.Text = String.Format("{0} {1}", valorOLD_trabajo, " Trab/min")

        'VALORES DE HOY
        valor_mesa1 = consultar("Cantidad Tableros Mesa 1", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_mesa2 = consultar("Cantidad Tableros Mesa 2", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_mesa3 = consultar("Cantidad Tableros Mesa 3", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_trabajo = consultar("Velocidad de trabajo", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")

        'MUESTRA VALORES HOY
        Label_ValorMesa1.Text = String.Format("{0} {1}", valor_mesa1, " Tableros")
        Label_ValorMesa2.Text = String.Format("{0} {1}", valor_mesa2, " Tableros")
        Label_ValorMesa3.Text = String.Format("{0} {1}", valor_mesa3, " Tableros")
        Label_ValorTrabajo.Text = String.Format("{0} {1}", valor_trabajo, " Trab/min")

        'MUESTRA LAS FLECHAS QUE COMPARAN VALORES DE HOY CON LOS DE AYER
        mostrarFlecha_mesa1(valor_mesa1, valorOLD_mesaUno)
        mostrarFlecha_mesa2(valor_mesa2, valorOLD_mesaDos)
        mostrarFlecha_mesa3(valor_mesa3, valorOLD_mesaTres)
        mostrarFlecha_trabajo(valor_trabajo, valorOLD_trabajo)

        'DETECTA CHISPAS Y CONSULTA ESTADO DE LA LINEA
        detectarChispa(consultar("Chispa detectada", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new"))
        estado(consultar("Funcionamiento", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new"))

        'MUESTRA EL NUMERO DE CHISPAS EN EL TURNO
        num_chispas.Text = contador_chispas()

        Tiempo_Lijadora.Start()
    End Sub

    Private Function consultar(ByVal equivalencia As String, ByVal fecha As Date, ByVal tipo As String) As Integer

        Dim consulta As New SqlCommand("SELECT FloatLijadora.Val
                                        FROM  EquivalenciaLijadora INNER JOIN
                                        FloatLijadora ON EquivalenciaLijadora.TagIndex = FloatLijadora.TagIndex
                                        WHERE (EquivalenciaLijadora.Equivalencia = @equi) AND (FloatLijadora.DateAndTime between @fecha AND @fecha2)", conexion_eTata)



        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            consulta.Parameters.AddWithValue("@equi", equivalencia)
            consulta.Parameters.AddWithValue("@fecha", String.Format("{0} {1}", Format(fecha, "yyyy/MM/dd"), Format(TimeSerial(Hour(fecha), Minute(fecha), 0), "HH:mm:ss")))
            consulta.Parameters.AddWithValue("@fecha2", String.Format("{0} {1}", Format(fecha, "yyyy/MM/dd"), Format(TimeSerial(Hour(fecha), Minute(fecha) + 1, 0), "HH:mm:ss")))

            If (consulta.ExecuteScalar().ToString = "") Then
                Label_EstadoLijadora.Text = "Linea Parada"
                Label_EstadoLijadora.ForeColor = Color.Red

                Return 0
            Else
                Return consulta.ExecuteScalar().ToString
            End If

            conexion_eTata.Close()
        Catch

            If (tipo = "old") Then
                If (equivalencia = "Cantidad Tableros Mesa 1") Then
                    ValorOLD_mesa1.ForeColor = Color.Red
                    Label9.ForeColor = Color.Red
                End If

                If (equivalencia = "Cantidad Tableros Mesa 2") Then
                    ValorOLD_mesa2.ForeColor = Color.Red
                    Label5.ForeColor = Color.Red
                End If

                If (equivalencia = "Cantidad Tableros Mesa 3") Then
                    ValorOLD_mesa3.ForeColor = Color.Red
                    Label6.ForeColor = Color.Red
                End If

                If (equivalencia = "Velocidad de trabajo") Then
                    ValorOLD_velocidad.ForeColor = Color.Red
                    Label7.ForeColor = Color.Red
                End If

                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "No se encontraron datos del día de ayer.", ToolTipIcon.Info)
                ToolTip1.SetToolTip(Label_EstadoLijadora, "No se encontraron algunos datos del día de ayer.")

            Else
                If (equivalencia = "Cantidad Tableros Mesa 1") Then
                    ToolTip1.SetToolTip(Label_EstadoLijadora, equivalencia + ": Error en la captura de datos.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (equivalencia = "Cantidad Tableros Mesa 2") Then
                    ToolTip1.SetToolTip(Label_EstadoLijadora, equivalencia + ": Error en la captura de datos.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (equivalencia = "Cantidad Tableros Mesa 3") Then
                    ToolTip1.SetToolTip(Label_EstadoLijadora, equivalencia + ": Error en la captura de datos.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (equivalencia = "Velocidad de trabajo") Then
                    ToolTip1.SetToolTip(Label_EstadoLijadora, equivalencia + ": Error en la captura de datos.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (tolerancia = 4) Then
                    Label_EstadoLijadora.Text = "ERROR"
                    Label_EstadoLijadora.ForeColor = Color.Red
                End If

                If (tolerancia < 4 Or tolerancia > 2) Then
                    Label_EstadoLijadora.Text = "ERROR"
                    Label_EstadoLijadora.ForeColor = Color.OrangeRed
                End If

                If (tolerancia < 2) Then
                    Label_EstadoLijadora.Text = "ERROR"
                    Label_EstadoLijadora.ForeColor = Color.Orange
                End If
            End If


            Return 0
        End Try

    End Function

    Private Function contador_chispas() As Integer
        Dim inicio_TA As String = inicio_turno("TA")
        Dim inicio_TB As String = inicio_turno("TB")
        Dim inicio_TC As String = inicio_turno("TC")
        Dim fin_TA As String = fin_turno("TA")
        Dim fin_TB As String = fin_turno("TB")
        Dim fin_TC As String = fin_turno("TC")

        Dim hora_actual As Date = Now.ToString("HH:mm:ss")

        Dim consulta As New SqlCommand("SELECT COUNT(FloatLijadora.Val)
                                        FROM  EquivalenciaLijadora INNER JOIN
                                        FloatLijadora ON EquivalenciaLijadora.TagIndex = FloatLijadora.TagIndex
                                        WHERE (EquivalenciaLijadora.Equivalencia = 'Chispa detectada') AND (FloatLijadora.DateAndTime between @fecha1 AND @fecha2)", conexion_eTata)

        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            If (CDate(inicio_TA) < hora_actual And CDate(fin_TA) > hora_actual) Then
                consulta.Parameters.AddWithValue("@fecha1", String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(CDate(inicio_TA), "HH:mm:ss")))
                consulta.Parameters.AddWithValue("@fecha2", String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(CDate(fin_TA), "HH:mm:ss")))

                Return consulta.ExecuteScalar().ToString
            End If

            If (CDate(inicio_TB) < hora_actual And CDate(fin_TB) > hora_actual) Then
                consulta.Parameters.AddWithValue("@fecha1", String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(CDate(inicio_TB), "HH:mm:ss")))
                consulta.Parameters.AddWithValue("@fecha2", String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(CDate(fin_TB), "HH:mm:ss")))

                Return consulta.ExecuteScalar().ToString
            End If

            If (CDate(inicio_TC) < hora_actual And CDate(fin_TC) > hora_actual) Then
                consulta.Parameters.AddWithValue("@fecha1", String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(CDate(inicio_TC), "HH:mm:ss")))
                consulta.Parameters.AddWithValue("@fecha2", String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(CDate(fin_TC), "HH:mm:ss")))

                Return consulta.ExecuteScalar().ToString
            End If

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Lijadora")
            Return 0
        End Try

    End Function

    Private Function inicio_turno(ByVal codigo As String) As String

        Dim consulta As New SqlCommand("SELECT hora_inicio FROM Turno WHERE CodTurno = @cod ", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@cod", codigo)

            Return consulta.ExecuteScalar.ToString()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar inicio turno")
            Return ""
        End Try
    End Function

    Private Function fin_turno(ByVal codigo As String) As String

        Dim consulta As New SqlCommand("SELECT hora_finalizacion FROM Turno WHERE CodTurno = @cod ", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@cod", codigo)

            Return consulta.ExecuteScalar.ToString()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar fin del turno")
            Return ""
        End Try
    End Function

    Private Sub mostrarFlecha_mesa1(ByVal valor As Double, ByVal valorAntiguo As Double)
        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_mesa1.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_mesa1, "Los tableros de la mesa 1 aumentaron " & delta & " unidades respecto al mismo instante el día de ayer.")

        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_mesa1.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_mesa1, "Los tableros de la mesa 1 disminuyeron " & delta & " unidades respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_mesa1.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_mesa1, "Los tableros de la mesa 1 tienen igual número de unidades en este mismo instante que el día de ayer.")
        End If
    End Sub

    Private Sub mostrarFlecha_mesa2(ByVal valor As Double, ByVal valorAntiguo As Double)
        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_mesa2.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_mesa2, "Los tableros de la mesa 2 aumentaron " & delta & " unidades respecto al mismo instante el día de ayer.")
        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_mesa2.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_mesa2, "Los tableros de la mesa 2 disminuyeron " & delta & " unidades respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_mesa2.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_mesa2, "Los tableros de la mesa 2 tienen igual número de unidades en este mismo instante que el día de ayer.")
        End If
    End Sub

    Private Sub mostrarFlecha_mesa3(ByVal valor As Double, ByVal valorAntiguo As Double)
        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_mesa3.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_mesa3, "Los tableros de la mesa 3 aumentaron " & delta & " unidades respecto al mismo instante el día de ayer.")
        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_mesa3.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_mesa3, "Los tableros de la mesa 3 disminuyeron " & delta & " unidades respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_mesa3.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_mesa3, "Los tableros de la mesa 3 tienen igual número de unidades en este mismo instante que el día de ayer.")
        End If
    End Sub

    Private Sub mostrarFlecha_trabajo(ByVal valor As Double, ByVal valorAntiguo As Double)
        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_trabajo.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_trabajo, "La velocidad de trabajo aumentó " & delta & " Trab/min respecto al mismo instante el día de ayer.")

        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_trabajo.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_trabajo, "La velocidad de trabajo disminuyó " & delta & " Trab/min respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_trabajo.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_trabajo, "La velocidad de trabajo tiene igual valor en este mismo instante el día de ayer.")
        End If
    End Sub

    Private Sub detectarChispa(ByVal valor As Integer)

        If (valor = 1) Then
            Imagen_Chispa.Visible = True
            Label_Chispa.Visible = True
        Else
            Imagen_Chispa.Visible = False
            Label_Chispa.Visible = False
        End If
    End Sub

    Private Sub estado(ByVal valor As Integer)
        If (valor = 0) Then
            Label_EstadoLijadora.Text = "Funcionando"
            Label_EstadoLijadora.ForeColor = Color.Green
        Else
            Label_EstadoLijadora.Text = "Linea Parada"
            Label_EstadoLijadora.ForeColor = Color.Red
        End If
    End Sub

    Private Sub Tiempo_Lijadora_Tick(sender As Object, e As EventArgs) Handles Tiempo_Lijadora.Tick
        Tiempo_Lijadora.Stop()

        tolerancia = 0

        'VALORES AYER
        valorOLD_mesaUno = consultar("Cantidad Tableros Mesa 1", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_mesaDos = consultar("Cantidad Tableros Mesa 2", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_mesaTres = consultar("Cantidad Tableros Mesa 3", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_trabajo = consultar("Velocidad de trabajo", DateAdd(DateInterval.Day, -1, Now), "old")

        'MUESTRA LOS VALORES DE AYER
        ValorOLD_mesa1.Text = String.Format("{0} {1}", valorOLD_mesaUno, " Tableros")
        ValorOLD_mesa2.Text = String.Format("{0} {1}", valorOLD_mesaDos, " Tableros")
        ValorOLD_mesa3.Text = String.Format("{0} {1}", valorOLD_mesaTres, " Tableros")
        ValorOLD_velocidad.Text = String.Format("{0} {1}", valorOLD_trabajo, " Trab/min")

        'VALORES HOY
        valor_mesa1 = consultar("Cantidad Tableros Mesa 1", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_mesa2 = consultar("Cantidad Tableros Mesa 2", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_mesa3 = consultar("Cantidad Tableros Mesa 3", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_trabajo = consultar("Velocidad de trabajo", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")

        'MUESTRA VALORES HOY
        Label_ValorMesa1.Text = String.Format("{0} {1}", valor_mesa1, " Tableros")
        Label_ValorMesa2.Text = String.Format("{0} {1}", valor_mesa2, " Tableros")
        Label_ValorMesa3.Text = String.Format("{0} {1}", valor_mesa3, " Tableros")
        Label_ValorTrabajo.Text = String.Format("{0} {1}", valor_trabajo, " Trab/min")

        'MUESTRA LAS FLECHAS QUE COMPARAN VALORES DE HOY CON LOS DE AYER
        mostrarFlecha_mesa1(valor_mesa1, valorOLD_mesaUno)
        mostrarFlecha_mesa2(valor_mesa2, valorOLD_mesaDos)
        mostrarFlecha_mesa3(valor_mesa3, valorOLD_mesaTres)
        mostrarFlecha_trabajo(valor_trabajo, valorOLD_trabajo)

        detectarChispa(consultar("Chispa detectada", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new"))
        estado(consultar("Funcionamiento", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new"))

        num_chispas.Text = contador_chispas()

        Tiempo_Lijadora.Start()
    End Sub
End Class
