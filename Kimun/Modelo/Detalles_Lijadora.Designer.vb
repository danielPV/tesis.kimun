﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Detalles_Lijadora
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim IndicatorState1 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState2 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState3 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState4 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState5 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState6 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState7 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState8 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState9 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState10 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState11 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState12 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState13 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState14 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState15 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState16 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState17 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState18 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState19 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState20 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState21 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState22 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState23 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState24 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState25 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState26 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState27 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState28 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.GaugeControl5 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.StateIndicatorGauge7 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge()
        Me.semaforo_chispa = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent()
        Me.comparacion_chispa = New System.Windows.Forms.RichTextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.XtraScrollableControl2 = New DevExpress.XtraEditors.XtraScrollableControl()
        Me.comparacion_fo = New System.Windows.Forms.RichTextBox()
        Me.GaugeControl4 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.StateIndicatorGauge6 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge()
        Me.semaforo_fo = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent()
        Me.comparacion_productividad = New System.Windows.Forms.RichTextBox()
        Me.comparacion_disponibilidad = New System.Windows.Forms.RichTextBox()
        Me.GaugeControl3 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.StateIndicatorGauge5 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge()
        Me.semaforo_productividad = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent()
        Me.sembilidad = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.StateIndicatorGauge4 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge()
        Me.semaforo_disponibilidad = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent()
        Me.comparacion_produccion2 = New System.Windows.Forms.RichTextBox()
        Me.GaugeControl1 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.StateIndicatorGauge1 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge()
        Me.semaforo_IndicadoresProduccion = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent()
        Me.comparacion_IndicadoresProduccion = New System.Windows.Forms.RichTextBox()
        Me.GaugeControl2 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.StateIndicatorGauge3 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge()
        Me.semaforo_produccion2 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent()
        Me.semaforo_produccion1 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.StateIndicatorGauge2 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge()
        Me.semaforo_produccion = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent()
        Me.comparacion_produccion = New System.Windows.Forms.RichTextBox()
        Me.GroupBox6 = New System.Windows.Forms.GroupBox()
        Me.Prod_MesAnterior = New System.Windows.Forms.Label()
        Me.Label34 = New System.Windows.Forms.Label()
        Me.SeparatorControl4 = New DevExpress.XtraEditors.SeparatorControl()
        Me.Prod_Hoy = New System.Windows.Forms.Label()
        Me.Prod_AnioAnterior = New System.Windows.Forms.Label()
        Me.Label37 = New System.Windows.Forms.Label()
        Me.Label38 = New System.Windows.Forms.Label()
        Me.Label39 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.SeparatorControl3 = New DevExpress.XtraEditors.SeparatorControl()
        Me.MAC_FO = New System.Windows.Forms.Label()
        Me.MAC_Producti = New System.Windows.Forms.Label()
        Me.MAC_Dispo = New System.Windows.Forms.Label()
        Me.MAC_Produccion = New System.Windows.Forms.Label()
        Me.MA_FO = New System.Windows.Forms.Label()
        Me.MA_Producti = New System.Windows.Forms.Label()
        Me.MA_Dispo = New System.Windows.Forms.Label()
        Me.MA_Produccion = New System.Windows.Forms.Label()
        Me.Label27 = New System.Windows.Forms.Label()
        Me.Label28 = New System.Windows.Forms.Label()
        Me.Label29 = New System.Windows.Forms.Label()
        Me.Label30 = New System.Windows.Forms.Label()
        Me.Label31 = New System.Windows.Forms.Label()
        Me.Label32 = New System.Windows.Forms.Label()
        Me.DashboardViewer2 = New DevExpress.DashboardWin.DashboardViewer(Me.components)
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.DashboardViewer1 = New DevExpress.DashboardWin.DashboardViewer(Me.components)
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.tabla_pronostico1 = New DevExpress.XtraGrid.GridControl()
        Me.GridView4 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Tabla_RegresionLineal = New DevExpress.XtraGrid.GridControl()
        Me.GridView1 = New DevExpress.XtraGrid.Views.Grid.GridView()
        Me.grafico_lijadora = New DevExpress.XtraCharts.ChartControl()
        Me.SqlDataSource1 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.lijadora_informeMensual = New System.Windows.Forms.Button()
        Me.lijadora_informeDiario = New System.Windows.Forms.Button()
        Me.Lijadora_calendarioFinal = New System.Windows.Forms.MonthCalendar()
        Me.Lijadora_calendarioInicial = New System.Windows.Forms.MonthCalendar()
        Me.generar_Informe = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ETataDataSetLijadoraBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.ETataDataSet_Lijadora = New Kimun.eTataDataSet_Lijadora()
        Me.SqlDataSource2 = New DevExpress.DataAccess.Sql.SqlDataSource(Me.components)
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.StateIndicatorGauge7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.semaforo_chispa, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox4.SuspendLayout()
        Me.XtraScrollableControl2.SuspendLayout()
        CType(Me.StateIndicatorGauge6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.semaforo_fo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateIndicatorGauge5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.semaforo_productividad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateIndicatorGauge4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.semaforo_disponibilidad, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateIndicatorGauge1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.semaforo_IndicadoresProduccion, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateIndicatorGauge3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.semaforo_produccion2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateIndicatorGauge2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.semaforo_produccion, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox6.SuspendLayout()
        CType(Me.SeparatorControl4, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox5.SuspendLayout()
        CType(Me.SeparatorControl3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DashboardViewer2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.DashboardViewer1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.tabla_pronostico1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Tabla_RegresionLineal, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.grafico_lijadora, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.ETataDataSetLijadoraBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.ETataDataSet_Lijadora, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Controls.Add(Me.TabPage4)
        Me.TabControl1.Location = New System.Drawing.Point(0, 0)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(1064, 601)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.White
        Me.TabPage1.Controls.Add(Me.GroupBox1)
        Me.TabPage1.Controls.Add(Me.GroupBox4)
        Me.TabPage1.Controls.Add(Me.GroupBox6)
        Me.TabPage1.Controls.Add(Me.GroupBox5)
        Me.TabPage1.Controls.Add(Me.DashboardViewer2)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(1056, 575)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "General"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.GaugeControl5)
        Me.GroupBox1.Controls.Add(Me.comparacion_chispa)
        Me.GroupBox1.Location = New System.Drawing.Point(545, 261)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(496, 79)
        Me.GroupBox1.TabIndex = 16
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Número de Chispas"
        '
        'GaugeControl5
        '
        Me.GaugeControl5.AutoLayout = False
        Me.GaugeControl5.BackColor = System.Drawing.Color.Transparent
        Me.GaugeControl5.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.GaugeControl5.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.StateIndicatorGauge7})
        Me.GaugeControl5.Location = New System.Drawing.Point(17, 14)
        Me.GaugeControl5.Name = "GaugeControl5"
        Me.GaugeControl5.Size = New System.Drawing.Size(47, 42)
        Me.GaugeControl5.TabIndex = 15
        '
        'StateIndicatorGauge7
        '
        Me.StateIndicatorGauge7.Bounds = New System.Drawing.Rectangle(2, 2, 45, 43)
        Me.StateIndicatorGauge7.Indicators.AddRange(New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent() {Me.semaforo_chispa})
        Me.StateIndicatorGauge7.Name = "StateIndicatorGauge7"
        '
        'semaforo_chispa
        '
        Me.semaforo_chispa.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(124.0!, 124.0!)
        Me.semaforo_chispa.Name = "stateIndicatorComponent4"
        Me.semaforo_chispa.Size = New System.Drawing.SizeF(100.0!, 200.0!)
        Me.semaforo_chispa.StateIndex = 3
        IndicatorState1.Name = "State1"
        IndicatorState1.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight1
        IndicatorState2.Name = "State2"
        IndicatorState2.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight2
        IndicatorState3.Name = "State3"
        IndicatorState3.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight3
        IndicatorState4.Name = "State4"
        IndicatorState4.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight4
        Me.semaforo_chispa.States.AddRange(New DevExpress.XtraGauges.Core.Model.IIndicatorState() {IndicatorState1, IndicatorState2, IndicatorState3, IndicatorState4})
        '
        'comparacion_chispa
        '
        Me.comparacion_chispa.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.comparacion_chispa.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.comparacion_chispa.Font = New System.Drawing.Font("Tahoma", 10.0!)
        Me.comparacion_chispa.Location = New System.Drawing.Point(82, 18)
        Me.comparacion_chispa.Name = "comparacion_chispa"
        Me.comparacion_chispa.ReadOnly = True
        Me.comparacion_chispa.Size = New System.Drawing.Size(406, 55)
        Me.comparacion_chispa.TabIndex = 14
        Me.comparacion_chispa.Text = ""
        '
        'GroupBox4
        '
        Me.GroupBox4.Controls.Add(Me.XtraScrollableControl2)
        Me.GroupBox4.Location = New System.Drawing.Point(18, 261)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Size = New System.Drawing.Size(511, 311)
        Me.GroupBox4.TabIndex = 7
        Me.GroupBox4.TabStop = False
        Me.GroupBox4.Text = "Comparaciones Indicadores Producción"
        '
        'XtraScrollableControl2
        '
        Me.XtraScrollableControl2.Controls.Add(Me.comparacion_fo)
        Me.XtraScrollableControl2.Controls.Add(Me.GaugeControl4)
        Me.XtraScrollableControl2.Controls.Add(Me.comparacion_productividad)
        Me.XtraScrollableControl2.Controls.Add(Me.comparacion_disponibilidad)
        Me.XtraScrollableControl2.Controls.Add(Me.GaugeControl3)
        Me.XtraScrollableControl2.Controls.Add(Me.sembilidad)
        Me.XtraScrollableControl2.Controls.Add(Me.comparacion_produccion2)
        Me.XtraScrollableControl2.Controls.Add(Me.GaugeControl1)
        Me.XtraScrollableControl2.Controls.Add(Me.comparacion_IndicadoresProduccion)
        Me.XtraScrollableControl2.Controls.Add(Me.GaugeControl2)
        Me.XtraScrollableControl2.Controls.Add(Me.semaforo_produccion1)
        Me.XtraScrollableControl2.Controls.Add(Me.comparacion_produccion)
        Me.XtraScrollableControl2.Location = New System.Drawing.Point(8, 19)
        Me.XtraScrollableControl2.Name = "XtraScrollableControl2"
        Me.XtraScrollableControl2.Size = New System.Drawing.Size(497, 286)
        Me.XtraScrollableControl2.TabIndex = 0
        '
        'comparacion_fo
        '
        Me.comparacion_fo.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.comparacion_fo.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.comparacion_fo.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.comparacion_fo.Location = New System.Drawing.Point(59, 300)
        Me.comparacion_fo.Name = "comparacion_fo"
        Me.comparacion_fo.ReadOnly = True
        Me.comparacion_fo.Size = New System.Drawing.Size(406, 42)
        Me.comparacion_fo.TabIndex = 12
        Me.comparacion_fo.Text = ""
        '
        'GaugeControl4
        '
        Me.GaugeControl4.AutoLayout = False
        Me.GaugeControl4.BackColor = System.Drawing.Color.Transparent
        Me.GaugeControl4.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.GaugeControl4.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.StateIndicatorGauge6})
        Me.GaugeControl4.Location = New System.Drawing.Point(0, 294)
        Me.GaugeControl4.Name = "GaugeControl4"
        Me.GaugeControl4.Size = New System.Drawing.Size(55, 51)
        Me.GaugeControl4.TabIndex = 11
        '
        'StateIndicatorGauge6
        '
        Me.StateIndicatorGauge6.Bounds = New System.Drawing.Rectangle(2, 2, 52, 46)
        Me.StateIndicatorGauge6.Indicators.AddRange(New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent() {Me.semaforo_fo})
        Me.StateIndicatorGauge6.Name = "StateIndicatorGauge6"
        '
        'semaforo_fo
        '
        Me.semaforo_fo.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(124.0!, 124.0!)
        Me.semaforo_fo.Name = "stateIndicatorComponent4"
        Me.semaforo_fo.Size = New System.Drawing.SizeF(100.0!, 200.0!)
        Me.semaforo_fo.StateIndex = 3
        IndicatorState5.Name = "State1"
        IndicatorState5.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight1
        IndicatorState6.Name = "State2"
        IndicatorState6.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight2
        IndicatorState7.Name = "State3"
        IndicatorState7.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight3
        IndicatorState8.Name = "State4"
        IndicatorState8.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight4
        Me.semaforo_fo.States.AddRange(New DevExpress.XtraGauges.Core.Model.IIndicatorState() {IndicatorState5, IndicatorState6, IndicatorState7, IndicatorState8})
        '
        'comparacion_productividad
        '
        Me.comparacion_productividad.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.comparacion_productividad.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.comparacion_productividad.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.comparacion_productividad.Location = New System.Drawing.Point(59, 246)
        Me.comparacion_productividad.Name = "comparacion_productividad"
        Me.comparacion_productividad.ReadOnly = True
        Me.comparacion_productividad.Size = New System.Drawing.Size(406, 42)
        Me.comparacion_productividad.TabIndex = 10
        Me.comparacion_productividad.Text = ""
        '
        'comparacion_disponibilidad
        '
        Me.comparacion_disponibilidad.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.comparacion_disponibilidad.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.comparacion_disponibilidad.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.comparacion_disponibilidad.Location = New System.Drawing.Point(59, 189)
        Me.comparacion_disponibilidad.Name = "comparacion_disponibilidad"
        Me.comparacion_disponibilidad.ReadOnly = True
        Me.comparacion_disponibilidad.Size = New System.Drawing.Size(406, 42)
        Me.comparacion_disponibilidad.TabIndex = 9
        Me.comparacion_disponibilidad.Text = ""
        '
        'GaugeControl3
        '
        Me.GaugeControl3.AutoLayout = False
        Me.GaugeControl3.BackColor = System.Drawing.Color.Transparent
        Me.GaugeControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.GaugeControl3.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.StateIndicatorGauge5})
        Me.GaugeControl3.Location = New System.Drawing.Point(1, 237)
        Me.GaugeControl3.Name = "GaugeControl3"
        Me.GaugeControl3.Size = New System.Drawing.Size(55, 51)
        Me.GaugeControl3.TabIndex = 9
        '
        'StateIndicatorGauge5
        '
        Me.StateIndicatorGauge5.Bounds = New System.Drawing.Rectangle(2, 2, 52, 46)
        Me.StateIndicatorGauge5.Indicators.AddRange(New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent() {Me.semaforo_productividad})
        Me.StateIndicatorGauge5.Name = "StateIndicatorGauge5"
        '
        'semaforo_productividad
        '
        Me.semaforo_productividad.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(124.0!, 124.0!)
        Me.semaforo_productividad.Name = "stateIndicatorComponent4"
        Me.semaforo_productividad.Size = New System.Drawing.SizeF(100.0!, 200.0!)
        Me.semaforo_productividad.StateIndex = 3
        IndicatorState9.Name = "State1"
        IndicatorState9.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight1
        IndicatorState10.Name = "State2"
        IndicatorState10.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight2
        IndicatorState11.Name = "State3"
        IndicatorState11.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight3
        IndicatorState12.Name = "State4"
        IndicatorState12.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight4
        Me.semaforo_productividad.States.AddRange(New DevExpress.XtraGauges.Core.Model.IIndicatorState() {IndicatorState9, IndicatorState10, IndicatorState11, IndicatorState12})
        '
        'sembilidad
        '
        Me.sembilidad.AutoLayout = False
        Me.sembilidad.BackColor = System.Drawing.Color.Transparent
        Me.sembilidad.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.sembilidad.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.StateIndicatorGauge4})
        Me.sembilidad.Location = New System.Drawing.Point(0, 180)
        Me.sembilidad.Name = "sembilidad"
        Me.sembilidad.Size = New System.Drawing.Size(55, 51)
        Me.sembilidad.TabIndex = 8
        '
        'StateIndicatorGauge4
        '
        Me.StateIndicatorGauge4.Bounds = New System.Drawing.Rectangle(2, 2, 52, 46)
        Me.StateIndicatorGauge4.Indicators.AddRange(New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent() {Me.semaforo_disponibilidad})
        Me.StateIndicatorGauge4.Name = "StateIndicatorGauge4"
        '
        'semaforo_disponibilidad
        '
        Me.semaforo_disponibilidad.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(124.0!, 124.0!)
        Me.semaforo_disponibilidad.Name = "stateIndicatorComponent4"
        Me.semaforo_disponibilidad.Size = New System.Drawing.SizeF(100.0!, 200.0!)
        Me.semaforo_disponibilidad.StateIndex = 3
        IndicatorState13.Name = "State1"
        IndicatorState13.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight1
        IndicatorState14.Name = "State2"
        IndicatorState14.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight2
        IndicatorState15.Name = "State3"
        IndicatorState15.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight3
        IndicatorState16.Name = "State4"
        IndicatorState16.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight4
        Me.semaforo_disponibilidad.States.AddRange(New DevExpress.XtraGauges.Core.Model.IIndicatorState() {IndicatorState13, IndicatorState14, IndicatorState15, IndicatorState16})
        '
        'comparacion_produccion2
        '
        Me.comparacion_produccion2.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.comparacion_produccion2.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.comparacion_produccion2.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.comparacion_produccion2.Location = New System.Drawing.Point(59, 132)
        Me.comparacion_produccion2.Name = "comparacion_produccion2"
        Me.comparacion_produccion2.ReadOnly = True
        Me.comparacion_produccion2.Size = New System.Drawing.Size(406, 42)
        Me.comparacion_produccion2.TabIndex = 6
        Me.comparacion_produccion2.Text = ""
        '
        'GaugeControl1
        '
        Me.GaugeControl1.AutoLayout = False
        Me.GaugeControl1.BackColor = System.Drawing.Color.Transparent
        Me.GaugeControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.GaugeControl1.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.StateIndicatorGauge1})
        Me.GaugeControl1.Location = New System.Drawing.Point(-2, 9)
        Me.GaugeControl1.Name = "GaugeControl1"
        Me.GaugeControl1.Size = New System.Drawing.Size(55, 51)
        Me.GaugeControl1.TabIndex = 2
        '
        'StateIndicatorGauge1
        '
        Me.StateIndicatorGauge1.Bounds = New System.Drawing.Rectangle(2, 2, 52, 46)
        Me.StateIndicatorGauge1.Indicators.AddRange(New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent() {Me.semaforo_IndicadoresProduccion})
        Me.StateIndicatorGauge1.Name = "StateIndicatorGauge1"
        '
        'semaforo_IndicadoresProduccion
        '
        Me.semaforo_IndicadoresProduccion.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(124.0!, 124.0!)
        Me.semaforo_IndicadoresProduccion.Name = "stateIndicatorComponent4"
        Me.semaforo_IndicadoresProduccion.Size = New System.Drawing.SizeF(100.0!, 200.0!)
        Me.semaforo_IndicadoresProduccion.StateIndex = 3
        IndicatorState17.Name = "State1"
        IndicatorState17.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight1
        IndicatorState18.Name = "State2"
        IndicatorState18.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight2
        IndicatorState19.Name = "State3"
        IndicatorState19.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight3
        IndicatorState20.Name = "State4"
        IndicatorState20.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight4
        Me.semaforo_IndicadoresProduccion.States.AddRange(New DevExpress.XtraGauges.Core.Model.IIndicatorState() {IndicatorState17, IndicatorState18, IndicatorState19, IndicatorState20})
        '
        'comparacion_IndicadoresProduccion
        '
        Me.comparacion_IndicadoresProduccion.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.comparacion_IndicadoresProduccion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.comparacion_IndicadoresProduccion.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.comparacion_IndicadoresProduccion.Location = New System.Drawing.Point(59, 18)
        Me.comparacion_IndicadoresProduccion.Name = "comparacion_IndicadoresProduccion"
        Me.comparacion_IndicadoresProduccion.ReadOnly = True
        Me.comparacion_IndicadoresProduccion.Size = New System.Drawing.Size(406, 42)
        Me.comparacion_IndicadoresProduccion.TabIndex = 1
        Me.comparacion_IndicadoresProduccion.Text = ""
        '
        'GaugeControl2
        '
        Me.GaugeControl2.AutoLayout = False
        Me.GaugeControl2.BackColor = System.Drawing.Color.Transparent
        Me.GaugeControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.GaugeControl2.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.StateIndicatorGauge3})
        Me.GaugeControl2.Location = New System.Drawing.Point(-2, 123)
        Me.GaugeControl2.Name = "GaugeControl2"
        Me.GaugeControl2.Size = New System.Drawing.Size(55, 51)
        Me.GaugeControl2.TabIndex = 7
        '
        'StateIndicatorGauge3
        '
        Me.StateIndicatorGauge3.Bounds = New System.Drawing.Rectangle(2, 2, 52, 46)
        Me.StateIndicatorGauge3.Indicators.AddRange(New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent() {Me.semaforo_produccion2})
        Me.StateIndicatorGauge3.Name = "StateIndicatorGauge3"
        '
        'semaforo_produccion2
        '
        Me.semaforo_produccion2.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(124.0!, 124.0!)
        Me.semaforo_produccion2.Name = "stateIndicatorComponent4"
        Me.semaforo_produccion2.Size = New System.Drawing.SizeF(100.0!, 200.0!)
        Me.semaforo_produccion2.StateIndex = 3
        IndicatorState21.Name = "State1"
        IndicatorState21.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight1
        IndicatorState22.Name = "State2"
        IndicatorState22.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight2
        IndicatorState23.Name = "State3"
        IndicatorState23.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight3
        IndicatorState24.Name = "State4"
        IndicatorState24.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight4
        Me.semaforo_produccion2.States.AddRange(New DevExpress.XtraGauges.Core.Model.IIndicatorState() {IndicatorState21, IndicatorState22, IndicatorState23, IndicatorState24})
        '
        'semaforo_produccion1
        '
        Me.semaforo_produccion1.AutoLayout = False
        Me.semaforo_produccion1.BackColor = System.Drawing.Color.Transparent
        Me.semaforo_produccion1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.semaforo_produccion1.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.StateIndicatorGauge2})
        Me.semaforo_produccion1.Location = New System.Drawing.Point(-2, 66)
        Me.semaforo_produccion1.Name = "semaforo_produccion1"
        Me.semaforo_produccion1.Size = New System.Drawing.Size(55, 51)
        Me.semaforo_produccion1.TabIndex = 3
        '
        'StateIndicatorGauge2
        '
        Me.StateIndicatorGauge2.Bounds = New System.Drawing.Rectangle(2, 2, 52, 46)
        Me.StateIndicatorGauge2.Indicators.AddRange(New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent() {Me.semaforo_produccion})
        Me.StateIndicatorGauge2.Name = "StateIndicatorGauge2"
        '
        'semaforo_produccion
        '
        Me.semaforo_produccion.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(124.0!, 124.0!)
        Me.semaforo_produccion.Name = "stateIndicatorComponent4"
        Me.semaforo_produccion.Size = New System.Drawing.SizeF(100.0!, 200.0!)
        Me.semaforo_produccion.StateIndex = 3
        IndicatorState25.Name = "State1"
        IndicatorState25.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight1
        IndicatorState26.Name = "State2"
        IndicatorState26.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight2
        IndicatorState27.Name = "State3"
        IndicatorState27.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight3
        IndicatorState28.Name = "State4"
        IndicatorState28.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight4
        Me.semaforo_produccion.States.AddRange(New DevExpress.XtraGauges.Core.Model.IIndicatorState() {IndicatorState25, IndicatorState26, IndicatorState27, IndicatorState28})
        '
        'comparacion_produccion
        '
        Me.comparacion_produccion.BackColor = System.Drawing.SystemColors.ControlLightLight
        Me.comparacion_produccion.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.comparacion_produccion.Font = New System.Drawing.Font("Tahoma", 9.0!)
        Me.comparacion_produccion.Location = New System.Drawing.Point(59, 75)
        Me.comparacion_produccion.Name = "comparacion_produccion"
        Me.comparacion_produccion.ReadOnly = True
        Me.comparacion_produccion.Size = New System.Drawing.Size(406, 42)
        Me.comparacion_produccion.TabIndex = 4
        Me.comparacion_produccion.Text = ""
        '
        'GroupBox6
        '
        Me.GroupBox6.Controls.Add(Me.Prod_MesAnterior)
        Me.GroupBox6.Controls.Add(Me.Label34)
        Me.GroupBox6.Controls.Add(Me.SeparatorControl4)
        Me.GroupBox6.Controls.Add(Me.Prod_Hoy)
        Me.GroupBox6.Controls.Add(Me.Prod_AnioAnterior)
        Me.GroupBox6.Controls.Add(Me.Label37)
        Me.GroupBox6.Controls.Add(Me.Label38)
        Me.GroupBox6.Controls.Add(Me.Label39)
        Me.GroupBox6.Location = New System.Drawing.Point(545, 487)
        Me.GroupBox6.Name = "GroupBox6"
        Me.GroupBox6.Size = New System.Drawing.Size(496, 79)
        Me.GroupBox6.TabIndex = 5
        Me.GroupBox6.TabStop = False
        Me.GroupBox6.Text = "Producción "
        '
        'Prod_MesAnterior
        '
        Me.Prod_MesAnterior.AutoSize = True
        Me.Prod_MesAnterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prod_MesAnterior.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Prod_MesAnterior.Location = New System.Drawing.Point(272, 54)
        Me.Prod_MesAnterior.Name = "Prod_MesAnterior"
        Me.Prod_MesAnterior.Size = New System.Drawing.Size(38, 17)
        Me.Prod_MesAnterior.TabIndex = 19
        Me.Prod_MesAnterior.Text = "label"
        '
        'Label34
        '
        Me.Label34.AutoSize = True
        Me.Label34.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label34.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label34.Location = New System.Drawing.Point(270, 18)
        Me.Label34.Name = "Label34"
        Me.Label34.Size = New System.Drawing.Size(88, 17)
        Me.Label34.TabIndex = 18
        Me.Label34.Text = "Mes Anterior"
        '
        'SeparatorControl4
        '
        Me.SeparatorControl4.Location = New System.Drawing.Point(147, 36)
        Me.SeparatorControl4.Name = "SeparatorControl4"
        Me.SeparatorControl4.Size = New System.Drawing.Size(343, 18)
        Me.SeparatorControl4.TabIndex = 17
        '
        'Prod_Hoy
        '
        Me.Prod_Hoy.AutoSize = True
        Me.Prod_Hoy.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prod_Hoy.Location = New System.Drawing.Point(382, 53)
        Me.Prod_Hoy.Name = "Prod_Hoy"
        Me.Prod_Hoy.Size = New System.Drawing.Size(38, 17)
        Me.Prod_Hoy.TabIndex = 16
        Me.Prod_Hoy.Text = "label"
        '
        'Prod_AnioAnterior
        '
        Me.Prod_AnioAnterior.AutoSize = True
        Me.Prod_AnioAnterior.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prod_AnioAnterior.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Prod_AnioAnterior.Location = New System.Drawing.Point(158, 53)
        Me.Prod_AnioAnterior.Name = "Prod_AnioAnterior"
        Me.Prod_AnioAnterior.Size = New System.Drawing.Size(38, 17)
        Me.Prod_AnioAnterior.TabIndex = 15
        Me.Prod_AnioAnterior.Text = "label"
        '
        'Label37
        '
        Me.Label37.AutoSize = True
        Me.Label37.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label37.Location = New System.Drawing.Point(401, 18)
        Me.Label37.Name = "Label37"
        Me.Label37.Size = New System.Drawing.Size(33, 17)
        Me.Label37.TabIndex = 4
        Me.Label37.Text = "Hoy"
        '
        'Label38
        '
        Me.Label38.AutoSize = True
        Me.Label38.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label38.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label38.Location = New System.Drawing.Point(154, 18)
        Me.Label38.Name = "Label38"
        Me.Label38.Size = New System.Drawing.Size(87, 17)
        Me.Label38.TabIndex = 3
        Me.Label38.Text = "Año Anterior"
        '
        'Label39
        '
        Me.Label39.AutoSize = True
        Me.Label39.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label39.Location = New System.Drawing.Point(39, 54)
        Me.Label39.Name = "Label39"
        Me.Label39.Size = New System.Drawing.Size(83, 17)
        Me.Label39.TabIndex = 2
        Me.Label39.Text = "Producción:"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.SeparatorControl3)
        Me.GroupBox5.Controls.Add(Me.MAC_FO)
        Me.GroupBox5.Controls.Add(Me.MAC_Producti)
        Me.GroupBox5.Controls.Add(Me.MAC_Dispo)
        Me.GroupBox5.Controls.Add(Me.MAC_Produccion)
        Me.GroupBox5.Controls.Add(Me.MA_FO)
        Me.GroupBox5.Controls.Add(Me.MA_Producti)
        Me.GroupBox5.Controls.Add(Me.MA_Dispo)
        Me.GroupBox5.Controls.Add(Me.MA_Produccion)
        Me.GroupBox5.Controls.Add(Me.Label27)
        Me.GroupBox5.Controls.Add(Me.Label28)
        Me.GroupBox5.Controls.Add(Me.Label29)
        Me.GroupBox5.Controls.Add(Me.Label30)
        Me.GroupBox5.Controls.Add(Me.Label31)
        Me.GroupBox5.Controls.Add(Me.Label32)
        Me.GroupBox5.Location = New System.Drawing.Point(545, 346)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(496, 135)
        Me.GroupBox5.TabIndex = 4
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Indicadores Producción "
        '
        'SeparatorControl3
        '
        Me.SeparatorControl3.Location = New System.Drawing.Point(166, 27)
        Me.SeparatorControl3.Name = "SeparatorControl3"
        Me.SeparatorControl3.Size = New System.Drawing.Size(302, 18)
        Me.SeparatorControl3.TabIndex = 15
        '
        'MAC_FO
        '
        Me.MAC_FO.AutoSize = True
        Me.MAC_FO.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MAC_FO.Location = New System.Drawing.Point(364, 106)
        Me.MAC_FO.Name = "MAC_FO"
        Me.MAC_FO.Size = New System.Drawing.Size(38, 17)
        Me.MAC_FO.TabIndex = 14
        Me.MAC_FO.Text = "label"
        '
        'MAC_Producti
        '
        Me.MAC_Producti.AutoSize = True
        Me.MAC_Producti.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MAC_Producti.Location = New System.Drawing.Point(364, 86)
        Me.MAC_Producti.Name = "MAC_Producti"
        Me.MAC_Producti.Size = New System.Drawing.Size(38, 17)
        Me.MAC_Producti.TabIndex = 13
        Me.MAC_Producti.Text = "label"
        '
        'MAC_Dispo
        '
        Me.MAC_Dispo.AutoSize = True
        Me.MAC_Dispo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MAC_Dispo.Location = New System.Drawing.Point(364, 64)
        Me.MAC_Dispo.Name = "MAC_Dispo"
        Me.MAC_Dispo.Size = New System.Drawing.Size(38, 17)
        Me.MAC_Dispo.TabIndex = 12
        Me.MAC_Dispo.Text = "label"
        '
        'MAC_Produccion
        '
        Me.MAC_Produccion.AutoSize = True
        Me.MAC_Produccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MAC_Produccion.Location = New System.Drawing.Point(364, 42)
        Me.MAC_Produccion.Name = "MAC_Produccion"
        Me.MAC_Produccion.Size = New System.Drawing.Size(38, 17)
        Me.MAC_Produccion.TabIndex = 11
        Me.MAC_Produccion.Text = "label"
        '
        'MA_FO
        '
        Me.MA_FO.AutoSize = True
        Me.MA_FO.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MA_FO.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MA_FO.Location = New System.Drawing.Point(219, 106)
        Me.MA_FO.Name = "MA_FO"
        Me.MA_FO.Size = New System.Drawing.Size(38, 17)
        Me.MA_FO.TabIndex = 10
        Me.MA_FO.Text = "label"
        '
        'MA_Producti
        '
        Me.MA_Producti.AutoSize = True
        Me.MA_Producti.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MA_Producti.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MA_Producti.Location = New System.Drawing.Point(219, 86)
        Me.MA_Producti.Name = "MA_Producti"
        Me.MA_Producti.Size = New System.Drawing.Size(38, 17)
        Me.MA_Producti.TabIndex = 9
        Me.MA_Producti.Text = "label"
        '
        'MA_Dispo
        '
        Me.MA_Dispo.AutoSize = True
        Me.MA_Dispo.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MA_Dispo.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MA_Dispo.Location = New System.Drawing.Point(219, 64)
        Me.MA_Dispo.Name = "MA_Dispo"
        Me.MA_Dispo.Size = New System.Drawing.Size(38, 17)
        Me.MA_Dispo.TabIndex = 8
        Me.MA_Dispo.Text = "label"
        '
        'MA_Produccion
        '
        Me.MA_Produccion.AutoSize = True
        Me.MA_Produccion.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.MA_Produccion.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.MA_Produccion.Location = New System.Drawing.Point(219, 42)
        Me.MA_Produccion.Name = "MA_Produccion"
        Me.MA_Produccion.Size = New System.Drawing.Size(38, 17)
        Me.MA_Produccion.TabIndex = 7
        Me.MA_Produccion.Text = "label"
        '
        'Label27
        '
        Me.Label27.AutoSize = True
        Me.Label27.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label27.Location = New System.Drawing.Point(357, 13)
        Me.Label27.Name = "Label27"
        Me.Label27.Size = New System.Drawing.Size(77, 17)
        Me.Label27.TabIndex = 6
        Me.Label27.Text = "Mes Actual"
        '
        'Label28
        '
        Me.Label28.AutoSize = True
        Me.Label28.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label28.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label28.Location = New System.Drawing.Point(206, 13)
        Me.Label28.Name = "Label28"
        Me.Label28.Size = New System.Drawing.Size(88, 17)
        Me.Label28.TabIndex = 5
        Me.Label28.Text = "Mes Anterior"
        '
        'Label29
        '
        Me.Label29.AutoSize = True
        Me.Label29.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label29.Location = New System.Drawing.Point(39, 106)
        Me.Label29.Name = "Label29"
        Me.Label29.Size = New System.Drawing.Size(133, 17)
        Me.Label29.TabIndex = 4
        Me.Label29.Text = "Factor Operacional:"
        '
        'Label30
        '
        Me.Label30.AutoSize = True
        Me.Label30.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label30.Location = New System.Drawing.Point(39, 86)
        Me.Label30.Name = "Label30"
        Me.Label30.Size = New System.Drawing.Size(98, 17)
        Me.Label30.TabIndex = 3
        Me.Label30.Text = "Productividad:"
        '
        'Label31
        '
        Me.Label31.AutoSize = True
        Me.Label31.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label31.Location = New System.Drawing.Point(39, 64)
        Me.Label31.Name = "Label31"
        Me.Label31.Size = New System.Drawing.Size(100, 17)
        Me.Label31.TabIndex = 2
        Me.Label31.Text = "Disponibilidad:"
        '
        'Label32
        '
        Me.Label32.AutoSize = True
        Me.Label32.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label32.Location = New System.Drawing.Point(39, 42)
        Me.Label32.Name = "Label32"
        Me.Label32.Size = New System.Drawing.Size(83, 17)
        Me.Label32.TabIndex = 1
        Me.Label32.Text = "Producción:"
        '
        'DashboardViewer2
        '
        Me.DashboardViewer2.CustomDBSchemaProviderEx = Nothing
        Me.DashboardViewer2.DashboardSource = "Kimun.Win_Dashboards.Dash_TM_Lijadora"
        Me.DashboardViewer2.Location = New System.Drawing.Point(18, 6)
        Me.DashboardViewer2.Name = "DashboardViewer2"
        Me.DashboardViewer2.Size = New System.Drawing.Size(1032, 249)
        Me.DashboardViewer2.TabIndex = 0
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.DashboardViewer1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(1056, 575)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Indicadores Producción"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'DashboardViewer1
        '
        Me.DashboardViewer1.CustomDBSchemaProviderEx = Nothing
        Me.DashboardViewer1.DashboardSource = "Kimun.Win_Dashboards.Dash_Lijadora_IndicadoresProduccion"
        Me.DashboardViewer1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DashboardViewer1.Location = New System.Drawing.Point(3, 3)
        Me.DashboardViewer1.Name = "DashboardViewer1"
        Me.DashboardViewer1.Size = New System.Drawing.Size(1050, 569)
        Me.DashboardViewer1.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.tabla_pronostico1)
        Me.TabPage3.Controls.Add(Me.Label3)
        Me.TabPage3.Controls.Add(Me.Label2)
        Me.TabPage3.Controls.Add(Me.Tabla_RegresionLineal)
        Me.TabPage3.Controls.Add(Me.grafico_lijadora)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(1056, 575)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "Pronósticos"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'tabla_pronostico1
        '
        Me.tabla_pronostico1.Location = New System.Drawing.Point(734, 288)
        Me.tabla_pronostico1.MainView = Me.GridView4
        Me.tabla_pronostico1.Name = "tabla_pronostico1"
        Me.tabla_pronostico1.Size = New System.Drawing.Size(304, 281)
        Me.tabla_pronostico1.TabIndex = 12
        Me.tabla_pronostico1.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView4})
        '
        'GridView4
        '
        Me.GridView4.GridControl = Me.tabla_pronostico1
        Me.GridView4.Name = "GridView4"
        Me.GridView4.OptionsBehavior.Editable = False
        Me.GridView4.OptionsView.ShowGroupPanel = False
        Me.GridView4.PaintStyleName = "Skin"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.Location = New System.Drawing.Point(439, 255)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(210, 20)
        Me.Label3.TabIndex = 3
        Me.Label3.Text = "Tabla Pronóstico Producción"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(406, 5)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(223, 20)
        Me.Label2.TabIndex = 2
        Me.Label2.Text = "Gráfico Pronóstico Producción"
        '
        'Tabla_RegresionLineal
        '
        Me.Tabla_RegresionLineal.Location = New System.Drawing.Point(26, 288)
        Me.Tabla_RegresionLineal.MainView = Me.GridView1
        Me.Tabla_RegresionLineal.Name = "Tabla_RegresionLineal"
        Me.Tabla_RegresionLineal.Size = New System.Drawing.Size(682, 281)
        Me.Tabla_RegresionLineal.TabIndex = 1
        Me.Tabla_RegresionLineal.UseDisabledStatePainter = False
        Me.Tabla_RegresionLineal.ViewCollection.AddRange(New DevExpress.XtraGrid.Views.Base.BaseView() {Me.GridView1})
        '
        'GridView1
        '
        Me.GridView1.GridControl = Me.Tabla_RegresionLineal
        Me.GridView1.Name = "GridView1"
        Me.GridView1.OptionsBehavior.Editable = False
        Me.GridView1.OptionsView.ShowGroupPanel = False
        Me.GridView1.PaintStyleName = "Skin"
        '
        'grafico_lijadora
        '
        Me.grafico_lijadora.AnimationStartMode = DevExpress.XtraCharts.ChartAnimationMode.OnLoad
        Me.grafico_lijadora.DataBindings = Nothing
        Me.grafico_lijadora.DataSource = Me.SqlDataSource1
        Me.grafico_lijadora.EmptyChartText.EnableAntialiasing = DevExpress.Utils.DefaultBoolean.[True]
        Me.grafico_lijadora.EmptyChartText.Text = "No existen datos suficientes para calcular el pronóstico." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Deben existir datos de" &
    " producción de la lijadora por lo menos " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "de un mes para poder realizar los calc" &
    "ulos.  "
        Me.grafico_lijadora.EmptyChartText.TextColor = System.Drawing.Color.Black
        Me.grafico_lijadora.Legend.Name = "Default Legend"
        Me.grafico_lijadora.Location = New System.Drawing.Point(142, 28)
        Me.grafico_lijadora.Name = "grafico_lijadora"
        Me.grafico_lijadora.SeriesSerializable = New DevExpress.XtraCharts.Series(-1) {}
        Me.grafico_lijadora.Size = New System.Drawing.Size(793, 209)
        Me.grafico_lijadora.TabIndex = 0
        '
        'SqlDataSource1
        '
        Me.SqlDataSource1.Name = "SqlDataSource1"
        '
        'TabPage4
        '
        Me.TabPage4.Controls.Add(Me.GroupBox2)
        Me.TabPage4.Location = New System.Drawing.Point(4, 22)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage4.Size = New System.Drawing.Size(1056, 575)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "Reportes"
        Me.TabPage4.UseVisualStyleBackColor = True
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.Label9)
        Me.GroupBox2.Controls.Add(Me.lijadora_informeMensual)
        Me.GroupBox2.Controls.Add(Me.lijadora_informeDiario)
        Me.GroupBox2.Controls.Add(Me.Lijadora_calendarioFinal)
        Me.GroupBox2.Controls.Add(Me.Lijadora_calendarioInicial)
        Me.GroupBox2.Controls.Add(Me.generar_Informe)
        Me.GroupBox2.Controls.Add(Me.Label5)
        Me.GroupBox2.Controls.Add(Me.Label6)
        Me.GroupBox2.Controls.Add(Me.Label7)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.Location = New System.Drawing.Point(282, 32)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(493, 465)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Reporte Lijadora"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.BackColor = System.Drawing.Color.Transparent
        Me.Label9.Font = New System.Drawing.Font("Tahoma", 12.25!)
        Me.Label9.ForeColor = System.Drawing.Color.Black
        Me.Label9.Location = New System.Drawing.Point(175, 328)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(146, 21)
        Me.Label9.TabIndex = 36
        Me.Label9.Text = "Informes rápidos: "
        '
        'lijadora_informeMensual
        '
        Me.lijadora_informeMensual.BackColor = System.Drawing.Color.RoyalBlue
        Me.lijadora_informeMensual.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.lijadora_informeMensual.ForeColor = System.Drawing.Color.White
        Me.lijadora_informeMensual.Location = New System.Drawing.Point(295, 382)
        Me.lijadora_informeMensual.Name = "lijadora_informeMensual"
        Me.lijadora_informeMensual.Size = New System.Drawing.Size(140, 55)
        Me.lijadora_informeMensual.TabIndex = 35
        Me.lijadora_informeMensual.Text = "Informe Mensual"
        Me.lijadora_informeMensual.UseVisualStyleBackColor = False
        '
        'lijadora_informeDiario
        '
        Me.lijadora_informeDiario.BackColor = System.Drawing.Color.RoyalBlue
        Me.lijadora_informeDiario.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.lijadora_informeDiario.ForeColor = System.Drawing.Color.White
        Me.lijadora_informeDiario.Location = New System.Drawing.Point(62, 382)
        Me.lijadora_informeDiario.Name = "lijadora_informeDiario"
        Me.lijadora_informeDiario.Size = New System.Drawing.Size(140, 55)
        Me.lijadora_informeDiario.TabIndex = 33
        Me.lijadora_informeDiario.Text = "Informe Diario"
        Me.lijadora_informeDiario.UseVisualStyleBackColor = False
        '
        'Lijadora_calendarioFinal
        '
        Me.Lijadora_calendarioFinal.Location = New System.Drawing.Point(257, 83)
        Me.Lijadora_calendarioFinal.MaxSelectionCount = 1
        Me.Lijadora_calendarioFinal.Name = "Lijadora_calendarioFinal"
        Me.Lijadora_calendarioFinal.TabIndex = 32
        '
        'Lijadora_calendarioInicial
        '
        Me.Lijadora_calendarioInicial.FirstDayOfWeek = System.Windows.Forms.Day.Monday
        Me.Lijadora_calendarioInicial.Location = New System.Drawing.Point(47, 83)
        Me.Lijadora_calendarioInicial.MaxSelectionCount = 1
        Me.Lijadora_calendarioInicial.Name = "Lijadora_calendarioInicial"
        Me.Lijadora_calendarioInicial.TabIndex = 31
        '
        'generar_Informe
        '
        Me.generar_Informe.BackColor = System.Drawing.Color.DarkCyan
        Me.generar_Informe.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.generar_Informe.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.generar_Informe.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.generar_Informe.Font = New System.Drawing.Font("Tahoma", 10.25!)
        Me.generar_Informe.ForeColor = System.Drawing.Color.White
        Me.generar_Informe.Location = New System.Drawing.Point(82, 267)
        Me.generar_Informe.Name = "generar_Informe"
        Me.generar_Informe.Size = New System.Drawing.Size(319, 47)
        Me.generar_Informe.TabIndex = 30
        Me.generar_Informe.Text = "Generar Informe"
        Me.generar_Informe.UseVisualStyleBackColor = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.BackColor = System.Drawing.Color.Transparent
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(254, 66)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(49, 17)
        Me.Label5.TabIndex = 29
        Me.Label5.Text = "Hasta:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.BackColor = System.Drawing.Color.Transparent
        Me.Label6.ForeColor = System.Drawing.Color.Black
        Me.Label6.Location = New System.Drawing.Point(44, 66)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(53, 17)
        Me.Label6.TabIndex = 28
        Me.Label6.Text = "Desde:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.BackColor = System.Drawing.Color.Transparent
        Me.Label7.Font = New System.Drawing.Font("Tahoma", 12.25!)
        Me.Label7.ForeColor = System.Drawing.Color.Black
        Me.Label7.Location = New System.Drawing.Point(121, 28)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(238, 21)
        Me.Label7.TabIndex = 27
        Me.Label7.Text = "Seleccione el rango de tiempo:"
        '
        'ETataDataSetLijadoraBindingSource
        '
        Me.ETataDataSetLijadoraBindingSource.DataSource = Me.ETataDataSet_Lijadora
        Me.ETataDataSetLijadoraBindingSource.Position = 0
        '
        'ETataDataSet_Lijadora
        '
        Me.ETataDataSet_Lijadora.DataSetName = "eTataDataSet_Lijadora"
        Me.ETataDataSet_Lijadora.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SqlDataSource2
        '
        Me.SqlDataSource2.Name = "SqlDataSource2"
        '
        'Detalles_Lijadora
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.DarkCyan
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Detalles_Lijadora"
        Me.Size = New System.Drawing.Size(1064, 601)
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        CType(Me.StateIndicatorGauge7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.semaforo_chispa, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox4.ResumeLayout(False)
        Me.XtraScrollableControl2.ResumeLayout(False)
        CType(Me.StateIndicatorGauge6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.semaforo_fo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateIndicatorGauge5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.semaforo_productividad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateIndicatorGauge4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.semaforo_disponibilidad, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateIndicatorGauge1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.semaforo_IndicadoresProduccion, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateIndicatorGauge3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.semaforo_produccion2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateIndicatorGauge2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.semaforo_produccion, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox6.ResumeLayout(False)
        Me.GroupBox6.PerformLayout()
        CType(Me.SeparatorControl4, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.SeparatorControl3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DashboardViewer2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.DashboardViewer1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.tabla_pronostico1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Tabla_RegresionLineal, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.GridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.grafico_lijadora, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        CType(Me.ETataDataSetLijadoraBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.ETataDataSet_Lijadora, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TabControl1 As TabControl
    Friend WithEvents TabPage1 As TabPage
    Friend WithEvents TabPage2 As TabPage
    Friend WithEvents DashboardViewer1 As DevExpress.DashboardWin.DashboardViewer
    Friend WithEvents TabPage3 As TabPage
    Friend WithEvents grafico_lijadora As DevExpress.XtraCharts.ChartControl
    Friend WithEvents ETataDataSetLijadoraBindingSource As BindingSource
    Friend WithEvents ETataDataSet_Lijadora As eTataDataSet_Lijadora
    Friend WithEvents SqlDataSource1 As DevExpress.DataAccess.Sql.SqlDataSource
    Friend WithEvents SqlDataSource2 As DevExpress.DataAccess.Sql.SqlDataSource
    Friend WithEvents Tabla_RegresionLineal As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView1 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents DashboardViewer2 As DevExpress.DashboardWin.DashboardViewer
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents SeparatorControl3 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents MAC_FO As Label
    Friend WithEvents MAC_Producti As Label
    Friend WithEvents MAC_Dispo As Label
    Friend WithEvents MAC_Produccion As Label
    Friend WithEvents MA_FO As Label
    Friend WithEvents MA_Producti As Label
    Friend WithEvents MA_Dispo As Label
    Friend WithEvents MA_Produccion As Label
    Friend WithEvents Label27 As Label
    Friend WithEvents Label28 As Label
    Friend WithEvents Label29 As Label
    Friend WithEvents Label30 As Label
    Friend WithEvents Label31 As Label
    Friend WithEvents Label32 As Label
    Friend WithEvents GroupBox6 As GroupBox
    Friend WithEvents Prod_MesAnterior As Label
    Friend WithEvents Label34 As Label
    Friend WithEvents SeparatorControl4 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents Prod_Hoy As Label
    Friend WithEvents Prod_AnioAnterior As Label
    Friend WithEvents Label37 As Label
    Friend WithEvents Label38 As Label
    Friend WithEvents Label39 As Label
    Friend WithEvents tabla_pronostico1 As DevExpress.XtraGrid.GridControl
    Friend WithEvents GridView4 As DevExpress.XtraGrid.Views.Grid.GridView
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents XtraScrollableControl2 As DevExpress.XtraEditors.XtraScrollableControl
    Friend WithEvents comparacion_fo As RichTextBox
    Friend WithEvents GaugeControl4 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents StateIndicatorGauge6 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge
    Private WithEvents semaforo_fo As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent
    Friend WithEvents comparacion_productividad As RichTextBox
    Friend WithEvents comparacion_disponibilidad As RichTextBox
    Friend WithEvents GaugeControl3 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents StateIndicatorGauge5 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge
    Private WithEvents semaforo_productividad As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent
    Friend WithEvents sembilidad As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents StateIndicatorGauge4 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge
    Private WithEvents semaforo_disponibilidad As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent
    Friend WithEvents comparacion_produccion2 As RichTextBox
    Friend WithEvents GaugeControl1 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents StateIndicatorGauge1 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge
    Private WithEvents semaforo_IndicadoresProduccion As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent
    Friend WithEvents comparacion_IndicadoresProduccion As RichTextBox
    Friend WithEvents GaugeControl2 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents StateIndicatorGauge3 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge
    Private WithEvents semaforo_produccion2 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent
    Friend WithEvents semaforo_produccion1 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents StateIndicatorGauge2 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge
    Private WithEvents semaforo_produccion As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent
    Friend WithEvents comparacion_produccion As RichTextBox
    Friend WithEvents GaugeControl5 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents StateIndicatorGauge7 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge
    Private WithEvents semaforo_chispa As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent
    Friend WithEvents comparacion_chispa As RichTextBox
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TabPage4 As TabPage
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents Label9 As Label
    Friend WithEvents lijadora_informeMensual As Button
    Friend WithEvents lijadora_informeDiario As Button
    Friend WithEvents Lijadora_calendarioFinal As MonthCalendar
    Friend WithEvents Lijadora_calendarioInicial As MonthCalendar
    Friend WithEvents generar_Informe As Button
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents ToolTip1 As ToolTip
End Class
