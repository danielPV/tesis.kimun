﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Indicadores_Lijadora
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Imagen_Chispa = New System.Windows.Forms.PictureBox()
        Me.Label_Chispa = New System.Windows.Forms.Label()
        Me.Label_ValorTrabajo = New System.Windows.Forms.Label()
        Me.Label_ValorMesa3 = New System.Windows.Forms.Label()
        Me.Label_ValorMesa2 = New System.Windows.Forms.Label()
        Me.Label_ValorMesa1 = New System.Windows.Forms.Label()
        Me.Tiempo_Lijadora = New System.Windows.Forms.Timer(Me.components)
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ValorOLD_mesa1 = New System.Windows.Forms.Label()
        Me.ValorOLD_mesa2 = New System.Windows.Forms.Label()
        Me.ValorOLD_mesa3 = New System.Windows.Forms.Label()
        Me.ValorOLD_velocidad = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.num_chispas = New System.Windows.Forms.Label()
        Me.Imagen_mesa1 = New System.Windows.Forms.PictureBox()
        Me.Imagen_mesa2 = New System.Windows.Forms.PictureBox()
        Me.Imagen_mesa3 = New System.Windows.Forms.PictureBox()
        Me.Imagen_trabajo = New System.Windows.Forms.PictureBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.SeparatorControl2 = New DevExpress.XtraEditors.SeparatorControl()
        Me.Label_EstadoLijadora = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        CType(Me.Imagen_Chispa, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_mesa1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_mesa2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_mesa3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_trabajo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SeparatorControl2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label1.Location = New System.Drawing.Point(57, 54)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(83, 22)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Mesa 1:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label2.Location = New System.Drawing.Point(57, 110)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(83, 22)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Mesa 2:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label3.Location = New System.Drawing.Point(57, 160)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(83, 22)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Mesa 3:"
        Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label4.Location = New System.Drawing.Point(22, 216)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(159, 22)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Vel. de Trabajo: "
        '
        'Imagen_Chispa
        '
        Me.Imagen_Chispa.Image = Global.Kimun.My.Resources.Resources.chispa
        Me.Imagen_Chispa.Location = New System.Drawing.Point(65, 287)
        Me.Imagen_Chispa.Name = "Imagen_Chispa"
        Me.Imagen_Chispa.Size = New System.Drawing.Size(28, 26)
        Me.Imagen_Chispa.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.Imagen_Chispa.TabIndex = 4
        Me.Imagen_Chispa.TabStop = False
        '
        'Label_Chispa
        '
        Me.Label_Chispa.AutoSize = True
        Me.Label_Chispa.BackColor = System.Drawing.Color.Transparent
        Me.Label_Chispa.Font = New System.Drawing.Font("Arial", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Chispa.ForeColor = System.Drawing.Color.FromArgb(CType(CType(215, Byte), Integer), CType(CType(2, Byte), Integer), CType(CType(2, Byte), Integer))
        Me.Label_Chispa.Location = New System.Drawing.Point(99, 291)
        Me.Label_Chispa.Name = "Label_Chispa"
        Me.Label_Chispa.Size = New System.Drawing.Size(135, 16)
        Me.Label_Chispa.TabIndex = 5
        Me.Label_Chispa.Text = "Chispa Detectada "
        '
        'Label_ValorTrabajo
        '
        Me.Label_ValorTrabajo.AutoSize = True
        Me.Label_ValorTrabajo.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorTrabajo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorTrabajo.Location = New System.Drawing.Point(201, 218)
        Me.Label_ValorTrabajo.Name = "Label_ValorTrabajo"
        Me.Label_ValorTrabajo.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorTrabajo.TabIndex = 6
        '
        'Label_ValorMesa3
        '
        Me.Label_ValorMesa3.AutoSize = True
        Me.Label_ValorMesa3.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorMesa3.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorMesa3.Location = New System.Drawing.Point(168, 162)
        Me.Label_ValorMesa3.Name = "Label_ValorMesa3"
        Me.Label_ValorMesa3.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorMesa3.TabIndex = 7
        '
        'Label_ValorMesa2
        '
        Me.Label_ValorMesa2.AutoSize = True
        Me.Label_ValorMesa2.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorMesa2.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorMesa2.Location = New System.Drawing.Point(168, 109)
        Me.Label_ValorMesa2.Name = "Label_ValorMesa2"
        Me.Label_ValorMesa2.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorMesa2.TabIndex = 8
        '
        'Label_ValorMesa1
        '
        Me.Label_ValorMesa1.AutoSize = True
        Me.Label_ValorMesa1.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorMesa1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorMesa1.Location = New System.Drawing.Point(166, 56)
        Me.Label_ValorMesa1.Name = "Label_ValorMesa1"
        Me.Label_ValorMesa1.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorMesa1.TabIndex = 9
        '
        'Tiempo_Lijadora
        '
        Me.Tiempo_Lijadora.Enabled = True
        Me.Tiempo_Lijadora.Interval = 60000
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label9.Location = New System.Drawing.Point(68, 80)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(68, 13)
        Me.Label9.TabIndex = 19
        Me.Label9.Text = "Mesa 1 ayer:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label5.Location = New System.Drawing.Point(68, 136)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(68, 13)
        Me.Label5.TabIndex = 20
        Me.Label5.Text = "Mesa 1 ayer:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label6.Location = New System.Drawing.Point(68, 186)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(68, 13)
        Me.Label6.TabIndex = 21
        Me.Label6.Text = "Mesa 1 ayer:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label7.Location = New System.Drawing.Point(52, 242)
        Me.Label7.Name = "Label7"
        Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.Label7.Size = New System.Drawing.Size(101, 13)
        Me.Label7.TabIndex = 22
        Me.Label7.Text = "Vel. de trabajo ayer:"
        '
        'ValorOLD_mesa1
        '
        Me.ValorOLD_mesa1.AutoSize = True
        Me.ValorOLD_mesa1.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_mesa1.Location = New System.Drawing.Point(142, 80)
        Me.ValorOLD_mesa1.Name = "ValorOLD_mesa1"
        Me.ValorOLD_mesa1.Size = New System.Drawing.Size(90, 13)
        Me.ValorOLD_mesa1.TabIndex = 23
        Me.ValorOLD_mesa1.Text = "ValorOLD_mesa1"
        '
        'ValorOLD_mesa2
        '
        Me.ValorOLD_mesa2.AutoSize = True
        Me.ValorOLD_mesa2.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_mesa2.Location = New System.Drawing.Point(143, 136)
        Me.ValorOLD_mesa2.Name = "ValorOLD_mesa2"
        Me.ValorOLD_mesa2.Size = New System.Drawing.Size(39, 13)
        Me.ValorOLD_mesa2.TabIndex = 24
        Me.ValorOLD_mesa2.Text = "Label8"
        '
        'ValorOLD_mesa3
        '
        Me.ValorOLD_mesa3.AutoSize = True
        Me.ValorOLD_mesa3.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_mesa3.Location = New System.Drawing.Point(137, 186)
        Me.ValorOLD_mesa3.Name = "ValorOLD_mesa3"
        Me.ValorOLD_mesa3.Size = New System.Drawing.Size(45, 13)
        Me.ValorOLD_mesa3.TabIndex = 25
        Me.ValorOLD_mesa3.Text = "Label10"
        '
        'ValorOLD_velocidad
        '
        Me.ValorOLD_velocidad.AutoSize = True
        Me.ValorOLD_velocidad.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_velocidad.Location = New System.Drawing.Point(159, 242)
        Me.ValorOLD_velocidad.Name = "ValorOLD_velocidad"
        Me.ValorOLD_velocidad.Size = New System.Drawing.Size(45, 13)
        Me.ValorOLD_velocidad.TabIndex = 26
        Me.ValorOLD_velocidad.Text = "Label11"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label8.Location = New System.Drawing.Point(41, 261)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(149, 22)
        Me.Label8.TabIndex = 27
        Me.Label8.Text = "N° de chispas: "
        '
        'num_chispas
        '
        Me.num_chispas.AutoSize = True
        Me.num_chispas.BackColor = System.Drawing.Color.Snow
        Me.num_chispas.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.num_chispas.Location = New System.Drawing.Point(190, 263)
        Me.num_chispas.Name = "num_chispas"
        Me.num_chispas.Size = New System.Drawing.Size(0, 20)
        Me.num_chispas.TabIndex = 28
        '
        'Imagen_mesa1
        '
        Me.Imagen_mesa1.InitialImage = Nothing
        Me.Imagen_mesa1.Location = New System.Drawing.Point(140, 59)
        Me.Imagen_mesa1.Name = "Imagen_mesa1"
        Me.Imagen_mesa1.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_mesa1.TabIndex = 29
        Me.Imagen_mesa1.TabStop = False
        '
        'Imagen_mesa2
        '
        Me.Imagen_mesa2.InitialImage = Nothing
        Me.Imagen_mesa2.Location = New System.Drawing.Point(140, 112)
        Me.Imagen_mesa2.Name = "Imagen_mesa2"
        Me.Imagen_mesa2.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_mesa2.TabIndex = 30
        Me.Imagen_mesa2.TabStop = False
        '
        'Imagen_mesa3
        '
        Me.Imagen_mesa3.InitialImage = Nothing
        Me.Imagen_mesa3.Location = New System.Drawing.Point(140, 165)
        Me.Imagen_mesa3.Name = "Imagen_mesa3"
        Me.Imagen_mesa3.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_mesa3.TabIndex = 31
        Me.Imagen_mesa3.TabStop = False
        '
        'Imagen_trabajo
        '
        Me.Imagen_trabajo.InitialImage = Nothing
        Me.Imagen_trabajo.Location = New System.Drawing.Point(175, 221)
        Me.Imagen_trabajo.Name = "Imagen_trabajo"
        Me.Imagen_trabajo.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_trabajo.TabIndex = 32
        Me.Imagen_trabajo.TabStop = False
        '
        'SeparatorControl2
        '
        Me.SeparatorControl2.AutoSizeMode = True
        Me.SeparatorControl2.LineThickness = 4
        Me.SeparatorControl2.Location = New System.Drawing.Point(-6, 32)
        Me.SeparatorControl2.Name = "SeparatorControl2"
        Me.SeparatorControl2.Size = New System.Drawing.Size(315, 22)
        Me.SeparatorControl2.TabIndex = 36
        '
        'Label_EstadoLijadora
        '
        Me.Label_EstadoLijadora.AutoSize = True
        Me.Label_EstadoLijadora.BackColor = System.Drawing.Color.Transparent
        Me.Label_EstadoLijadora.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_EstadoLijadora.Location = New System.Drawing.Point(119, 12)
        Me.Label_EstadoLijadora.Name = "Label_EstadoLijadora"
        Me.Label_EstadoLijadora.Size = New System.Drawing.Size(63, 20)
        Me.Label_EstadoLijadora.TabIndex = 35
        Me.Label_EstadoLijadora.Text = "Label2"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.BackColor = System.Drawing.Color.Transparent
        Me.Label10.Location = New System.Drawing.Point(130, 8)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(0, 13)
        Me.Label10.TabIndex = 34
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.BackColor = System.Drawing.Color.Transparent
        Me.Label11.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label11.Location = New System.Drawing.Point(53, 12)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(68, 20)
        Me.Label11.TabIndex = 33
        Me.Label11.Text = "Estado: "
        '
        'Indicadores_Lijadora
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.SeparatorControl2)
        Me.Controls.Add(Me.Label_EstadoLijadora)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Imagen_trabajo)
        Me.Controls.Add(Me.Imagen_mesa3)
        Me.Controls.Add(Me.Imagen_mesa2)
        Me.Controls.Add(Me.Imagen_mesa1)
        Me.Controls.Add(Me.num_chispas)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.ValorOLD_velocidad)
        Me.Controls.Add(Me.ValorOLD_mesa3)
        Me.Controls.Add(Me.ValorOLD_mesa2)
        Me.Controls.Add(Me.ValorOLD_mesa1)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label_ValorMesa1)
        Me.Controls.Add(Me.Label_ValorMesa2)
        Me.Controls.Add(Me.Label_ValorMesa3)
        Me.Controls.Add(Me.Label_ValorTrabajo)
        Me.Controls.Add(Me.Label_Chispa)
        Me.Controls.Add(Me.Imagen_Chispa)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.DoubleBuffered = True
        Me.Name = "Indicadores_Lijadora"
        Me.Size = New System.Drawing.Size(315, 335)
        CType(Me.Imagen_Chispa, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_mesa1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_mesa2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_mesa3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_trabajo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SeparatorControl2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Imagen_Chispa As PictureBox
    Friend WithEvents Label_Chispa As Label
    Friend WithEvents Label_ValorTrabajo As Label
    Friend WithEvents Label_ValorMesa3 As Label
    Friend WithEvents Label_ValorMesa2 As Label
    Friend WithEvents Label_ValorMesa1 As Label
    Friend WithEvents Tiempo_Lijadora As Timer
    Friend WithEvents Label9 As Label
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents ValorOLD_mesa1 As Label
    Friend WithEvents ValorOLD_mesa2 As Label
    Friend WithEvents ValorOLD_mesa3 As Label
    Friend WithEvents ValorOLD_velocidad As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents num_chispas As Label
    Friend WithEvents Imagen_mesa1 As PictureBox
    Friend WithEvents Imagen_mesa2 As PictureBox
    Friend WithEvents Imagen_mesa3 As PictureBox
    Friend WithEvents Imagen_trabajo As PictureBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents SeparatorControl2 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents Label_EstadoLijadora As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Label11 As Label
End Class
