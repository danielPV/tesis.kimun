﻿Imports System.Data.SqlClient
Imports DevExpress.XtraCharts
Imports DevExpress.XtraPrinting
Imports DevExpress.XtraReports.UI
Imports DevExpress.XtraSplashScreen

Public Class Detalles_Caldera

    Dim resumen_nivelDomo As String = ""
    Dim resumen_presionDomo As String = ""
    Dim resumen_consumoVapor As String = ""
    Dim resumen_tempVapor As String = ""

    Private Sub Detalles_Caldera_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        Dim inicio_mes As Date = DateSerial(Year(Now), Month(Now), 1)
        Dim ahora As Date = Now

        'PORCENTAJES NIVEL DOMO
        Dim nivelDomo_mayorLimiteSup As Double = porcentaje_LimiteMayor("NIVEL DOMO", inicio_mes, ahora)
        Dim nivelDomo_casiLimiteSup As Double = porcentaje_casiLimiteMayor("NIVEL DOMO", inicio_mes, ahora)
        Dim nivelDomo_medio As Double = porcentaje_normal("NIVEL DOMO", inicio_mes, ahora)
        Dim nivelDomo_casiLimiteInf As Double = porcentaje_casiLimiteMenor("NIVEL DOMO", inicio_mes, ahora)
        Dim nivelDomo_menorLimiteInf As Double = porcentaje_LimiteMenor("NIVEL DOMO", inicio_mes, ahora)

        'PORCENTAJES PRESION DOMO
        Dim presionDomo_mayorLimiteSup As Double = porcentaje_LimiteMayor("PRESION DOMO", inicio_mes, ahora)
        Dim presionDomo_casiLimiteSup As Double = porcentaje_casiLimiteMayor("PRESION DOMO", inicio_mes, ahora)
        Dim presionDomo_medio As Double = porcentaje_normal("PRESION DOMO", inicio_mes, ahora)
        Dim presionDomo_casiLimiteInf As Double = porcentaje_casiLimiteMenor("PRESION DOMO", inicio_mes, ahora)
        Dim presionDomo_menorLimiteInf As Double = porcentaje_LimiteMenor("PRESION DOMO", inicio_mes, ahora)

        'PORCENTAJES CONSUMO VAPOR
        Dim consumoVapor_mayorLimiteSup As Double = porcentaje_LimiteMayor("CONSUMO VAPOR DOMO", inicio_mes, ahora)
        Dim consumoVapor_casiLimiteSup As Double = porcentaje_casiLimiteMayor("CONSUMO VAPOR DOMO", inicio_mes, ahora)
        Dim consumoVapor_medio As Double = porcentaje_normal("CONSUMO VAPOR DOMO", inicio_mes, ahora)
        Dim consumoVapor_casiLimiteInf As Double = porcentaje_casiLimiteMenor("CONSUMO VAPOR DOMO", inicio_mes, ahora)
        Dim consumoVapor_menorLimiteInf As Double = porcentaje_LimiteMenor("CONSUMO VAPOR DOMO", inicio_mes, ahora)

        'PORCENTAJES TEMPERATURA VAPOR
        Dim tempVapor_mayorLimiteSup As Double = porcentaje_LimiteMayor("T° VAPOR", inicio_mes, ahora)
        Dim tempVapor_casiLimiteSup As Double = porcentaje_casiLimiteMayor("T° VAPOR", inicio_mes, ahora)
        Dim tempVapor_medio As Double = porcentaje_normal("T° VAPOR", inicio_mes, ahora)
        Dim tempVapor_casiLimiteInf As Double = porcentaje_casiLimiteMenor("T° VAPOR", inicio_mes, ahora)
        Dim tempVapor_menorLimiteInf As Double = porcentaje_LimiteMenor("T° VAPOR", inicio_mes, ahora)

        Dim sb1 = New Text.StringBuilder()

        'SE ASIGNAN LOS RESUMENES A LOS STRING
        resumen_nivelDomo = mostrar_NivelDomo(nivelDomo_mayorLimiteSup, nivelDomo_casiLimiteSup, nivelDomo_medio, nivelDomo_casiLimiteInf, nivelDomo_menorLimiteInf)
        resumen_presionDomo = mostrar_presionDomo(presionDomo_mayorLimiteSup, presionDomo_casiLimiteSup, presionDomo_medio, presionDomo_casiLimiteInf, presionDomo_menorLimiteInf)
        resumen_consumoVapor = mostrar_consumoVapor(consumoVapor_mayorLimiteSup, consumoVapor_casiLimiteSup, consumoVapor_medio, consumoVapor_casiLimiteInf, consumoVapor_menorLimiteInf)
        resumen_tempVapor = mostrar_temperaturaVapor(tempVapor_mayorLimiteSup, tempVapor_casiLimiteSup, tempVapor_medio, tempVapor_casiLimiteInf, tempVapor_menorLimiteInf)

        'SE AGREGAN LOS STRING AL RESUMEN PRINCIPAL QUE SE MUESTRA
        sb1.Append("{\rtf1\ansi " & vbCr)
        sb1.Append(resumen_nivelDomo)
        sb1.Append(resumen_presionDomo)
        sb1.Append(resumen_consumoVapor)
        sb1.Append(resumen_tempVapor)
        sb1.Append("\f1\fs22\par}")

        cuadro_resumen.Rtf = sb1.ToString()

        'DA VALORES A LOS GRAFICOS PIE
        mostrar_pie("NIVEL DOMO", nivelDomo_mayorLimiteSup, nivelDomo_casiLimiteSup, nivelDomo_medio, nivelDomo_casiLimiteInf, nivelDomo_menorLimiteInf)
        mostrar_pie("PRESION DOMO", presionDomo_mayorLimiteSup, presionDomo_casiLimiteSup, presionDomo_medio, presionDomo_casiLimiteInf, presionDomo_menorLimiteInf)
        mostrar_pie("CONSUMO VAPOR DOMO", consumoVapor_mayorLimiteSup, consumoVapor_casiLimiteSup, consumoVapor_medio, consumoVapor_casiLimiteInf, consumoVapor_menorLimiteInf)
        mostrar_pie("T° VAPOR", tempVapor_mayorLimiteSup, tempVapor_casiLimiteSup, tempVapor_medio, tempVapor_casiLimiteInf, tempVapor_menorLimiteInf)

        'MUESTRA LAS IMAGENES SEGUN EL VALOR
        mostrar_Imagen("NIVEL DOMO", nivelDomo_mayorLimiteSup, nivelDomo_casiLimiteSup, nivelDomo_medio, nivelDomo_casiLimiteInf, nivelDomo_menorLimiteInf)
        mostrar_Imagen("PRESION DOMO", presionDomo_mayorLimiteSup, presionDomo_casiLimiteSup, presionDomo_medio, presionDomo_casiLimiteInf, presionDomo_menorLimiteInf)
        mostrar_Imagen("CONSUMO VAPOR DOMO", consumoVapor_mayorLimiteSup, consumoVapor_casiLimiteSup, consumoVapor_medio, consumoVapor_casiLimiteInf, consumoVapor_menorLimiteInf)
        mostrar_Imagen("T° VAPOR", tempVapor_mayorLimiteSup, tempVapor_casiLimiteSup, tempVapor_medio, tempVapor_casiLimiteInf, tempVapor_menorLimiteInf)

    End Sub

    Private Sub mostrar_pie(ByVal equivalencia As String, ByVal porcentaje_mayorLimiteSup As Double, ByVal porcentaje_casiLimiteSup As Double, ByVal porcentaje_medio As Double, ByVal porcentaje_casiLimiteInf As Double, ByVal porcentaje_menorLimiteInf As Double)


        If (equivalencia = "NIVEL DOMO") Then
            Dim serie1 = New Series("Registros Mes Actual", ViewType.Doughnut)

            serie1.Points.Add(New SeriesPoint("Mayores al límite superior", porcentaje_mayorLimiteSup * 100))
            serie1.Points.Add(New SeriesPoint("Cerca del límite superior", porcentaje_casiLimiteSup * 100))
            serie1.Points.Add(New SeriesPoint("Normal", porcentaje_medio * 100))
            serie1.Points.Add(New SeriesPoint("Cerca del límite inferior", porcentaje_casiLimiteInf * 100))
            serie1.Points.Add(New SeriesPoint("Menores al límite inferior", porcentaje_menorLimiteInf * 100))

            pie_nivelDomo.Series.Add(serie1)
            pie_nivelDomo.Series(0).LegendTextPattern = "{A}"
            pie_nivelDomo.Legend.Visibility = DevExpress.Utils.DefaultBoolean.True

            ' Access the view-type-specific options of the series.
            Dim myView As PieSeriesView = CType(serie1.View, PieSeriesView)

            ' Show a title for the series.
            myView.Titles.Add(New SeriesTitle())
            myView.Titles(0).Text = serie1.Name

            ' Enable chart tooltips. 
            pie_nivelDomo.ToolTipEnabled = DevExpress.Utils.DefaultBoolean.True

            ' Show a tooltip's beak
            Dim controller As New DevExpress.Utils.ToolTipController()
            pie_nivelDomo.ToolTipController = controller
            controller.ShowBeak = True

            ' Specify the tooltip point pattern.
            serie1.ToolTipPointPattern = "{A}"

        End If

        If (equivalencia = "PRESION DOMO") Then
            Dim serie2 = New Series("Registros Mes Actual", ViewType.Doughnut)

            serie2.Points.Add(New SeriesPoint("Mayores al límite superior", porcentaje_mayorLimiteSup * 100))
            serie2.Points.Add(New SeriesPoint("Cerca del límite superior", porcentaje_casiLimiteSup * 100))
            serie2.Points.Add(New SeriesPoint("Normal", porcentaje_medio * 100))
            serie2.Points.Add(New SeriesPoint("Cerca del límite inferior", porcentaje_casiLimiteInf * 100))
            serie2.Points.Add(New SeriesPoint("Menores al límite inferior", porcentaje_menorLimiteInf * 100))

            pie_presionDomo.Series.Add(serie2)

            'serie.Label.TextPattern = "{A}:"
            pie_presionDomo.Series(0).LegendTextPattern = "{A}"

            pie_presionDomo.Legend.Visibility = DevExpress.Utils.DefaultBoolean.True

            ' Access the view-type-specific options of the series.
            Dim myView As PieSeriesView = CType(serie2.View, PieSeriesView)

            ' Show a title for the series.
            myView.Titles.Add(New SeriesTitle())
            myView.Titles(0).Text = serie2.Name

            ' Enable chart tooltips. 
            pie_presionDomo.ToolTipEnabled = DevExpress.Utils.DefaultBoolean.True

            ' Show a tooltip's beak
            Dim controller As New DevExpress.Utils.ToolTipController()
            pie_presionDomo.ToolTipController = controller
            controller.ShowBeak = True

            ' Specify the tooltip point pattern.
            serie2.ToolTipPointPattern = "{A}"
        End If

        If (equivalencia = "CONSUMO VAPOR DOMO") Then
            Dim serie3 = New Series("Registros Mes Actual", ViewType.Doughnut)

            serie3.Points.Add(New SeriesPoint("Mayores al límite superior", porcentaje_mayorLimiteSup * 100))
            serie3.Points.Add(New SeriesPoint("Cerca del límite superior", porcentaje_casiLimiteSup * 100))
            serie3.Points.Add(New SeriesPoint("Normal", porcentaje_medio * 100))
            serie3.Points.Add(New SeriesPoint("Cerca del límite inferior", porcentaje_casiLimiteInf * 100))
            serie3.Points.Add(New SeriesPoint("Menores al límite inferior", porcentaje_menorLimiteInf * 100))

            pie_consumoVapor.Series.Add(serie3)

            'serie.Label.TextPattern = "{A}:"
            pie_consumoVapor.Series(0).LegendTextPattern = "{A}"

            pie_consumoVapor.Legend.Visibility = DevExpress.Utils.DefaultBoolean.True

            ' Access the view-type-specific options of the series.
            Dim myView As PieSeriesView = CType(serie3.View, PieSeriesView)

            ' Show a title for the series.
            myView.Titles.Add(New SeriesTitle())
            myView.Titles(0).Text = serie3.Name

            ' Enable chart tooltips. 
            pie_consumoVapor.ToolTipEnabled = DevExpress.Utils.DefaultBoolean.True

            ' Show a tooltip's beak
            Dim controller As New DevExpress.Utils.ToolTipController()
            pie_consumoVapor.ToolTipController = controller
            controller.ShowBeak = True

            ' Specify the tooltip point pattern.
            serie3.ToolTipPointPattern = "{A}"
        End If

        If (equivalencia = "T° VAPOR") Then
            Dim serie4 = New Series("Registros Mes Actual", ViewType.Doughnut)

            serie4.Points.Add(New SeriesPoint("Mayores al límite superior", porcentaje_mayorLimiteSup))
            serie4.Points.Add(New SeriesPoint("Cerca del límite superior", porcentaje_casiLimiteSup))
            serie4.Points.Add(New SeriesPoint("Normal", porcentaje_medio))
            serie4.Points.Add(New SeriesPoint("Cerca del límite inferior", porcentaje_casiLimiteInf))
            serie4.Points.Add(New SeriesPoint("Menores al límite inferior", porcentaje_menorLimiteInf))

            pie_tempVapor.Series.Add(serie4)

            'serie.Label.TextPattern = "{A}:"
            pie_tempVapor.Series(0).LegendTextPattern = "{A}"

            pie_tempVapor.Legend.Visibility = DevExpress.Utils.DefaultBoolean.True

            ' Access the view-type-specific options of the series.
            Dim myView As PieSeriesView = CType(serie4.View, PieSeriesView)

            ' Show a title for the series.
            myView.Titles.Add(New SeriesTitle())
            myView.Titles(0).Text = serie4.Name

            ' Enable chart tooltips. 
            pie_tempVapor.ToolTipEnabled = DevExpress.Utils.DefaultBoolean.True

            ' Show a tooltip's beak
            Dim controller As New DevExpress.Utils.ToolTipController()
            pie_tempVapor.ToolTipController = controller
            controller.ShowBeak = True

            ' Specify the tooltip point pattern.
            serie4.ToolTipPointPattern = "{A}"
        End If

    End Sub

    Private Function mostrar_NivelDomo(ByVal mayorLimSup As Double, ByVal casiLimSup As Double, ByVal medio As Double, ByVal casiLimInf As Double, ByVal menorLimInf As Double) As String

        Dim cadena As String = ""

        Dim porcentaje_mayorLimiteSup As Double = mayorLimSup
        Dim porcentaje_casiLimiteSup As Double = casiLimSup
        Dim porcentaje_medio As Double = medio
        Dim porcentaje_casiLimiteInf As Double = casiLimInf
        Dim porcentaje_menorLimiteInf As Double = menorLimInf

        Dim limiteSup As Double = False
        Dim limiteInf As Double = False

        If (porcentaje_mayorLimiteSup > 0) Then
            limiteSup = True
        End If

        If (porcentaje_menorLimiteInf > 0) Then
            limiteInf = True
        End If

        If (limiteSup = True And limiteInf = True) Then
            Dim suma_porcentajes As String = FormatPercent(porcentaje_mayorLimiteSup + porcentaje_menorLimiteInf)
            cadena = "Este mes, el nivel de agua del domo contiene registros \b muy preocupantes\b0  con un \b " & suma_porcentajes & "\b0  de mediciones fuera de los rangos."
        Else
            If (limiteSup = True) Then
                cadena = "Este mes, el nivel de agua del domo contiene registros \b preocupantes\b0  con un \b " & FormatPercent(porcentaje_mayorLimiteSup) & "\b0  de mediciones mayores al límite superior."
            End If

            If (limiteInf = True) Then
                cadena = "Este mes, el nivel de agua del domo contiene registros \b preocupantes\b0  con un \b " & FormatPercent(porcentaje_menorLimiteInf) & "\b0  de mediciones menores al límite inferior."
            End If
        End If

        If (limiteSup = False And limiteInf = False) Then
            cadena = "Este mes, el nivel de agua del domo contiene registros \b normales\b0 ."
        End If


        Return cadena

    End Function

    Private Function mostrar_presionDomo(ByVal mayorLimSup As Double, ByVal casiLimSup As Double, ByVal medio As Double, ByVal casiLimInf As Double, ByVal menorLimInf As Double) As String

        Dim cadena As String = ""

        Dim porcentaje_mayorLimiteSup As Double = mayorLimSup
        Dim porcentaje_casiLimiteSup As Double = casiLimSup
        Dim porcentaje_medio As Double = medio
        Dim porcentaje_casiLimiteInf As Double = casiLimInf
        Dim porcentaje_menorLimiteInf As Double = menorLimInf

        Dim limiteSup As Double = False
        Dim limiteInf As Double = False

        If (porcentaje_mayorLimiteSup > 0) Then
            limiteSup = True
        End If

        If (porcentaje_menorLimiteInf > 0) Then
            limiteInf = True
        End If

        If (limiteSup = True And limiteInf = True) Then
            Dim suma_porcentajes As String = FormatPercent(porcentaje_mayorLimiteSup + porcentaje_menorLimiteInf)

            If (resumen_nivelDomo.Contains("registros \b muy preocupantes\b0") = True) Then
                cadena = " Del mismo modo, se encontraron registros \b muy preocupantes\b0  en la presión del domo con un \b " & suma_porcentajes & "\b0  de mediciones fuera de los rangos."
            Else
                cadena = " Mientras que, la presión del domo contiene registros \b muy preocupantes\b0  con un \b " & suma_porcentajes & "\b0  de mediciones fuera de los rangos."
            End If
        Else
            If (limiteSup = True) Then
                If (resumen_nivelDomo.Contains("registros \b preocupantes\b0") = True) Then
                    cadena = " Del mismo modo, se encontraron registros \b preocupantes\b0  en la presión del domo con un \b " & FormatPercent(porcentaje_mayorLimiteSup) & "\b0  de mediciones mayores al límite superior."
                Else
                    cadena = " Mientras que, la presión del domo contiene registros \b preocupantes\b0 , con un \b " & FormatPercent(porcentaje_mayorLimiteSup) & "\b0  de mediciones mayores al límite superior."
                End If
            End If

            If (limiteInf = True) Then
                If (resumen_nivelDomo.Contains("registros \b preocupantes\b0") = True) Then
                    cadena = " Del mismo modo, se encontraron registros \b preocupantes\b0  en la presión del domo con un \b " & FormatPercent(porcentaje_menorLimiteInf) & "\b0  de mediciones menores al límite inferior."
                Else
                    cadena = " Mientras que, la presión del domo contiene registros \b preocupantes\b0 , con un \b " & FormatPercent(porcentaje_menorLimiteInf) & "\b0  de mediciones menores al límite inferior."
                End If
            End If
        End If

        If (limiteSup = False And limiteInf = False) Then
            If (resumen_nivelDomo.Contains("\b normales\b0") = True) Then
                cadena = " Del mismo modo, la presión del domo, también contiene registros \b normales\b0 ."
            Else
                cadena = " Mientras que, la presión del domo contiene registros \b normales\b0 ."
            End If

        End If

        Return cadena

    End Function

    Private Function mostrar_consumoVapor(ByVal mayorLimSup As Double, ByVal casiLimSup As Double, ByVal medio As Double, ByVal casiLimInf As Double, ByVal menorLimInf As Double) As String
        Dim cadena As String = ""

        Dim porcentaje_mayorLimiteSup As Double = mayorLimSup
        Dim porcentaje_casiLimiteSup As Double = casiLimSup
        Dim porcentaje_medio As Double = medio
        Dim porcentaje_casiLimiteInf As Double = casiLimInf
        Dim porcentaje_menorLimiteInf As Double = menorLimInf

        Dim limiteSup As Double = False
        Dim limiteInf As Double = False

        If (porcentaje_mayorLimiteSup > 0) Then
            limiteSup = True
        End If

        If (porcentaje_menorLimiteInf > 0) Then
            limiteInf = True
        End If

        If (limiteSup = True And limiteInf = True) Then
            Dim suma_porcentajes As String = FormatPercent(porcentaje_mayorLimiteSup + porcentaje_menorLimiteInf)

            If (resumen_presionDomo.Contains("registros \b muy preocupantes\b0") = True) Then
                cadena = " Igualmente, se hallaron registros \b muy preocupantes\b0  en el consumo de vapor del domo con un \b " & suma_porcentajes & "\b0  de mediciones fuera de los rangos."
            Else
                cadena = " Además, el consumo de vapor del domo contiene registros \b muy preocupantes\b0 , con un \b " & suma_porcentajes & "\b0  de mediciones fuera de los rangos."
            End If
        Else
            If (limiteSup = True) Then
                If (resumen_presionDomo.Contains("registros \b preocupantes\b0") = True) Then
                    cadena = " Igualmente, se hallaron registros \b preocupantes\b0  en el consumo de vapor del domo con un \b " & FormatPercent(porcentaje_mayorLimiteSup) & "\b0  de mediciones mayores al límite superior."
                Else
                    cadena = " Además, el consumo de vapor del domo contiene registros \b preocupantes\b0 , con un \b " & FormatPercent(porcentaje_mayorLimiteSup) & "\b0  de mediciones mayores al límite superior."
                End If

            End If

            If (limiteInf = True) Then
                If (resumen_presionDomo.Contains("registros \b preocupantes\b0") = True) Then
                    cadena = " Igualmente, se hallaron registros \b preocupantes\b0  en el consumo de vapor del domo con un \b " & FormatPercent(porcentaje_menorLimiteInf) & "\b0  de mediciones menores al límite inferior."
                Else
                    cadena = " Además, el consumo de vapor del domo contiene registros \b preocupantes\b0 , con un \b " & FormatPercent(porcentaje_menorLimiteInf) & "\b0  de mediciones menores al límite inferior."
                End If

            End If
        End If

        If (limiteSup = False And limiteInf = False) Then
            If (resumen_presionDomo.Contains("\b normales\b0") = True) Then
                cadena = " Igualmente, el consumo de vapor del domo, también contiene registros \b normales\b0 ."
            Else
                cadena = " Además, el consumo de vapor del domo contiene registros \b normales\b0 ."
            End If
        End If

        Return cadena

    End Function

    Private Function mostrar_temperaturaVapor(ByVal mayorLimSup As Double, ByVal casiLimSup As Double, ByVal medio As Double, ByVal casiLimInf As Double, ByVal menorLimInf As Double) As String
        Dim cadena As String = ""

        Dim porcentaje_mayorLimiteSup As Double = mayorLimSup
        Dim porcentaje_casiLimiteSup As Double = casiLimSup
        Dim porcentaje_medio As Double = medio
        Dim porcentaje_casiLimiteInf As Double = casiLimInf
        Dim porcentaje_menorLimiteInf As Double = menorLimInf

        Dim limiteSup As Double = False
        Dim limiteInf As Double = False

        If (porcentaje_mayorLimiteSup > 0) Then
            limiteSup = True
        End If

        If (porcentaje_menorLimiteInf > 0) Then
            limiteInf = True
        End If

        If (limiteSup = True And limiteInf = True) Then
            Dim suma_porcentajes As String = FormatPercent(porcentaje_mayorLimiteSup + porcentaje_menorLimiteInf)

            If (resumen_consumoVapor.Contains("registros \b muy preocupantes\b0") = True) Then
                cadena = " Por último, al igual que el indicador anterior, se detectaron registros \b muy preocupantes\b0  en la temperatura del vapor con un \b " & suma_porcentajes & "\b0  de mediciones fuera de los rangos."
            Else
                cadena = " Por último, la temperatura del vapor contiene registros \b muy preocupantes\b0 , con un \b " & suma_porcentajes & "\b0  de mediciones fuera de los rangos."
            End If
        Else
            If (limiteSup = True) Then
                If (resumen_consumoVapor.Contains("registros \b preocupantes\b0") = True) Then
                    cadena = " Por último, al igual que el indicador anterior, se detectaron registros \b preocupantes\b0  en la temperatura del vapor con un \b " & FormatPercent(porcentaje_mayorLimiteSup) & "\b0  de mediciones mayores al límite superior."
                Else
                    cadena = " Por último, la temperatura del vapor contiene registros \b preocupantes\b0 , con un \b " & FormatPercent(porcentaje_mayorLimiteSup) & "\b0  de mediciones mayores al límite superior."
                End If
            End If

            If (limiteInf = True) Then
                If (resumen_consumoVapor.Contains("registros \b preocupantes\b0") = True) Then
                    cadena = " Por último, al igual que el indicador anterior, se detectaron registros \b preocupantes\b0  en la temperatura del vapor con un \b " & FormatPercent(porcentaje_menorLimiteInf) & "\b0  de mediciones menores al límite inferior."
                Else
                    cadena = " Por último, la temperatura del vapor contiene registros \b preocupantes\b0 , con un \b " & FormatPercent(porcentaje_menorLimiteInf) & "\b0  de mediciones menores al límite inferior."
                End If
            End If
        End If

        If (limiteSup = False And limiteInf = False) Then
            If (resumen_consumoVapor.Contains("\b normales\b0") = True) Then
                cadena = " Por último, al igual que el indicador anterior, la temperatura del vapor contiene registros \b normales\b0 ."
            Else
                cadena = " Por último, la temperatura del vapor contiene registros \b normales\b0 ."
            End If
        End If

        Return cadena

    End Function

    Private Function cantidad_datos(ByVal porcentaje As Double) As String

        If (porcentaje >= 0.5) Then
            If (porcentaje >= 0.65) Then
                If (porcentaje >= 0.8) Then
                    If (porcentaje >= 0.9) Then
                        Return "la gran mayoria de las mediciones"
                    Else
                        Return "la mayoria de las mediciones"
                    End If
                Else
                    Return "más de la mitad de las mediciones"
                End If
            Else
                Return "un poco más de la mitad de las mediciones"
            End If
        Else
            If (porcentaje >= 0.45) Then
                Return "un poco menos de la mitad de las mediciones"
            Else
                If (porcentaje >= 0.2) Then
                    Return "menos de la mitad de las mediciones"
                Else
                    If (porcentaje >= 0.1) Then
                        Return "pocas mediciones"
                    Else
                        If (porcentaje = 0) Then
                            Return "Ninguna de las mediciones"
                        Else
                            Return "muy pocas mediciones"
                        End If
                    End If
                End If
            End If
        End If
    End Function

    Private Sub mostrar_Imagen(ByVal equivalencia As String, ByVal porcentaje_mayorLimiteSup As Double, ByVal porcentaje_casiLimiteSup As Double, ByVal porcentaje_medio As Double, ByVal porcentaje_casiLimiteInf As Double, ByVal porcentaje_menorLimiteInf As Double)

        Dim valor_nivelDomo As Double = 0
        Dim valor_presionDomo As Double = 0
        Dim valor_consumoVapor As Double = 0
        Dim valor_tempVapor As Double = 0

        If (equivalencia = "NIVEL DOMO") Then

            Dim limiteSup As Double = False
            Dim limiteInf As Double = False

            If (porcentaje_mayorLimiteSup > 0) Then
                limiteSup = True
            End If

            If (porcentaje_menorLimiteInf > 0) Then
                limiteInf = True
            End If

            If (limiteInf = True And limiteSup = True) Then
                imagen_nivelDomo.Image = My.Resources.nivelAgua_rojo

                LinearGauge1.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                LinearGauge1.OptionsToolTip.TooltipTitle = "Registros muy preocupantes"


                valor_nivelDomo = (porcentaje_mayorLimiteSup * 100) + (porcentaje_menorLimiteInf * 100)
                tolerancia_nivelDomo.Value = valor_nivelDomo

                If (valor_nivelDomo <= 25) Then
                    LinearGauge1.OptionsToolTip.Tooltip = "Se han registrado algunas mediciones del nivel del agua mayores al límite superior y menores al límite inferior."
                End If

                If (valor_nivelDomo > 25 And valor_nivelDomo <= 75) Then
                    LinearGauge1.OptionsToolTip.Tooltip = "Se han registrado mediciones del nivel del agua mayores al límite superior y menores al límite inferior."
                End If

                If (valor_nivelDomo > 75) Then
                    LinearGauge1.OptionsToolTip.Tooltip = "Se han registrado muchas mediciones del nivel del agua mayores al límite superior y menores al límite inferior."
                End If

            Else
                If (limiteInf = True) Then
                    LinearGauge1.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                    LinearGauge1.OptionsToolTip.TooltipTitle = "Registros preocupantes"
                    LinearGauge1.OptionsToolTip.Tooltip = "Mediciones del nivel de agua muy bajas."
                    tolerancia_nivelDomo.Value = porcentaje_menorLimiteInf * 100
                End If

                If (limiteSup = True) Then
                    LinearGauge1.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                    LinearGauge1.OptionsToolTip.TooltipTitle = "Registros preocupantes"
                    LinearGauge1.OptionsToolTip.Tooltip = "Mediciones del nivel de agua muy altas."
                    tolerancia_nivelDomo.Value = porcentaje_mayorLimiteSup * 100
                End If
            End If

            If (limiteInf = False And limiteSup = False) Then
                If (porcentaje_casiLimiteSup > porcentaje_medio Or porcentaje_casiLimiteInf > porcentaje_medio) Then
                    imagen_nivelDomo.Image = My.Resources.nivelAgua_amarillo
                    GaugeControl1.Visible = False
                Else
                    imagen_nivelDomo.Image = My.Resources.nivelAgua_verde
                    GaugeControl1.Visible = False
                End If
            End If
        End If

        If (equivalencia = "PRESION DOMO") Then
            Dim limiteSup As Double = False
            Dim limiteInf As Double = False

            If (porcentaje_mayorLimiteSup > 0) Then
                limiteSup = True
            End If

            If (porcentaje_menorLimiteInf > 0) Then
                limiteInf = True
            End If

            If (limiteInf = True And limiteSup = True) Then
                imagen_presionDomo.Image = My.Resources.presionVapor_rojo

                LinearGauge2.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                LinearGauge2.OptionsToolTip.TooltipTitle = "Registros muy preocupantes"


                valor_presionDomo = (porcentaje_mayorLimiteSup * 100) + (porcentaje_menorLimiteInf * 100)
                tolerancia_presionDomo.Value = valor_presionDomo

                If (valor_presionDomo <= 25) Then
                    LinearGauge2.OptionsToolTip.Tooltip = "Se han registrado algunas mediciones de la presión del domo mayores al límite superior y menores al límite inferior."
                End If

                If (valor_presionDomo > 25 And valor_presionDomo <= 75) Then
                    LinearGauge2.OptionsToolTip.Tooltip = "Se han registrado mediciones de la presión del domo mayores al límite superior y menores al límite inferior."
                End If

                If (valor_presionDomo > 75) Then
                    LinearGauge2.OptionsToolTip.Tooltip = "Se han registrado muchas mediciones de la presión del domo mayores al límite superior y menores al límite inferior."
                End If

            Else
                If (limiteInf = True) Then
                    LinearGauge2.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                    LinearGauge2.OptionsToolTip.TooltipTitle = "Registros preocupantes"
                    LinearGauge2.OptionsToolTip.Tooltip = "Mediciones de la presión del domo muy bajas."
                    tolerancia_presionDomo.Value = porcentaje_menorLimiteInf * 100
                    imagen_presionDomo.Image = My.Resources.presionVapor_rojo
                End If

                If (limiteSup = True) Then
                    LinearGauge2.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                    LinearGauge2.OptionsToolTip.TooltipTitle = "Registros preocupantes"
                    LinearGauge2.OptionsToolTip.Tooltip = "Mediciones de la presión del domo muy altas."
                    tolerancia_presionDomo.Value = porcentaje_mayorLimiteSup * 100
                    imagen_presionDomo.Image = My.Resources.presionVapor_rojo
                End If
            End If

            If (limiteInf = False And limiteSup = False) Then
                If (porcentaje_casiLimiteSup > porcentaje_medio Or porcentaje_casiLimiteInf > porcentaje_medio) Then
                    imagen_presionDomo.Image = My.Resources.presionVapor_amarillo
                    GaugeControl2.Visible = False
                Else
                    imagen_presionDomo.Image = My.Resources.presionVapor_verde
                    GaugeControl2.Visible = False
                End If
            End If
        End If

        If (equivalencia = "CONSUMO VAPOR DOMO") Then
            Dim limiteSup As Double = False
            Dim limiteInf As Double = False

            If (porcentaje_mayorLimiteSup > 0) Then
                limiteSup = True
            End If

            If (porcentaje_menorLimiteInf > 0) Then
                limiteInf = True
            End If

            If (limiteInf = True And limiteSup = True) Then
                imagen_consumoVapor.Image = My.Resources.consumoVapor_rojo

                LinearGauge3.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                LinearGauge3.OptionsToolTip.TooltipTitle = "Registros muy preocupantes"


                valor_consumoVapor = (porcentaje_mayorLimiteSup * 100) + (porcentaje_menorLimiteInf * 100)
                tolerancia_consumoVapor.Value = valor_consumoVapor

                If (valor_consumoVapor <= 25) Then
                    LinearGauge3.OptionsToolTip.Tooltip = "Se han registrado algunas mediciones del consumo de vapor del domo mayores al límite superior y menores al límite inferior."
                End If

                If (valor_consumoVapor > 25 And valor_consumoVapor <= 75) Then
                    LinearGauge3.OptionsToolTip.Tooltip = "Se han registrado mediciones del consumo de vapor del domo mayores al límite superior y menores al límite inferior."
                End If

                If (valor_consumoVapor > 75) Then
                    LinearGauge3.OptionsToolTip.Tooltip = "Se han registrado muchas mediciones del consumo de vapor del domo mayores al límite superior y menores al límite inferior."
                End If

            Else
                If (limiteInf = True) Then
                    LinearGauge3.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                    LinearGauge3.OptionsToolTip.TooltipTitle = "Registros preocupantes"
                    LinearGauge3.OptionsToolTip.Tooltip = "Mediciones de consumo de vapor del domo muy bajo."
                    tolerancia_consumoVapor.Value = porcentaje_menorLimiteInf * 100
                    imagen_consumoVapor.Image = My.Resources.consumoVapor_rojo
                End If

                If (limiteSup = True) Then
                    LinearGauge3.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                    LinearGauge3.OptionsToolTip.TooltipTitle = "Registros preocupantes"
                    LinearGauge3.OptionsToolTip.Tooltip = "Mediciones de consumo de vapor del domo muy altos."
                    tolerancia_consumoVapor.Value = porcentaje_mayorLimiteSup * 100
                    imagen_consumoVapor.Image = My.Resources.consumoVapor_rojo
                End If
            End If

            If (limiteInf = False And limiteSup = False) Then
                If (porcentaje_casiLimiteSup > porcentaje_medio Or porcentaje_casiLimiteInf > porcentaje_medio) Then
                    imagen_consumoVapor.Image = My.Resources.consumoVapor_amarillo
                    GaugeControl3.Visible = False
                Else
                    imagen_consumoVapor.Image = My.Resources.consumoVapor_verde
                    GaugeControl3.Visible = False
                End If
            End If
        End If

        If (equivalencia = "T° VAPOR") Then
            Dim limiteSup As Double = False
            Dim limiteInf As Double = False

            If (porcentaje_mayorLimiteSup > 0) Then
                limiteSup = True
            End If

            If (porcentaje_menorLimiteInf > 0) Then
                limiteInf = True
            End If

            If (limiteInf = True And limiteSup = True) Then
                imagen_tempVapor.Image = My.Resources.tempVapor_rojo

                LinearGauge4.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                LinearGauge4.OptionsToolTip.TooltipTitle = "Registros muy preocupantes"


                valor_tempVapor = (porcentaje_mayorLimiteSup * 100) + (porcentaje_menorLimiteInf * 100)
                tolerancia_tempVapor.Value = valor_tempVapor

                If (valor_tempVapor <= 25) Then
                    LinearGauge4.OptionsToolTip.Tooltip = "Se han registrado algunas mediciones de la temperatura del vapor mayores al límite superior y menores al límite inferior."
                End If

                If (valor_tempVapor > 25 And valor_tempVapor <= 75) Then
                    LinearGauge4.OptionsToolTip.Tooltip = "Se han registrado mediciones de la temperatura del vapor mayores al límite superior y menores al límite inferior."
                End If

                If (valor_tempVapor > 75) Then
                    LinearGauge4.OptionsToolTip.Tooltip = "Se han registrado muchas mediciones de la temperatura del vapor mayores al límite superior y menores al límite inferior."
                End If

            Else
                If (limiteInf = True) Then
                    LinearGauge4.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                    LinearGauge4.OptionsToolTip.TooltipTitle = "Registros preocupantes"
                    LinearGauge4.OptionsToolTip.Tooltip = "Mediciones de la temperatura del vapor muy bajas."
                    tolerancia_tempVapor.Value = porcentaje_menorLimiteInf * 100
                    imagen_tempVapor.Image = My.Resources.tempVapor_rojo
                End If

                If (limiteSup = True) Then
                    LinearGauge4.OptionsToolTip.TooltipIconType = DevExpress.Utils.ToolTipIconType.Warning
                    LinearGauge4.OptionsToolTip.TooltipTitle = "Registros preocupantes"
                    LinearGauge4.OptionsToolTip.Tooltip = "Mediciones de la temperatura del vapor muy altas."
                    tolerancia_tempVapor.Value = porcentaje_mayorLimiteSup * 100
                    imagen_tempVapor.Image = My.Resources.tempVapor_rojo
                End If
            End If

            If (limiteInf = False And limiteSup = False) Then
                If (porcentaje_casiLimiteSup > porcentaje_medio Or porcentaje_casiLimiteInf > porcentaje_medio) Then
                    imagen_tempVapor.Image = My.Resources.tempVapor_amarillo
                    GaugeControl4.Visible = False
                Else
                    imagen_tempVapor.Image = My.Resources.tempVapor_verde
                    GaugeControl4.Visible = False
                End If
            End If
        End If

    End Sub

    Private Function porcentaje_LimiteMayor(ByVal equivalencia As String, ByVal fecha1 As Date, ByVal fecha2 As Date) As Double

        Dim contador_limiteMayor As New SqlCommand("SELECT COUNT(FloatTableCaldera.Val) 
                                        FROM EquivalenciaCaldera INNER JOIN
                                        FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                                        LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                                        WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                                        AND (EquivalenciaCaldera.Equivalencia = @equi) 
                                        AND FloatTableCaldera.Val >= LimitesCaldera.Limite", conexion_eTata)

        Dim contador As New SqlCommand("SELECT COUNT(FloatTableCaldera.Val) 
                                        FROM EquivalenciaCaldera INNER JOIN
                                        FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                                        LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                                        WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                                        AND (EquivalenciaCaldera.Equivalencia = @equi)", conexion_eTata)

        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            contador_limiteMayor.Parameters.AddWithValue("@equi", equivalencia)
            contador_limiteMayor.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            contador_limiteMayor.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            contador.Parameters.AddWithValue("@equi", equivalencia)
            contador.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            contador.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            Dim contador_mayoresAlLimite As Double = contador_limiteMayor.ExecuteScalar()
            Dim contador_general As Double = contador.ExecuteScalar()

            Dim promedio As Double = (contador_mayoresAlLimite / contador_general)

            Return Format(promedio, "000.00")


        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar valores sobre el límite superior de " + equivalencia)
            Return 0
        End Try

    End Function

    Private Function porcentaje_casiLimiteMayor(ByVal equivalencia As String, ByVal fecha1 As Date, ByVal fecha2 As Date) As Double

        Dim cadena As String = ""

        If (equivalencia = "T° VAPOR") Then
            cadena = "SELECT COUNT(FloatTableCaldera.Val) 
                    FROM EquivalenciaCaldera INNER JOIN
                    FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                    LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                    WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                    AND (EquivalenciaCaldera.Equivalencia = @equi) 
                    AND FloatTableCaldera.Val >= (LimitesCaldera.Limite - 5) 
                    AND FloatTableCaldera.Val < (LimitesCaldera.Limite)"

        Else
            cadena = "SELECT COUNT(FloatTableCaldera.Val) 
                    FROM EquivalenciaCaldera INNER JOIN
                    FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                    LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                    WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                    AND (EquivalenciaCaldera.Equivalencia = @equi) 
                    AND FloatTableCaldera.Val >= (LimitesCaldera.Limite - (LimitesCaldera.Limite * 0.2)) 
                    AND FloatTableCaldera.Val < (LimitesCaldera.Limite)"
        End If

        Dim contador_casiLimiteMayor As New SqlCommand(cadena, conexion_eTata)


        Dim contador As New SqlCommand("SELECT COUNT(FloatTableCaldera.Val) 
                                        FROM EquivalenciaCaldera INNER JOIN
                                        FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                                        LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                                        WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                                        AND (EquivalenciaCaldera.Equivalencia = @equi)", conexion_eTata)

        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            contador_casiLimiteMayor.Parameters.AddWithValue("@equi", equivalencia)
            contador_casiLimiteMayor.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            contador_casiLimiteMayor.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            contador.Parameters.AddWithValue("@equi", equivalencia)
            contador.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            contador.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            Dim contador_casiAlLimite As Double = contador_casiLimiteMayor.ExecuteScalar()
            Dim contador_general As Double = contador.ExecuteScalar()

            Dim promedio As Double = (contador_casiAlLimite / contador_general)

            Return Format(promedio, "000.00")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar valores cercanos al limite de " + equivalencia)
            Return 0
        End Try

    End Function

    Private Function porcentaje_normal(ByVal equivalencia As String, ByVal fecha1 As Date, ByVal fecha2 As Date) As Double

        Dim cadena As String = ""

        If (equivalencia = "T° VAPOR") Then
            cadena = "SELECT COUNT(FloatTableCaldera.Val) 
                    FROM EquivalenciaCaldera INNER JOIN
                    FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                    LimiteMinCaldera ON EquivalenciaCaldera.TagIndex = LimiteMinCaldera.TagIndex INNER JOIN
                    LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                    WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                    AND (EquivalenciaCaldera.Equivalencia = @equi) 
                    AND FloatTableCaldera.Val < (LimitesCaldera.Limite - 5) 
                    AND FloatTableCaldera.Val > (LimiteMinCaldera.LimiteMin + 5)"

        Else
            cadena = "SELECT COUNT(FloatTableCaldera.Val) 
                    FROM EquivalenciaCaldera INNER JOIN
                    FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                    LimiteMinCaldera ON EquivalenciaCaldera.TagIndex = LimiteMinCaldera.TagIndex INNER JOIN
                    LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                    WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                    AND (EquivalenciaCaldera.Equivalencia = @equi) 
                    AND FloatTableCaldera.Val < (LimitesCaldera.Limite - (LimitesCaldera.Limite * 0.2)) 
                    AND FloatTableCaldera.Val > (LimiteMinCaldera.LimiteMin + (LimiteMinCaldera.LimiteMin * 0.2))"
        End If

        Dim contador_normal As New SqlCommand(cadena, conexion_eTata)

        Dim contador As New SqlCommand("SELECT COUNT(FloatTableCaldera.Val) 
                                        FROM EquivalenciaCaldera INNER JOIN
                                        FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                                        LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                                        WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                                        AND (EquivalenciaCaldera.Equivalencia = @equi)", conexion_eTata)

        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            contador_normal.Parameters.AddWithValue("@equi", equivalencia)
            contador_normal.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            contador_normal.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            contador.Parameters.AddWithValue("@equi", equivalencia)
            contador.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            contador.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            Dim contador_rangoMedio As Double = contador_normal.ExecuteScalar()
            Dim contador_general As Double = contador.ExecuteScalar()

            Dim promedio As Double = (contador_rangoMedio / contador_general)

            Return Format(promedio, "000.00")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar valores normales de " + equivalencia)
            Return 0
        End Try


    End Function

    Private Function porcentaje_casiLimiteMenor(ByVal equivalencia As String, ByVal fecha1 As Date, ByVal fecha2 As Date) As Double

        Dim cadena As String = ""

        If (equivalencia = "T° VAPOR") Then
            cadena = "SELECT COUNT(FloatTableCaldera.Val) 
                    FROM EquivalenciaCaldera INNER JOIN
                    FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                    LimiteMinCaldera ON EquivalenciaCaldera.TagIndex = LimiteMinCaldera.TagIndex INNER JOIN
                    LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                    WHERE (FloatTableCaldera.DateAndTime BETWEEN  @fecha1 AND @fecha2)
                    AND (EquivalenciaCaldera.Equivalencia = @equi) 
                    AND FloatTableCaldera.Val <= (LimiteMinCaldera.LimiteMin + 5) 
                    AND FloatTableCaldera.Val > (LimiteMinCaldera.LimiteMin)"
        Else
            cadena = "SELECT COUNT(FloatTableCaldera.Val) 
                    FROM EquivalenciaCaldera INNER JOIN
                    FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                    LimiteMinCaldera ON EquivalenciaCaldera.TagIndex = LimiteMinCaldera.TagIndex INNER JOIN
                    LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                    WHERE (FloatTableCaldera.DateAndTime BETWEEN  @fecha1 AND @fecha2)
                    AND (EquivalenciaCaldera.Equivalencia = @equi) 
                    AND FloatTableCaldera.Val <= (LimiteMinCaldera.LimiteMin + (LimiteMinCaldera.LimiteMin * 0.2)) 
                    AND FloatTableCaldera.Val > (LimiteMinCaldera.LimiteMin)"
        End If

        Dim contador_casiLimiteMenor As New SqlCommand(cadena, conexion_eTata)

        Dim contador As New SqlCommand("SELECT COUNT(FloatTableCaldera.Val) 
                                        FROM EquivalenciaCaldera INNER JOIN
                                        FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                                        LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                                        WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                                        AND (EquivalenciaCaldera.Equivalencia = @equi)", conexion_eTata)

        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            contador_casiLimiteMenor.Parameters.AddWithValue("@equi", equivalencia)
            contador_casiLimiteMenor.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            contador_casiLimiteMenor.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            contador.Parameters.AddWithValue("@equi", equivalencia)
            contador.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            contador.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            Dim contador_casiLimiteInf As Double = contador_casiLimiteMenor.ExecuteScalar()
            Dim contador_general As Double = contador.ExecuteScalar()

            Dim promedio As Double = (contador_casiLimiteInf / contador_general)

            Return Format(promedio, "000.00")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar valores cercanos al límite inferior de " + equivalencia)
            Return 0
        End Try

    End Function

    Private Function porcentaje_LimiteMenor(ByVal equivalencia As String, ByVal fecha1 As Date, ByVal fecha2 As Date) As Double

        Dim contador_limiteMenor As New SqlCommand("SELECT COUNT(FloatTableCaldera.Val) 
                                        FROM EquivalenciaCaldera INNER JOIN
                                        FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                                        LimiteMinCaldera ON EquivalenciaCaldera.TagIndex = LimiteMinCaldera.TagIndex
                                        WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                                        AND (EquivalenciaCaldera.Equivalencia = @equi) 
                                        AND FloatTableCaldera.Val <= LimiteMinCaldera.LimiteMin", conexion_eTata)

        Dim contador As New SqlCommand("SELECT COUNT(FloatTableCaldera.Val) 
                                        FROM EquivalenciaCaldera INNER JOIN
                                        FloatTableCaldera ON EquivalenciaCaldera.TagIndex = FloatTableCaldera.TagIndex INNER JOIN
                                        LimitesCaldera ON EquivalenciaCaldera.TagIndex = LimitesCaldera.TagIndex
                                        WHERE (FloatTableCaldera.DateAndTime BETWEEN @fecha1 AND @fecha2)
                                        AND (EquivalenciaCaldera.Equivalencia = @equi)", conexion_eTata)



        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            contador_limiteMenor.Parameters.AddWithValue("@equi", equivalencia)
            contador_limiteMenor.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            contador_limiteMenor.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            contador.Parameters.AddWithValue("@equi", equivalencia)
            contador.Parameters.AddWithValue("@fecha1", Format(fecha1, "yyyy-MM-dd HH:mm:ss"))
            contador.Parameters.AddWithValue("@fecha2", Format(fecha2, "yyyy-MM-dd HH:mm:ss"))

            Dim contador_menoresAlLimite As Double = contador_limiteMenor.ExecuteScalar()
            Dim contador_general As Double = contador.ExecuteScalar()

            Dim promedio As Double = (contador_menoresAlLimite / contador_general)

            Return Format(promedio, "000.00")

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar valores bajo el límite inferior de " + equivalencia)
            Return 0
        End Try

    End Function

    Private Sub generar_InformeCalidad_Click(sender As Object, e As EventArgs) Handles generar_InformeCalidad.Click

        Try
            Dim c As Cursor = Me.Cursor

            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            abrir_controlProduccion(GetConexion_controlProduccion())

            Dim reporte_rangos As New Reporte_CalidadAguas

            Dim tool As New ReportPrintTool(reporte_rangos)

            reporte_rangos.SqlDataSource2.Connection.ConnectionString = GetConexion_controlProduccion()

            reporte_rangos.Parameters("Fecha1").Value = Calidad_calendarioInicial.SelectionRange.Start
            reporte_rangos.Parameters("Fecha2").Value = Calidad_calendarioFinal.SelectionRange.Start

            reporte_rangos.Parameters("Fecha1").Visible = False
            reporte_rangos.Parameters("Fecha2").Visible = False

            'tool.PreviewForm.ControlBox = False
            tool.PreviewForm.Hide()
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
            tool.ShowPreview()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de calidad de aguas")
        End Try

    End Sub

    Private Sub calidad_informeDiario_Click(sender As Object, e As EventArgs) Handles calidad_informeDiario.Click

        Try
            Dim c As Cursor = Me.Cursor

            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            abrir_controlProduccion(GetConexion_controlProduccion())

            Dim reporte_diario As New Reporte_CalidadAguas

            Dim tool As New ReportPrintTool(reporte_diario)

            Dim final_dia As Date = String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

            reporte_diario.SqlDataSource2.Connection.ConnectionString = GetConexion_controlProduccion()

            reporte_diario.Parameters("Fecha1").Value = Format(Now, "yyyy/MM/dd")
            reporte_diario.Parameters("Fecha2").Value = final_dia

            reporte_diario.Parameters("Fecha1").Visible = False
            reporte_diario.Parameters("Fecha2").Visible = False

            'tool.PreviewForm.ControlBox = False
            tool.PreviewForm.Hide()
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
            tool.ShowPreview()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de calidad de aguas")
        End Try

    End Sub

    Private Sub calidad_informeMensual_Click(sender As Object, e As EventArgs) Handles calidad_informeMensual.Click

        Try
            Dim c As Cursor = Me.Cursor

            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            abrir_controlProduccion(GetConexion_controlProduccion())

            Dim reporte_mensual As New Reporte_CalidadAguas

            Dim tool As New ReportPrintTool(reporte_mensual)

            Dim final_mes As Date = String.Format("{0} {1}", Format(DateSerial(Year(Now), Month(Now) + 1, 0), "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

            'reporte_mensual.SqlDataSource2.ConnectionName = "conectar_controlProduccion"
            reporte_mensual.SqlDataSource2.Connection.ConnectionString = GetConexion_controlProduccion()

            reporte_mensual.Parameters("Fecha1").Value = DateSerial(Year(Now), Month(Now), 1)
            reporte_mensual.Parameters("Fecha2").Value = final_mes

            reporte_mensual.Parameters("Fecha1").Visible = False
            reporte_mensual.Parameters("Fecha2").Visible = False

            'tool.PreviewForm.ControlBox = False
            tool.PreviewForm.Hide()
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
            tool.ShowPreview()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de calidad de aguas")
        End Try
    End Sub

    Private Sub Generar_informeIndicadores_Click(sender As Object, e As EventArgs) Handles Generar_informeIndicadores.Click

        Try
            Dim c As Cursor = Me.Cursor

            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            abrir_eTata(GetConexion_eTata())

            Dim reporte_rangos As New Reporte_IndicadoresCaldera

            Dim tool As New ReportPrintTool(reporte_rangos)

            'reporte_rangos.SqlDataSource2.ConnectionName = "conectar_eTata"
            reporte_rangos.SqlDataSource2.Connection.ConnectionString = GetConexion_eTata()

            reporte_rangos.Parameters("fecha1").Value = Indicadores_calendarioInicial.SelectionRange.Start
            reporte_rangos.Parameters("fecha2").Value = Indicadores_calendarioFinal.SelectionRange.Start

            reporte_rangos.Parameters("fecha1").Visible = False
            reporte_rangos.Parameters("fecha2").Visible = False

            'tool.PreviewForm.ControlBox = False
            tool.PreviewForm.Hide()
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
            tool.ShowPreview()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de indicadores de la caldera")
        End Try

    End Sub

    Private Sub indicadores_informeDiario_Click(sender As Object, e As EventArgs) Handles indicadores_informeDiario.Click

        Try
            Dim c As Cursor = Me.Cursor

            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            abrir_eTata(GetConexion_eTata())

            Dim reporte_diario As New Reporte_IndicadoresCaldera

            Dim tool As New ReportPrintTool(reporte_diario)

            Dim final_dia As Date = String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

            'reporte_diario.SqlDataSource2.ConnectionName = "conectar_eTata"
            reporte_diario.SqlDataSource2.Connection.ConnectionString = GetConexion_eTata()

            reporte_diario.Parameters("fecha1").Value = Format(Now, "yyyy/MM/dd")
            reporte_diario.Parameters("fecha2").Value = final_dia

            reporte_diario.Parameters("fecha1").Visible = False
            reporte_diario.Parameters("fecha2").Visible = False

            'tool.PreviewForm.ControlBox = False
            tool.PreviewForm.Hide()
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
            tool.ShowPreview()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de indicadores de la caldera")
        End Try

    End Sub

    Private Sub Indicadores_InformeMensual_Click(sender As Object, e As EventArgs) Handles Indicadores_InformeMensual.Click

        Try
            Dim c As Cursor = Me.Cursor

            Me.Cursor = Cursors.WaitCursor
            SplashScreenManager.ShowForm(GetType(WaitForm1))

            abrir_eTata(GetConexion_eTata())

            Dim reporte_mensual As New Reporte_IndicadoresCaldera

            Dim tool As New ReportPrintTool(reporte_mensual)

            Dim final_mes As Date = String.Format("{0} {1}", Format(DateSerial(Year(Now), Month(Now) + 1, 0), "yyyy/MM/dd"), Format(TimeSerial(23, 59, 59), "HH:mm:ss"))

            'reporte_mensual.SqlDataSource2.ConnectionName = "conectar_eTata"
            reporte_mensual.SqlDataSource2.Connection.ConnectionString = GetConexion_eTata()

            reporte_mensual.Parameters("fecha1").Value = DateSerial(Year(Now), Month(Now), 1)
            reporte_mensual.Parameters("fecha2").Value = final_mes

            reporte_mensual.Parameters("fecha1").Visible = False
            reporte_mensual.Parameters("fecha2").Visible = False

            'tool.PreviewForm.ControlBox = False
            tool.PreviewForm.Hide()
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.File, True)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.View, False)
            tool.PrintingSystem.SetCommandVisibility(PrintingSystemCommand.Background, False)
            tool.ShowPreview()
            Me.Cursor = c
            SplashScreenManager.CloseForm()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical, "Error al crear informe de indicadores de la caldera")
        End Try

    End Sub

End Class
