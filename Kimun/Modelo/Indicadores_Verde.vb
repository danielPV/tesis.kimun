﻿Imports System.Data.SqlClient

Public Class Indicadores_Verde

    Dim valor_Descortezados As Double
    Dim valor_Full As Double
    Dim valor_Media As Double
    Dim valor_Random As Double
    Dim valor_Fish As Double
    Dim valor_TM As Double

    Dim valorOLD_Descortezados As Double
    Dim valorOLD_Full As Double
    Dim valorOLD_Media As Double
    Dim valorOLD_Random As Double
    Dim valorOLD_Fish As Double
    Dim valorOLD_TM As Double

    Dim tolerancia As Integer = 0

    Private Sub Indicadores_Verde_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Tiempo_Verde.Interval = 60000

        'VALORES AYER
        valorOLD_Descortezados = consultar("Trozos Descortezador Actual", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_Full = consultar("Conteo Full Sheet", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_Media = consultar("Conteo Media Lamina", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_Random = consultar("Conteo Random", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_Fish = consultar("Conteo Fish Tail", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_TM = consultar("Tiempo Muerto Actual", DateAdd(DateInterval.Day, -1, Now), "old")

        'MUESTRA VALORES DE AYER
        ValorOLD_trozosDescr.Text = String.Format("{0} {1}", valorOLD_Descortezados, "Trozos")
        ValorOLD_fullSheet.Text = String.Format("{0} {1}", valorOLD_Full, "m³")
        ValorOLD_mediaLamina.Text = String.Format("{0} {1}", valorOLD_Media, "m³")
        ValorOLD_conteoRandom.Text = String.Format("{0} {1}", valorOLD_Random, "m³")
        ValorOLD_fishTail.Text = String.Format("{0} {1}", valorOLD_Fish, "m³")
        ValorOLD_tiempoMuerto.Text = String.Format("{0} {1}", valorOLD_TM, "minutos")

        'VALORES HOY
        valor_Descortezados = consultar("Trozos Descortezador Actual", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_Full = consultar("Conteo Full Sheet", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_Media = consultar("Conteo Media Lamina", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_Random = consultar("Conteo Random", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_Fish = consultar("Conteo Fish Tail", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_TM = consultar("Tiempo Muerto Actual", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")

        'Dibujar valores en los labels
        Label_ValorDescortezados.Text = String.Format("{0} {1}", valor_Descortezados, "Trozos")
        mostrar_metaDescortezador(valor_Descortezados, consultar_Meta("Descortezador"))
        Label_ValorFull.Text = String.Format("{0} {1}", valor_Full, "m³")
        mostrar_metaFull(valor_Full, consultar_Meta("Torno"))
        Label_ValorMedia.Text = String.Format("{0} {1}", valor_Media, "m³")
        mostrar_metaMedia(valor_Media, consultar_Meta("Torno"))
        Label_ValorRandom.Text = String.Format("{0} {1}", valor_Random, "m³")
        mostrar_metaRandom(valor_Random, consultar_Meta("Torno"))
        Label_ValorFish.Text = String.Format("{0} {1}", valor_Fish, "m³")
        mostrar_metaFish(valor_Fish, consultar_Meta("Torno"))
        Label_ValorTM.Text = String.Format("{0} {1}", valor_TM, "minutos")
        mostrar_colorTM(valor_TM)

        'MUESTRA LAS FLECHAS QUE COMPARAN VALORES DE HOY CON LOS DE AYER
        mostrarFlecha_Descortezador(valor_Descortezados, valorOLD_Descortezados)
        mostrarFlecha_Full(valor_Full, valorOLD_Full)
        mostrarFlecha_Media(valor_Media, valorOLD_Media)
        mostrarFlecha_Random(valor_Random, valorOLD_Random)
        mostrarFlecha_Fish(valor_Fish, valorOLD_Fish)
        mostrarFlecha_TM(valor_TM, valorOLD_TM)

        Tiempo_Verde.Start()
    End Sub

    Private Sub mostrarFlecha_Descortezador(ByVal valor As Double, ByVal valorAntiguo As Double)

        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Descortezador.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_Descortezador, "Los trozos del descortezador aumentaron " & delta & " unidades respecto al mismo instante el día de ayer.")

        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Descortezador.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_Descortezador, "Los trozos del descortezador disminuyeron " & delta & " unidades respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_Descortezador.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_Descortezador, "Los trozos del descortezador tiene igual número de unidades en este mismo instante el día de ayer.")
        End If

    End Sub

    Private Sub mostrarFlecha_Media(ByVal valor As Double, ByVal valorAntiguo As Double)

        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Media.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_Media, "Los paneles media lámina aumentaron " & delta & " m³ respecto al mismo instante el día de ayer.")

        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Media.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_Media, "Los paneles media lámina disminuyeron " & delta & " m³ respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_Media.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_Media, "Los paneles media lámina tienen igual número de m³ que en este mismo instante el día de ayer.")
        End If

    End Sub

    Private Sub mostrarFlecha_Random(ByVal valor As Double, ByVal valorAntiguo As Double)

        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Random.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_Random, "Los paneles random aumentaron " & delta & " m³ respecto al mismo instante el día de ayer.")

        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Random.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_Random, "Los paneles random disminuyeron " & delta & " m³ respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_Random.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_Random, "Los paneles random tienen igual número de m³ que en este mismo instante el día de ayer.")
        End If

    End Sub

    Private Sub mostrarFlecha_Full(ByVal valor As Double, ByVal valorAntiguo As Double)

        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Full.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_Full, "Los paneles full sheet aumentaron " & delta & " m³ respecto al mismo instante el día de ayer.")

        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Full.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_Full, "Los paneles full sheet disminuyeron " & delta & " m³ respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_Full.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_Full, "Los paneles full sheet tienen igual número de m³ que en este mismo instante el día de ayer.")
        End If

    End Sub

    Private Sub mostrarFlecha_Fish(ByVal valor As Double, ByVal valorAntiguo As Double)

        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Fish.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_Fish, "Los paneles fish tail aumentaron " & delta & " m³ respecto al mismo instante el día de ayer.")

        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_Fish.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_Fish, "Los paneles fish tail disminuyeron " & delta & " m³ respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_Fish.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_Fish, "Los paneles fish tail tienen igual número de m³ que en este mismo instante el día de ayer.")
        End If

    End Sub

    Private Sub mostrarFlecha_TM(ByVal valor As Double, ByVal valorAntiguo As Double)

        Dim delta As Double

        If (valor > valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_TM.Image = My.Resources.flechaArriba
            ToolTip1.SetToolTip(Imagen_TM, "Los tiempos muertos aumentaron " & delta & " mimutos respecto al mismo instante el día de ayer.")

        End If

        If (valor < valorAntiguo) Then
            delta = Format(Math.Abs(valor - valorAntiguo), "000.00")
            Imagen_TM.Image = My.Resources.flechaAbajo
            ToolTip1.SetToolTip(Imagen_TM, "Los tiempos muertos disminuyeron " & delta & " minutos respecto al mismo instante el día de ayer.")
        End If

        If (valor = valorAntiguo) Then
            Imagen_TM.Image = My.Resources.igual
            ToolTip1.SetToolTip(Imagen_TM, "Los tiempos muertos tienen igual número de minutos que en este mismo instante el día de ayer.")
        End If

    End Sub

    Private Function consultar(ByVal equivalencia As String, ByVal fecha As Date, ByVal tipo As String) As Integer

        Dim consulta As New SqlCommand("SELECT FloatTableVerde.Val
                                        FROM  EquivalenciaVerde INNER JOIN
	                                        FloatTableVerde ON EquivalenciaVerde.TagIndex = FloatTableVerde.TagIndex
                                        WHERE (EquivalenciaVerde.Equivalencia = @equi) AND (FloatTableVerde.DateAndTime between @fecha AND @fecha2)", conexion_eTata)

        Try
            If (conexion_eTata.State = ConnectionState.Closed) Then
                conexion_eTata.Open()
            End If

            consulta.Parameters.AddWithValue("@equi", equivalencia)
            consulta.Parameters.AddWithValue("@fecha", String.Format("{0} {1}", Format(fecha, "yyyy/MM/dd"), Format(TimeSerial(Hour(fecha), Minute(fecha), 0), "HH:mm:ss")))
            consulta.Parameters.AddWithValue("@fecha2", String.Format("{0} {1}", Format(fecha, "yyyy/MM/dd"), Format(TimeSerial(Hour(fecha), Minute(fecha) + 1, 0), "HH:mm:ss")))

            Label_EstadoVerde.Text = "Funcionando"
            Label_EstadoVerde.ForeColor = Color.Green


            If (tipo = "new") Then
                If (consulta.ExecuteScalar().ToString = "") Then
                    Label_EstadoVerde.Text = "Linea Parada"
                    Label_EstadoVerde.ForeColor = Color.Red

                    Return 0
                Else
                    Return consulta.ExecuteScalar().ToString
                End If
            Else
                If (equivalencia = "Tiempo Muerto Actual") Then
                    If (consulta.ExecuteScalar().ToString = "") Then
                        Label_EstadoVerde.Text = "Linea Parada"
                        Label_EstadoVerde.ForeColor = Color.Red

                        Return 0
                    Else
                        Return consulta.ExecuteScalar()
                    End If
                Else
                    If (consulta.ExecuteScalar().ToString = "") Then
                        Label_EstadoVerde.Text = "Linea Parada"
                        Label_EstadoVerde.ForeColor = Color.Red

                        Return 0
                    Else
                        Return consulta.ExecuteScalar().ToString
                    End If
                End If

            End If

            conexion_eTata.Close()
        Catch ex As Exception

            If (tipo = "old") Then
                If (equivalencia = "Trozos Descortezador Actual") Then
                    ValorOLD_trozosDescr.ForeColor = Color.Red
                    Label9.ForeColor = Color.Red
                End If

                If (equivalencia = "Conteo Full Sheet") Then
                    ValorOLD_fullSheet.ForeColor = Color.Red
                    Label10.ForeColor = Color.Red
                End If

                If (equivalencia = "Conteo Media Lamina") Then
                    ValorOLD_mediaLamina.ForeColor = Color.Red
                    Label11.ForeColor = Color.Red
                End If

                If (equivalencia = "Conteo Random") Then
                    ValorOLD_conteoRandom.ForeColor = Color.Red
                    Label12.ForeColor = Color.Red
                End If

                If (equivalencia = "Conteo Fish Tail") Then
                    ValorOLD_fishTail.ForeColor = Color.Red
                    Label8.ForeColor = Color.Red
                End If

                If (equivalencia = "Tiempo Muerto Actual") Then
                    ValorOLD_tiempoMuerto.ForeColor = Color.Red
                    Label13.ForeColor = Color.Red
                End If

                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "No se encontraron datos del día de ayer.", ToolTipIcon.Info)
                ToolTip1.SetToolTip(Label_EstadoVerde, "No se encontraron algunos datos del día de ayer.")

            Else

                If (equivalencia = "Trozos Descortezador Actual") Then
                    ToolTip1.SetToolTip(Label_EstadoVerde, equivalencia + ": Error en la captura de datos.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (equivalencia = "Conteo Full Sheet") Then
                    ToolTip1.SetToolTip(Label_EstadoVerde, equivalencia + ": Error en la captura de datos.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (equivalencia = "Conteo Media Lamina") Then
                    ToolTip1.SetToolTip(Label_EstadoVerde, equivalencia + ": Error en la captura de datos.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (equivalencia = "Conteo Random") Then
                    ToolTip1.SetToolTip(Label_EstadoVerde, equivalencia + ": Error en la captura de datos.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (equivalencia = "Conteo Fish Tail") Then
                    ToolTip1.SetToolTip(Label_EstadoVerde, equivalencia + ": Error en la captura de datos.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If

                If (equivalencia = "Tiempo Muerto Actual") Then
                    ToolTip1.SetToolTip(Label_EstadoVerde, equivalencia + ": Error en la captura de datos.")
                    PanelDeControl.icono_notificacion.ShowBalloonTip(5000, equivalencia, "Error en la captura de datos.", ToolTipIcon.Warning)
                    tolerancia = tolerancia + 1
                End If


                If (tolerancia = 6) Then
                    Label_EstadoVerde.Text = "ERROR"
                    Label_EstadoVerde.ForeColor = Color.Red
                End If

                If (tolerancia < 6 Or tolerancia > 4) Then
                    Label_EstadoVerde.Text = "ERROR"
                    Label_EstadoVerde.ForeColor = Color.OrangeRed
                End If

                If (tolerancia < 4) Then
                    Label_EstadoVerde.Text = "ERROR"
                    Label_EstadoVerde.ForeColor = Color.Orange
                End If

            End If

            MsgBox(ex.Message, MsgBoxStyle.Critical, equivalencia + " " + tipo)

            Return 0
        End Try

    End Function


    Private Function mostrar_TM(ByVal fecha1 As Date, ByVal fecha2 As Date) As String

        Dim consulta As New SqlCommand("SELECT CausaDetenciones.Detencion
                                        FROM CausaDetenciones INNER JOIN
                                            Produccion INNER JOIN
                                            Linea ON Produccion.IdLinea = Linea.IdLinea INNER JOIN
                                            TM ON Linea.IdLinea = TM.IdLinea ON CausaDetenciones.CodDetencion = TM.CodDetencion
                                        WHERE (Linea.Linea = 'Descortezador' or Linea.Linea = 'Torno') AND Produccion.FechaProduccion between @fecha1 AND @fecha2", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@fecha1", fecha1)
            consulta.Parameters.AddWithValue("@fecha2", fecha2)

            Return consulta.ExecuteScalar()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al mostrar la causa de detencion de la linea verde.")
            Return ""
        End Try

    End Function

    Private Function consultar_Meta(ByVal linea As String) As Double

        Dim consulta As New SqlCommand("SELECT Linea.Meta
                                        FROM  Linea
                                        WHERE Linea.Linea = @linea ", conexion_controlProduccion)

        Try
            If (conexion_controlProduccion.State = ConnectionState.Closed) Then
                conexion_controlProduccion.Open()
            End If

            consulta.Parameters.AddWithValue("@linea", linea)

            Return consulta.ExecuteScalar()

        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information, "Error al consultar la meta")
            Return 0
        End Try
    End Function

    Private Sub mostrar_metaDescortezador(ByVal valor As Double, ByVal meta As Double)

        Dim delta As Double

        If (valor > meta) Then
            delta = Math.Abs(valor - meta)
            Label_ValorDescortezados.ForeColor = Color.Green
            ToolTip1.SetToolTip(Label_ValorDescortezados, "El número de trozos descortezados sobrepasaron la meta en " & delta & " unidades.")
            'PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Descortezador", "El número de trozos descortezados sobrepasaron la meta en " & delta & " unidades.", ToolTipIcon.Info)
        Else
            If (valor = meta) Then
                Label_ValorDescortezados.ForeColor = Color.ForestGreen
                ToolTip1.SetToolTip(Label_ValorDescortezados, "Se alcanzo la meta.")
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Descortezador", "Se ha alcanzado la meta.", ToolTipIcon.Info)
            End If

            If (valor > (meta * 0.65)) Then
                Label_ValorDescortezados.ForeColor = Color.YellowGreen
                ToolTip1.SetToolTip(Label_ValorDescortezados, "Faltan " & delta & " trozos descortezados para llegar a la meta.")
            Else
                Label_ValorDescortezados.ForeColor = Color.Black
                ToolTip1.SetToolTip(Label_ValorDescortezados, "Estamos muy lejos de la meta. La meta es " & meta & " trozos.")
            End If
        End If
    End Sub

    Private Sub mostrar_metaFull(ByVal valor As Double, ByVal meta As Double)

        Dim delta As Double

        If (valor > meta) Then
            delta = Math.Abs(valor - meta)
            Label_ValorFull.ForeColor = Color.Green
            ToolTip1.SetToolTip(Label_ValorFull, "Los paneles full sheet sobrepasaron la meta en " & delta & " m³.")
            'PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Torno", "Los paneles full sheet sobrepasaron la meta en " & delta & " m³.", ToolTipIcon.Info)
        Else
            If (valor = meta) Then
                Label_ValorFull.ForeColor = Color.ForestGreen
                ToolTip1.SetToolTip(Label_ValorFull, "Se alcanzo la meta.")
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Conteo Full Sheet", "Se ha alcanzado la meta", ToolTipIcon.Info)
            End If

            If (valor > (meta * 0.65)) Then
                Label_ValorFull.ForeColor = Color.YellowGreen
                ToolTip1.SetToolTip(Label_ValorFull, "Faltan " & delta & " m³ de paneles full sheet para llegar a la meta.")
            Else
                Label_ValorFull.ForeColor = Color.Black
                ToolTip1.SetToolTip(Label_ValorFull, "Estamos muy lejos de la meta. La meta es " & meta & " m³ de paneles.")
            End If
        End If
    End Sub

    Private Sub mostrar_metaMedia(ByVal valor As Double, ByVal meta As Double)

        Dim delta As Double

        If (valor > meta) Then
            delta = Math.Abs(valor - meta)
            Label_ValorMedia.ForeColor = Color.Green
            ToolTip1.SetToolTip(Label_ValorMedia, "Los paneles media lámina sobrepasaron la meta en " & delta & " m³.")
            'PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Torno", "Los paneles media lámina sobrepasaron la meta en " & delta & " m³.", ToolTipIcon.Info)
        Else
            If (valor = meta) Then
                Label_ValorMedia.ForeColor = Color.ForestGreen
                ToolTip1.SetToolTip(Label_ValorMedia, "Se alcanzo la meta.")
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Conteo Media Lámina", "Se ha alcanzado la meta", ToolTipIcon.Info)
            End If

            If (valor > (meta * 0.65)) Then
                Label_ValorMedia.ForeColor = Color.YellowGreen
                ToolTip1.SetToolTip(Label_ValorMedia, "Faltan " & delta & " m³ de paneles media lámina para llegar a la meta.")
            Else
                Label_ValorMedia.ForeColor = Color.Black
                ToolTip1.SetToolTip(Label_ValorMedia, "Estamos muy lejos de la meta. La meta es " & meta & " m³ de paneles.")
            End If
        End If
    End Sub

    Private Sub mostrar_metaRandom(ByVal valor As Double, ByVal meta As Double)

        Dim delta As Double

        If (valor > meta) Then
            delta = Math.Abs(valor - meta)
            Label_ValorRandom.ForeColor = Color.Green
            ToolTip1.SetToolTip(Label_ValorRandom, "Los paneles random sobrepasaron la meta en " & delta & " m³.")
            'PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Torno", "Los paneles random sobrepasaron la meta en " & delta & " m³.", ToolTipIcon.Info)
        Else
            If (valor = meta) Then
                Label_ValorRandom.ForeColor = Color.ForestGreen
                ToolTip1.SetToolTip(Label_ValorRandom, "Se alcanzo la meta.")
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Conteo Random", "Se ha alcanzado la meta", ToolTipIcon.Info)
            End If

            If (valor > (meta * 0.65)) Then
                Label_ValorRandom.ForeColor = Color.YellowGreen
                ToolTip1.SetToolTip(Label_ValorRandom, "Faltan " & delta & " m³ de paneles random para llegar a la meta.")
            Else
                Label_ValorRandom.ForeColor = Color.Black
                ToolTip1.SetToolTip(Label_ValorRandom, "Estamos muy lejos de la meta. La meta es " & meta & " m³ de paneles.")
            End If
        End If
    End Sub

    Private Sub mostrar_metaFish(ByVal valor As Double, ByVal meta As Double)

        Dim delta As Double

        If (valor > meta) Then
            delta = Math.Abs(valor - meta)
            Label_ValorFish.ForeColor = Color.Green
            ToolTip1.SetToolTip(Label_ValorFish, "Los paneles fish tail sobrepasaron la meta en " & delta & " m³.")
            'PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Torno", "Los paneles fish tail sobrepasaron la meta en " & delta & " m³.", ToolTipIcon.Info)
        Else
            If (valor = meta) Then
                Label_ValorFish.ForeColor = Color.ForestGreen
                ToolTip1.SetToolTip(Label_ValorFish, "Se alcanzo la meta.")
                PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Conteo Fish Tail", "Se ha alcanzado la meta", ToolTipIcon.Info)
            End If

            If (valor > (meta * 0.65)) Then
                Label_ValorFish.ForeColor = Color.YellowGreen
                ToolTip1.SetToolTip(Label_ValorFish, "Faltan " & delta & " m³ de paneles fish tail para llegar a la meta.")
            Else
                Label_ValorFish.ForeColor = Color.Black
                ToolTip1.SetToolTip(Label_ValorFish, "Estamos muy lejos de la meta. La meta es " & meta & " m³ de paneles.")
            End If
        End If
    End Sub

    Private Sub mostrar_colorTM(ByVal valor As Double)

        Dim detencion As String = ""
        Dim delta As Double = 0
        Dim consulta As New mostrar_Turno
        Dim hora_inicio As Date

        Dim inicio_TA As String = consulta.inicio_turno("TA")
        Dim inicio_TB As String = consulta.inicio_turno("TB")
        Dim inicio_TC As String = consulta.inicio_turno("TC")

        If (consulta.consultar_turno() = "Turno A") Then
            hora_inicio = CDate(inicio_TA)
        End If

        If (consulta.consultar_turno() = "Turno B") Then
            hora_inicio = CDate(inicio_TB)
        End If

        If (consulta.consultar_turno() = "Turno C") Then
            hora_inicio = CDate(inicio_TC)
        End If

        If (valor >= 450) Then
            Label_ValorTM.ForeColor = Color.Red
            detencion = mostrar_TM(String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(TimeSerial(Hour(hora_inicio), Minute(hora_inicio), 0), "HH:mm:ss")), Now)
            ToolTip1.SetToolTip(Label_ValorTM, "Causa: " & detencion)
            PanelDeControl.icono_notificacion.ShowBalloonTip(5000, "Linea Verde", "Los tiempos muertos excedieron la meta.", ToolTipIcon.Warning)
        Else
            If (valor >= 400) Then
                Label_ValorTM.ForeColor = Color.OrangeRed
                detencion = mostrar_TM(String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(TimeSerial(Hour(hora_inicio), Minute(hora_inicio), 0), "HH:mm:ss")), Now)
                ToolTip1.SetToolTip(Label_ValorTM, "Causa: " & detencion)
            Else
                If (valor < 400) Then
                    Label_ValorTM.ForeColor = Color.Orange
                    detencion = mostrar_TM(String.Format("{0} {1}", Format(Now, "yyyy/MM/dd"), Format(TimeSerial(Hour(hora_inicio), Minute(hora_inicio), 0), "HH:mm:ss")), Now)
                    ToolTip1.SetToolTip(Label_ValorTM, "Causa: " & detencion)
                End If
                If (valor = 0) Then
                    Label_ValorTM.ForeColor = Color.Blue
                    ToolTip1.SetToolTip(Label_ValorTM, "Los tiempos muertos son ideales.")
                End If
            End If
        End If
    End Sub



    Private Sub Tiempo_Verde_Tick(sender As Object, e As EventArgs) Handles Tiempo_Verde.Tick
        Tiempo_Verde.Stop()

        tolerancia = 0

        'VALORES AYER
        valorOLD_Descortezados = consultar("Trozos Descortezador Actual", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_Full = consultar("Conteo Full Sheet", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_Media = consultar("Conteo Media Lamina", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_Random = consultar("Conteo Random", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_Fish = consultar("Conteo Fish Tail", DateAdd(DateInterval.Day, -1, Now), "old")
        valorOLD_TM = consultar("Tiempo Muerto Actual", DateAdd(DateInterval.Day, -1, Now), "old")

        'MUESTRA VALORES DE AYER
        ValorOLD_trozosDescr.Text = String.Format("{0} {1}", valorOLD_Descortezados, "Trozos")
        ValorOLD_fullSheet.Text = String.Format("{0} {1}", valorOLD_Full, "m³")
        ValorOLD_mediaLamina.Text = String.Format("{0} {1}", valorOLD_Media, "m³")
        ValorOLD_conteoRandom.Text = String.Format("{0} {1}", valorOLD_Random, "m³")
        ValorOLD_fishTail.Text = String.Format("{0} {1}", valorOLD_Fish, "m³")
        ValorOLD_tiempoMuerto.Text = String.Format("{0} {1}", valorOLD_TM, "minutos")

        'VALORES HOY
        valor_Descortezados = consultar("Trozos Descortezador Actual", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_Full = consultar("Conteo Full Sheet", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_Media = consultar("Conteo Media Lamina", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_Random = consultar("Conteo Random", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_Fish = consultar("Conteo Fish Tail", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")
        valor_TM = consultar("Tiempo Muerto Actual", Format(Now, "yyyy-MM-dd HH:mm:ss"), "new")

        'MUESTRA VALORES HOY
        Label_ValorDescortezados.Text = String.Format("{0} {1}", valor_Descortezados, "Trozos")
        mostrar_metaDescortezador(valor_Descortezados, consultar_Meta("Descortezador"))
        Label_ValorFull.Text = String.Format("{0} {1}", valor_Full, "m³")
        mostrar_metaFull(valor_Full, consultar_Meta("Torno"))
        Label_ValorMedia.Text = String.Format("{0} {1}", valor_Media, "m³")
        mostrar_metaMedia(valor_Media, consultar_Meta("Torno"))
        Label_ValorRandom.Text = String.Format("{0} {1}", valor_Random, "m³")
        mostrar_metaRandom(valor_Random, consultar_Meta("Torno"))
        Label_ValorFish.Text = String.Format("{0} {1}", valor_Fish, "m³")
        mostrar_metaFish(valor_Fish, consultar_Meta("Torno"))
        Label_ValorTM.Text = String.Format("{0} {1}", valor_TM, "minutos")
        mostrar_colorTM(valor_TM)

        'MUESTRA LAS FLECHAS QUE COMPARAN VALORES DE HOY CON LOS DE AYER
        mostrarFlecha_Descortezador(valor_Descortezados, valorOLD_Descortezados)
        mostrarFlecha_Full(valor_Full, valorOLD_Full)
        mostrarFlecha_Media(valor_Media, valorOLD_Media)
        mostrarFlecha_Random(valor_Random, valorOLD_Random)
        mostrarFlecha_Fish(valor_Fish, valorOLD_Fish)
        mostrarFlecha_TM(valor_TM, valorOLD_TM)

        Tiempo_Verde.Start()
    End Sub

End Class
