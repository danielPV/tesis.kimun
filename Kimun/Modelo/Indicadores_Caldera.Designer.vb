﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Indicadores_Caldera
    Inherits System.Windows.Forms.UserControl

    'UserControl reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim IndicatorState1 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState2 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState3 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState4 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState5 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState6 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState7 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState8 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState9 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState10 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState11 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Dim IndicatorState12 As DevExpress.XtraGauges.Core.Model.IndicatorState = New DevExpress.XtraGauges.Core.Model.IndicatorState()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label_ValorNivelDomo = New System.Windows.Forms.Label()
        Me.Tiempo = New System.Windows.Forms.Timer(Me.components)
        Me.ETataDataSet_Caldera = New Kimun.eTataDataSet_Caldera()
        Me.FloatTableCalderaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.FloatTableCalderaTableAdapter = New Kimun.eTataDataSet_CalderaTableAdapters.FloatTableCalderaTableAdapter()
        Me.Label_ValorConsumo = New System.Windows.Forms.Label()
        Me.Label_ValorTemperatura = New System.Windows.Forms.Label()
        Me.Imagen_Temperatura = New System.Windows.Forms.PictureBox()
        Me.Imagen_Consumo = New System.Windows.Forms.PictureBox()
        Me.Imagen_NivelDomo = New System.Windows.Forms.PictureBox()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.ValorOLD_nivel = New System.Windows.Forms.Label()
        Me.ValorOLD_consumoDomo = New System.Windows.Forms.Label()
        Me.ValorOLD_temp = New System.Windows.Forms.Label()
        Me.GaugeControl1 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.StateIndicatorGauge1 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge()
        Me.semaforo_nivelDomo = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent()
        Me.GaugeControl2 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.StateIndicatorGauge2 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge()
        Me.semaforo_consumoDomo = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent()
        Me.GaugeControl3 = New DevExpress.XtraGauges.Win.GaugeControl()
        Me.StateIndicatorGauge3 = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge()
        Me.semaforo_tempVapor = New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent()
        Me.SeparatorControl1 = New DevExpress.XtraEditors.SeparatorControl()
        Me.Label_Estado = New System.Windows.Forms.Label()
        Me.Label_EstadoCaldera = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        CType(Me.ETataDataSet_Caldera, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FloatTableCalderaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_Temperatura, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_Consumo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.Imagen_NivelDomo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateIndicatorGauge1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.semaforo_nivelDomo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateIndicatorGauge2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.semaforo_consumoDomo, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.StateIndicatorGauge3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.semaforo_tempVapor, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SeparatorControl1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label1.Location = New System.Drawing.Point(53, 64)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(127, 22)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Nivel Domo: "
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label2.Location = New System.Drawing.Point(53, 146)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(110, 22)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Consumo  "
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label3.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label3.Location = New System.Drawing.Point(53, 255)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(108, 22)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "T° Vapor:  "
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Arial", 14.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.SystemColors.WindowFrame
        Me.Label4.Location = New System.Drawing.Point(53, 168)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(132, 22)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Vapor Domo:"
        '
        'Label_ValorNivelDomo
        '
        Me.Label_ValorNivelDomo.AutoSize = True
        Me.Label_ValorNivelDomo.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorNivelDomo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorNivelDomo.Location = New System.Drawing.Point(210, 64)
        Me.Label_ValorNivelDomo.Name = "Label_ValorNivelDomo"
        Me.Label_ValorNivelDomo.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorNivelDomo.TabIndex = 4
        '
        'Tiempo
        '
        Me.Tiempo.Enabled = True
        Me.Tiempo.Interval = 60000
        '
        'ETataDataSet_Caldera
        '
        Me.ETataDataSet_Caldera.DataSetName = "eTataDataSet_Caldera"
        Me.ETataDataSet_Caldera.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FloatTableCalderaBindingSource
        '
        Me.FloatTableCalderaBindingSource.DataMember = "FloatTableCaldera"
        Me.FloatTableCalderaBindingSource.DataSource = Me.ETataDataSet_Caldera
        '
        'FloatTableCalderaTableAdapter
        '
        Me.FloatTableCalderaTableAdapter.ClearBeforeFill = True
        '
        'Label_ValorConsumo
        '
        Me.Label_ValorConsumo.AutoSize = True
        Me.Label_ValorConsumo.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorConsumo.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorConsumo.Location = New System.Drawing.Point(210, 168)
        Me.Label_ValorConsumo.Name = "Label_ValorConsumo"
        Me.Label_ValorConsumo.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorConsumo.TabIndex = 5
        '
        'Label_ValorTemperatura
        '
        Me.Label_ValorTemperatura.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.Label_ValorTemperatura.AutoSize = True
        Me.Label_ValorTemperatura.BackColor = System.Drawing.Color.Snow
        Me.Label_ValorTemperatura.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_ValorTemperatura.Location = New System.Drawing.Point(210, 257)
        Me.Label_ValorTemperatura.Name = "Label_ValorTemperatura"
        Me.Label_ValorTemperatura.Size = New System.Drawing.Size(0, 20)
        Me.Label_ValorTemperatura.TabIndex = 6
        '
        'Imagen_Temperatura
        '
        Me.Imagen_Temperatura.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Imagen_Temperatura.InitialImage = Nothing
        Me.Imagen_Temperatura.Location = New System.Drawing.Point(184, 260)
        Me.Imagen_Temperatura.Name = "Imagen_Temperatura"
        Me.Imagen_Temperatura.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_Temperatura.TabIndex = 12
        Me.Imagen_Temperatura.TabStop = False
        '
        'Imagen_Consumo
        '
        Me.Imagen_Consumo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Imagen_Consumo.InitialImage = Nothing
        Me.Imagen_Consumo.Location = New System.Drawing.Point(184, 171)
        Me.Imagen_Consumo.Name = "Imagen_Consumo"
        Me.Imagen_Consumo.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_Consumo.TabIndex = 11
        Me.Imagen_Consumo.TabStop = False
        '
        'Imagen_NivelDomo
        '
        Me.Imagen_NivelDomo.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center
        Me.Imagen_NivelDomo.InitialImage = Nothing
        Me.Imagen_NivelDomo.Location = New System.Drawing.Point(184, 67)
        Me.Imagen_NivelDomo.Name = "Imagen_NivelDomo"
        Me.Imagen_NivelDomo.Size = New System.Drawing.Size(20, 22)
        Me.Imagen_NivelDomo.TabIndex = 10
        Me.Imagen_NivelDomo.TabStop = False
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label5.Location = New System.Drawing.Point(80, 103)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(86, 13)
        Me.Label5.TabIndex = 13
        Me.Label5.Text = "Nivel domo ayer:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label6.Location = New System.Drawing.Point(30, 207)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(136, 13)
        Me.Label6.TabIndex = 14
        Me.Label6.Text = "Consumo vapor domo ayer:"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.Label7.Location = New System.Drawing.Point(43, 290)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(123, 13)
        Me.Label7.TabIndex = 15
        Me.Label7.Text = "Temperatura vapor ayer:"
        '
        'ValorOLD_nivel
        '
        Me.ValorOLD_nivel.AutoSize = True
        Me.ValorOLD_nivel.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_nivel.Location = New System.Drawing.Point(173, 103)
        Me.ValorOLD_nivel.Name = "ValorOLD_nivel"
        Me.ValorOLD_nivel.Size = New System.Drawing.Size(81, 13)
        Me.ValorOLD_nivel.TabIndex = 16
        Me.ValorOLD_nivel.Text = "ValorOLD_nivel"
        '
        'ValorOLD_consumoDomo
        '
        Me.ValorOLD_consumoDomo.AutoSize = True
        Me.ValorOLD_consumoDomo.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_consumoDomo.Location = New System.Drawing.Point(173, 207)
        Me.ValorOLD_consumoDomo.Name = "ValorOLD_consumoDomo"
        Me.ValorOLD_consumoDomo.Size = New System.Drawing.Size(130, 13)
        Me.ValorOLD_consumoDomo.TabIndex = 17
        Me.ValorOLD_consumoDomo.Text = "ValorOLD_consumoDomo"
        '
        'ValorOLD_temp
        '
        Me.ValorOLD_temp.AutoSize = True
        Me.ValorOLD_temp.ForeColor = System.Drawing.SystemColors.ControlDarkDark
        Me.ValorOLD_temp.Location = New System.Drawing.Point(173, 290)
        Me.ValorOLD_temp.Name = "ValorOLD_temp"
        Me.ValorOLD_temp.Size = New System.Drawing.Size(82, 13)
        Me.ValorOLD_temp.TabIndex = 18
        Me.ValorOLD_temp.Text = "ValorOLD_temp"
        '
        'GaugeControl1
        '
        Me.GaugeControl1.AutoLayout = False
        Me.GaugeControl1.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.GaugeControl1.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.StateIndicatorGauge1})
        Me.GaugeControl1.Location = New System.Drawing.Point(2, 52)
        Me.GaugeControl1.Name = "GaugeControl1"
        Me.GaugeControl1.Size = New System.Drawing.Size(43, 58)
        Me.GaugeControl1.TabIndex = 19
        '
        'StateIndicatorGauge1
        '
        Me.StateIndicatorGauge1.Bounds = New System.Drawing.Rectangle(1, 3, 48, 48)
        Me.StateIndicatorGauge1.Indicators.AddRange(New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent() {Me.semaforo_nivelDomo})
        Me.StateIndicatorGauge1.Name = "StateIndicatorGauge1"
        '
        'semaforo_nivelDomo
        '
        Me.semaforo_nivelDomo.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(124.0!, 124.0!)
        Me.semaforo_nivelDomo.Name = "stateIndicatorComponent4"
        Me.semaforo_nivelDomo.Size = New System.Drawing.SizeF(100.0!, 200.0!)
        Me.semaforo_nivelDomo.StateIndex = 3
        IndicatorState1.Name = "State1"
        IndicatorState1.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight1
        IndicatorState2.Name = "State2"
        IndicatorState2.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight2
        IndicatorState3.Name = "State3"
        IndicatorState3.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight3
        IndicatorState4.Name = "State4"
        IndicatorState4.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight4
        Me.semaforo_nivelDomo.States.AddRange(New DevExpress.XtraGauges.Core.Model.IIndicatorState() {IndicatorState1, IndicatorState2, IndicatorState3, IndicatorState4})
        '
        'GaugeControl2
        '
        Me.GaugeControl2.AutoLayout = False
        Me.GaugeControl2.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.GaugeControl2.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.StateIndicatorGauge2})
        Me.GaugeControl2.Location = New System.Drawing.Point(2, 145)
        Me.GaugeControl2.Name = "GaugeControl2"
        Me.GaugeControl2.Size = New System.Drawing.Size(43, 58)
        Me.GaugeControl2.TabIndex = 20
        '
        'StateIndicatorGauge2
        '
        Me.StateIndicatorGauge2.Bounds = New System.Drawing.Rectangle(1, 1, 48, 50)
        Me.StateIndicatorGauge2.Indicators.AddRange(New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent() {Me.semaforo_consumoDomo})
        Me.StateIndicatorGauge2.Name = "StateIndicatorGauge2"
        '
        'semaforo_consumoDomo
        '
        Me.semaforo_consumoDomo.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(124.0!, 124.0!)
        Me.semaforo_consumoDomo.Name = "stateIndicatorComponent4"
        Me.semaforo_consumoDomo.Size = New System.Drawing.SizeF(100.0!, 200.0!)
        Me.semaforo_consumoDomo.StateIndex = 1
        IndicatorState5.Name = "State1"
        IndicatorState5.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight1
        IndicatorState6.Name = "State2"
        IndicatorState6.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight2
        IndicatorState7.Name = "State3"
        IndicatorState7.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight3
        IndicatorState8.Name = "State4"
        IndicatorState8.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight4
        Me.semaforo_consumoDomo.States.AddRange(New DevExpress.XtraGauges.Core.Model.IIndicatorState() {IndicatorState5, IndicatorState6, IndicatorState7, IndicatorState8})
        '
        'GaugeControl3
        '
        Me.GaugeControl3.AutoLayout = False
        Me.GaugeControl3.BorderStyle = DevExpress.XtraEditors.Controls.BorderStyles.NoBorder
        Me.GaugeControl3.Gauges.AddRange(New DevExpress.XtraGauges.Base.IGauge() {Me.StateIndicatorGauge3})
        Me.GaugeControl3.Location = New System.Drawing.Point(2, 243)
        Me.GaugeControl3.Name = "GaugeControl3"
        Me.GaugeControl3.Size = New System.Drawing.Size(43, 58)
        Me.GaugeControl3.TabIndex = 21
        '
        'StateIndicatorGauge3
        '
        Me.StateIndicatorGauge3.Bounds = New System.Drawing.Rectangle(2, 0, 47, 51)
        Me.StateIndicatorGauge3.Indicators.AddRange(New DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent() {Me.semaforo_tempVapor})
        Me.StateIndicatorGauge3.Name = "StateIndicatorGauge3"
        '
        'semaforo_tempVapor
        '
        Me.semaforo_tempVapor.Center = New DevExpress.XtraGauges.Core.Base.PointF2D(124.0!, 124.0!)
        Me.semaforo_tempVapor.Name = "stateIndicatorComponent4"
        Me.semaforo_tempVapor.Size = New System.Drawing.SizeF(100.0!, 200.0!)
        Me.semaforo_tempVapor.StateIndex = 2
        IndicatorState9.Name = "State1"
        IndicatorState9.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight1
        IndicatorState10.Name = "State2"
        IndicatorState10.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight2
        IndicatorState11.Name = "State3"
        IndicatorState11.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight3
        IndicatorState12.Name = "State4"
        IndicatorState12.ShapeType = DevExpress.XtraGauges.Core.Model.StateIndicatorShapeType.TrafficLight4
        Me.semaforo_tempVapor.States.AddRange(New DevExpress.XtraGauges.Core.Model.IIndicatorState() {IndicatorState9, IndicatorState10, IndicatorState11, IndicatorState12})
        '
        'SeparatorControl1
        '
        Me.SeparatorControl1.AutoSizeMode = True
        Me.SeparatorControl1.Location = New System.Drawing.Point(-6, 32)
        Me.SeparatorControl1.Name = "SeparatorControl1"
        Me.SeparatorControl1.Size = New System.Drawing.Size(315, 20)
        Me.SeparatorControl1.TabIndex = 25
        '
        'Label_Estado
        '
        Me.Label_Estado.AutoSize = True
        Me.Label_Estado.BackColor = System.Drawing.Color.Transparent
        Me.Label_Estado.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label_Estado.Location = New System.Drawing.Point(117, 12)
        Me.Label_Estado.Name = "Label_Estado"
        Me.Label_Estado.Size = New System.Drawing.Size(63, 20)
        Me.Label_Estado.TabIndex = 24
        Me.Label_Estado.Text = "Label2"
        '
        'Label_EstadoCaldera
        '
        Me.Label_EstadoCaldera.AutoSize = True
        Me.Label_EstadoCaldera.BackColor = System.Drawing.Color.Transparent
        Me.Label_EstadoCaldera.Location = New System.Drawing.Point(122, 17)
        Me.Label_EstadoCaldera.Name = "Label_EstadoCaldera"
        Me.Label_EstadoCaldera.Size = New System.Drawing.Size(0, 13)
        Me.Label_EstadoCaldera.TabIndex = 23
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.BackColor = System.Drawing.Color.Transparent
        Me.Label8.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label8.Location = New System.Drawing.Point(53, 12)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(68, 20)
        Me.Label8.TabIndex = 22
        Me.Label8.Text = "Estado: "
        '
        'Indicadores_Caldera
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Snow
        Me.Controls.Add(Me.SeparatorControl1)
        Me.Controls.Add(Me.Label_Estado)
        Me.Controls.Add(Me.Label_EstadoCaldera)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.GaugeControl3)
        Me.Controls.Add(Me.GaugeControl2)
        Me.Controls.Add(Me.GaugeControl1)
        Me.Controls.Add(Me.ValorOLD_temp)
        Me.Controls.Add(Me.ValorOLD_consumoDomo)
        Me.Controls.Add(Me.ValorOLD_nivel)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.Label6)
        Me.Controls.Add(Me.Label5)
        Me.Controls.Add(Me.Imagen_Temperatura)
        Me.Controls.Add(Me.Imagen_Consumo)
        Me.Controls.Add(Me.Imagen_NivelDomo)
        Me.Controls.Add(Me.Label_ValorTemperatura)
        Me.Controls.Add(Me.Label_ValorConsumo)
        Me.Controls.Add(Me.Label_ValorNivelDomo)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.Label3)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.DoubleBuffered = True
        Me.Name = "Indicadores_Caldera"
        Me.Size = New System.Drawing.Size(315, 335)
        CType(Me.ETataDataSet_Caldera, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FloatTableCalderaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_Temperatura, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_Consumo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.Imagen_NivelDomo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateIndicatorGauge1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.semaforo_nivelDomo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateIndicatorGauge2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.semaforo_consumoDomo, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.StateIndicatorGauge3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.semaforo_tempVapor, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SeparatorControl1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents Label_ValorNivelDomo As Label
    Friend WithEvents Tiempo As Timer
    Friend WithEvents FloatTableCalderaBindingSource As BindingSource
    Friend WithEvents ETataDataSet_Caldera As eTataDataSet_Caldera
    Friend WithEvents FloatTableCalderaTableAdapter As eTataDataSet_CalderaTableAdapters.FloatTableCalderaTableAdapter
    Friend WithEvents Label_ValorConsumo As Label
    Friend WithEvents Label_ValorTemperatura As Label
    Friend WithEvents Imagen_NivelDomo As PictureBox
    Friend WithEvents Imagen_Consumo As PictureBox
    Friend WithEvents Imagen_Temperatura As PictureBox
    Friend WithEvents ToolTip1 As ToolTip
    Friend WithEvents Label5 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents ValorOLD_nivel As Label
    Friend WithEvents ValorOLD_consumoDomo As Label
    Friend WithEvents ValorOLD_temp As Label
    Friend WithEvents GaugeControl1 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents StateIndicatorGauge1 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge
    Private WithEvents semaforo_nivelDomo As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent
    Friend WithEvents GaugeControl2 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents StateIndicatorGauge2 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge
    Private WithEvents semaforo_consumoDomo As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent
    Friend WithEvents GaugeControl3 As DevExpress.XtraGauges.Win.GaugeControl
    Friend WithEvents StateIndicatorGauge3 As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorGauge
    Private WithEvents semaforo_tempVapor As DevExpress.XtraGauges.Win.Gauges.State.StateIndicatorComponent
    Friend WithEvents SeparatorControl1 As DevExpress.XtraEditors.SeparatorControl
    Friend WithEvents Label_Estado As Label
    Friend WithEvents Label_EstadoCaldera As Label
    Friend WithEvents Label8 As Label
End Class
