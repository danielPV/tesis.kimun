﻿
Imports DevExpress.DashboardCommon

Namespace Win_Dashboards
    Partial Public Class Dashboard1
        Inherits Dashboard
        Public Sub New()
            InitializeComponent()

            Try
                Dim inicioMes As Date = DateSerial(Year(Now), Month(Now), 1)

                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim sqlDataSource As New DashboardSqlDataSource("Data Source 1", GetConexion_controlProduccion())

                Me.DataSources.Add(sqlDataSource)

                Dim param1 = New DashboardParameter
                Dim param2 = New DashboardParameter

                param1.Name = "fecha1"
                param1.Value = inicioMes

                param2.Name = "fecha2"
                param2.Value = Now

                Me.Parameters.Add(param1)
                Me.Parameters.Add(param2)

                cerrar_controlProduccion(GetConexion_controlProduccion())
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al cargar el dashboard.")
            End Try
        End Sub
    End Class
End Namespace