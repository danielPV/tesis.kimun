﻿Namespace Win_Dashboards
    Partial Public Class Dash_IndicadoresCaldera
        ''' <summary> 
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary> 
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Component Designer generated code"

        ''' <summary> 
        ''' Required method for Designer support - do not modify 
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Dim SelectQuery1 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
            Dim Column1 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression1 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Table1 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
            Dim Column2 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression2 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Column3 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression3 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Table2 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
            Dim Column4 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression4 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Column5 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression5 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Table3 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
            Dim Column6 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression6 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Table4 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
            Dim Join1 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
            Dim RelationColumnInfo1 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
            Dim Join2 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
            Dim RelationColumnInfo2 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
            Dim Join3 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
            Dim RelationColumnInfo3 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Dash_IndicadoresCaldera))
            Dim Dimension1 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Dimension2 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Measure1 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim Measure2 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim Measure3 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim PivotItemFormatRule1 As DevExpress.DashboardCommon.PivotItemFormatRule = New DevExpress.DashboardCommon.PivotItemFormatRule()
            Dim FormatConditionExpression1 As DevExpress.DashboardCommon.FormatConditionExpression = New DevExpress.DashboardCommon.FormatConditionExpression()
            Dim AppearanceSettings1 As DevExpress.DashboardCommon.AppearanceSettings = New DevExpress.DashboardCommon.AppearanceSettings()
            Dim Dimension3 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim GridDimensionColumn1 As DevExpress.DashboardCommon.GridDimensionColumn = New DevExpress.DashboardCommon.GridDimensionColumn()
            Dim Measure4 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim GridMeasureColumn1 As DevExpress.DashboardCommon.GridMeasureColumn = New DevExpress.DashboardCommon.GridMeasureColumn()
            Dim Measure5 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim GridMeasureColumn2 As DevExpress.DashboardCommon.GridMeasureColumn = New DevExpress.DashboardCommon.GridMeasureColumn()
            Dim Dimension4 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Dimension5 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Measure6 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim DateTimePeriod1 As DevExpress.DashboardCommon.DateTimePeriod = New DevExpress.DashboardCommon.DateTimePeriod()
            Dim FlowDateTimePeriodLimit1 As DevExpress.DashboardCommon.FlowDateTimePeriodLimit = New DevExpress.DashboardCommon.FlowDateTimePeriodLimit()
            Dim FlowDateTimePeriodLimit2 As DevExpress.DashboardCommon.FlowDateTimePeriodLimit = New DevExpress.DashboardCommon.FlowDateTimePeriodLimit()
            Dim SimpleSeries1 As DevExpress.DashboardCommon.SimpleSeries = New DevExpress.DashboardCommon.SimpleSeries()
            Dim Dimension6 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Dimension7 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Measure7 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim ChartPane1 As DevExpress.DashboardCommon.ChartPane = New DevExpress.DashboardCommon.ChartPane()
            Dim SimpleSeries2 As DevExpress.DashboardCommon.SimpleSeries = New DevExpress.DashboardCommon.SimpleSeries()
            Dim DashboardLayoutGroup1 As DevExpress.DashboardCommon.DashboardLayoutGroup = New DevExpress.DashboardCommon.DashboardLayoutGroup()
            Dim DashboardLayoutGroup2 As DevExpress.DashboardCommon.DashboardLayoutGroup = New DevExpress.DashboardCommon.DashboardLayoutGroup()
            Dim DashboardLayoutGroup3 As DevExpress.DashboardCommon.DashboardLayoutGroup = New DevExpress.DashboardCommon.DashboardLayoutGroup()
            Dim DashboardLayoutItem1 As DevExpress.DashboardCommon.DashboardLayoutItem = New DevExpress.DashboardCommon.DashboardLayoutItem()
            Dim DashboardLayoutItem2 As DevExpress.DashboardCommon.DashboardLayoutItem = New DevExpress.DashboardCommon.DashboardLayoutItem()
            Dim DashboardLayoutItem3 As DevExpress.DashboardCommon.DashboardLayoutItem = New DevExpress.DashboardCommon.DashboardLayoutItem()
            Dim DashboardLayoutGroup4 As DevExpress.DashboardCommon.DashboardLayoutGroup = New DevExpress.DashboardCommon.DashboardLayoutGroup()
            Dim DashboardLayoutItem4 As DevExpress.DashboardCommon.DashboardLayoutItem = New DevExpress.DashboardCommon.DashboardLayoutItem()
            Dim DashboardLayoutItem5 As DevExpress.DashboardCommon.DashboardLayoutItem = New DevExpress.DashboardCommon.DashboardLayoutItem()
            Me.DashboardSqlDataSource1 = New DevExpress.DashboardCommon.DashboardSqlDataSource()
            Me.PivotDashboardItem1 = New DevExpress.DashboardCommon.PivotDashboardItem()
            Me.GridDashboardItem1 = New DevExpress.DashboardCommon.GridDashboardItem()
            Me.TextBoxDashboardItem1 = New DevExpress.DashboardCommon.TextBoxDashboardItem()
            Me.RangeFilterDashboardItem1 = New DevExpress.DashboardCommon.RangeFilterDashboardItem()
            Me.ChartDashboardItem1 = New DevExpress.DashboardCommon.ChartDashboardItem()
            CType(Me.DashboardSqlDataSource1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PivotDashboardItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridDashboardItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextBoxDashboardItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.RangeFilterDashboardItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ChartDashboardItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'DashboardSqlDataSource1
            '
            Me.DashboardSqlDataSource1.ComponentName = "DashboardSqlDataSource1"
            Me.DashboardSqlDataSource1.ConnectionName = "Kimun.My.MySettings.String_conexion_eTata"
            Me.DashboardSqlDataSource1.Name = "Origen de datos SQL 1"
            ColumnExpression1.ColumnName = "Equivalencia"
            Table1.MetaSerializable = "30|30|125|134"
            Table1.Name = "EquivalenciaCaldera"
            ColumnExpression1.Table = Table1
            Column1.Expression = ColumnExpression1
            ColumnExpression2.ColumnName = "UM"
            ColumnExpression2.Table = Table1
            Column2.Expression = ColumnExpression2
            ColumnExpression3.ColumnName = "Val"
            Table2.MetaSerializable = "185|30|125|172"
            Table2.Name = "FloatTableCaldera"
            ColumnExpression3.Table = Table2
            Column3.Expression = ColumnExpression3
            ColumnExpression4.ColumnName = "DateAndTime"
            ColumnExpression4.Table = Table2
            Column4.Expression = ColumnExpression4
            ColumnExpression5.ColumnName = "LimiteMin"
            Table3.MetaSerializable = "300|260|125|115"
            Table3.Name = "LimiteMinCaldera"
            ColumnExpression5.Table = Table3
            Column5.Expression = ColumnExpression5
            ColumnExpression6.ColumnName = "Limite"
            Table4.MetaSerializable = "340|30|125|115"
            Table4.Name = "LimitesCaldera"
            ColumnExpression6.Table = Table4
            Column6.Expression = ColumnExpression6
            SelectQuery1.Columns.Add(Column1)
            SelectQuery1.Columns.Add(Column2)
            SelectQuery1.Columns.Add(Column3)
            SelectQuery1.Columns.Add(Column4)
            SelectQuery1.Columns.Add(Column5)
            SelectQuery1.Columns.Add(Column6)
            SelectQuery1.FilterString = ""
            SelectQuery1.GroupFilterString = ""
            SelectQuery1.Name = "EquivalenciaCaldera"
            RelationColumnInfo1.NestedKeyColumn = "TagIndex"
            RelationColumnInfo1.ParentKeyColumn = "TagIndex"
            Join1.KeyColumns.Add(RelationColumnInfo1)
            Join1.Nested = Table2
            Join1.Parent = Table1
            RelationColumnInfo2.NestedKeyColumn = "TagIndex"
            RelationColumnInfo2.ParentKeyColumn = "TagIndex"
            Join2.KeyColumns.Add(RelationColumnInfo2)
            Join2.Nested = Table3
            Join2.Parent = Table2
            Join2.Type = DevExpress.Xpo.DB.JoinType.LeftOuter
            RelationColumnInfo3.NestedKeyColumn = "TagIndex"
            RelationColumnInfo3.ParentKeyColumn = "TagIndex"
            Join3.KeyColumns.Add(RelationColumnInfo3)
            Join3.Nested = Table4
            Join3.Parent = Table2
            Join3.Type = DevExpress.Xpo.DB.JoinType.LeftOuter
            SelectQuery1.Relations.Add(Join1)
            SelectQuery1.Relations.Add(Join2)
            SelectQuery1.Relations.Add(Join3)
            SelectQuery1.Tables.Add(Table1)
            SelectQuery1.Tables.Add(Table2)
            SelectQuery1.Tables.Add(Table3)
            SelectQuery1.Tables.Add(Table4)
            Me.DashboardSqlDataSource1.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {SelectQuery1})
            Me.DashboardSqlDataSource1.ResultSchemaSerializable = resources.GetString("DashboardSqlDataSource1.ResultSchemaSerializable")
            '
            'PivotDashboardItem1
            '
            Me.PivotDashboardItem1.AutoExpandColumnGroups = True
            Me.PivotDashboardItem1.AutoExpandRowGroups = True
            Dimension1.DataMember = "DateAndTime"
            Dimension1.DateTimeGroupInterval = DevExpress.DashboardCommon.DateTimeGroupInterval.DayMonthYear
            Me.PivotDashboardItem1.Columns.AddRange(New DevExpress.DashboardCommon.Dimension() {Dimension1})
            Me.PivotDashboardItem1.ComponentName = "PivotDashboardItem1"
            Dimension2.DataMember = "Equivalencia"
            Measure1.DataMember = "Limite"
            Measure1.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            Measure2.DataMember = "LimiteMin"
            Measure2.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            Measure3.DataMember = "Val"
            Measure3.NumericFormat.FormatType = DevExpress.DashboardCommon.DataItemNumericFormatType.Number
            Measure3.NumericFormat.IncludeGroupSeparator = True
            Measure3.NumericFormat.Unit = DevExpress.DashboardCommon.DataItemNumericUnit.Ones
            Measure3.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            Me.PivotDashboardItem1.DataItemRepository.Clear()
            Me.PivotDashboardItem1.DataItemRepository.Add(Dimension2, "DataItem0")
            Me.PivotDashboardItem1.DataItemRepository.Add(Measure1, "DataItem3")
            Me.PivotDashboardItem1.DataItemRepository.Add(Measure2, "DataItem4")
            Me.PivotDashboardItem1.DataItemRepository.Add(Measure3, "DataItem1")
            Me.PivotDashboardItem1.DataItemRepository.Add(Dimension1, "DataItem2")
            Me.PivotDashboardItem1.DataMember = "EquivalenciaCaldera"
            Me.PivotDashboardItem1.DataSource = Me.DashboardSqlDataSource1
            Me.PivotDashboardItem1.FilterString = "[DataItem0] <> 'RETROLAVADO AGUA BLANDA'"
            FormatConditionExpression1.Expression = "Not [DataItem1] Between([DataItem4], [DataItem3])"
            AppearanceSettings1.AppearanceType = DevExpress.DashboardCommon.FormatConditionAppearanceType.Red
            FormatConditionExpression1.StyleSettings = AppearanceSettings1
            PivotItemFormatRule1.Condition = FormatConditionExpression1
            PivotItemFormatRule1.DataItem = Dimension2
            PivotItemFormatRule1.DataItemApplyTo = Measure3
            PivotItemFormatRule1.IntersectionLevelMode = DevExpress.DashboardCommon.FormatConditionIntersectionLevelMode.AllLevels
            PivotItemFormatRule1.Name = "FormatRule 1"
            Me.PivotDashboardItem1.FormatRules.AddRange(New DevExpress.DashboardCommon.PivotItemFormatRule() {PivotItemFormatRule1})
            Me.PivotDashboardItem1.HiddenMeasures.AddRange(New DevExpress.DashboardCommon.Measure() {Measure1, Measure2})
            Me.PivotDashboardItem1.InteractivityOptions.IgnoreMasterFilters = False
            Me.PivotDashboardItem1.Name = "Tabla Indicadores"
            Me.PivotDashboardItem1.Rows.AddRange(New DevExpress.DashboardCommon.Dimension() {Dimension2})
            Me.PivotDashboardItem1.ShowCaption = True
            Me.PivotDashboardItem1.ShowColumnGrandTotals = False
            Me.PivotDashboardItem1.ShowColumnTotals = False
            Me.PivotDashboardItem1.ShowRowGrandTotals = False
            Me.PivotDashboardItem1.ShowRowTotals = False
            Me.PivotDashboardItem1.Values.AddRange(New DevExpress.DashboardCommon.Measure() {Measure3})
            '
            'GridDashboardItem1
            '
            Dimension3.DataMember = "Equivalencia"
            Dimension3.Name = "Indicador"
            GridDimensionColumn1.WidthType = DevExpress.DashboardCommon.GridColumnFixedWidthType.Weight
            GridDimensionColumn1.AddDataItem("Dimension", Dimension3)
            Measure4.DataMember = "LimiteMin"
            Measure4.Name = "Mín"
            Measure4.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            GridMeasureColumn1.WidthType = DevExpress.DashboardCommon.GridColumnFixedWidthType.Weight
            GridMeasureColumn1.AddDataItem("Measure", Measure4)
            Measure5.DataMember = "Limite"
            Measure5.Name = "Máx"
            Measure5.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            GridMeasureColumn2.WidthType = DevExpress.DashboardCommon.GridColumnFixedWidthType.Weight
            GridMeasureColumn2.AddDataItem("Measure", Measure5)
            Me.GridDashboardItem1.Columns.AddRange(New DevExpress.DashboardCommon.GridColumnBase() {GridDimensionColumn1, GridMeasureColumn1, GridMeasureColumn2})
            Me.GridDashboardItem1.ComponentName = "GridDashboardItem1"
            Me.GridDashboardItem1.DataItemRepository.Clear()
            Me.GridDashboardItem1.DataItemRepository.Add(Dimension3, "DataItem0")
            Me.GridDashboardItem1.DataItemRepository.Add(Measure4, "DataItem1")
            Me.GridDashboardItem1.DataItemRepository.Add(Measure5, "DataItem2")
            Me.GridDashboardItem1.DataItemRepository.Add(Dimension4, "DataItem3")
            Me.GridDashboardItem1.DataMember = "EquivalenciaCaldera"
            Me.GridDashboardItem1.DataSource = Me.DashboardSqlDataSource1
            Me.GridDashboardItem1.FilterString = "[DataItem0] <> 'RETROLAVADO AGUA BLANDA'"
            Me.GridDashboardItem1.InteractivityOptions.IgnoreMasterFilters = True
            Me.GridDashboardItem1.Name = "Límites"
            Me.GridDashboardItem1.ShowCaption = True
            Me.GridDashboardItem1.SparklineArgument = Dimension4
            '
            'TextBoxDashboardItem1
            '
            Me.TextBoxDashboardItem1.ComponentName = "TextBoxDashboardItem1"
            Me.TextBoxDashboardItem1.DataItemRepository.Clear()
            Me.TextBoxDashboardItem1.InnerRtf = resources.GetString("TextBoxDashboardItem1.InnerRtf")
            Me.TextBoxDashboardItem1.InteractivityOptions.IgnoreMasterFilters = False
            Me.TextBoxDashboardItem1.Name = "Cuadro de Texto 1"
            Me.TextBoxDashboardItem1.ShowCaption = False
            '
            'RangeFilterDashboardItem1
            '
            Dimension5.DataMember = "DateAndTime"
            Dimension5.DateTimeGroupInterval = DevExpress.DashboardCommon.DateTimeGroupInterval.MonthYear
            Me.RangeFilterDashboardItem1.Argument = Dimension5
            Me.RangeFilterDashboardItem1.ComponentName = "RangeFilterDashboardItem1"
            Measure6.DataMember = "Val"
            Measure6.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            Me.RangeFilterDashboardItem1.DataItemRepository.Clear()
            Me.RangeFilterDashboardItem1.DataItemRepository.Add(Measure6, "DataItem0")
            Me.RangeFilterDashboardItem1.DataItemRepository.Add(Dimension5, "DataItem1")
            Me.RangeFilterDashboardItem1.DataMember = "EquivalenciaCaldera"
            Me.RangeFilterDashboardItem1.DataSource = Me.DashboardSqlDataSource1
            FlowDateTimePeriodLimit1.Offset = 1
            DateTimePeriod1.End = FlowDateTimePeriodLimit1
            DateTimePeriod1.Name = "Este año"
            DateTimePeriod1.Start = FlowDateTimePeriodLimit2
            Me.RangeFilterDashboardItem1.DateTimePeriods.AddRange(New DevExpress.DashboardCommon.DateTimePeriod() {DateTimePeriod1})
            Me.RangeFilterDashboardItem1.DefaultDateTimePeriodName = "Este año"
            Me.RangeFilterDashboardItem1.InteractivityOptions.IgnoreMasterFilters = True
            Me.RangeFilterDashboardItem1.IsMasterFilterCrossDataSource = True
            Me.RangeFilterDashboardItem1.Name = "Gráfico Indicadores Calidad del Agua"
            SimpleSeries1.SeriesType = DevExpress.DashboardCommon.SimpleSeriesType.Line
            SimpleSeries1.AddDataItem("Value", Measure6)
            Me.RangeFilterDashboardItem1.Series.AddRange(New DevExpress.DashboardCommon.SimpleSeries() {SimpleSeries1})
            Me.RangeFilterDashboardItem1.ShowCaption = False
            '
            'ChartDashboardItem1
            '
            Dimension6.DataMember = "DateAndTime"
            Dimension6.DateTimeGroupInterval = DevExpress.DashboardCommon.DateTimeGroupInterval.DayMonthYear
            Me.ChartDashboardItem1.Arguments.AddRange(New DevExpress.DashboardCommon.Dimension() {Dimension6})
            Me.ChartDashboardItem1.AxisX.TitleVisible = False
            Me.ChartDashboardItem1.ComponentName = "ChartDashboardItem1"
            Dimension7.DataMember = "Equivalencia"
            Measure7.DataMember = "Val"
            Measure7.Name = "Valor Indicadores"
            Measure7.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            Me.ChartDashboardItem1.DataItemRepository.Clear()
            Me.ChartDashboardItem1.DataItemRepository.Add(Dimension7, "DataItem1")
            Me.ChartDashboardItem1.DataItemRepository.Add(Dimension6, "DataItem2")
            Me.ChartDashboardItem1.DataItemRepository.Add(Measure7, "DataItem0")
            Me.ChartDashboardItem1.DataMember = "EquivalenciaCaldera"
            Me.ChartDashboardItem1.DataSource = Me.DashboardSqlDataSource1
            Me.ChartDashboardItem1.FilterString = "[DataItem1] <> 'RETROLAVADO AGUA BLANDA'"
            Me.ChartDashboardItem1.InteractivityOptions.IgnoreMasterFilters = False
            Me.ChartDashboardItem1.Name = "Gráfico Indicadores Calidad del Agua"
            ChartPane1.Name = "Panel 1"
            ChartPane1.PrimaryAxisY.AlwaysShowZeroLevel = True
            ChartPane1.PrimaryAxisY.ShowGridLines = True
            ChartPane1.PrimaryAxisY.TitleVisible = True
            ChartPane1.SecondaryAxisY.AlwaysShowZeroLevel = True
            ChartPane1.SecondaryAxisY.ShowGridLines = False
            ChartPane1.SecondaryAxisY.TitleVisible = True
            SimpleSeries2.SeriesType = DevExpress.DashboardCommon.SimpleSeriesType.Line
            SimpleSeries2.AddDataItem("Value", Measure7)
            ChartPane1.Series.AddRange(New DevExpress.DashboardCommon.ChartSeries() {SimpleSeries2})
            Me.ChartDashboardItem1.Panes.AddRange(New DevExpress.DashboardCommon.ChartPane() {ChartPane1})
            Me.ChartDashboardItem1.SeriesDimensions.AddRange(New DevExpress.DashboardCommon.Dimension() {Dimension7})
            Me.ChartDashboardItem1.ShowCaption = True
            '
            'Dash_IndicadoresCaldera
            '
            Me.DataSources.AddRange(New DevExpress.DashboardCommon.IDashboardDataSource() {Me.DashboardSqlDataSource1})
            Me.EnableAutomaticUpdates = False
            Me.Items.AddRange(New DevExpress.DashboardCommon.DashboardItem() {Me.PivotDashboardItem1, Me.TextBoxDashboardItem1, Me.GridDashboardItem1, Me.RangeFilterDashboardItem1, Me.ChartDashboardItem1})
            DashboardLayoutItem1.DashboardItem = Me.PivotDashboardItem1
            DashboardLayoutItem1.Weight = 80.3921568627451R
            DashboardLayoutItem2.DashboardItem = Me.TextBoxDashboardItem1
            DashboardLayoutItem2.Weight = 19.607843137254903R
            DashboardLayoutGroup3.ChildNodes.AddRange(New DevExpress.DashboardCommon.DashboardLayoutNode() {DashboardLayoutItem1, DashboardLayoutItem2})
            DashboardLayoutGroup3.DashboardItem = Nothing
            DashboardLayoutGroup3.Orientation = DevExpress.DashboardCommon.DashboardLayoutGroupOrientation.Vertical
            DashboardLayoutGroup3.Weight = 80.685920577617324R
            DashboardLayoutItem3.DashboardItem = Me.GridDashboardItem1
            DashboardLayoutItem3.Weight = 19.314079422382672R
            DashboardLayoutGroup2.ChildNodes.AddRange(New DevExpress.DashboardCommon.DashboardLayoutNode() {DashboardLayoutGroup3, DashboardLayoutItem3})
            DashboardLayoutGroup2.DashboardItem = Nothing
            DashboardLayoutGroup2.Weight = 40.078585461689585R
            DashboardLayoutItem4.DashboardItem = Me.ChartDashboardItem1
            DashboardLayoutItem4.Weight = 74.098360655737707R
            DashboardLayoutItem5.DashboardItem = Me.RangeFilterDashboardItem1
            DashboardLayoutItem5.Weight = 25.901639344262296R
            DashboardLayoutGroup4.ChildNodes.AddRange(New DevExpress.DashboardCommon.DashboardLayoutNode() {DashboardLayoutItem4, DashboardLayoutItem5})
            DashboardLayoutGroup4.DashboardItem = Nothing
            DashboardLayoutGroup4.Orientation = DevExpress.DashboardCommon.DashboardLayoutGroupOrientation.Vertical
            DashboardLayoutGroup4.Weight = 59.921414538310415R
            DashboardLayoutGroup1.ChildNodes.AddRange(New DevExpress.DashboardCommon.DashboardLayoutNode() {DashboardLayoutGroup2, DashboardLayoutGroup4})
            DashboardLayoutGroup1.DashboardItem = Nothing
            DashboardLayoutGroup1.Orientation = DevExpress.DashboardCommon.DashboardLayoutGroupOrientation.Vertical
            Me.LayoutRoot = DashboardLayoutGroup1
            Me.Title.Text = "Datos Historicos Caldera"
            CType(Me.DashboardSqlDataSource1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PivotDashboardItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridDashboardItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextBoxDashboardItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.RangeFilterDashboardItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ChartDashboardItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents DashboardSqlDataSource1 As DevExpress.DashboardCommon.DashboardSqlDataSource
        Friend WithEvents PivotDashboardItem1 As DevExpress.DashboardCommon.PivotDashboardItem
        Friend WithEvents TextBoxDashboardItem1 As DevExpress.DashboardCommon.TextBoxDashboardItem
        Friend WithEvents GridDashboardItem1 As DevExpress.DashboardCommon.GridDashboardItem
        Friend WithEvents RangeFilterDashboardItem1 As DevExpress.DashboardCommon.RangeFilterDashboardItem
        Friend WithEvents ChartDashboardItem1 As DevExpress.DashboardCommon.ChartDashboardItem

#End Region
    End Class
End Namespace