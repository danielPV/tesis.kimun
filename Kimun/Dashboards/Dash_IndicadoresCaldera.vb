﻿
Imports DevExpress.DashboardCommon

Namespace Win_Dashboards
    Partial Public Class Dash_IndicadoresCaldera
        Inherits Dashboard
        Public Sub New()
            InitializeComponent()


            Try
                abrir_eTata(GetConexion_eTata())

                Dim sqlDataSource As New DashboardSqlDataSource("Data Source 1", GetConexion_eTata())

                Me.DataSources.Add(sqlDataSource)

                cerrar_eTata(GetConexion_eTata())
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al cargar el dashboard.")
            End Try

        End Sub
    End Class
End Namespace