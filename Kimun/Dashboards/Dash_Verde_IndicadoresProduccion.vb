﻿Imports DevExpress.DashboardCommon

Namespace Win_Dashboards
    Partial Public Class Dash_Verde_IndicadoresProduccion
        Inherits Dashboard
        Public Sub New()
            InitializeComponent()

            Try
                abrir_controlProduccion(GetConexion_controlProduccion())

                Dim sqlDataSource As New DashboardSqlDataSource("Data Source 1", GetConexion_controlProduccion())

                Me.DataSources.Add(sqlDataSource)
                cerrar_controlProduccion(GetConexion_controlProduccion())
            Catch ex As Exception
                MsgBox(ex.Message, MsgBoxStyle.Information, "Error al cargar el dashboard.")
            End Try

        End Sub
    End Class
End Namespace