﻿Namespace Win_Dashboards
    Partial Public Class Dash_CalidadCaldera
        ''' <summary> 
        ''' Required designer variable.
        ''' </summary>
        Private components As System.ComponentModel.IContainer = Nothing

        ''' <summary> 
        ''' Clean up any resources being used.
        ''' </summary>
        ''' <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        Protected Overrides Sub Dispose(ByVal disposing As Boolean)
            If disposing AndAlso (components IsNot Nothing) Then
                components.Dispose()
            End If
            MyBase.Dispose(disposing)
        End Sub

#Region "Component Designer generated code"

        ''' <summary> 
        ''' Required method for Designer support - do not modify 
        ''' the contents of this method with the code editor.
        ''' </summary>
        Private Sub InitializeComponent()
            Dim Dimension1 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Dimension2 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Dimension3 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim CalculatedField1 As DevExpress.DashboardCommon.CalculatedField = New DevExpress.DashboardCommon.CalculatedField()
            Dim CalculatedField2 As DevExpress.DashboardCommon.CalculatedField = New DevExpress.DashboardCommon.CalculatedField()
            Dim SelectQuery1 As DevExpress.DataAccess.Sql.SelectQuery = New DevExpress.DataAccess.Sql.SelectQuery()
            Dim Column1 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression1 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Table1 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
            Dim Column2 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression2 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Table2 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
            Dim Column3 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression3 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Table3 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
            Dim Column4 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression4 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Table4 As DevExpress.DataAccess.Sql.Table = New DevExpress.DataAccess.Sql.Table()
            Dim Column5 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression5 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Column6 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression6 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Column7 As DevExpress.DataAccess.Sql.Column = New DevExpress.DataAccess.Sql.Column()
            Dim ColumnExpression7 As DevExpress.DataAccess.Sql.ColumnExpression = New DevExpress.DataAccess.Sql.ColumnExpression()
            Dim Join1 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
            Dim RelationColumnInfo1 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
            Dim Join2 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
            Dim RelationColumnInfo2 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
            Dim Join3 As DevExpress.DataAccess.Sql.Join = New DevExpress.DataAccess.Sql.Join()
            Dim RelationColumnInfo3 As DevExpress.DataAccess.Sql.RelationColumnInfo = New DevExpress.DataAccess.Sql.RelationColumnInfo()
            Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Dash_CalidadCaldera))
            Dim Dimension4 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Dimension5 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Measure1 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim Dimension6 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Measure2 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim Measure3 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim PivotItemFormatRule1 As DevExpress.DashboardCommon.PivotItemFormatRule = New DevExpress.DashboardCommon.PivotItemFormatRule()
            Dim FormatConditionExpression1 As DevExpress.DashboardCommon.FormatConditionExpression = New DevExpress.DashboardCommon.FormatConditionExpression()
            Dim AppearanceSettings1 As DevExpress.DashboardCommon.AppearanceSettings = New DevExpress.DashboardCommon.AppearanceSettings()
            Dim Dimension7 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim GridDimensionColumn1 As DevExpress.DashboardCommon.GridDimensionColumn = New DevExpress.DashboardCommon.GridDimensionColumn()
            Dim Dimension8 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim GridDimensionColumn2 As DevExpress.DashboardCommon.GridDimensionColumn = New DevExpress.DashboardCommon.GridDimensionColumn()
            Dim Measure4 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim GridMeasureColumn1 As DevExpress.DashboardCommon.GridMeasureColumn = New DevExpress.DashboardCommon.GridMeasureColumn()
            Dim Measure5 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim GridMeasureColumn2 As DevExpress.DashboardCommon.GridMeasureColumn = New DevExpress.DashboardCommon.GridMeasureColumn()
            Dim Dimension9 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Dimension10 As DevExpress.DashboardCommon.Dimension = New DevExpress.DashboardCommon.Dimension()
            Dim Measure6 As DevExpress.DashboardCommon.Measure = New DevExpress.DashboardCommon.Measure()
            Dim ChartPane1 As DevExpress.DashboardCommon.ChartPane = New DevExpress.DashboardCommon.ChartPane()
            Dim SimpleSeries1 As DevExpress.DashboardCommon.SimpleSeries = New DevExpress.DashboardCommon.SimpleSeries()
            Dim DashboardLayoutGroup1 As DevExpress.DashboardCommon.DashboardLayoutGroup = New DevExpress.DashboardCommon.DashboardLayoutGroup()
            Dim DashboardLayoutItem1 As DevExpress.DashboardCommon.DashboardLayoutItem = New DevExpress.DashboardCommon.DashboardLayoutItem()
            Dim DashboardLayoutGroup2 As DevExpress.DashboardCommon.DashboardLayoutGroup = New DevExpress.DashboardCommon.DashboardLayoutGroup()
            Dim DashboardLayoutGroup3 As DevExpress.DashboardCommon.DashboardLayoutGroup = New DevExpress.DashboardCommon.DashboardLayoutGroup()
            Dim DashboardLayoutItem2 As DevExpress.DashboardCommon.DashboardLayoutItem = New DevExpress.DashboardCommon.DashboardLayoutItem()
            Dim DashboardLayoutItem3 As DevExpress.DashboardCommon.DashboardLayoutItem = New DevExpress.DashboardCommon.DashboardLayoutItem()
            Dim DashboardLayoutGroup4 As DevExpress.DashboardCommon.DashboardLayoutGroup = New DevExpress.DashboardCommon.DashboardLayoutGroup()
            Dim DashboardLayoutItem4 As DevExpress.DashboardCommon.DashboardLayoutItem = New DevExpress.DashboardCommon.DashboardLayoutItem()
            Dim DashboardLayoutItem5 As DevExpress.DashboardCommon.DashboardLayoutItem = New DevExpress.DashboardCommon.DashboardLayoutItem()
            Me.ListBoxDashboardItem1 = New DevExpress.DashboardCommon.ListBoxDashboardItem()
            Me.DashboardSqlDataSource1 = New DevExpress.DashboardCommon.DashboardSqlDataSource()
            Me.PivotDashboardItem1 = New DevExpress.DashboardCommon.PivotDashboardItem()
            Me.GridDashboardItem1 = New DevExpress.DashboardCommon.GridDashboardItem()
            Me.TextBoxDashboardItem1 = New DevExpress.DashboardCommon.TextBoxDashboardItem()
            Me.ChartDashboardItem1 = New DevExpress.DashboardCommon.ChartDashboardItem()
            CType(Me.ListBoxDashboardItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.DashboardSqlDataSource1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.PivotDashboardItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure2, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure3, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.GridDashboardItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension7, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension8, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure4, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure5, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.TextBoxDashboardItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me.ChartDashboardItem1, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension9, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Dimension10, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Measure6, System.ComponentModel.ISupportInitialize).BeginInit()
            CType(Me, System.ComponentModel.ISupportInitialize).BeginInit()
            '
            'ListBoxDashboardItem1
            '
            Me.ListBoxDashboardItem1.ComponentName = "ListBoxDashboardItem1"
            Dimension1.DataMember = "FechaControlCaldera"
            Dimension1.DateTimeGroupInterval = DevExpress.DashboardCommon.DateTimeGroupInterval.DayMonthYear
            Dimension2.DataMember = "Hoy"
            Dimension2.DateTimeGroupInterval = DevExpress.DashboardCommon.DateTimeGroupInterval.DayMonthYear
            Dimension3.DataMember = "Inicio Mes"
            Dimension3.DateTimeGroupInterval = DevExpress.DashboardCommon.DateTimeGroupInterval.DayMonthYear
            Me.ListBoxDashboardItem1.DataItemRepository.Clear()
            Me.ListBoxDashboardItem1.DataItemRepository.Add(Dimension1, "DataItem0")
            Me.ListBoxDashboardItem1.DataItemRepository.Add(Dimension2, "DataItem1")
            Me.ListBoxDashboardItem1.DataItemRepository.Add(Dimension3, "DataItem2")
            Me.ListBoxDashboardItem1.DataMember = "CalidadCaldera"
            Me.ListBoxDashboardItem1.DataSource = Me.DashboardSqlDataSource1
            Me.ListBoxDashboardItem1.FilterDimensions.AddRange(New DevExpress.DashboardCommon.Dimension() {Dimension1})
            Me.ListBoxDashboardItem1.FilterString = "[DataItem0] Between([DataItem2], [DataItem1])"
            Me.ListBoxDashboardItem1.HiddenDimensions.AddRange(New DevExpress.DashboardCommon.Dimension() {Dimension2, Dimension3})
            Me.ListBoxDashboardItem1.InteractivityOptions.IgnoreMasterFilters = True
            Me.ListBoxDashboardItem1.Name = "Fecha"
            Me.ListBoxDashboardItem1.ShowCaption = True
            '
            'DashboardSqlDataSource1
            '
            CalculatedField1.DataMember = "CalidadCaldera"
            CalculatedField1.Expression = "LocalDateTimeThisMonth()"
            CalculatedField1.Name = "Inicio Mes"
            CalculatedField2.DataMember = "CalidadCaldera"
            CalculatedField2.Expression = "LocalDateTimeToday()"
            CalculatedField2.Name = "Hoy"
            Me.DashboardSqlDataSource1.CalculatedFields.AddRange(New DevExpress.DashboardCommon.CalculatedField() {CalculatedField1, CalculatedField2})
            Me.DashboardSqlDataSource1.ComponentName = "DashboardSqlDataSource1"
            Me.DashboardSqlDataSource1.ConnectionName = "conexion_ControlProduccion.dbo"
            Me.DashboardSqlDataSource1.Name = "Origen de datos SQL 1"
            ColumnExpression1.ColumnName = "FechaControlCaldera"
            Table1.MetaSerializable = "30|30|125|134"
            Table1.Name = "CalidadCaldera"
            ColumnExpression1.Table = Table1
            Column1.Expression = ColumnExpression1
            ColumnExpression2.ColumnName = "Valor"
            Table2.MetaSerializable = "185|30|125|134"
            Table2.Name = "RegistroCaldera"
            ColumnExpression2.Table = Table2
            Column2.Expression = ColumnExpression2
            ColumnExpression3.ColumnName = "Turno"
            Table3.MetaSerializable = "40|210|125|134"
            Table3.Name = "Turno"
            ColumnExpression3.Table = Table3
            Column3.Expression = ColumnExpression3
            ColumnExpression4.ColumnName = "ParametroMedicionCaldera"
            Table4.MetaSerializable = "330|30|125|172"
            Table4.Name = "ParamMedicionCaldera"
            ColumnExpression4.Table = Table4
            Column4.Expression = ColumnExpression4
            ColumnExpression5.ColumnName = "MinMedicionCaldera"
            ColumnExpression5.Table = Table4
            Column5.Expression = ColumnExpression5
            ColumnExpression6.ColumnName = "UMMedicionCaldera"
            ColumnExpression6.Table = Table4
            Column6.Expression = ColumnExpression6
            ColumnExpression7.ColumnName = "MaxMedicionCaldera"
            ColumnExpression7.Table = Table4
            Column7.Expression = ColumnExpression7
            SelectQuery1.Columns.Add(Column1)
            SelectQuery1.Columns.Add(Column2)
            SelectQuery1.Columns.Add(Column3)
            SelectQuery1.Columns.Add(Column4)
            SelectQuery1.Columns.Add(Column5)
            SelectQuery1.Columns.Add(Column6)
            SelectQuery1.Columns.Add(Column7)
            SelectQuery1.Name = "CalidadCaldera"
            RelationColumnInfo1.NestedKeyColumn = "idControlCaldera"
            RelationColumnInfo1.ParentKeyColumn = "idControlCaldera"
            Join1.KeyColumns.Add(RelationColumnInfo1)
            Join1.Nested = Table2
            Join1.Parent = Table1
            RelationColumnInfo2.NestedKeyColumn = "idMedicionCaldera"
            RelationColumnInfo2.ParentKeyColumn = "idMedicionCaldera"
            Join2.KeyColumns.Add(RelationColumnInfo2)
            Join2.Nested = Table4
            Join2.Parent = Table2
            RelationColumnInfo3.NestedKeyColumn = "CodTurno"
            RelationColumnInfo3.ParentKeyColumn = "TurnoControlCaldera"
            Join3.KeyColumns.Add(RelationColumnInfo3)
            Join3.Nested = Table3
            Join3.Parent = Table1
            SelectQuery1.Relations.Add(Join1)
            SelectQuery1.Relations.Add(Join2)
            SelectQuery1.Relations.Add(Join3)
            SelectQuery1.Tables.Add(Table1)
            SelectQuery1.Tables.Add(Table2)
            SelectQuery1.Tables.Add(Table4)
            SelectQuery1.Tables.Add(Table3)
            Me.DashboardSqlDataSource1.Queries.AddRange(New DevExpress.DataAccess.Sql.SqlQuery() {SelectQuery1})
            Me.DashboardSqlDataSource1.ResultSchemaSerializable = resources.GetString("DashboardSqlDataSource1.ResultSchemaSerializable")
            '
            'PivotDashboardItem1
            '
            Me.PivotDashboardItem1.AutoExpandColumnGroups = True
            Me.PivotDashboardItem1.AutoExpandRowGroups = True
            Dimension4.DataMember = "FechaControlCaldera"
            Dimension4.DateTimeGroupInterval = DevExpress.DashboardCommon.DateTimeGroupInterval.DayMonthYear
            Dimension5.DataMember = "Turno"
            Me.PivotDashboardItem1.Columns.AddRange(New DevExpress.DashboardCommon.Dimension() {Dimension4, Dimension5})
            Me.PivotDashboardItem1.ComponentName = "PivotDashboardItem1"
            Measure1.DataMember = "Valor"
            Measure1.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            Dimension6.DataMember = "ParametroMedicionCaldera"
            Measure2.DataMember = "MinMedicionCaldera"
            Measure2.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            Measure3.DataMember = "MaxMedicionCaldera"
            Measure3.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            Me.PivotDashboardItem1.DataItemRepository.Clear()
            Me.PivotDashboardItem1.DataItemRepository.Add(Measure1, "DataItem0")
            Me.PivotDashboardItem1.DataItemRepository.Add(Dimension6, "DataItem1")
            Me.PivotDashboardItem1.DataItemRepository.Add(Dimension4, "DataItem2")
            Me.PivotDashboardItem1.DataItemRepository.Add(Dimension5, "DataItem3")
            Me.PivotDashboardItem1.DataItemRepository.Add(Measure2, "DataItem5")
            Me.PivotDashboardItem1.DataItemRepository.Add(Measure3, "DataItem4")
            Me.PivotDashboardItem1.DataMember = "CalidadCaldera"
            Me.PivotDashboardItem1.DataSource = Me.DashboardSqlDataSource1
            Me.PivotDashboardItem1.FilterString = "[DataItem1] <> 'Limpieza Tubos                          '"
            FormatConditionExpression1.Expression = "Not [DataItem0] Between([DataItem5], [DataItem4])"
            AppearanceSettings1.AppearanceType = DevExpress.DashboardCommon.FormatConditionAppearanceType.Red
            FormatConditionExpression1.StyleSettings = AppearanceSettings1
            PivotItemFormatRule1.Condition = FormatConditionExpression1
            PivotItemFormatRule1.DataItem = Measure1
            PivotItemFormatRule1.IntersectionLevelMode = DevExpress.DashboardCommon.FormatConditionIntersectionLevelMode.AllLevels
            PivotItemFormatRule1.Name = "FormatRule 1"
            Me.PivotDashboardItem1.FormatRules.AddRange(New DevExpress.DashboardCommon.PivotItemFormatRule() {PivotItemFormatRule1})
            Me.PivotDashboardItem1.HiddenMeasures.AddRange(New DevExpress.DashboardCommon.Measure() {Measure2, Measure3})
            Me.PivotDashboardItem1.InteractivityOptions.IgnoreMasterFilters = False
            Me.PivotDashboardItem1.Name = "Tabla Indicadores"
            Me.PivotDashboardItem1.Rows.AddRange(New DevExpress.DashboardCommon.Dimension() {Dimension6})
            Me.PivotDashboardItem1.ShowCaption = True
            Me.PivotDashboardItem1.ShowColumnGrandTotals = False
            Me.PivotDashboardItem1.ShowColumnTotals = False
            Me.PivotDashboardItem1.ShowRowGrandTotals = False
            Me.PivotDashboardItem1.ShowRowTotals = False
            Me.PivotDashboardItem1.Values.AddRange(New DevExpress.DashboardCommon.Measure() {Measure1})
            '
            'GridDashboardItem1
            '
            Dimension7.DataMember = "ParametroMedicionCaldera"
            GridDimensionColumn1.Name = "Indicadores"
            GridDimensionColumn1.Weight = 154.05405405405406R
            GridDimensionColumn1.WidthType = DevExpress.DashboardCommon.GridColumnFixedWidthType.Weight
            GridDimensionColumn1.AddDataItem("Dimension", Dimension7)
            Dimension8.DataMember = "UMMedicionCaldera"
            GridDimensionColumn2.Name = "UM"
            GridDimensionColumn2.Weight = 48.648648648648646R
            GridDimensionColumn2.WidthType = DevExpress.DashboardCommon.GridColumnFixedWidthType.Weight
            GridDimensionColumn2.AddDataItem("Dimension", Dimension8)
            Measure4.DataMember = "MinMedicionCaldera"
            Measure4.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            GridMeasureColumn1.Name = "Mín"
            GridMeasureColumn1.Weight = 47.2972972972973R
            GridMeasureColumn1.WidthType = DevExpress.DashboardCommon.GridColumnFixedWidthType.Weight
            GridMeasureColumn1.AddDataItem("Measure", Measure4)
            Measure5.DataMember = "MaxMedicionCaldera"
            Measure5.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            GridMeasureColumn2.Name = "Máx"
            GridMeasureColumn2.Weight = 50.0R
            GridMeasureColumn2.WidthType = DevExpress.DashboardCommon.GridColumnFixedWidthType.Weight
            GridMeasureColumn2.AddDataItem("Measure", Measure5)
            Me.GridDashboardItem1.Columns.AddRange(New DevExpress.DashboardCommon.GridColumnBase() {GridDimensionColumn1, GridDimensionColumn2, GridMeasureColumn1, GridMeasureColumn2})
            Me.GridDashboardItem1.ComponentName = "GridDashboardItem1"
            Me.GridDashboardItem1.DataItemRepository.Clear()
            Me.GridDashboardItem1.DataItemRepository.Add(Dimension7, "DataItem0")
            Me.GridDashboardItem1.DataItemRepository.Add(Dimension8, "DataItem1")
            Me.GridDashboardItem1.DataItemRepository.Add(Measure4, "DataItem2")
            Me.GridDashboardItem1.DataItemRepository.Add(Measure5, "DataItem3")
            Me.GridDashboardItem1.DataMember = "CalidadCaldera"
            Me.GridDashboardItem1.DataSource = Me.DashboardSqlDataSource1
            Me.GridDashboardItem1.GridOptions.ColumnWidthMode = DevExpress.DashboardCommon.GridColumnWidthMode.Manual
            Me.GridDashboardItem1.InteractivityOptions.IgnoreMasterFilters = False
            Me.GridDashboardItem1.Name = "Rango Indicadores"
            Me.GridDashboardItem1.ShowCaption = True
            '
            'TextBoxDashboardItem1
            '
            Me.TextBoxDashboardItem1.ComponentName = "TextBoxDashboardItem1"
            Me.TextBoxDashboardItem1.DataItemRepository.Clear()
            Me.TextBoxDashboardItem1.InnerRtf = resources.GetString("TextBoxDashboardItem1.InnerRtf")
            Me.TextBoxDashboardItem1.InteractivityOptions.IgnoreMasterFilters = False
            Me.TextBoxDashboardItem1.Name = "Cuadro de Texto 1"
            Me.TextBoxDashboardItem1.ShowCaption = False
            '
            'ChartDashboardItem1
            '
            Dimension9.DataMember = "FechaControlCaldera"
            Dimension9.DateTimeGroupInterval = DevExpress.DashboardCommon.DateTimeGroupInterval.DayMonthYear
            Me.ChartDashboardItem1.Arguments.AddRange(New DevExpress.DashboardCommon.Dimension() {Dimension9})
            Me.ChartDashboardItem1.AxisX.TitleVisible = False
            Me.ChartDashboardItem1.ComponentName = "ChartDashboardItem1"
            Dimension10.DataMember = "ParametroMedicionCaldera"
            Measure6.DataMember = "Valor"
            Measure6.Name = "Valor Indicadores"
            Measure6.SummaryType = DevExpress.DashboardCommon.SummaryType.Average
            Me.ChartDashboardItem1.DataItemRepository.Clear()
            Me.ChartDashboardItem1.DataItemRepository.Add(Dimension9, "DataItem0")
            Me.ChartDashboardItem1.DataItemRepository.Add(Dimension10, "DataItem1")
            Me.ChartDashboardItem1.DataItemRepository.Add(Measure6, "DataItem2")
            Me.ChartDashboardItem1.DataMember = "CalidadCaldera"
            Me.ChartDashboardItem1.DataSource = Me.DashboardSqlDataSource1
            Me.ChartDashboardItem1.FilterString = "[DataItem1] <> 'Limpieza Tubos                          '"
            Me.ChartDashboardItem1.InteractivityOptions.IgnoreMasterFilters = False
            Me.ChartDashboardItem1.Name = "Gráfico Indicadores"
            ChartPane1.Name = "Panel 1"
            ChartPane1.PrimaryAxisY.AlwaysShowZeroLevel = True
            ChartPane1.PrimaryAxisY.ShowGridLines = True
            ChartPane1.PrimaryAxisY.TitleVisible = True
            ChartPane1.SecondaryAxisY.AlwaysShowZeroLevel = True
            ChartPane1.SecondaryAxisY.ShowGridLines = False
            ChartPane1.SecondaryAxisY.TitleVisible = True
            SimpleSeries1.SeriesType = DevExpress.DashboardCommon.SimpleSeriesType.Line
            SimpleSeries1.AddDataItem("Value", Measure6)
            ChartPane1.Series.AddRange(New DevExpress.DashboardCommon.ChartSeries() {SimpleSeries1})
            Me.ChartDashboardItem1.Panes.AddRange(New DevExpress.DashboardCommon.ChartPane() {ChartPane1})
            Me.ChartDashboardItem1.SeriesDimensions.AddRange(New DevExpress.DashboardCommon.Dimension() {Dimension10})
            Me.ChartDashboardItem1.ShowCaption = True
            '
            'Dash_CalidadCaldera
            '
            Me.DataSources.AddRange(New DevExpress.DashboardCommon.IDashboardDataSource() {Me.DashboardSqlDataSource1})
            Me.Items.AddRange(New DevExpress.DashboardCommon.DashboardItem() {Me.PivotDashboardItem1, Me.ListBoxDashboardItem1, Me.ChartDashboardItem1, Me.GridDashboardItem1, Me.TextBoxDashboardItem1})
            DashboardLayoutItem1.DashboardItem = Me.ListBoxDashboardItem1
            DashboardLayoutItem1.Weight = 16.819012797074954R
            DashboardLayoutItem2.DashboardItem = Me.PivotDashboardItem1
            DashboardLayoutItem2.Weight = 69.890109890109883R
            DashboardLayoutItem3.DashboardItem = Me.GridDashboardItem1
            DashboardLayoutItem3.Weight = 30.10989010989011R
            DashboardLayoutGroup3.ChildNodes.AddRange(New DevExpress.DashboardCommon.DashboardLayoutNode() {DashboardLayoutItem2, DashboardLayoutItem3})
            DashboardLayoutGroup3.DashboardItem = Nothing
            DashboardLayoutGroup3.Weight = 44.9901768172888R
            DashboardLayoutItem4.DashboardItem = Me.TextBoxDashboardItem1
            DashboardLayoutItem4.Weight = 14.285714285714287R
            DashboardLayoutItem5.DashboardItem = Me.ChartDashboardItem1
            DashboardLayoutItem5.Weight = 85.714285714285708R
            DashboardLayoutGroup4.ChildNodes.AddRange(New DevExpress.DashboardCommon.DashboardLayoutNode() {DashboardLayoutItem4, DashboardLayoutItem5})
            DashboardLayoutGroup4.DashboardItem = Nothing
            DashboardLayoutGroup4.Orientation = DevExpress.DashboardCommon.DashboardLayoutGroupOrientation.Vertical
            DashboardLayoutGroup4.Weight = 55.0098231827112R
            DashboardLayoutGroup2.ChildNodes.AddRange(New DevExpress.DashboardCommon.DashboardLayoutNode() {DashboardLayoutGroup3, DashboardLayoutGroup4})
            DashboardLayoutGroup2.DashboardItem = Nothing
            DashboardLayoutGroup2.Orientation = DevExpress.DashboardCommon.DashboardLayoutGroupOrientation.Vertical
            DashboardLayoutGroup2.Weight = 83.180987202925053R
            DashboardLayoutGroup1.ChildNodes.AddRange(New DevExpress.DashboardCommon.DashboardLayoutNode() {DashboardLayoutItem1, DashboardLayoutGroup2})
            DashboardLayoutGroup1.DashboardItem = Nothing
            Me.LayoutRoot = DashboardLayoutGroup1
            Me.Title.Text = "Calidad del Agua "
            CType(Dimension1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ListBoxDashboardItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.DashboardSqlDataSource1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure2, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure3, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.PivotDashboardItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension7, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension8, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure4, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure5, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.GridDashboardItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.TextBoxDashboardItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension9, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Dimension10, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Measure6, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me.ChartDashboardItem1, System.ComponentModel.ISupportInitialize).EndInit()
            CType(Me, System.ComponentModel.ISupportInitialize).EndInit()

        End Sub

        Friend WithEvents DashboardSqlDataSource1 As DevExpress.DashboardCommon.DashboardSqlDataSource
        Friend WithEvents PivotDashboardItem1 As DevExpress.DashboardCommon.PivotDashboardItem
        Friend WithEvents ListBoxDashboardItem1 As DevExpress.DashboardCommon.ListBoxDashboardItem
        Friend WithEvents ChartDashboardItem1 As DevExpress.DashboardCommon.ChartDashboardItem
        Friend WithEvents GridDashboardItem1 As DevExpress.DashboardCommon.GridDashboardItem
        Friend WithEvents TextBoxDashboardItem1 As DevExpress.DashboardCommon.TextBoxDashboardItem

#End Region
    End Class
End Namespace